import Vue from 'vue'
import Vuex from 'vuex'
import cookies from "@/mixins/cookies";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    lang: 'ru',
    interfaceTransactionsInitDefaultFilter:{
          'type':[],
        'status':[],
        'docRequest':[],
        'sub_type':[],
        'purpose':[],
        'taxeType':[],
        'paymodel':[],
        'projectsCategory':[],
        'projectsSubcategory':[],
        'brands':[],
        'projectStage':[],
        'units':[],
        'accounts':[],
        'isHidden':[],
        'isEditable':[],
    },
    interfaceTransactionsInitDefaultColumn:{
      name : true,
      transaction_type : true,
      status : true,
      doc_status : true,
      subcontractor : true,
      subcontractor_type : true,
      pay_purpose : true,
      date : true,
      date_fact : true,
      date_doc : true,
      sum_before_taxe : true,
      sum_after_taxe : true,
      sum_taxe : true,
      subcontractor_taxetype : true,
      agreement_start_date : true,
      agreement_end_date : true,
      pay_delay : true,
      delayed : true,
      pay_model : true,
      date_act : true,
      comment : true,
      estimation_link : true,
      project : true,
      project_category : true,
      project_subcategory : true,
      project_brand : true,
      project_short_name : true,
      stage : true,
      unit : true,
      account : true,
      created_at : true,
      created_by : true,
      modified_at : true,
      modified_by : true,
      is_hidden : true,
      is_editable : true,
    },
    interfaceTransactionsInitSetFilter:{},
    interfaceTransactionsInitSetColumn:{},
    interfaceProjectsInitDefaultFilter:{
        'category':[],
        'subcategory':[],
        'brand':[],
        'work_format':[],
        'agreement_format':[],
        'stage':[],
        'nbsource':[],
        'nbformat':[],
        'products':[],
        'units':[],
        'isEditable':[],
    },
    interfaceProjectsInitDefaultColumn:{
      name : true,
      name_short : true,
      category : true,
      subcategory : true,
      brand : true,
      work_format : true,
      stage : true,
      date_start : true,
      date_end : true,
      nbsource : true,
      nbformat : true,
      products : true,
      created_at : true,
      created_by : true,
      modified_at : true,
      modified_by : true,
      leader : true,
      team : true,
      units : true,
      income : true,
      outcome : true,
      gross_profit : true,
      gross_profitability : true,
      is_editable : true,
    },
    interfaceProjectsInitSetFilter:{},
    interfaceProjectsInitSetColumn:{},
    interfaceAgreementsInitDefaultFilter:{
      'status':[],
      'subcontractorRole':[],
      'mycompany':[],
      'prolongationType':[],
      'hasAttachments':[],
      'hasScan':[],
      'hasSrc':[],
      'namefull':false,
    },
    interfaceAgreementsInitDefaultColumn:{
      type : true,
      full_name : true,
      signed_at : true,
      status : true,
      status_date : true,
      subcontractor_name : true,
      subcontractor_role : true,
      mycompany : true,
      validity_period_from : true,
      validity_period_till : true,
      prolongation_type : true,
      prolongation_alert : true,
      description : true,
      project : true,
      transaction : true,
      report : true,
      doc_template : true,
      has_parent_full : true,
      has_attachment : true,
      has_scan : true,
      has_src : true,
    },
    interfaceAgreementsInitSetFilter:{},
    interfaceAgreementsInitSetColumn:{},
    interfaceAccountingsInitDefaultFilter:{
      'status':[],
      'subcontractorRole':[],
      'mycompany':[],
      'hasAttachments':[],
      'hasScan':[],
      'hasSrc':[],
    },
    interfaceAccountingsInitDefaultColumn:{
      type : true,
      full_name : true,
      signed_at : true,
      status : true,
      status_date : true,
      agreement : true,
      subcontractor : true,
      mycompany : true,
      covered_period_from : true,
      covered_period_till : true,
      project : true,
      transaction : true,
      report : true,
      has_attachment : true,
      has_scan : true,
      has_src : true,
    },
    interfaceAccountingsInitSetFilter:{},
    interfaceAccountingsInitSetColumn:{},
  },
  getters: {
    LANGUAGE: state => {
      return state.lang;
    }
  },
  mutations: {
    changelang (state, n) {
      state.lang = n
    },
    interfaceTransactionsFilterInitSet(state, filter){
      state.interfaceTransactionsInitSetFilter = filter
      cookies.methods.setCookie('transaction_interface_filter', JSON.stringify(filter))
    },
    interfaceTransactionsColumnInitSet(state, column){
      state.interfaceTransactionsInitSetColumn = column
      cookies.methods.setCookie('transaction_interface_column', JSON.stringify(column))
    },
    interfaceProjectsFilterInitSet(state, filter){
      state.interfaceProjectsInitSetFilter = filter
      cookies.methods.setCookie('projects_interface_filter', JSON.stringify(filter))
    },
    interfaceProjectsColumnInitSet(state, column){
      state.interfaceProjectsInitSetColumn = column
      cookies.methods.setCookie('projects_interface_column', JSON.stringify(column))
    },
    interfaceAgreementsFilterInitSet(state, filter){
      state.interfaceAgreementsInitSetFilter = filter
      cookies.methods.setCookie('agreements_interface_filter', JSON.stringify(filter))
    },
    interfaceAgreementsColumnInitSet(state, column){
      state.interfaceAgreementsInitSetColumn = column
      cookies.methods.setCookie('agreements_interface_column', JSON.stringify(column))
    },
    interfaceAccountingsFilterInitSet(state, filter){
      state.interfaceAccountingsInitSetFilter = filter
      cookies.methods.setCookie('accountings_interface_filter', JSON.stringify(filter))
    },
    interfaceAccountingsColumnInitSet(state, column){
      state.interfaceAccountingsInitSetColumn = column
      cookies.methods.setCookie('accountings_interface_column', JSON.stringify(column))
    },
  },
  actions: {
  },
  modules: {
  }
})
