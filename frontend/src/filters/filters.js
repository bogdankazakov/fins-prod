import Vue from 'vue'
import moment from 'moment'

Vue.filter('toCurrency', function (value) {
    if (typeof value !== "number") {
        value = parseFloat(value)
        if (value === null || value === undefined || isNaN(value)){
            return 0
        }
    }
    var formatter = new Intl.NumberFormat('ru-RU', {
        style: 'currency',
        currency: 'RUB',
    });
    return formatter.format(value);
});

Vue.filter('toCurrency2', function (value) {
    if (typeof value !== "number") {
        return value;
    }
    var formatter = new Intl.NumberFormat('ru-RU', {
        style: 'decimal',
        minimumFractionDigits: '2',

    });
    return formatter.format(value);
});

Vue.filter('toDate', function (value) {
    if (value === null) {
        return value;
    }
    var formated = moment(value).format('DD/MM/YYYY')
    return formated ;
});

Vue.filter('toNone', function (value) {
    if (value === null || value === undefined) {
        return '---';
    }
    return value;
});