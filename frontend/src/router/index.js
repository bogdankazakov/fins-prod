import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [

  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect: { name: 'Projects' }
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import(/* webpackChunkName: "dashboard" */ '../views/Dashboard.vue')
  },
  {
    path: '/transactions',
    name: 'Transactions',
    component: () => import(/* webpackChunkName: "transactions" */ '../views/Transactions.vue')
  },
  {
    path: '/projects',
    component: () => import(/* webpackChunkName: "projects_rppt" */ '../views/Projects/Root.vue'),
    children: [
      {
          path: '',
          name: 'Projects',
          component: () => import(/* webpackChunkName: "projects" */ '../views/Projects/Projects.vue'),
      },
      {
          path: ':projectId',
          name: 'Project',
          component: () => import(/* webpackChunkName: "project" */ '../views/Projects/Project.vue'),
      },

    ],
  },
  {
    path: '/docs',
    component: () => import(/* webpackChunkName: "documents_root" */ '../views/Documents/Root.vue'),
    children: [
      {
          path: '',
          name: 'Documents',
          component: () => import(/* webpackChunkName: "documents" */ '../views/Documents/Documents.vue'),
      },
      {
          path: 'agreements',
          name: 'Agreements',
          component: () => import(/* webpackChunkName: "agreements" */ '../views/Documents/Agreements.vue'),
      },
      {
          path: 'accountings',
          name: 'Accountings',
          component: () => import(/* webpackChunkName: "accountings" */ '../views/Documents/Accountings.vue'),
      },
    ],
  },
  {
    path: '/reports',
    component: () => import(/* webpackChunkName: "reports_root" */ '../views/Reports/Root.vue'),
    children: [
      {
          path: '',
          name: 'Reports',
          component: () => import(/* webpackChunkName: "reports" */ '../views/Reports/Reports.vue'),
      },
      {
          path: 'pl',
          name: 'ReportPL',
          component: () => import(/* webpackChunkName: "reports_pl" */ '../views/Reports/PL.vue'),
      },
      {
          path: 'chashflow',
          name: 'ReportCashflow',
          component: () => import(/* webpackChunkName: "reports_chashflow" */ '../views/Reports/Cashflow.vue'),
      },
      {
          path: 'subcontractors',
          name: 'ReportSubcontractors',
          component: () => import(/* webpackChunkName: "reports_subcontractors" */ '../views/Reports/Subcontractors.vue'),
      },
      {
          path: 'brands',
          name: 'ReportBrands',
          component: () => import(/* webpackChunkName: "reports_brands" */ '../views/Reports/Brands.vue'),
      },
    ],
  },
  {
    path: '/subcontractors',
    name: 'Subcontractors',
    component: () => import(/* webpackChunkName: "subcontractors" */ '../views/Subcontractors.vue')
  },
  {
    path: '/setup',
    component: () => import(/* webpackChunkName: "setup_root" */ '../views/Setup/Root.vue'),
    children: [
      {
          path: '',
          name: 'Setup',
          component: () => import(/* webpackChunkName: "setup" */ '../views/Setup/Setup.vue'),
      },
      {
          path: 'subtype',
          name: 'SetupSubcontractorType',
          component: () => import(/* webpackChunkName: "setup_subtype" */ '../views/Setup/SubcontractorType.vue'),
      },
      {
          path: 'units',
          name: 'SetupUnits',
          component: () => import(/* webpackChunkName: "setup_units" */ '../views/Setup/Units.vue'),
      },
      {
          path: 'paypurposes',
          name: 'SetupPayPurposes',
          component: () => import(/* webpackChunkName: "setup_paypurposes" */ '../views/Setup/PayPurpose.vue'),
      },
      {
          path: 'currency',
          name: 'SetupCurrency',
          component: () => import(/* webpackChunkName: "setup_currency" */ '../views/Setup/Currency.vue'),
      },
      {
          path: 'accounts',
          name: 'SetupAccounts',
          component: () => import(/* webpackChunkName: "setup_accounts" */ '../views/Setup/Accounts.vue'),
      },
      {
          path: 'legalform',
          name: 'SetupLF',
          component: () => import(/* webpackChunkName: "setup_legalform" */ '../views/Setup/LF.vue'),
      },
      {
          path: 'taxetypes',
          name: 'SetupTaxeTypes',
          component: () => import(/* webpackChunkName: "setup_taxetypes" */'../views/Setup/TaxeTypes.vue'),
      },
      {
          path: 'projectcategories',
          name: 'SetupProjectCategories',
          component: () => import(/* webpackChunkName: "setup_projectcategories" */ '../views/Setup/ProjectCategories.vue'),
      },
      {
          path: 'projectsubategories',
          name: 'SetupProjectSubategories',
          component: () => import(/* webpackChunkName: "setup_projectsubategories" */ '../views/Setup/ProjectSubcategories.vue'),
      },
      {
          path: 'brands',
          name: 'SetupBrands',
          component: () => import(/* webpackChunkName: "setup_brands" */ '../views/Setup/Brands.vue'),
      },
      {
          path: 'nbsources',
          name: 'SetupNBSources',
          component: () => import(/* webpackChunkName: "setup_nbsources" */ '../views/Setup/NBSources.vue'),
      },
      {
          path: 'nbformats',
          name: 'SetupNBFormats',
          component: () => import(/* webpackChunkName: "setup_nbformats" */ '../views/Setup/NBFormats.vue'),
      },
      {
          path: 'productscategories',
          name: 'SetupProductsCategories',
          component: () => import(/* webpackChunkName: "setup_productscategories" */ '../views/Setup/ProductsCategories.vue'),
      },
      {
          path: 'products',
          name: 'SetupProducts',
          component: () => import(/* webpackChunkName: "setup_products" */ '../views/Setup/Products.vue'),
      },

    ],
  },
  {
    path: '/team',
    name: 'Team',
    component: () => import(/* webpackChunkName: "team" */ '../views/Team.vue')
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () => import(/* webpackChunkName: "profile" */ '../views/Profile.vue')
  },
  {
      path: '*',
      redirect: { name: 'Projects' }
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
