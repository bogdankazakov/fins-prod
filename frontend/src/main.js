import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "@/filters/filters"
// import './registerServiceWorker'
import cookies from "@/mixins/cookies";

Vue.config.productionTip = false

Vue.prototype.$filters = Vue.options.filters

Vue.mixin({
  methods: {
    getCookie: cookies.methods.getCookie,
    setCookie: cookies.methods.setCookie,
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
