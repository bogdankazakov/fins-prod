import axios from 'axios';
import moment from 'moment'


export default {
    computed:{
        cuForm_dateDoc: function () {
            if (this.cuForm_payModel.toString() === "0"){
                if (this.cuForm_agreement_start_date !== null) {
                    return moment(this.cuForm_agreement_start_date).subtract(this.cuForm_payDelay, 'days').format('YYYY-MM-DD')
                }
            } else {
                if(this.cuForm_agreement_end_date !== null) {
                    return moment(this.cuForm_agreement_end_date).add(this.cuForm_payDelay, 'days').format('YYYY-MM-DD')
                }
            }
        },
        cuForm_taxe_multiplicator: function () {
            if (this.cuForm_type === 'True'){
                return this.cuForm_company_owner_taxe_multiplicator
            }
            return this.cuForm_subcontractor_taxe_multiplicator
        },
        url_detail: function () {
            return "/transactions/api/" + this.instance.id + "/"
        },
    },
    methods: {
        instanceSendCompiler:function () {
            this.instance.account = this.cuForm_account;
            this.instance.type = this.cuForm_type;
            this.instance.purpose = this.cuForm_purpose;
            this.instance.subcontractor = this.cuForm_subcontractor;
            this.instance.sum_before_taxes = this.cuForm_sbt;
            this.instance.agreement_start_date = this.cuForm_agreement_start_date;
            this.instance.agreement_end_date = this.cuForm_agreement_end_date;
            this.instance.pay_model = this.cuForm_payModel;
            this.instance.pay_delay = this.cuForm_payDelay;
            this.instance.date_doc = this.cuForm_dateDoc;

            this.instance.sum_before_taxes = this.cuForm_sbt.replace(',','.').replace(/\s/g, '');

        },
        instanceSendDecompiler:function () {
            this.cuForm_account = this.instance.account;
            this.cuForm_type = this.boolConverter(this.instance.type);
            this.cuForm_purpose = this.instance.purpose;
            this.cuForm_subcontractor = this.instance.subcontractor;
            this.cuForm_sbt = this.instance.sum_before_taxes;
            this.cuForm_agreement_start_date = this.instance.agreement_start_date;
            this.cuForm_agreement_end_date = this.instance.agreement_end_date;
            this.cuForm_payModel = this.instance.pay_model;
            this.cuForm_payDelay = this.instance.pay_delay;
            this.cuForm_company_owner_taxe_name = this.instance.pay_delay;
            this.instance.is_editable = this.boolConverter(this.instance.is_editable);
            this.instance.hidden = this.boolConverter(this.instance.hidden);

            this.cuForm_sbt = this.instance.sum_before_taxes;
        },
        boolConverter: function(bool){
            if (bool === true){
                return 'True'
            } else{
                return 'False'
            }
        },
        getExtra(){
            axios({
                method: 'get',
                url: this.urlExtra,
            })
            .then(response => {
                this.extra = response.data.extra;
            })
        },
        taxedependantCalcul: function (newValue) {
            this.cuForm_sat = this.$filters.toCurrency2(parseFloat((this.numberClean(newValue) * this.cuForm_taxe_multiplicator).toFixed(2)))
            this.cuForm_st =  this.$filters.toCurrency2(parseFloat((this.numberClean(newValue) * this.cuForm_taxe_multiplicator - this.numberClean(newValue)).toFixed(2)))
        },
        purpose_list: function () {
            var list = [];
            if (this.cuForm_type === 'True'){
                for (const purpose of this.extra.purposes){
                    if (purpose.type === true) {
                        list.push(purpose)
                    }
                }
            } else {
                for (const purpose of this.extra.purposes){
                    if (purpose.type === false) {
                        list.push(purpose)
                    }
                }
            }
            this.cuForm_purpose_list = list
        },
        unit: function () {
            this.cuForm_isVisibleUnit = (this.cuForm_type === 'False');
        },
        numberClean: function (num) {
            var formated = String(num);
            formated = formated.replace(/\s/g, '');
            formated = formated.replace(',', '.');
            formated = parseFloat(formated);
            return formated
        },
        setDefault:function(){
            this.unit();
            this.purpose_list();

            this.instance  = JSON.parse(JSON.stringify(this.instanceDefault)) ;

            if(this.editedInProjectId){
                this.instance.project = this.editedInProjectId
            }
            if(this.editedInPeriodId){
                this.instance.period = this.editedPeriod
                this.instance.project = this.editedProject
            }
            this.cuForm_account = '';
            this.cuForm_subcontractor = '';
            this.cuForm_company_owner_taxe_name = null;
            this.cuForm_currency_name = null;
            this.cuForm_type = 'True';
            this.cuForm_subcontractor = '';
            this.cuForm_subcontractor_taxe = null;
            this.cuForm_subcontractor_lf = null;
            this.cuForm_subcontractor_isDisabled = true;
            this.cuForm_sbt = 0;
            this.cuForm_sat = 0;
            this.cuForm_st = 0;
            this.cuForm_sbt_isDisabled = true;
            this.cuForm_sat_isDisabled = true;
            this.cuForm_payModel = 0;
            this.cuForm_payDelay = 0;
            this.cuForm_agreement_start_date = null;
            this.cuForm_agreement_end_date = null;


            try {
                var list = [];
                for (const purpose of this.cuForm_purpose_list) {
                    list.push(purpose.id)
                }
                this.cuForm_purpose = list.sort()[0]
            }
            catch (e) {}
        },
    },
    watch:{
        cuForm_account: function (newValue, oldValue) {
                this.cuForm_currency_name = null;
                this.cuForm_company_owner_taxe_name = null;
                this.cuForm_subcontractor_isDisabled = true;

                for (const account of this.extra.accounts){
                    if (String(account.id) === String(newValue)){
                        this.cuForm_currency_name = account.currency_name;
                        this.cuForm_company_owner_taxe_name = account.company_owner_taxe_name;
                        this.cuForm_company_owner_taxe_multiplicator = account.company_owner_taxe_multiplicator;
                        this.cuForm_subcontractor_isDisabled = false;
                        this.taxedependantCalcul(this.cuForm_sbt)
                    }
                }
            },
        cuForm_type: function (newValue, oldValue) {
            this.purpose_list();
            this.unit();
            this.taxedependantCalcul(this.cuForm_sbt)
        },
        cuForm_subcontractor: function (newValue, oldValue) {
            this.cuForm_subcontractor_taxe = null;
            this.cuForm_subcontractor_lf = null;
            this.cuForm_sbt_isDisabled = true;
            this.cuForm_sat_isDisabled = true;
            this.cuForm_payDelay = 0;

            for (const sub of this.extra.subcontractors){
                if (String(sub.id) === String(newValue)){
                    this.cuForm_subcontractor_taxe = sub.taxe_type_name;
                    this.cuForm_subcontractor_lf = sub.legal_form_name;
                    this.cuForm_subcontractor_taxe_multiplicator = sub.taxe_type_multiplicator;
                    this.cuForm_sbt_isDisabled = false;
                    this.cuForm_sat_isDisabled = false;
                    this.cuForm_payDelay = sub.pay_delay;
                    this.taxedependantCalcul(this.cuForm_sbt)

                }
            }
        },
        cuForm_sbt: _.debounce(function (newValue, oldValue) {
                this.taxedependantCalcul(newValue)
                this.cuForm_sbt =  this.$filters.toCurrency2(parseFloat(this.numberClean(newValue).toFixed(2)))
        }, 400) ,
        cuForm_sat: _.debounce(function (newValue, oldValue) {
                    this.cuForm_sbt = this.$filters.toCurrency2(parseFloat((this.numberClean(newValue) / this.cuForm_taxe_multiplicator).toFixed(2)))
                    this.cuForm_st =  this.$filters.toCurrency2(parseFloat((this.numberClean(newValue) - this.numberClean(newValue) / this.cuForm_taxe_multiplicator).toFixed(2)))
                    this.cuForm_sat =  this.$filters.toCurrency2(parseFloat(this.numberClean(newValue).toFixed(2)))
            }, 400) ,
    },
};