import Wording from '@/components/setup/subcontractors/wording.json'

export default {
    computed: {
        wording(){
            if(this.$store.getters.LANGUAGE === 'ru'){
                return Wording.ru
            } else if (this.$store.getters.LANGUAGE === 'en'){
                return Wording.en
            }
        }
    },
};