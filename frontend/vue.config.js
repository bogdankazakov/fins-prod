const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const { GenerateSW } = require("workbox-webpack-plugin");

module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
    ? '/app/'
    : '/app/',
    // outputDir: '../fins/app_frontend/',
    // assetsDir: '../static/',
    // indexPath: './templates/app_frontend/index.html',
    configureWebpack:{
        optimization: {
            splitChunks: {
                chunks: 'all'
            },
        },
        plugins: [
            // new BundleAnalyzerPlugin(),
            // new GenerateSW({})
        ]
    },
    chainWebpack: config => {
        config.plugins.delete('prefetch');
        config.plugins.delete('pwa');
        config.plugins.delete('workbox');
    },

    // pwa: {
    //     name: 'FinS',
    //     themeColor: '#2f4c98',
    //     msTileColor: '#000000',
    //     appleMobileWebAppCapable: 'yes',
    //     appleMobileWebAppStatusBarStyle: 'black',
    //     iconPaths:{
    //         favicon32: 'img/icons/favicon-32x32.png',
    //         favicon16: 'img/icons/favicon-16x16.png',
    //         appleTouchIcon: 'img/icons/apple-touch-icon-152x152.png',
    //         msTileImage: 'img/icons/msapplication-icon-144x144.png'
    //     },
    //
    //     // настройки манифеста
    //     manifestOptions: {
    //         display: 'standalone',
    //         orientation: 'portrait',
    //         icons: [
    //             {
    //               "src": "img/icons/icon-72x72.png",
    //               "sizes": "72x72",
    //               "type": "image/png"
    //             },
    //             {
    //               "src": "img/icons/icon-96x96.png",
    //               "sizes": "96x96",
    //               "type": "image/png"
    //             },
    //             {
    //               "src": "img/icons/icon-128x128.png",
    //               "sizes": "128x128",
    //               "type": "image/png"
    //             },
    //             {
    //               "src": "img/icons/icon-144x144.png",
    //               "sizes": "144x144",
    //               "type": "image/png"
    //             },
    //             {
    //               "src": "img/icons/icon-152x152.png",
    //               "sizes": "152x152",
    //               "type": "image/png"
    //             },
    //             {
    //               "src": "img/icons/icon-192x192.png",
    //               "sizes": "192x192",
    //               "type": "image/png"
    //             },
    //             {
    //               "src": "img/icons/icon-384x384.png",
    //               "sizes": "384x384",
    //               "type": "image/png"
    //             },
    //             {
    //               "src": "img/icons/icon-512x512.png",
    //               "sizes": "512x512",
    //               "type": "image/png"
    //             }
    //           ],
    //     },
    //
    //     // настройка workbox-плагина
    //     // workboxPluginMode: 'GenerateSW',
    //     // workboxOptions: {
    //     // // swSrc необходимо в режиме InjectManifest
    //     // swSrc: 'dev/sw.js',
    //     // // ...другие настройки Workbox...
    //     // }
    // }
};
