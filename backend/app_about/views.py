from django.shortcuts import render
import logging

logger = logging.getLogger(__name__)

def index(request):
    context = {}
    return render(request, 'app_about/index.html', context)

def index_en(request):
    context = {}
    return render(request, 'app_about/index_en.html', context)
