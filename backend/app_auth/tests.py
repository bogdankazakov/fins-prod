from django.test import TestCase, SimpleTestCase, RequestFactory
from django.test import Client
from django.urls import reverse
from app_setup import models as app_setup_models
from app_signup import models as app_signup_models
from app_signup import views as app_signup_views
from app_auth import views as app_auth_views
from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
from django.conf import settings
from fins import urls  as fins_url
import psycopg2
from django.contrib.messages.storage.fallback import FallbackStorage

def load_url_pattern_names(patterns, Exeption_app_list_default, Exeption_app_list_no_login,
                           Exeption_name_list_default, Exeption_name_list_no_login, app_name=[] ):
    """Retrieve a list of urlpattern names"""
    ALL_URLS = []
    Exeption_app_list = Exeption_app_list_default + Exeption_app_list_no_login
    Exeption_name_list = Exeption_name_list_default + Exeption_name_list_no_login

    new_patterns =[]
    if not app_name:
        new_patterns = patterns
    else:
        for pat in patterns:
            if pat.__class__.__name__ == 'URLResolver' and pat.app_name in app_name:
                new_patterns.append(pat)

    print('patterns', len(patterns))
    print('new_patterns', len(new_patterns))
    for pat in new_patterns:
        if pat.__class__.__name__ == 'URLResolver' and pat.app_name not in Exeption_app_list:
            for el in pat.url_patterns:
                if el.__class__.__name__ != 'URLResolver' and el.name not in Exeption_name_list:
                    if el.name is not None and el.name not in ALL_URLS:
                        try:
                            ALL_URLS.append(reverse(str(pat.app_name) + ":" + str(el.name)))
                        except Exception:
                            try:
                                ALL_URLS.append(reverse(str(pat.app_name) + ":" + str(el.name), kwargs={'pk': "1"}))
                            except Exception:
                                try:
                                    ALL_URLS.append(
                                        reverse(str(pat.app_name) + ":" + str(el.name), kwargs={'pk': "1", 'el': "1"}))
                                except Exception:
                                    try:
                                        ALL_URLS.append(reverse(str(pat.app_name) + ":" + str(el.name),
                                                                kwargs={'pk': "1", 'pk_c': "1"}))
                                    except Exception:
                                        try:
                                            ALL_URLS.append(reverse(str(pat.app_name) + ":" + str(el.name),
                                                                    kwargs={'pk_c': "1"}))
                                        except Exception:
                                            try:
                                                ALL_URLS.append(reverse(str(pat.app_name) + ":" + str(el.name),
                                                                        kwargs={'pk': "1", 'type': "1"}))
                                            except Exception:
                                                try:
                                                    ALL_URLS.append(reverse(str(pat.app_name) + ":" + str(el.name),
                                                                            kwargs={'projectpk': "1"}))
                                                except Exception:
                                                    try:
                                                        ALL_URLS.append(reverse(str(pat.app_name) + ":" + str(el.name),
                                                                                kwargs={'projectpk': "1", 'periodpk': "1"}))
                                                    except Exception:
                                                        try:
                                                            ALL_URLS.append(
                                                                reverse(str(pat.app_name) + ":" + str(el.name),
                                                                        kwargs={'projectpk': "1", 'pk': "1"}))
                                                        except Exception:
                                                            raise Exception(
                                                                '"It looks like you have more parameters combinations"')

        elif pat.__class__.__name__ == 'URLPattern':
            if pat.name is not None and pat.name not in ALL_URLS and pat.name not in Exeption_name_list:
                ALL_URLS.append(reverse(str(pat.name)))
    print("pattern list", ALL_URLS)
    return ALL_URLS



class CompanyAccessPermissionsTest(TestCase):

    def setUp(self):
        # self.client = Client(SERVER_NAME="fins.my")
        self.client = Client()
        site = Site.objects.get(pk=1)
        site.domain = 'testserver'
        site.save()

        self.c0 = app_signup_models.Account(name='MyCompany', subdomain='badsubdomain')
        self.c0.save()
        self.c0.create_database()
        self.c = app_signup_models.Account(name='MyCompany2', subdomain='mysubdomain')
        self.c.save()
        self.c.create_database()


        self.user = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b@b.ru')
        self.user.set_password('123')
        self.user.created_account_pk = self.c.pk
        self.user.save()
        self.user.account.add(self.c)


        self.c1 = app_signup_models.Account(name='MyCompany3', subdomain='mysubdomain3', is_active=False)
        self.c1.save()
        self.c1.create_database()

        self.user2 = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b23@b.ru')
        self.user2.set_password('qweqwe111')
        self.user2.created_account_pk = self.c1.pk
        self.user2.save()
        self.user2.account.add(self.c1)

    def tearDown(self):

        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

    def test_company_access_permission(self):
        """
        Tests:
        - that all pages under auth control & redirection is correct
        - that all pages under company access control & redirection is correct

        Exceptions are given in exception list


        """

        Exeption_app_list_default = ['admin', ]
        Exeption_app_list_no_login = ['app_about', 'app_auth', 'app_signup',]
        Exeption_name_list_default = []
        Exeption_name_list_no_login = ['join']
        for item in load_url_pattern_names(fins_url.urlpatterns,
                                                Exeption_app_list_default, Exeption_app_list_no_login,
                                                Exeption_name_list_default, Exeption_name_list_no_login):

            print('item', item)
            # tests that page under auth control & redirection is correct
            self.client.logout()
            response = self.client.get(item)
            print(str(item) + ' - start user not logged in test')
            self.assertRedirects(response, '/auth/?next='+str(item))
            print(str(item) + ' - finished user not logged in test')


            # that page is under company access control & redirection is correct
            self.client.login(username='b@b.ru', password='123')
            response = self.client.get(
                str(item), HTTP_HOST='badsubdomain.testserver')
            self.assertRedirects(response,
                                 sub_reverse(settings.ACCESS_DENIED_TO_COMPANY_REDIRECT_URL, subdomain=None),
                                 status_code=302, target_status_code=200,
                                 msg_prefix='', fetch_redirect_response=True)

            self.client.login(username='b@b.ru', password='123')
            response = self.client.get(
                str(item), HTTP_HOST='doesntexist.testserver')
            self.assertRedirects(response,
                                 sub_reverse(settings.ACCESS_DENIED_TO_COMPANY_REDIRECT_URL, subdomain=None),
                                 status_code=302, target_status_code=200,
                                 msg_prefix='', fetch_redirect_response=True)

            # that page is under company access control - IS ACTIVE
            self.client.login(username='b23@b.ru', password='qweqwe111')
            response = self.client.get(
                str(item), HTTP_HOST='mysubdomain3.testserver')
            self.assertRedirects(response,
                                 sub_reverse(settings.ACCESS_DENIED_TO_COMPANY_REDIRECT_URL, subdomain=None),
                                 status_code=302, target_status_code=200,
                                 msg_prefix='', fetch_redirect_response=True)

class MultiloginTest(TestCase):

    def setUp(self):
        # self.client = Client(SERVER_NAME="fins.my")
        self.client = Client()
        self.factory = RequestFactory()
        site = Site.objects.get(pk=1)
        site.domain = 'testserver'
        site.save()

    def tearDown(self):
        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

    def get_db_name(self):
        q = app_signup_models.Account.objects.all().order_by('-pk')[0]
        name = 'account_' + str(q.pk)
        return name

    def test_multilogin_and_is_active(self):
        """
        checks if user if redirected if attached to multiple accounts and account can be blocked separatly
        """

        # create ONE user and ONE attached account

        self.client.post(reverse('app_signup:signup'),
                                    {
                                        'first_name': ['test'],
                                        'last_name': ['test'],
                                        'email': ['test@test.ru'],
                                        'password1': ['qweqwe111'],
                                        'password2': ['qweqwe111'],
                                        'name': ['Company'],
                                        'subdomain': ['mysubdomain']
                                    }, follow=True)

        user = app_signup_models.User.objects.get(email='test@test.ru')
        account_1 = app_signup_models.Account.objects.get(name='Company')
        dbuser_1 = app_setup_models.DBUser.objects.using(self.get_db_name()).get(user_id=user.pk)

        self.client.get(reverse('app_auth:logout'))




        #login with only ONE account attached user IS active

        link = sub_reverse('app_auth:login',subdomain=None)
        data = {
                 'username': ['test@test.ru'],
                 'password': ['qweqwe111'],
                }

        request = self.factory.post(link, data)
        request.user = user
        request.account = account_1
        request.session = self.client.session
        response = app_auth_views.login_user(request)

        self.assertRedirects(response,
                             sub_reverse(settings.LOGIN_REDIRECT_URL, subdomain=account_1.subdomain),
                             status_code=302, target_status_code=200,
                             msg_prefix='', fetch_redirect_response=False) # we are loged in

        self.client.get(reverse('app_auth:logout'))

        #login with only ONE account attached user IS NOT active

        link = sub_reverse('app_auth:login',subdomain=None)
        data = {
                 'username': ['test@test.ru'],
                 'password': ['qweqwe111'],
                }

        dbuser_1.is_active = False
        dbuser_1.save(using=self.get_db_name())

        request = self.factory.post(link, data)
        request.user = user
        request.account = account_1
        request.session = self.client.session

        # need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        response = app_auth_views.login_user(request)

        self.assertEqual(response.status_code, 200) # we are still in the same login page because the account is blocked

        self.client.get(reverse('app_auth:logout'))



        # create another account and attach same user

        self.client.post(reverse('app_signup:signup'),
                                    {
                                        'first_name': ['test'],
                                        'last_name': ['test'],
                                        'email': ['test2@test2.ru'],
                                        'password1': ['qweqwe111'],
                                        'password2': ['qweqwe111'],
                                        'name': ['Company2'],
                                        'subdomain': ['mysubdomain2']
                                    }, follow=True)

        user_2 = app_signup_models.User.objects.get(email='test2@test2.ru')
        account_2 = app_signup_models.Account.objects.get(name='Company2')
        app_setup_models.DBUser.objects.using(self.get_db_name()).get(user_id=user_2.pk)

        self.client.get(reverse('app_auth:logout'))

        user.account.add(account_2)
        dbuser_2 = app_setup_models.DBUser.objects.using(self.get_db_name()).create(
                user_id=user.pk)

        print(dbuser_1.is_active, dbuser_2.is_active)

        #login with TWO account attached. We will check access decorator work.

        link = sub_reverse('app_auth:login', subdomain=None)
        data = {
            'username': ['test@test.ru'],
            'password': ['qweqwe111'],
        }
        response = self.client.post(link,data, follow=True)

        self.assertRedirects(response,
                             sub_reverse('app_auth:multilogin', kwargs={'pk':user.pk}, subdomain=None),
                             status_code=302, target_status_code=200,
                             msg_prefix='', fetch_redirect_response=False) # we are redirected to multilogin page


        # the user is_active=True in the account_2, lets check if we can get in
        self.client.login(username='test@test.ru', password='qweqwe111')
        response = self.client.get(
            reverse('app_dashboard:index'), HTTP_HOST='mysubdomain2.testserver')

        self.assertEqual(response.status_code, 200) # we are in

        #but user is blocked in account_1, lets check if we cannot get in.

        response = self.client.get(
            reverse('app_dashboard:index'), HTTP_HOST='mysubdomain.testserver')
        self.assertEqual(response.status_code, 302) # we cannot get in


