from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache
from django.http import HttpResponseRedirect, QueryDict
from django.utils.http import is_safe_url, urlsafe_base64_decode
from django.contrib.auth import (
    REDIRECT_FIELD_NAME, get_user_model, login as auth_login,
    logout as auth_logout, update_session_auth_hash,
)
from django.shortcuts import resolve_url
from django.urls import reverse, reverse_lazy
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from subdomains.utils import reverse as sub_reverse
from django.conf import settings
from .forms import *
from django.contrib import messages
from django.contrib.auth.views import PasswordContextMixin, TemplateView, FormView, PasswordResetForm, SetPasswordForm
from django.utils.translation import gettext_lazy as _
from app_signup.models import User
from app_transaction import models as app_transaction_models
from app_setup import models as app_setup_models

import logging
import threading

logger = logging.getLogger(__name__)


def login_user(request):
    next = False
    if request.POST:
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                account_list = user.account.all()
                if len(account_list) == 1:
                    dbuser = app_setup_models.DBUser.objects.using('account_' + str(account_list[0].pk)).get(user_id=user.id)
                    if account_list[0].is_active:
                        if dbuser.is_active:
                            login(request, user)
                            subdomain = account_list[0].subdomain
                            return redirect(sub_reverse(settings.LOGIN_REDIRECT_URL, subdomain=subdomain, scheme='https'))
                        else:
                            messages.error(request, 'Доступ в аккаунт {0} для вас закрыт.'.format(account_list[0]))
                    else:
                        messages.error(request, 'Аккаунт {0} заблокирован.'.format(account_list[0]))
                else:
                    return redirect(sub_reverse('app_auth:multilogin', kwargs={'pk': user.pk}, subdomain=None))
            else:
                messages.error(request, 'Неверный логин или пароль')
    else:
        if '?next=' in str(request.get_full_path()):
            next=True
        form = LoginForm()
    return render(request, 'app_auth/login.html',{'form': form, 'next': next})

def login_user_en(request):
    next = False
    if request.POST:
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                account_list = user.account.all()
                if len(account_list) == 1:
                    dbuser = app_setup_models.DBUser.objects.using('account_' + str(account_list[0].pk)).get(user_id=user.id)
                    if account_list[0].is_active:
                        if dbuser.is_active:
                            login(request, user)
                            subdomain = account_list[0].subdomain
                            return redirect(sub_reverse(settings.LOGIN_REDIRECT_URL, subdomain=subdomain, scheme='https'))
                        else:
                            messages.error(request, 'Доступ в аккаунт {0} для вас закрыт.'.format(account_list[0]))
                    else:
                        messages.error(request, 'Аккаунт {0} заблокирован.'.format(account_list[0]))
                else:
                    return redirect(sub_reverse('app_auth:multilogin', kwargs={'pk': user.pk}, subdomain=None, scheme='https'))
            else:
                messages.error(request, 'Неверный логин или пароль')
    else:
        if '?next=' in str(request.get_full_path()):
            next=True
        form = LoginForm()
    return render(request, 'app_auth/login_en.html',{'form': form, 'next': next})



def multilogin(request, pk):
    user = User.objects.get(pk=pk)
    account_list = user.account.all()
    for account in account_list:
        dbuser = app_setup_models.DBUser.objects.using('account_' + str(account.pk)).get(user_id=user.id)
        if account.is_active:
            if dbuser.is_active:
                login(request, user)
                account.link = sub_reverse(settings.LOGIN_REDIRECT_URL, subdomain=account.subdomain, scheme='https')
                account.userblocked = False
            else:
                account.message = 'Доступ в аккаунт {0} для вас закрыт.'.format(account)
                account.userblocked = True
        else:
            messages.error(request, 'Аккаунт {0} заблокирован.'.format(account))
    return render(request, 'app_auth/multilogin.html', {'account_list': account_list, })



def logout_user(request):
    env = threading.local()
    logout(request)
    return redirect(sub_reverse(settings.LOGOUT_REDIRECT_URL, subdomain=None, scheme='https'))

class PasswordResetView(PasswordContextMixin, FormView):
    email_template_name = 'app_auth/password_reset/password_reset_email.html'
    extra_email_context = None
    form_class = PasswordResetForm
    from_email = None
    html_email_template_name = None
    subject_template_name = 'app_auth/password_reset/password_reset_subject.txt'
    success_url = reverse_lazy('app_auth:password_reset_done')
    template_name = 'app_auth/password_reset/password_reset_form.html'
    title = _('Password reset')
    token_generator = default_token_generator

    @method_decorator(csrf_protect)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def form_valid(self, form):
        opts = {
            'use_https': self.request.is_secure(),
            'token_generator': self.token_generator,
            'from_email': self.from_email,
            'email_template_name': self.email_template_name,
            'subject_template_name': self.subject_template_name,
            'request': self.request,
            'html_email_template_name': self.html_email_template_name,
            'extra_email_context': self.extra_email_context,
        }
        form.save(**opts)
        return super().form_valid(form)


INTERNAL_RESET_URL_TOKEN = 'set-password'
INTERNAL_RESET_SESSION_TOKEN = '_password_reset_token'


class PasswordResetDoneView(PasswordContextMixin, TemplateView):
    template_name = 'app_auth/password_reset/password_reset_done.html'
    title = _('Password reset sent')

UserModel = get_user_model()

class PasswordResetConfirmView(PasswordContextMixin, FormView):
    form_class = SetPasswordForm
    post_reset_login = False
    post_reset_login_backend = None
    success_url = reverse_lazy('app_auth:password_reset_complete')
    template_name = 'app_auth/password_reset/password_reset_confirm.html'
    title = _('Enter new password')
    token_generator = default_token_generator

    @method_decorator(sensitive_post_parameters())
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        assert 'uidb64' in kwargs and 'token' in kwargs

        self.validlink = False
        self.user = self.get_user(kwargs['uidb64'])

        if self.user is not None:
            token = kwargs['token']
            if token == INTERNAL_RESET_URL_TOKEN:
                session_token = self.request.session.get(INTERNAL_RESET_SESSION_TOKEN)
                if self.token_generator.check_token(self.user, session_token):
                    # If the token is valid, display the password reset form.
                    self.validlink = True
                    return super().dispatch(*args, **kwargs)
            else:
                if self.token_generator.check_token(self.user, token):
                    # Store the token in the session and redirect to the
                    # password reset form at a URL without the token. That
                    # avoids the possibility of leaking the token in the
                    # HTTP Referer header.
                    self.request.session[INTERNAL_RESET_SESSION_TOKEN] = token
                    redirect_url = self.request.path.replace(token, INTERNAL_RESET_URL_TOKEN)
                    return HttpResponseRedirect(redirect_url)

        # Display the "Password reset unsuccessful" page.
        return self.render_to_response(self.get_context_data())

    def get_user(self, uidb64):
        try:
            # urlsafe_base64_decode() decodes to bytestring
            uid = urlsafe_base64_decode(uidb64).decode()
            user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            user = None
        return user

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.user
        return kwargs

    def form_valid(self, form):
        user = form.save()
        del self.request.session[INTERNAL_RESET_SESSION_TOKEN]
        if self.post_reset_login:
            auth_login(self.request, user, self.post_reset_login_backend)
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.validlink:
            context['validlink'] = True
        else:
            context.update({
                'form': None,
                'title': _('Password reset unsuccessful'),
                'validlink': False,
            })
        return context

class PasswordResetCompleteView(PasswordContextMixin, TemplateView):
    template_name = 'app_auth/password_reset/password_reset_complete.html'
    title = _('Password reset complete')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['login_url'] = resolve_url(settings.LOGIN_URL)
        return context



def demo(request):
    username = 'demo@finsapp.ru'
    password = 'testtest1'
    subdomain = 'demo'
    user = authenticate(username=username, password=password)
    login(request, user)
    return redirect(sub_reverse(settings.LOGIN_REDIRECT_URL, subdomain=subdomain, scheme='https'))



def demo_eng(request):
    username = 'demoeng@finsapp.ru'
    password = 'qweqwe111'
    subdomain = 'democompany'
    user = authenticate(username=username, password=password)
    login(request, user)
    response = redirect(sub_reverse(settings.LOGIN_REDIRECT_URL, subdomain=subdomain, scheme='https'))
    response.set_cookie('cookie_name', 'cookie_value', max_age=1000)
    return response


