from django.urls import path, re_path, include
from django.contrib.auth import views as auth_views
from . import views

app_name = 'app_auth'
urlpatterns = [
    # path('', include('django.contrib.auth.urls'), name='login'),
    path('', views.login_user, name='login'),
    path('en/', views.login_user_en, name='login_en'),
    path('multilogin/<pk>', views.multilogin, name='multilogin'),
    path('logout/', views.logout_user, name='logout'),

    path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    re_path('reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/', views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),

    path('demo/', views.demo, name='demo'),
    path('demo_eng/', views.demo_eng, name='demo_eng'),

]

