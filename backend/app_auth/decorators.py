from django.shortcuts import redirect
from subdomains.utils import reverse as sub_reverse
from functools import wraps
from app_signup.models import User
from django.conf import settings
from app_signup.models import Account
from app_setup import models as app_setup_models
from django.contrib import messages
import pytz
from datetime import datetime




def access_company_level(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Middleware check if subdomain matches any account. So if requesr.account is None,
        that means that subdomain is wrong - we redirect it.
        Than we check that accoutn is active.
        Than we check if any of users accounts matches the given, if no — redirect.

        We also check that user is not for the first time in the company - if yes - add info to DB
        '''


        user_accounts = request.user.account.all()
        requested_account = request.account

        if requested_account:
            if requested_account.is_active:
                for item in user_accounts:
                    if item == requested_account:
                        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
                        if dbuser.is_active:
                            if dbuser.is_joined is False:
                                dbuser.joined_at = pytz.utc.localize(datetime.utcnow())
                                dbuser.is_joined = True
                                dbuser.save()
                            return function(request, *args, **kwargs)
                messages.error(request, 'У вас нет доступа в аккаунт')
                return redirect(
                    sub_reverse(settings.ACCESS_DENIED_TO_COMPANY_REDIRECT_URL, subdomain=None, scheme='https'))

            else:
                messages.error(request,'Аккаунт заблокирован')
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_COMPANY_REDIRECT_URL, subdomain=None, scheme='https'))
        else:
            messages.error(request, 'Аккаунт не существует')
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_COMPANY_REDIRECT_URL, subdomain=None, scheme='https'))



    return wrap

