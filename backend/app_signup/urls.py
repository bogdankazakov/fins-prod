from django.contrib import admin
from django.urls import path, include, re_path
from django.contrib.auth import views as auth_views
from . import views

app_name = 'app_signup'
urlpatterns = [
    path('', views.signup, name='signup'),
    path('en/', views.signup_en, name='signup_en'),
    path('modal-help/', views.modal_help, name='modal_help'),



]


