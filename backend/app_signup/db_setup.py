from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from app_setup import models as app_setup_models
from app_project.tools import numgen
from decimal import *
from app_project.tools import numgen as numgen_p
from datetime import datetime, date, time, timedelta
from app_transaction.tools import numgen as numgen_t
from datetime import datetime, date, time, timedelta

def initial_setup(name):

    lf = app_transaction_models.Legalform(name='ООО', is_deletable=False)
    lf.save(using=name)
    app_transaction_models.Legalform(name='ИП', is_deletable=False).save(using=name)
    app_transaction_models.Legalform(name='Физ', is_deletable=False).save(using=name)

    app_transaction_models.SubcontractorType(name='Клиент', is_deletable=False).save(using=name)
    app_transaction_models.SubcontractorType(name='Подрядчик', is_deletable=False).save(using=name)
    app_transaction_models.SubcontractorType(name='Сотрудник', is_deletable=False).save(using=name)
    st = app_transaction_models.SubcontractorType(name='Моя компания', is_deletable=False)
    st.save(using=name)

    tt = app_transaction_models.TaxeType(name='НДС', multiplicator=1.2, is_deletable=False)
    tt.save(using=name)
    app_transaction_models.TaxeType(name='УСН 6%', multiplicator=1.06, is_deletable=False).save(using=name)
    app_transaction_models.TaxeType(name='НДФЛ', multiplicator=1.13, is_deletable=False).save(using=name)
    app_transaction_models.TaxeType(name='Без налога', multiplicator=1).save(using=name)

    my = app_transaction_models.Subcontractor(
        name='Моя компания',
        is_deletable=False,
         )
    my.save(using=name)
    my.type = st
    my.taxe_type = tt
    my.legal_form = lf
    my.is_my_company = True
    my.save(using=name)

    app_setup_models.Unit(name='New Business').save(using=name)
    app_setup_models.Unit(name='Client Service').save(using=name)
    app_setup_models.Unit(name='Production').save(using=name)

    app_transaction_models.Purpose(name='Оплата', type=True, is_deletable=False).save(using=name)
    app_transaction_models.Purpose(name='Внутренний перевод', type=True, is_deletable=False).save(using=name)
    app_transaction_models.Purpose(name='Предоплата проекта', type=True).save(using=name)
    app_transaction_models.Purpose(name='Расход', type=False, is_deletable=False).save(using=name)
    app_transaction_models.Purpose(name='Внутренний перевод', type=False, is_deletable=False).save(using=name)
    app_transaction_models.Purpose(name='Проектирование', type=False).save(using=name)
    app_transaction_models.Purpose(name='Дизайн', type=False).save(using=name)
    app_transaction_models.Purpose(name='Разработка', type=False).save(using=name)




    a = app_transaction_models.Currency(name='Rub', is_deletable=False, is_default=True)
    a.save(using=name)
    b = app_transaction_models.Currency(name='USD', is_default=False)
    b.save(using=name)

    c = app_transaction_models.DefaultRate(rate=Decimal("2.5"))
    c.save(using=name)
    c.target = a
    c.source = b
    c.save(using=name)


    b = app_transaction_models.Account(name='Мой банк',type=0, is_deletable=False)
    b.save(using=name)
    b.currency = a
    b.company_owner = my
    b.save(using=name)

    c = app_transaction_models.Account(name='Моя касса', type=1)
    c.save(using=name)
    c.currency = a
    c.company_owner = my
    c.save(using=name)

    app_project_models.ProjectCategory(name='Проекты', is_deletable=False).save(using=name)

    company = app_project_models.ProjectCategory(name='Компания', is_deletable=False)
    company.save(using=name)

    subcat = app_project_models.ProjectSubcategory(name='АХО')
    subcat.save(using=name)
    subcat.category = company
    subcat.save(using=name)

    subcat = app_project_models.ProjectSubcategory(name='Коммуникация')
    subcat.save(using=name)
    subcat.category = company
    subcat.save(using=name)

    subcat = app_project_models.ProjectSubcategory(name='Налоги')
    subcat.save(using=name)
    subcat.category = company
    subcat.save(using=name)

    subcat = app_project_models.ProjectSubcategory(name='Аренда')
    subcat.save(using=name)
    subcat.category = company
    subcat.save(using=name)

    subcat = app_project_models.ProjectSubcategory(name='ПО')
    subcat.save(using=name)
    subcat.category = company
    subcat.save(using=name)

    subcat = app_project_models.ProjectSubcategory(name='ФОТ')
    subcat.save(using=name)
    subcat.category = company
    subcat.save(using=name)

    subcat = app_project_models.ProjectSubcategory(name='Маркетинг')
    subcat.save(using=name)
    subcat.category = company
    subcat.save(using=name)

    subcat = app_project_models.ProjectSubcategory(name='Юр. и фин. обеспечение')
    subcat.save(using=name)
    subcat.category = company
    subcat.save(using=name)

    subcat = app_project_models.ProjectSubcategory(name='HR&Найм')
    subcat.save(using=name)
    subcat.category = company
    subcat.save(using=name)

    cat = app_project_models.ProjectCategory(name='Технические операции', is_deletable=False)
    cat.save(using=name)

    proj = app_project_models.Project(name_short='Внутренние переводы', is_deletable=False)
    proj.save(using=name)
    numgen(proj)
    proj.category = cat
    proj.save(using=name)

    app_project_models.NBSource(name='Маректинг-активность').save(using=name)
    app_project_models.NBSource(name='Личные связи').save(using=name)

    app_project_models.NBFormat(name='Комплексный тендер').save(using=name)
    app_project_models.NBFormat(name='Ценовой тендер').save(using=name)
    app_project_models.NBFormat(name='Ценовой запрос').save(using=name)

    prodcat = app_project_models.ProductCategory(name='Разработка')
    prodcat.save(using=name)

    prod = app_project_models.ProjectProduct(name='Корпоративный сайт')
    prod.save(using=name)
    prod.type = prodcat
    prod.save(using=name)

    prod = app_project_models.ProjectProduct(name='Продуктовый сайт')
    prod.save(using=name)
    prod.type = prodcat
    prod.save(using=name)

    prod = app_project_models.ProjectProduct(name='Баннеры')
    prod.save(using=name)
    prod.type = prodcat
    prod.save(using=name)

    prodcat = app_project_models.ProductCategory(name='SMM')
    prodcat.save(using=name)

    prod = app_project_models.ProjectProduct(name='Ведение групп')
    prod.save(using=name)
    prod.type = prodcat
    prod.save(using=name)

    prod = app_project_models.ProjectProduct(name='Таргетированная реклама')
    prod.save(using=name)
    prod.type = prodcat
    prod.save(using=name)

    app_project_models.Brand(name='Бренд-клиент').save(using=name)

    pass


def demo_setup(name):

    today = datetime.today()

    lf = app_transaction_models.Legalform.objects.using(name).get(name='ООО')
    lf2 = app_transaction_models.Legalform.objects.using(name).get(name='Физ')
    st = app_transaction_models.SubcontractorType.objects.using(name).get(name='Подрядчик')
    st2 = app_transaction_models.SubcontractorType.objects.using(name).get(name='Клиент')
    st3 = app_transaction_models.SubcontractorType.objects.using(name).get(name='Сотрудник')
    st4 = app_transaction_models.SubcontractorType.objects.using(name).get(name='Моя компания')
    tt = app_transaction_models.TaxeType.objects.using(name).get(name='НДС')
    tt2 = app_transaction_models.TaxeType.objects.using(name).get(name='Без налога')


    sub = app_transaction_models.Subcontractor(name='Подрядчик')
    sub.save(using=name)
    sub.type = st
    sub.taxe_type = tt
    sub.legal_form = lf
    sub.save(using=name)

    my = app_transaction_models.Subcontractor(name='Моя компания 2')
    my.save(using=name)
    my.type = st4
    my.taxe_type = tt
    my.legal_form = lf
    my.is_my_company = True
    my.save(using=name)

    cur1 = app_transaction_models.Currency.objects.using(name).get(name='Rub', is_default=True)
    cur2 = app_transaction_models.Currency.objects.using(name).get(name='USD')


    rub = app_transaction_models.Account(name='Альфабанк Rub', type=0)
    rub.save(using=name)
    rub.currency = cur1
    rub.company_owner = my
    rub.save(using=name)

    usd = app_transaction_models.Account(name='Альфабанк USD', type=0)
    usd.save(using=name)
    usd.currency = cur2
    usd.company_owner = my
    usd.save(using=name)


    cat = app_project_models.ProjectCategory.objects.using(name).get(name='Проекты')


    testproj = app_project_models.Project(name_short='Разработка сайта', is_deletable=False)
    testproj.save(using=name)
    numgen_p(testproj)
    testproj.category = cat
    testproj.save(using=name)


    proj2 = app_project_models.Project(name_short='Продуктовый баннер', is_deletable=False)
    proj2.save(using=name)
    numgen_p(proj2)
    proj2.category = cat
    proj2.save(using=name)






    mydate = today
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('5.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)




    mydate = today
    t_src = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('500.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t_src.save(using=name)
    numgen_t(t_src)
    t_src.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_src.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_src.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_src.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_src.account = rub
    t_src.save(using=name)

    mydate =today
    t_rec = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('100.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t_rec.save(using=name)
    numgen_t(t_rec)
    t_rec.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_rec.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_rec.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_rec.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_rec.account = usd
    t_rec.save(using=name)

    t_src.is_transfer = True
    t_src.transfer = t_rec.pk
    t_src.save(using=name)
    t_rec.is_transfer = True
    t_rec.transfer = t_src.pk
    t_rec.save(using=name)





    mydate = today - timedelta(days=1)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('30.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate = today - timedelta(days=1)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('10.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)







    mydate = today
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('15.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)







    mydate = today + timedelta(days=2)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('10.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)





    mydate = today + timedelta(days=3)
    t_src = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('700.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t_src.save(using=name)
    numgen_t(t_src)
    t_src.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_src.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_src.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_src.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_src.account = rub
    t_src.save(using=name)

    mydate = today + timedelta(days=2)
    t_rec = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('200.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t_rec.save(using=name)
    numgen_t(t_rec)
    t_rec.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_rec.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_rec.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_rec.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_rec.account = usd
    t_rec.save(using=name)

    t_src.is_transfer = True
    t_src.transfer = t_rec.pk
    t_src.save(using=name)
    t_rec.is_transfer = True
    t_rec.transfer = t_src.pk
    t_rec.save(using=name)





    mydate = today + timedelta(days=3)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('600.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = rub
    t.save(using=name)






    mydate = today + timedelta(days=4)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('15.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate = today + timedelta(days=5)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('500.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = rub
    t.save(using=name)






    mydate = today + timedelta(days=6)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('90.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate = today + timedelta(days=6)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('960.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate = today - timedelta(days=6)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('20.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate = today - timedelta(days=7)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('35.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)





    mydate = today - timedelta(days=5)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('500.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = rub
    t.save(using=name)







    mydate = today + timedelta(days=8)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('1500.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = rub
    t.save(using=name)







    mydate = today + timedelta(days=9)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('200.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = rub
    t.save(using=name)







    mydate = today + timedelta(days=10)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('30.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate = today + timedelta(days=11)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('200.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate = today + timedelta(days=12)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('100.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate = today + timedelta(days=14)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('60.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate = today + timedelta(days=15)
    t_src = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('140.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t_src.save(using=name)
    numgen_t(t_src)
    t_src.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_src.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_src.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_src.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_src.account = usd
    t_src.save(using=name)

    mydate = today + timedelta(days=16)
    t_rec = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('420.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t_rec.save(using=name)
    numgen_t(t_rec)
    t_rec.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_rec.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_rec.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_rec.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_rec.account = rub
    t_rec.save(using=name)

    t_src.is_transfer = True
    t_src.transfer = t_rec.pk
    t_src.save(using=name)
    t_rec.is_transfer = True
    t_rec.transfer = t_src.pk
    t_rec.save(using=name)


















    mydate = today + timedelta(days=18)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('75.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)









    mydate = today + timedelta(days=19)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('50.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate = today + timedelta(days=20)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('670.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)







    mydate = today + timedelta(days=21)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('5.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)





    mydate = today + timedelta(days=22)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('50.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)









    mydate = today + timedelta(days=23)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('50.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)













    mydate = today + timedelta(days=24)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('20.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)













    mydate = today + timedelta(days=25)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('5.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)

    pass