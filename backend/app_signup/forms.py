from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm, Textarea
from .models import *
from django.utils.translation import gettext_lazy as _
from app_signup import models as app_signup_models

class SignUpForm(UserCreationForm):


    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2',
        )


class JoinSignUpForm(UserCreationForm):

    class Meta:
        model = User
        fields = (
            'password1',
            'password2',
        )

class AccountCreationForm(ModelForm):


    error_messages = {
            'duplicate_subdomain': _("Субдомен уже занят"),
    }

    class Meta:
        model = Account
        fields = (
            'name',
            'subdomain',
        )

        labels = {
            'name': _('Название компании'),
            'subdomain': _('Субдомен компании'),
        }
        help_texts = {
            'name': _('Укажите название компании'),
            'subdomain': _('Укажите субдомен для отображения колмпании'),
        }


    def clean_subdomain(self):
        subdomain = self.cleaned_data["subdomain"]

        try:
            Account._default_manager.get(subdomain=subdomain)
            raise forms.ValidationError(
                self.error_messages['duplicate_subdomain'],
                code='duplicate_subdomain',
            )
        except Account.DoesNotExist:
            return subdomain


class WordingTextForm(ModelForm):
    text = forms.CharField( widget=forms.Textarea )



    class Meta:
        model = app_signup_models.Wording
        fields = ('__all__')



class SignUpFormEng(UserCreationForm):


    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2',
        )
        labels = {
            'first_name': _('First name'),
            'last_name': _('Last name'),
            'email': _('Email'),
            'password1': _('Password'),
            'password2': _('Repeat your password '),
        }


class AccountCreationFormEng(ModelForm):


    error_messages = {
            'duplicate_subdomain': _("Субдомен уже занят"),
    }

    class Meta:
        model = Account
        fields = (
            'name',
            'subdomain',
        )

        labels = {
            'name': _('Company name'),
            'subdomain': _('Choose your subdomain'),
        }
        help_texts = {
            'name': _('Укажите название компании'),
            'subdomain': _('Укажите субдомен для отображения колмпании'),
        }


    def clean_subdomain(self):
        subdomain = self.cleaned_data["subdomain"]

        try:
            Account._default_manager.get(subdomain=subdomain)
            raise forms.ValidationError(
                self.error_messages['duplicate_subdomain'],
                code='duplicate_subdomain',
            )
        except Account.DoesNotExist:
            return subdomain