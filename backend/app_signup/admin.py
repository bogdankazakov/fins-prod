from django.contrib import admin
from app_signup import models as app_signup_models
from itertools import chain
from app_signup import forms








class WordingAdmin(admin.ModelAdmin):
    list_display = ('code', 'type', 'section', 'subsection',)
    form = forms.WordingTextForm

class AccountAdmin(admin.ModelAdmin):
    list_display = (
        'pk',
        'name',
        'subdomain',
        'created_at',
        'modified_at',
        'is_active',
        'is_free',
        'is_mine',
    )

    list_filter = (
        'is_active',
        'is_free',
        'is_mine',
    )

    search_fields = (
        'name',
        'subdomain',

    )

class UserAdmin(admin.ModelAdmin):
    list_display = (
        'pk',
        'first_name',
        'last_name',
        'email',
        'created_at',
        'is_active',

    )

    list_filter = (
        'is_active',

    )

    search_fields = (
        'first_name',
        'last_name',
        'email',
        'account',

    )

    def has_delete_permission(self, request, obj=None):
        return False

class OwnerAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'account',
    )
    search_fields = (
        'user',
        'account',

    )


class EmailsAdmin(admin.ModelAdmin):
    list_display = (
        'account',
        'welcome_is_sent',
        'welcome_date',
        'trial_ends_10_is_sent',
        'trial_ends_10_date',
        'trial_ends_1_is_sent',
        'trial_ends_1_date',
        'account_blocked_is_sent',
        'account_blocked_date',
        'account_unblocked_is_sent',
        'account_unblocked_date',
    )
    search_fields = (
        'account',

    )
admin.site.register(app_signup_models.Account, AccountAdmin)
admin.site.register(app_signup_models.User, UserAdmin)
admin.site.register(app_signup_models.Owner, OwnerAdmin)
admin.site.register(app_signup_models.Emails, EmailsAdmin)
admin.site.register(app_signup_models.Billing)
admin.site.register(app_signup_models.WordingSection)
admin.site.register(app_signup_models.WordingSubsection)
admin.site.register(app_signup_models.WordingType)
admin.site.register(app_signup_models.Wording, WordingAdmin)

