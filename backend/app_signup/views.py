from django.contrib.auth import login, authenticate, logout
from django.shortcuts import render, redirect
from subdomains.utils import reverse as sub_reverse
from .forms import *
from datetime import datetime
import pytz
from django.contrib import messages
import logging
from .models import *
from app_setup import models as app_setup_models
from django.contrib.auth.decorators import login_required
from app_auth.decorators import access_company_level
from django.http import JsonResponse
from django.template.loader import render_to_string
from app_signup.tools import access_default
from django.contrib.sites.shortcuts import get_current_site
from django.db import transaction


logger = logging.getLogger(__name__)



def signup(request):


    if request.method == 'POST':
        user_form = SignUpForm(request.POST)
        account_form = AccountCreationForm(request.POST)

        if account_form.is_valid() and user_form.is_valid():
            account_instance = account_form.save()
            account_instance.create_database()
            account_instance.fill_database()

            noerror = True
            if len(User.objects.filter(email=user_form.data['email'])) != 0:
                instance = User.objects.get(email=user_form.data['email'])
                instance.created_account_pk = account_instance.pk
                # email = instance.email
                # raw_password = instance.password
                try:
                    with transaction.atomic():
                        app_setup_models.DBUser.objects.create(user_id=instance.pk)
                except Exception:
                    if instance.is_superuser:
                        pass
                    else:
                        db = 'account_' + str(instance.created_account_pk)
                        with transaction.atomic():
                            app_setup_models.DBUser.objects.using(db).create(user_id=instance.pk)

            else:
                if user_form.is_valid():
                    instance = user_form.save(commit=False)
                    instance.created_account_pk = account_instance.pk
                    # email = user_form.cleaned_data.get('email')
                    # raw_password = user_form.cleaned_data.get('password1')
                else:
                    noerror = False

            if noerror:

                instance.save()
                instance.account.add(account_instance)
                dbuser = app_setup_models.DBUser.objects.using('account_' + str(account_instance.pk)).get(user_id=instance.id)
                dbuser.is_owner = True
                dbuser.is_joined = True
                dbuser.joined_at = pytz.utc.localize(datetime.utcnow())
                access_default(dbuser)
                dbuser.save(using=('account_' + str(account_instance.pk)))

                owner_info = app_signup_models.Owner(user= instance,account=account_instance)
                owner_info.save()


                # user = authenticate(username=email, password=raw_password)
                # login(request, user)
                login(request, instance)

                subject = 'Добро пожаловать в FinS'
                message = render_to_string('app_signup/emails/welcome_email.html', {
                    'user': instance,
                    'domain': get_current_site(request).domain,
                })
                instance.email_user(subject, message)
                email_info = app_signup_models.Emails(account=account_instance, welcome_is_sent=True, welcome_date = pytz.utc.localize(datetime.utcnow()))
                email_info.save()

                return redirect(sub_reverse(settings.LOGIN_REDIRECT_URL, subdomain=account_instance.subdomain, scheme='https'))
            else:
                messages.error(request, 'Ошибка валидации формы')
        else:
            messages.error(request, 'Ошибка валидации формы')
    else:
        if request.user.is_authenticated:
            logout(request)
        user_form = SignUpForm()
        account_form = AccountCreationForm()

    return render(request, 'app_signup/signup.html',
                {
                'user_form': user_form,
                'account_form': account_form,
                })



@login_required
@access_company_level
def modal_help(request):
    data = {}
    raw_code = request.GET.get('code')
    if raw_code:
        code = raw_code.split('_')[1]
        text = app_signup_models.Wording.objects.get(code=code).text
    else:
        text = 'Ошибка'
    context = {'text': text,}
    template = 'app_signup/help_modal.html'
    data['html'] = render_to_string(template, context)
    return JsonResponse(data)



def signup_en(request):


    if request.method == 'POST':
        user_form = SignUpFormEng(request.POST)
        account_form = AccountCreationFormEng(request.POST)
        if account_form.is_valid():
            account_instance = account_form.save()
            account_instance.create_database()
            account_instance.fill_database()

            noerror = True
            if len(User.objects.filter(email=user_form.data['email'])) != 0:
                instance = User.objects.get(email=user_form.data['email'])
                instance.created_account_pk = account_instance.pk
                # email = instance.email
                # raw_password = instance.password
                try:
                    with transaction.atomic():
                        app_setup_models.DBUser.objects.create(user_id=instance.pk)
                except Exception:
                    if instance.is_superuser:
                        pass
                    else:
                        db = 'account_' + str(instance.created_account_pk)
                        with transaction.atomic():
                            app_setup_models.DBUser.objects.using(db).create(user_id=instance.pk)

            else:
                if user_form.is_valid():
                    instance = user_form.save(commit=False)
                    instance.created_account_pk = account_instance.pk
                    # email = user_form.cleaned_data.get('email')
                    # raw_password = user_form.cleaned_data.get('password1')
                else:
                    noerror = False

            if noerror:

                instance.save()
                instance.account.add(account_instance)
                dbuser = app_setup_models.DBUser.objects.using('account_' + str(account_instance.pk)).get(user_id=instance.id)
                dbuser.is_owner = True
                dbuser.is_joined = True
                dbuser.joined_at = pytz.utc.localize(datetime.utcnow())
                access_default(dbuser)
                dbuser.save(using=('account_' + str(account_instance.pk)))

                owner_info = app_signup_models.Owner(user= instance,account=account_instance)
                owner_info.save()


                # user = authenticate(username=email, password=raw_password)
                # login(request, user)
                login(request, instance)

                subject = 'Добро пожаловать в FinS'
                message = render_to_string('app_signup/emails/welcome_email.html', {
                    'user': instance,
                    'domain': get_current_site(request).domain,
                })
                instance.email_user(subject, message)
                email_info = app_signup_models.Emails(account=account_instance, welcome_is_sent=True, welcome_date = pytz.utc.localize(datetime.utcnow()))
                email_info.save()

                return redirect(sub_reverse(settings.LOGIN_REDIRECT_URL, subdomain=account_instance.subdomain, scheme='https'))
            else:
                messages.error(request, 'Ошибка валидации формы')
        else:
            messages.error(request, 'Ошибка валидации формы')
    else:
        if request.user.is_authenticated:
            logout(request)
        user_form = SignUpFormEng()
        account_form = AccountCreationFormEng()

    return render(request, 'app_signup/signup_en.html',
                {
                'user_form': user_form,
                'account_form': account_form,
                })
