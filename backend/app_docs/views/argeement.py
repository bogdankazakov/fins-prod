from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from app_auth.decorators import access_company_level
from app_docs.decorators import *
from app_docs.forms import *
from django.urls import reverse_lazy
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404, redirect, render
from app_setup.tools import log_data
from app_docs import models as app_docs_models
from django.http import JsonResponse
from django.contrib import messages
from fins.global_functions import history_action_verbose, history_record
from app_signup import models as app_signup_models
import logging
from app_docs.tools import *
import os
from django.core.files.storage import default_storage
from fins.global_functions import get_db_name
import pytz
from datetime import datetime
from app_docs.filters import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework.decorators import api_view
from rest_framework.response import Response
import math
from app_docs.serializers import *
from rest_framework import status
from rest_framework.views import APIView


logger = logging.getLogger(__name__)

# ===SETUP====

def get_all_agr():
    qs = app_docs_models.Agreement.objects.all()
    return qs

def get_all_ad():
    qs = app_docs_models.AccountingDocs.objects.all()
    return qs

def get_item_agr(pk):
    item = get_object_or_404(app_docs_models.Agreement, pk=pk)
    return item

def get_item_ad(pk):
    item = get_object_or_404(app_docs_models.AccountingDocs, pk=pk)
    return item

def rearranger(list, all_agr):
    """
    :param list:
    :param all_agr:
    :return: rearrange list so that childern are inside parents

    """
    if len(all_agr) == 0:
        return list

    else:
        if len(list) == 0:
            exclude_list_agr = []
            infinite_checker = []
            for agr in all_agr:
                if agr.parent is None:
                    agr.ad = agr.account_docs.all()
                    agr.idx = 0
                    list.append(agr)
                    exclude_list_agr.append(agr.pk)
                    infinite_checker = infinite_checker + exclude_list_agr

            all_agr = all_agr.exclude(pk__in=exclude_list_agr)

            if len(infinite_checker) == 0:
                stange_list = []
                for el in all_agr:
                    stange_list.append(el)
                list = list + stange_list
                return list

            return rearranger(list, all_agr)

        else:
            l = list.copy()
            index_shift = 1
            el_index = 0
            infinite_checker = []
            for el in l:
                index = el_index
                exclude_list_agr = []
                for agr in all_agr:
                    if agr.parent == el.pk:
                        index = index + 1
                        index_shift += 1
                        agr.ad = agr.account_docs.all()
                        agr.idx = el.idx + 1
                        list.insert(index, agr)
                        exclude_list_agr.append(agr.pk)
                        infinite_checker = infinite_checker + exclude_list_agr

                all_agr = all_agr.exclude(pk__in=exclude_list_agr)

                el_index = l.index(el) + index_shift

            if len(infinite_checker) == 0:
                stange_list = []
                for el in all_agr:
                    stange_list.append(el)

                list = list + stange_list
                return list

            return rearranger(list, all_agr)

# ===========


@login_required
@access_company_level
@access_app_agr_list
def agreement_index(request, get_access_all):
    context = {
        'obj': app_docs_models.Agreement,
    }
    return render(request, 'app_docs/new/index_agreement.html', context)


@api_view(['GET', 'POST'])
@login_required
@access_company_level
@access_app_agr_list
def agreement_list(request, get_access_all):
    if request.method == 'GET':
        offset = int(request.GET.get('offset'))
        page = int(request.GET.get('page'))
        filter_param = request.GET.get('filter')
        qs = get_access_all()
        qs = filterMain(qs, filter_param)
        all = rearranger([], qs)

        start = (page - 1) * offset
        end = page * offset
        items = all[start:end]
        total_pages = math.ceil(len(all) / offset)


        data = {
            'list': AgreementSerializer(items, many=True).data,
            'total_pages': total_pages,
        }
        return Response(data)

    elif request.method == 'POST':
        if 'parent' in request.data:
            parent_pk = request.data['parent']
            parent = get_item_agr(parent_pk)

            request.data.update({
                "mycompany": parent.mycompany.pk,
                "subcontractor": parent.subcontractor.pk,
                "subcontractor_role": parent.subcontractor_role,
            })

        serializer = AgreementSerializer(data=request.data)

        if serializer.is_valid():

            instance = serializer.save(
                created_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                status_date = pytz.utc.localize(datetime.utcnow()).date(),
            )
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET', 'PUT', 'DELETE'])
def agreement_detail(request, pk):
    """
    Retrieve, update or delete a items.
    """

    try:
        item = get_item_agr(pk)
    except app_docs_models.Agreement.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = AgreementSerializer(item)
        deletable, related_obj = item.can_delete()
        new_serializer = {'deletable': deletable}
        new_serializer.update(serializer.data)
        return Response(new_serializer)

    elif request.method == 'PUT':
        serializer = AgreementSerializer(item, data=request.data)
        if serializer.is_valid(raise_exception=True):

            #edit children if master parent changed
            if item.parent is None:
                if item.mycompany.pk != int(request.data['mycompany']) or \
                        item.subcontractor_role != int(request.data['subcontractor_role']) or \
                        item.subcontractor.pk != int(request.data['subcontractor']):
                    parents_list = [item, ]
                    all = app_docs_models.Agreement.objects.all()
                    list = child_finder(all, parents_list, False)
                    list.pop(0)
                    for child in list:
                        child.mycompany = app_transaction_models.Subcontractor.objects.get(pk=int(request.data['mycompany']))
                        child.subcontractor_role = int(request.data['subcontractor_role'])
                        child.subcontractor = app_transaction_models.Subcontractor.objects.get(pk=int(request.data['subcontractor']))
                        child.save()



            if int(request.data['status']) != item.status:
                serializer.save(
                    modified_at=pytz.utc.localize(datetime.utcnow()),
                    modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                    status_date=pytz.utc.localize(datetime.utcnow()).date(),
                )
            else:
                serializer.save(
                    modified_at=pytz.utc.localize(datetime.utcnow()),
                    modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
                )


            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)




@api_view(['GET', ])
@login_required
@access_company_level
@access_app_docs_section
def agreement_extra(request):

    if request.method == 'GET':

        type = []
        for el in app_docs_models.Agreement.TYPE_CHOICES:
            type.append({
                'id': el[0],
                'name': el[1],
            })

        status_l = []
        for el in app_docs_models.Agreement.STATUS_CHOICES:
            status_l.append({
                'id': el[0],
                'name': el[1],
            })

        sub_role = []
        for el in app_docs_models.Agreement.ROLE_CHOICES:
            sub_role.append({
                'id': el[0],
                'name': el[1],
            })

        prolongation_type = []
        for el in app_docs_models.Agreement.PROLONGATION_TYPE_CHOICES:
            prolongation_type.append({
                'id': el[0],
                'name': el[1],
            })
        filetypes = []
        for el in app_docs_models.AgreementFile.FILE_TYPE_CHOICES:
            filetypes.append({
                'id': el[0],
                'name': el[1],
            })



        data = {
            'extra': {
                'type':type,
                'status':status_l,
                'subcontractors': SubcontractorSerializer(Subcontractor.objects.all(), many=True).data,
                'sub_role': sub_role,
                'mycompany': SubcontractorSerializer(Subcontractor.objects.filter(is_my_company=True), many=True).data,
                'prolongation_type': prolongation_type,
                'projects': ProjectSerializer(Project.objects.all(), many=True).data,
                'transactions': TransactionSerializer(Transaction.objects.all(), many=True).data,
                'reports': ReportSerializer(Report.objects.all(), many=True).data,
                'template': DocTemplateSerializer(DocTemplateFile.objects.all(), many=True).data,
                'filetypes': filetypes,

            },
        }

        return Response(data)



@api_view(['GET', ])
@login_required
@access_company_level
@access_app_docs_section
def agreement_history(request):

    if request.method == 'GET':
        history = app_docs_models.Agreement.history.all()[:3]
        records = history_record(history, 'Документ', True)
        data = {
            'records': records,
        }

        return Response(data)




# class UserUploadedPicture(APIView):
#     parser_classes = (MultiPartParser, )
#
#     def post(self, request, format=None):
#         print(request.data)
#         print("\n\n\n")
#         serializer = PictureSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data)
#         return JsonResponse(serializer.errors, status=400)


@api_view(['GET','PUT','POST', 'DELETE' ])
@login_required
@access_company_level
@access_app_docs_section
def agreement_files(request, pk):
    if request.method == 'GET':
        data = AgreementFileSerializer(app_docs_models.AgreementFile.objects.filter(agreement__pk=pk), many=True).data
        return Response(data)
    if request.method == 'POST':

        if len(request.data)!= 0:
            dbname = 'account_' + str(request.account.pk)
            user = app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            agreement = app_docs_models.Agreement.objects.get(pk=pk)
            try:
                l = len(request.data)
                i=0
                while i < (l/2):
                    app_docs_models.AgreementFile.objects.create(
                        file=request.data['file['+str(i)+']'],
                        type=request.data['file_type['+str(i)+']'],
                        agreement=agreement,
                        uploaded_by=user,
                        dbname=dbname
                    )
                    i+=1
                return Response(status=status.HTTP_200_OK)
            except Exception:
                return Response(status=status.HTTP_400_BAD_REQUEST)
    if request.method == 'PUT':
        if len(request.data) != 0:
            noerror = True
            for filedata in request.data:
                item = app_docs_models.AgreementFile.objects.get(pk=filedata['id'])
                serializer = AgreementFileSerializer(item, data=filedata)
                if serializer.is_valid(raise_exception=True):
                    serializer.save()
                else:
                    noerror = False

            if noerror:
                return Response(status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    if request.method == 'DELETE':
        if len(request.data) != 0:
            try:
                for filedata in request.data:
                    if 'id'in filedata:
                        item = app_docs_models.AgreementFile.objects.get(pk=filedata['id'])
                        item.delete()
                        os.remove(os.path.join(settings.MEDIA_ROOT, str(item.file)))
                return Response(status=status.HTTP_200_OK)
            except Exception:
                return Response(status=status.HTTP_400_BAD_REQUEST)

