from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from app_auth.decorators import access_company_level
from app_docs.decorators import *
from app_docs.forms import *
from django.urls import reverse_lazy
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404, redirect, render
from app_setup.tools import log_data
from app_docs import models as app_docs_models
from django.http import JsonResponse
from django.contrib import messages
from fins.global_functions import history_action_verbose, history_record
from app_signup import models as app_signup_models
import logging
from app_docs.tools import *
import os
from django.core.files.storage import default_storage
from fins.global_functions import get_db_name
import pytz
from datetime import datetime
from app_docs.filters import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from rest_framework.decorators import api_view
from rest_framework.response import Response
import math
from app_docs.serializers import *
from rest_framework import status

logger = logging.getLogger(__name__)

# ===SETUP====



def get_all_agr():
    qs = app_docs_models.Agreement.objects.all()
    return qs

def get_all_ad():
    qs = app_docs_models.AccountingDocs.objects.all()
    return qs

def get_item_agr(pk):
    item = get_object_or_404(app_docs_models.Agreement, pk=pk)
    return item

def get_item_ad(pk):
    item = get_object_or_404(app_docs_models.AccountingDocs, pk=pk)
    return item




@login_required
@access_company_level
@access_app_agr_list
def accounting_index(request, get_access_all):
    context = {
        'obj': app_docs_models.AccountingDocs,
    }
    return render(request, 'app_docs/new/index_accounting.html', context)


@api_view(['GET', 'POST'])
@login_required
@access_company_level
def accounting_list(request):
    if request.method == 'GET':
        offset = int(request.GET.get('offset'))
        page = int(request.GET.get('page'))
        filter_param = request.GET.get('filter')
        qs = AccountingDocs.objects.all()
        all = filterAccounting(qs, filter_param)

        start = (page - 1) * offset
        end = page * offset
        items = all[start:end]
        total_pages = math.ceil(len(all) / offset)


        data = {
            'list': AccountingSerializer(items, many=True).data,
            'total_pages': total_pages,
        }
        return Response(data)

    elif request.method == 'POST':

        if 'agreement' in request.data:
            agreement_id = request.data['agreement']
            agreement = app_docs_models.Agreement.objects.get(pk=agreement_id)
            request.data['subcontractor']= agreement.subcontractor.pk
            request.data['mycompany']= agreement.mycompany.pk

        serializer = AccountingSerializer(data=request.data)

        if serializer.is_valid():
            instance = serializer.save(
                created_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                status_date = pytz.utc.localize(datetime.utcnow()).date(),
            )
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET', 'PUT', 'DELETE'])
def accounting_detail(request, pk):
    """
    Retrieve, update or delete a items.
    """
    try:
        item = AccountingDocs.objects.get(pk=pk)
    except app_docs_models.AccountingDocs.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = AccountingSerializer(item)
        deletable, related_obj = item.can_delete()
        new_serializer = {'deletable': deletable}
        new_serializer.update(serializer.data)
        return Response(new_serializer)

    elif request.method == 'PUT':
        if 'agreement' in request.data and request.data['agreement'] is not None:
            agreement_id = request.data['agreement']
            agreement = app_docs_models.Agreement.objects.get(pk=agreement_id)
            request.data['subcontractor']= agreement.subcontractor.pk
            request.data['mycompany']= agreement.mycompany.pk

        serializer = AccountingSerializer(item, data=request.data)
        if serializer.is_valid(raise_exception=True):

            if int(request.data['status']) != item.status:
                serializer.save(
                    modified_at=pytz.utc.localize(datetime.utcnow()),
                    modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                    status_date=pytz.utc.localize(datetime.utcnow()).date(),
                )
            else:
                serializer.save(
                    modified_at=pytz.utc.localize(datetime.utcnow()),
                    modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
                )


            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)












# ===========
@api_view(['GET', ])
@login_required
@access_company_level
@access_app_docs_section
def accounting_extra(request):

    if request.method == 'GET':

        type = []
        for el in app_docs_models.AccountingDocs.TYPE_CHOICES:
            type.append({
                'id': el[0],
                'name': el[1],
            })

        status_l = []
        for el in app_docs_models.AccountingDocs.STATUS_CHOICES:
            status_l.append({
                'id': el[0],
                'name': el[1],
            })

        filetypes = []
        for el in app_docs_models.ADFile.FILE_TYPE_CHOICES:
            filetypes.append({
                'id': el[0],
                'name': el[1],
            })

        data = {
            'extra': {
                'type':type,
                'status':status_l,
                'subcontractors': SubcontractorSerializer(Subcontractor.objects.all(), many=True).data,
                'mycompany': SubcontractorSerializer(Subcontractor.objects.filter(is_my_company=True), many=True).data,
                'agreements': AgreementShortSerializer(Agreement.objects.all(), many=True).data,
                'filetypes': filetypes,
                'projects': ProjectSerializer(Project.objects.all(), many=True).data,
                'transactions': TransactionSerializer(Transaction.objects.all(), many=True).data,
                'reports': ReportSerializer(Report.objects.all(), many=True).data,
            },
        }

        return Response(data)


# ===========
@api_view(['GET', ])
@login_required
@access_company_level
@access_app_docs_section
def accounting_extra_agreement(request):
        id = request.GET.get('id')
        data = AgreementSerializer(app_docs_models.Agreement.objects.get(pk=id)).data
        return Response(data)

# ===========
@api_view(['GET', ])
@login_required
@access_company_level
@access_app_docs_section
def accounting_history(request):

    if request.method == 'GET':
        history = app_docs_models.AccountingDocs.history.all()[:3]
        records = history_record(history, 'Документ', True)
        data = {
            'records': records,
        }

        return Response(data)


@api_view(['GET','PUT','POST', 'DELETE' ])
@login_required
@access_company_level
@access_app_docs_section
def accounting_files(request, pk):
    if request.method == 'GET':
        data = AccountingFileSerializer(app_docs_models.ADFile.objects.filter(ad__pk=pk), many=True).data
        return Response(data)
    if request.method == 'POST':

        if len(request.data)!= 0:
            dbname = 'account_' + str(request.account.pk)
            user = app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            accounting_doc = app_docs_models.AccountingDocs.objects.get(pk=pk)
            try:
                l = len(request.data)
                i=0
                while i < (l/2):
                    app_docs_models.ADFile.objects.create(
                        file=request.data['file['+str(i)+']'],
                        type=request.data['file_type['+str(i)+']'],
                        ad=accounting_doc,
                        uploaded_by=user,
                        dbname=dbname
                    )
                    i+=1
                return Response(status=status.HTTP_200_OK)
            except Exception:
                return Response(status=status.HTTP_400_BAD_REQUEST)
    if request.method == 'PUT':
        if len(request.data) != 0:
            noerror = True
            for filedata in request.data:
                item = app_docs_models.ADFile.objects.get(pk=filedata['id'])
                serializer = AccountingFileSerializer(item, data=filedata)
                if serializer.is_valid(raise_exception=True):
                    serializer.save()
                else:
                    noerror = False

            if noerror:
                return Response(status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    if request.method == 'DELETE':
        if len(request.data) != 0:
            try:
                for filedata in request.data:
                    if 'id'in filedata:
                        item = app_docs_models.ADFile.objects.get(pk=filedata['id'])
                        item.delete()
                        os.remove(os.path.join(settings.MEDIA_ROOT, str(item.file)))
                return Response(status=status.HTTP_200_OK)
            except Exception:
                return Response(status=status.HTTP_400_BAD_REQUEST)
