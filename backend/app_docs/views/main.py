from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from app_auth.decorators import access_company_level
from app_docs.decorators import *
from app_docs.forms import *
from django.urls import reverse_lazy
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404, redirect, render
from app_setup.tools import log_data
from app_docs import models as app_docs_models
from django.http import JsonResponse
from django.contrib import messages
from fins.global_functions import history_action_verbose, history_record
from app_signup import models as app_signup_models
import logging
from app_docs.tools import *
import os
from django.core.files.storage import default_storage
from fins.global_functions import get_db_name
import pytz
from datetime import datetime
from app_docs.filters import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


logger = logging.getLogger(__name__)

# ===SETUP====


wording = {
    'btn_create': 'Создать',
    'btn_add_agr': 'Добавить договор',
    'btn_add_ad': 'Добавить первичку',
    'btn_delete': 'Удалить',
    'btn_cancel': 'Отмена',
    'btn_update': 'Редактировать',
    'btn_save': 'Сохранить',
    'btn_history': 'История',
    'btn_gen': 'Cгенерировать документ',
    'h1_list': 'Список документов',
    'h1_create_agr': 'Создать договор',
    'h1_update_agr': 'Редактировать договор',
    'h1_delete_agr': 'Удалить договор',
    'h1_create_ad': 'Создать первичную документацию',
    'h1_update_ad': 'Редактировать первичную документацию',
    'h1_delete_ad': 'Удалить первичную документацию',
    'h1_history': 'История изменений документов',
    'validation_error': 'Ошибка валидации',
    'not_deletable': 'Мы не можем удалить элемент, так как на него ссылаются либо транзакции, либо дочерние документы, либо к нему приложены файлы. Сначала удалите их.',
    'approve_delete': ' Вы уверены, что хотите удалить документ ',
    'empty_str': '———',
    'none': '———',
}
templates_agr = {
    'create': 'app_docs/agreements/partial_create.html',
    'update': 'app_docs/agreements/partial_update.html',
    'delete': 'app_docs/agreements/partial_delete.html',
    'history': 'app_docs/agreements/partial_history.html',
}
templates_ad = {
    'create': 'app_docs/ad/partial_create.html',
    'update': 'app_docs/ad/partial_update.html',
    'delete': 'app_docs/ad/partial_delete.html',
    'history': 'app_docs/ad/partial_history.html',
}
templates_global = {
    'list': 'app_docs/global/list.html',
    'partial_list': 'app_docs/global/partial_list.html',
    'history': 'app_docs/global/partial_history.html',
}

js = {
    'jscreate_agr': 'js-create-agreement',
    'jscreate_ad': 'js-create-ad',
    'jshistory': 'js-history-agreement',

}
url = {
    'create_agr': reverse_lazy('app_docs:agreement_create'),
    'create_ad': reverse_lazy('app_docs:accounting_docs_create'),
    'history': reverse_lazy('app_docs:docs_history'),
}
def get_all_agr():
    qs = app_docs_models.Agreement.objects.all()
    return qs

def get_all_ad():
    qs = app_docs_models.AccountingDocs.objects.all()
    return qs

def get_item_agr(pk):
    item = get_object_or_404(app_docs_models.Agreement, pk=pk)
    return item

def get_item_ad(pk):
    item = get_object_or_404(app_docs_models.AccountingDocs, pk=pk)
    return item

def lister(list, all_agr):

        if len(all_agr) == 0:
            return list

        else:
            if len(list) == 0:
                exclude_list_agr = []
                infinite_checker = []
                for agr in all_agr:
                    if agr.parent is None:
                        agr.ad = agr.account_docs.all()
                        agr.idx = 0
                        list.append(agr)
                        exclude_list_agr.append(agr.pk)
                        infinite_checker = infinite_checker + exclude_list_agr

                all_agr = all_agr.exclude(pk__in=exclude_list_agr)

                if len(infinite_checker) == 0:
                    stange_list = []
                    for el in all_agr:
                        stange_list.append(el)
                    list = list + stange_list
                    return list

                return lister(list, all_agr)

            else:
                l = list.copy()
                index_shift = 1
                el_index = 0
                infinite_checker = []
                for el in l:
                    index = el_index
                    exclude_list_agr = []
                    for agr in all_agr:
                        if agr.parent == el.pk:
                            index = index + 1
                            index_shift += 1
                            agr.ad = agr.account_docs.all()
                            agr.idx = 1
                            list.insert(index, agr)
                            exclude_list_agr.append(agr.pk)
                            infinite_checker = infinite_checker + exclude_list_agr

                    all_agr = all_agr.exclude(pk__in=exclude_list_agr)

                    el_index = l.index(el) + index_shift

                if len(infinite_checker) == 0:
                    stange_list = []
                    for el in all_agr:
                        stange_list.append(el)

                    list = list + stange_list
                    return list

                return lister(list, all_agr)



def filter_data(request, page, get_access_all):
    filter = DocsFilter(request.GET, queryset=get_access_all())


    obj = app_docs_models.Agreement

    if page is not None:
        request.GET = request.GET.copy()
        request.GET['page'] = page
    new_qs = lister([], filter.qs)
    new_qs = filter_extra_data(new_qs)


    page = request.GET.get('page', 1)
    paginator = Paginator(new_qs, 30)
    try:
        results = paginator.page(page)
    except PageNotAnInteger:
        results = paginator.page(1)
    except EmptyPage:
        results = paginator.page(paginator.num_pages)

    return filter, obj, results


# ===========


@login_required
@access_company_level
@access_app_agr_list
def docs_list(request, get_access_all):
    filter, obj, results = filter_data(request, None, get_access_all)

    context = {
        'list': results,
        'list_num': range(1, int(results.paginator.num_pages) + 1),
        'wording': wording,
        'templates': templates_agr,
        'js': js,
        'url': url,
        'obj': obj,
        'filter': filter,
    }




    return render(request, templates_global['list'], context)


# ===========


@login_required
@access_company_level
@access_app_docs_section
def agreement_create(request):

    files = app_docs_models.AgreementFile.objects.none()
    if request.method == 'POST':
        form = AgreementForm(request.POST, using=get_db_name(request))
        formset_file = AgreementFileFormSet(request.POST, request.FILES)
    else:
        form = AgreementForm(using=get_db_name(request))
        formset_file = AgreementFileFormSet(queryset=files)

    return agreement_save(request, form, formset_file, templates_agr['create'], files)


@login_required
@access_company_level
@access_app_agr_list
def agreement_save(request, get_access_all, form, formset_file, template_name, files):
    data = dict()
    formset_file_enrich(formset_file, files)
    if request.method == 'POST':

        if form.is_valid() and formset_file.is_valid():

            instance = form.save(commit=False)
            db_user = log_data(instance, request)
            if form.cleaned_data['parent'] is None:
                pass
            else:
                parent = get_item_agr(form.cleaned_data['parent'])
                instance.subcontractor_role = parent.subcontractor_role
                instance.subcontractor = parent.subcontractor

            if 'status' in form.changed_data:
                instance.status_date = datetime.now(pytz.utc)

            instance.save()
            for form_file in formset_file:
                if len(form_file.cleaned_data) != 0:
                    if form_file.cleaned_data['file'] != None:
                        file = form_file.save(commit=False)
                        if form_file.cleaned_data['DELETE'] is True:
                            file.delete()
                            os.remove(os.path.join(settings.MEDIA_ROOT, str(file.file) ))
                        else:

                            file.agreement = instance
                            file.uploaded_by = db_user[1]
                            file.account = request.account
                            file.save()

            data['form_is_valid'] = True
            page = request.POST.get('page')
            filter, obj, results = filter_data(request, page, get_access_all)
            message = 'Все прошло успешно'
            messages.add_message(request, messages.SUCCESS, message)
            context = {
                'list': results,
                'wording': wording,
                'js': js,
                'url': url,
                'messages': request._messages,

            }
            data['html_pagination'] = render_to_string('app_docs/global/pagination.html', context)
            data['html_list'] = render_to_string(templates_global['partial_list'], context)
            data['html_partial_project_messages'] = render_to_string(
                'app_project/project_card/partial_project_messages.html', context)

        else:
            messages.error(request, wording['validation_error'])

    if request.method == 'GET':
        data['form_is_valid'] = False

    context = {
                'form': form,
                'formset_file': formset_file,
                'wording': wording,
                'js': js,
                'url': url,
               }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
@access_company_level
@access_app_agr_list
def agreement_update(request, get_access_all , pk):
    item = get_item_agr(pk)
    files = item.agreement_files.all()
    if request.method == 'POST':
        form = AgreementForm(request.POST, instance=item, using=get_db_name(request))
        formset_file = AgreementFileFormSet(request.POST, request.FILES, queryset=files)
    else:
        form = AgreementForm(instance=item, using=get_db_name(request))
        formset_file = AgreementFileFormSet(queryset=files)
    return agreement_save(request, form, formset_file, templates_agr['update'], files)

@login_required
@access_company_level
@access_app_agr_list
def agreement_delete(request, get_access_all, pk):
    data = dict()
    item = get_item_agr(pk)
    if request.method == 'POST':
        item.delete()
        data['form_is_valid'] = True
        page = request.POST.get('page')
        filter, obj, results = filter_data(request, page, get_access_all)
        message = 'Все прошло успешно'
        messages.add_message(request, messages.SUCCESS, message)
        context = {
            'list': results,
            'wording': wording,
            'js': js,
            'url': url,
            'messages': request._messages,

        }
        data['html_pagination'] = render_to_string('app_docs/global/pagination.html', context)
        data['html_list'] = render_to_string(templates_global['partial_list'], context)
        data['html_partial_project_messages'] = render_to_string(
            'app_project/project_card/partial_project_messages.html', context)

    if request.method == 'GET':
        deletable, related_obj = item.can_delete() # checks for foreign key

        # checks for children elements
        for el in get_all_agr():
            if el.parent == item.pk:
                deletable = False
        context = {
            'item': item,
            'deletable':deletable,
            'wording': wording,
            'js': js,
            'url': url,
        }
        data['html_form'] = render_to_string(templates_agr['delete'],context,request=request,)
    return JsonResponse(data)


# ===========

@login_required
@access_company_level
@access_app_docs_section
def accounting_docs_create(request):
    files = app_docs_models.ADFile.objects.none()
    if request.method == 'POST':
        form = AccountingDocsForm(request.POST)
        formset_file = AccountingFileFormSet(request.POST, request.FILES)
    else:
        form = AccountingDocsForm()
        formset_file = AccountingFileFormSet(queryset=files)

    return accounting_docs_save(request, form, formset_file, templates_ad['create'], files)



@login_required
@access_company_level
@access_app_ad_list
def accounting_docs_save(request, get_access_all , form, formset_file, template_name, files):
    data = dict()
    formset_file_enrich(formset_file, files)
    if request.method == 'POST':
        if form.is_valid() and formset_file.is_valid():
            instance = form.save(commit=False)
            db_user = log_data(instance, request)
            instance.save()

            for form_file in formset_file:
                if len(form_file.cleaned_data) != 0:
                    if form_file.cleaned_data['file'] != None:
                        file = form_file.save(commit=False)
                        if form_file.cleaned_data['DELETE'] is True:
                            file.delete()
                            os.remove(os.path.join(settings.MEDIA_ROOT, str(file.file) ))
                        else:
                            file.ad = instance
                            file.uploaded_by = db_user[1]
                            file.account = request.account
                            file.save()

            data['form_is_valid'] = True

            page = request.POST.get('page')
            filter, obj, results = filter_data(request, page, get_access_all)
            message = 'Все прошло успешно'
            messages.add_message(request, messages.SUCCESS, message)
            context = {
                'list': results,
                'wording': wording,
                'js': js,
                'url': url,
                'messages': request._messages,

            }
            data['html_pagination'] = render_to_string('app_docs/global/pagination.html', context)
            data['html_list'] = render_to_string(templates_global['partial_list'], context)
            data['html_partial_project_messages'] = render_to_string(
                'app_project/project_card/partial_project_messages.html', context)

        else:
            messages.error(request, wording['validation_error'])

    if request.method == 'GET':
        data['form_is_valid'] = False
    context = {
                'form': form,
                'formset_file': formset_file,
                'wording': wording,
                'js': js,
                'url': url,
               }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
@access_company_level
@access_app_ad_list
def accounting_docs_update(request, get_access_all, pk):
    item = get_item_ad(pk)
    files = item.accounting_docs_files.all()

    if request.method == 'POST':
        form = AccountingDocsForm(request.POST, instance=item)
        formset_file = AccountingFileFormSet(request.POST, request.FILES, queryset=files)

    else:
        form = AccountingDocsForm(instance=item)
        formset_file = AccountingFileFormSet(queryset=files)

    return accounting_docs_save(request, form, formset_file, templates_ad['update'], files)

@login_required
@access_company_level
@access_app_ad_list
def accounting_docs_delete(request,get_access_all,  pk):
    data = dict()
    item = get_item_ad(pk)
    if request.method == 'POST':
        item.delete()
        data['form_is_valid'] = True
        page = request.POST.get('page')
        filter, obj, results = filter_data(request, page,get_access_all)
        message = 'Все прошло успешно'
        messages.add_message(request, messages.SUCCESS, message)
        context = {
            'list': results,
            'wording': wording,
            'js': js,
            'url': url,
            'messages': request._messages,

        }
        data['html_pagination'] = render_to_string('app_docs/global/pagination.html', context)
        data['html_list'] = render_to_string(templates_global['partial_list'], context)
        data['html_partial_project_messages'] = render_to_string(
            'app_project/project_card/partial_project_messages.html', context)

    if request.method == 'GET':
        deletable, related_obj = item.can_delete() # checks for foreign key
        context = {
            'item': item,
            'deletable':deletable,
            'wording': wording,
            'js': js,
            'url': url,
        }
        data['html_form'] = render_to_string(templates_ad['delete'],context,request=request,)
    return JsonResponse(data)


# ===========

@login_required
@access_company_level
@access_app_docs_section
def docs_history(request):

    history_agr = app_docs_models.Agreement.history.all()[:3]
    history_ad = app_docs_models.AccountingDocs.history.all()[:3]

    context = {
        'records_agr': history_record(history_agr, 'Документ', True),
        'records_ad': history_record(history_ad, 'Документ', True),
        'wording':wording,
    }

    data = {}
    data['html_form'] = render_to_string(templates_global['history'], context, request=request)

    return JsonResponse(data)


@login_required
@access_company_level
@access_app_docs_section
def parent_info(request):
    data={}

    pk = request.GET.get('pk')
    parent = app_docs_models.Agreement.objects.get(pk=pk)

    data={
        'parent_mycompany':str(parent.mycompany.pk),
        'parent_sub':str(parent.subcontractor.pk),
        'parent_sub_type': str(parent.subcontractor.type.pk),
    }

    return JsonResponse(data)


