from django.db import models
from fins.global_functions import IsDeletableMixin, BOOL_CHOICES_YESNO
from app_setup.models import DBUser
from app_transaction.models import Subcontractor, Transaction
from app_project.models import Project, Report
from simple_history.models import HistoricalRecords
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
import os
import psycopg2


def name_creator(item,  name):
    print('name', name)
    name = name + ' / ' + str(item.get_type_display()) + ' #' + str(item.name) + ' от ' + str(item.signed_at.strftime('%d/%m/%Y'))
    if item.parent:
        print('item.paren  t', Agreement.objects.get(id=item.parent))


        return name_creator(Agreement.objects.get(id=item.parent),  name)
    else:
        return name


class DocTemplateFile(models.Model, IsDeletableMixin):

    def user_directory_path(instance, filename):

        return 'account_{0}/doc_templates/{1}'.format(instance.account.pk, filename)

    name = models.CharField(_('Название шаблона'), max_length=500)
    file = models.FileField(_('Файл'), upload_to=user_directory_path, blank=True, help_text='Только формат *.docx')

    uploaded_at = models.DateTimeField(auto_now_add=True)
    uploaded_by =  models.ForeignKey(DBUser, related_name='doc_templates_uploaded_by', on_delete=models.PROTECT,
                                    null=True)
    history = HistoricalRecords(user_model=DBUser)

    @property
    def _history_user(self):
        return self.uploaded_by

    @_history_user.setter
    def _history_user(self, value):
        self.uploaded_by = value

    def __str__(self):
        return self.name

class Agreement(models.Model, IsDeletableMixin):
    STATUS_CHOICES = (
        (0, 'Не сделан'),
        (1, 'Согласование'),
        (2, 'Согласован'),
        (3, 'На подписании внутри, контрагентом не подписан'),
        (4, 'На подписании внутри, контрагентом подписан'),
        (5, 'На подписании у контрагента, нами не подписан'),
        (6, 'На подписании у контрагента, нами подписан'),
        (7, 'Подписан и подшит'),
    )

    PROLONGATION_TYPE_CHOICES = (
        (0, 'Автоматически'),
        (1, 'Заявка'),
        (2, 'Без продления'),
    )



    TYPE_CHOICES = (
        (0, 'Рамочный договор'),
        (1, 'Договор'),
        (2, 'Агентский договор'),
        (3, 'Приложение'),
        (4, 'Доп. соглашение'),
        (5, 'Дополнение'),
        (6, 'Заявка'),
        (7, 'Отчет'),
        (8, 'NDA'),
        (9, 'Другое'),
    )

    ROLE_CHOICES = (
        (0, 'Заказчик'),
        (1, 'Поставщик'),
    )

    name = models.CharField(_('Номер документа'), max_length=500)
    type = models.PositiveSmallIntegerField(_('Тип документа'), choices=TYPE_CHOICES, default=0)
    subcontractor_role = models.PositiveSmallIntegerField(_('Тип контрагента'), choices=ROLE_CHOICES, default=0)
    subcontractor = models.ForeignKey(Subcontractor, verbose_name = 'Контрагент',
                                      related_name='subs_agreement', on_delete=models.PROTECT)
    mycompany = models.ForeignKey(Subcontractor, verbose_name = 'Моя компания',
                                      related_name='mycompany_agreement', on_delete=models.PROTECT)
    signed_at = models.DateField(_('Дата подписания'))
    validity_period_from = models.DateField(_('Период действия с'), null=True, blank=True)
    validity_period_till = models.DateField(_('Период действия по'), null=True, blank=True)
    prolongation_type = models.PositiveSmallIntegerField(_('Условаия продления'), choices=PROLONGATION_TYPE_CHOICES, default=2)
    prolongation_alert = models.DateField(_('Дата начала действий для продления'), null=True, blank=True)

    status = models.PositiveSmallIntegerField(_('Статус документа'), choices=STATUS_CHOICES, default=0)
    status_date = models.DateField(_('Дата перехода в текущий статус'), null=True, blank=True)

    parent = models.IntegerField(_('Родительский документ'), null=True, blank=True)

    description = models.CharField(_('Описание документа'), max_length=500, blank=True)

    project = models.ForeignKey(Project, help_text='Связть с проектам обязательно для отображения у полтзователей с органиченным доступом', verbose_name = 'Сваязан с проектом',
                                      related_name='project_agreements', on_delete=models.SET_NULL, null=True, blank=True)
    transaction = models.ForeignKey(Transaction, verbose_name = 'Сваязан с транзакцией',
                                      related_name='transaction_agreements', on_delete=models.SET_NULL, null=True, blank=True)
    report = models.ForeignKey(Report, verbose_name = 'Сваязан с отчетом',
                                      related_name='report_agreements', on_delete=models.SET_NULL, null=True, blank=True)
    doc_template = models.ForeignKey(DocTemplateFile, verbose_name='Использовать шаблон',
                                     related_name='template_agreements', on_delete=models.SET_NULL, null=True, blank=True)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='agreements_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='agreements_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('Документ')
        verbose_name_plural = _('Документы')

    def __str__(self):
        return str(self.get_type_display()) + ' #' + str(self.name) + ' от ' + str(self.signed_at.strftime('%d/%m/%Y'))



    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

    @property
    def has_parent_full(self):
        if self.parent is not None:
            return Agreement.objects.get(pk=self.parent).name
        else:
            return None

    @property
    def has_attachment_display(self):
        if len(self.agreement_files.all()) != 0:
            return 'Да'
        else:
            return 'Нет'


    @property
    def has_scan_display(self):
        if self.has_attachment_display == 'Да':
            has_scan = 'Нет'
            for file in self.agreement_files.all():
                if file.type == 1:
                    has_scan = 'Да'
            return has_scan
        else:
            return 'Нет'

    @property
    def has_src_display(self):
        if self.has_attachment_display == 'Да':
            has_scan = 'Нет'
            for file in self.agreement_files.all():
                if file.type == 2:
                    has_scan = 'Да'
            return has_scan
        else:
            return 'Нет'

    @property
    def full_name(self):
        return str(self.get_type_display()) + ' #' + str(self.name) + ' от ' + self.signed_at.strftime("%d/%m/%Y")

    @property
    def full_name_with_parents(self):
        return name_creator(self, '')[2:]


class AccountingDocs(models.Model, IsDeletableMixin):
    PROLONGATION_TYPE_CHOICES = (
        (0, 'Автоматически по истечении периода'),
        (1, 'При своевременной подачи зайвки'),
        (2, 'Без продления'),
    )

    TYPE_CHOICES = (
        (0, 'Счет'),
        (1, 'Счет-фактура'),
        (2, 'Акт'),
        (3, 'Товарная накладная'),

    )

    STATUS_CHOICES = (
        (0, 'Не сделан'),
        (1, 'Согласование'),
        (2, 'Согласован'),
        (3, 'На подписании внутри, контрагентом не подписан'),
        (4, 'На подписании внутри, контрагентом подписан'),
        (5, 'На подписании у контрагента, нами не подписан'),
        (6, 'На подписании у контрагента, нами подписан'),
        (7, 'Подписан и подшит'),
    )
    name = models.CharField(_('Номер документа'), max_length=500)
    signed_at = models.DateField(_('Дата выставления'))
    type = models.PositiveSmallIntegerField(_('Тип документа'), choices=TYPE_CHOICES, default=0)
    covered_period_from = models.DateField(_('Охваченный период с'), null=True, blank=True)
    covered_period_till = models.DateField(_('Охваченный период по'), null=True, blank=True)
    status = models.PositiveSmallIntegerField(_('Статус документа'), choices=STATUS_CHOICES, default=0)
    status_date = models.DateField(_('Дата перехода в текущий статус'), null=True, blank=True)

    agreement = models.ForeignKey(Agreement, verbose_name = 'Документ', related_name='account_docs',
                                  on_delete=models.PROTECT, blank=True, null=True)

    subcontractor = models.ForeignKey(Subcontractor, verbose_name='Контрагент',
                                      related_name='subs_accounting_doc', on_delete=models.PROTECT, blank=True, null=True)
    mycompany = models.ForeignKey(Subcontractor, verbose_name='Моя компания',
                                  related_name='mycompany_accounting_doc', on_delete=models.PROTECT, blank=True, null=True)

    project = models.ForeignKey(Project, help_text='Связть с проектам обязательно для отображения у полтзователей с органиченным доступом', verbose_name = 'Сваязан с проектом',
                                      related_name='project_accounting_docs', on_delete=models.SET_NULL, null=True, blank=True)
    transaction = models.ForeignKey(Transaction, verbose_name = 'Сваязан с транзакцией',
                                      related_name='transaction_accounting_docs', on_delete=models.SET_NULL, null=True, blank=True)
    report = models.ForeignKey(Report, verbose_name = 'Сваязан с отчетом',
                                      related_name='report_accounting_docs', on_delete=models.SET_NULL, null=True, blank=True)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='account_docs_created_by', on_delete=models.PROTECT,
                                   null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='account_docs_modified_by', on_delete=models.PROTECT,
                                    null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('Документ')
        verbose_name_plural = _('Документы')

    def __str__(self):
        return self.name

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value


    @property
    def has_attachment_display(self):
        if len(self.accounting_docs_files.all()) != 0:
            return 'Да'
        else:
            return 'Нет'


    @property
    def has_scan_display(self):
        if self.has_attachment_display == 'Да':
            has_scan = 'Нет'
            for file in self.accounting_docs_files.all():
                if file.type == 1:
                    has_scan = 'Да'
            return has_scan
        else:
            return 'Нет'

    @property
    def has_src_display(self):
        if self.has_attachment_display == 'Да':
            has_scan = 'Нет'
            for file in self.accounting_docs_files.all():
                if file.type == 2:
                    has_scan = 'Да'
            return has_scan
        else:
            return 'Нет'


    @property
    def full_name(self):
        return str(self.get_type_display()) + ' №' + str(self.name) + ' от ' + self.signed_at.strftime("%d/%m/%Y")

class AgreementFile(models.Model, IsDeletableMixin):
    FILE_TYPE_CHOICES = (
        (0, 'Другое'),
        (1, 'Скан'),
        (2, 'Исходник'),

    )

    def user_directory_path(instance, filename):
        return '{0}/{1}'.format(instance.dbname, filename)

    file = models.FileField(_('Файл'), upload_to=user_directory_path, blank=True)


    uploaded_at = models.DateTimeField(auto_now_add=True)
    uploaded_by =  models.ForeignKey(DBUser, related_name='agreement_files_uploaded_by', on_delete=models.PROTECT,
                                    null=True)
    agreement = models.ForeignKey(Agreement, related_name='agreement_files', on_delete=models.PROTECT, blank=True)
    comment = models.CharField(_('Комментарий'), max_length=500, blank=True)
    type = models.PositiveSmallIntegerField(_('Тип документа'), choices=FILE_TYPE_CHOICES, default=0)
    dbname = models.CharField(_('Название БД'), max_length=100, blank=True)

    @property
    def filename(self):
        return str(self.file).split('/', 1)[1]

    @property
    def path(self):
        return os.path.join(settings.MEDIA_URL, str(self.file) )


class ADFile(models.Model, IsDeletableMixin):
    FILE_TYPE_CHOICES = (
        (0, 'Другое'),
        (1, 'Изображение подписанного оригинала'),
        (2, 'Редактируемая версия'),

    )

    def user_directory_path(instance, filename):
        return '{0}/{1}'.format(instance.dbname, filename)

    file = models.FileField(_('Файл'), upload_to=user_directory_path, blank=True)


    uploaded_at = models.DateTimeField(auto_now_add=True)
    uploaded_by =  models.ForeignKey(DBUser, related_name='accounting_docs_uploaded_by', on_delete=models.PROTECT,
                                    null=True)
    ad = models.ForeignKey(AccountingDocs, related_name='accounting_docs_files', on_delete=models.PROTECT, blank=True)
    comment = models.CharField(_('Комментарий'), max_length=500, blank=True)
    type = models.PositiveSmallIntegerField(_('Тип документа'), choices=FILE_TYPE_CHOICES, default=0)
    dbname = models.CharField(_('Название БД'), max_length=100, blank=True)

    @property
    def filename(self):
        return str(self.file).split('/', 1)[1]

    @property
    def path(self):
        return os.path.join(settings.MEDIA_URL, str(self.file) )
