from django.contrib import admin
from django.urls import path, include, re_path
from django.contrib.auth import views as auth_views
from . import views

app_name = 'app_docs'
urlpatterns = [
    # path('', views.index, name='index'),

    # agreement -----------------
    # path('agreement/', views.agreement_index, name='argeement_index'),
    path('api/', views.agreement_list),
    path('api/<int:pk>/', views.agreement_detail),
    path('api/extra/', views.agreement_extra),
    path('api/history', views.agreement_history),
    path('api/files/<int:pk>/', views.agreement_files),
    path('doc_generate/<pk>/<type>', views.doc_generate),

    # accounting -----------------
    # path('accoutning/', views.accounting_index, name='accounting_index'),
    path('api/accounting/', views.accounting_list),
    path('api/accounting/<int:pk>/', views.accounting_detail),
    path('api/accounting/extra/', views.accounting_extra),
    path('api/accounting/history/', views.accounting_history),
    path('api/accounting/files/<int:pk>/', views.accounting_files),
    path('api/accounting/extra_agreement/', views.accounting_extra_agreement),

]


