import datetime
import locale
from app_docs import models as app_docs_models
import pymorphy2
from transliterate import translit, get_available_language_codes
from app_project import models as app_project_models
from app_transaction import models as app_transaction_models

morph = pymorphy2.MorphAnalyzer()

def doc_full_name(doc):
    name = str(doc.get_type_display()) + ' №' + str(doc.name)
    return name

def doc_full_name_sklon(doc, padeg):
    ''' makes declesion'''

    if padeg == 'vinit':
        pad = 'datv'
    else:
        pad = ''

    word_list = str(doc.get_type_display()).split(" ")

    new_word_list = []
    i=0
    for w in word_list:
        word = morph.parse(w)[0]
        new_word = word.inflect({pad}).word
        if i == 0:
            new_word_list.append(new_word.capitalize())
        else:
            new_word_list.append(new_word)
        i+=1
    name = " ".join(new_word_list)
    print(name)
    return name

def lister(all, array, doc, full_name, doc_list):
    '''
    creates a list of dics with its parents
    '''

    if doc.parent is None:
        array.append(full_name)
        doc_list.append(doc)
        return array, doc_list
    else:
        array.append(full_name)
        doc_list.append(doc)
        parent = all.get(pk=doc.parent)
        full_name_parent = 'к ' + doc_full_name_sklon(parent, 'vinit') + ' от ' + parent.signed_at.strftime('%d %B %Y')
        return lister(all, array, parent, full_name_parent, doc_list)

def agreement_gen(doc):

    context = {}
    filename = 'Download'
    locale.setlocale(locale.LC_TIME, "ru_RU")

    project = doc.project
    if project is None:
        context.update({'project': None})

    transaction = doc.transaction
    if transaction is None:
        context.update({'transaction': None})


    report = doc.report
    if report is None:
        context.update({'report': None})
        context.update({'period': None})
        period = None
    else:
        period = report.period


    #data for default test rendering
    context.update({
        'item': None,

    })


    # UNIVERSAL - generate dependancy docs list for header and all docs separate info

    array, doc_list = lister(app_docs_models.Agreement.objects.all(), [], doc, doc_full_name(doc), [])

    docs = {'header': array,}

    i = 0
    for doc in doc_list:
        number = 'doc_' + str(i) + '_number'
        date = 'doc_' + str(i) + '_date'
        type = 'doc_' + str(i) + '_type'
        docs[number] = doc.name
        docs[date] = doc.signed_at.strftime('%d %B %Y')
        docs[type] = doc.get_type_display()
        i += 1


    context.update(docs)


    # UNIVERSAL - generate requisite info for subcontractor and mycompany

    def sub_info(sub):
        try:
            firstname_short = str(sub.director_firstname)[0]
        except Exception:
            firstname_short = ''

        try:
            fathername_short = str(sub.director_fathername)[0]
        except Exception:
            fathername_short = ''

        try:
            secondname_rod = morph.parse(str(sub.director_secondname))[0].inflect({'gent'}).word.capitalize()
        except Exception:
            secondname_rod = ''

        try:
            fathername_rod = morph.parse(str(sub.director_fathername))[0].inflect({'gent'}).word.capitalize()
        except Exception:
            fathername_rod = ''

        try:
            firstname_rod = morph.parse(str(sub.director_firstname))[0].inflect({'gent'}).word.capitalize()
        except Exception:
            firstname_rod = ''




        sub_info = {
                'name': sub,
                'legaltype_full': sub.legal_form.name_full,
                'legaltype_short': sub.legal_form.name,
                'taxetype': sub.taxe_type.name,


                'director_full': str(sub.director_secondname) + ' ' + str(sub.director_firstname) + ' ' + str(sub.director_fathername),
                'director_full_firstname': str(sub.director_firstname),
                'director_full_secondname': str(sub.director_secondname),
                'director_full_fathername': str(sub.director_fathername),
                'director_short': str(sub.director_secondname) + ' ' + firstname_short + '. ' + fathername_short + '.',

                'director_full_rod':  secondname_rod + ' ' + firstname_rod + ' ' + fathername_rod,
                'director_full_firstname_rod': firstname_rod,
                'director_full_secondname_rod': secondname_rod,
                'director_full_fathername_rod': fathername_rod,
                'director_short_rod': secondname_rod + ' ' + firstname_short + '. ' + fathername_short + '.',

                'site': sub.site,
                'pay_delay': sub.pay_delay,
                'requisites_full_name': sub.requisites_full_name,
                'requisites_official_address': sub.requisites_official_address,
                'requisites_real_address': sub.requisites_real_address,
                'requisites_inn': sub.requisites_inn,
                'requisites_kpp': sub.requisites_kpp,
                'requisites_okpo': sub.requisites_okpo,
                'requisites_okdev': sub.requisites_okdev,
                'requisites_ogrn': sub.requisites_ogrn,
                'requisites_bank_name': sub.requisites_bank_name,
                'requisites_bank_bik': sub.requisites_bank_bik,
                'requisites_bank_address': sub.requisites_bank_address,
                'requisites_bank_pay_acc': sub.requisites_bank_pay_acc,
                'requisites_bank_cor_acc': sub.requisites_bank_cor_acc,
                'requisites_bank_card_number': sub.requisites_bank_card_number,

                'contact_name': sub.contact_name,
                'contact_tel': sub.contact_tel,
                'contact_email': sub.contact_email,
                'contact_messenger_name': sub.contact_messenger_name,
                'contact_messenger_id': sub.contact_messenger_id,
                'contact_social_net': sub.contact_social_net,
                'contact_social_link': sub.contact_social_link,

            }
        return sub_info

    context.update({
        'subcontractor': sub_info(doc.subcontractor),
        'mycompany': sub_info(doc.mycompany),
    })




    # Project info

    if project is not None:

        try:
            project_start = project.project_start.strftime('%d %B %Y')
        except Exception:
            project_start = ''
        try:
            project_end = project.project_end.strftime('%d %B %Y')
        except Exception:
            project_end = ''
        try:
            subcategory = project.subcategory.name
        except Exception:
            subcategory = ''

        try:
            products_full = []
            for product in project.project_products.all():
                products_full.append(str(product))

            products_full = ', '.join(products_full)
        except Exception:
            products_full = ''

        context.update({'project':{
            'number': project.name,
            'name_short': project.name_short,
            'name_long': project.name_long,
            'category': project.category.name,
            'subcategory': subcategory,
            'work_format': project.get_work_format_display(),
            'agreement_format': project.get_agreement_format_display(),
            'commission': project.commission,
            'stage': project.get_stage_display(),
            'project_start': project_start,
            'project_end': project_end,
            'description': project.description,
            'brand': project.brand,
            'nb_source': project.nb_source,
            'nb_format': project.nb_format,
            'project_products': products_full,
        }})

    # Transaction info

    if transaction is not None:

        try:
            date_fact = transaction.date_fact.strftime('%d %B %Y')
        except Exception:
            date_fact = ''
        try:
            date_doc = transaction.date_doc.strftime('%d %B %Y')
        except Exception:
            date_doc = ''
        try:
            agreement_start_date = transaction.agreement_start_date.strftime('%d %B %Y')
        except Exception:
            agreement_start_date = ''
        try:
            agreement_end_date = transaction.agreement_end_date.strftime('%d %B %Y')
        except Exception:
            agreement_end_date = ''

        context.update({'transaction':{
            'number': transaction.name,
            'type': transaction.get_type_display(),
            'status': transaction.get_status_display(),
            'subcontractor': transaction.subcontractor.name,
            'purpose': transaction.purpose,
            'unit': transaction.unit,
            'account': transaction.account,
            'date_fact': date_fact,
            'date_doc': date_doc,
            'sum_before_taxes': transaction.sum_before_taxes,
            'agreement_start_date': agreement_start_date,
            'agreement_end_date': agreement_end_date,
            'description': transaction.description,
            'comment': transaction.comment,

        }})


    # Report info

    if report is not None:
        report = report.services_in_report.all()

        i=0
        total_cost=[]
        for r in report:
            r.num = i + 1
            cost = int(r.service.rate_bt) * int(r.hours)
            r.cost = '{:,}'.format(cost).replace(',', ' ')
            total_cost.append(cost)
            i+=1


        context.update({
            'report_period_start': period.start.strftime('%d %B %Y'),
            'report_period_end': period.end.strftime('%d %B %Y'),
            'report': report,
            'report_total_cost': '{:,}'.format(sum(total_cost)).replace(',', ' '),
        })

    else:
        report = [None, ]
        context.update({'report': report})

    filename = translit(str(doc.get_type_display()), reversed=True) + ' #' + doc.name + ' ot '+ doc.signed_at.strftime('%d_%m_%Y')

    return context, filename


def generate_doc(doc, type):
    if type == '0':
        context, filename = agreement_gen(doc)
    else:
        context = {}
        filename = 'download'

    return context, filename