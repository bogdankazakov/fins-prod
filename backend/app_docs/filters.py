from django.contrib.auth.models import User
import django_filters
from .models import *
from django.http.request import HttpRequest
from django import forms
from app_docs import models as app_docs_models
from app_setup import models as app_setup_models
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from django_filters import Filter
from django_filters.fields import Lookup
import json
from django.db.models import Case, When, DateField, DecimalField, ExpressionWrapper, BooleanField, Count,Exists
from django.db.models import Q, F
from app_docs.tools import *

from django.core.validators import EMPTY_VALUES




BOOL_CHOICES_YESNO = (
    (True,'Да'),
    (False,'Нет'),
)

TYPE_CHOICES = (
    (0, 'Рамочный договор'),
    (1, 'Договор'),
    (2, 'Агентский договор'),
    (3, 'Приложение'),
    (4, 'Дополнительное соглашение'),
    (5, 'Дополнение'),
    (6, 'Заявка'),
    (7, 'Отчет'),
    (8, 'Другое'),
)

ROLE_CHOICES = (
    (0, 'Заказчик'),
    (1, 'Поставщик'),
)

PROLONGATION_TYPE_CHOICES = (
    (0, 'Автоматически по истечении периода'),
    (1, 'При своевременной подачи зайвки'),
    (2, 'Без продления'),
)

STATUS_CHOICES = (
    (0, 'Не сделан'),
    (1, 'Утверждается'),
    (2, 'Электронная версия утверждена'),
    (3, 'Документ на подписании у нас, контрагентом не подписан'),
    (4, 'Документ на подписании у контрагента, нами не подписан'),
    (5, 'Документ подписан и подшит'),
)




# создан специальный фильтр чтобы определять что строка пустая
class EmptyStringFilter(django_filters.BooleanFilter):
    def filter(self, qs, value):
        if value in EMPTY_VALUES:
            return qs

        exclude = self.exclude ^ (value is True)
        method = qs.exclude if exclude else qs.filter

        return method(**{self.name: ""})



class DocsFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')
    name_full = django_filters.CharFilter(method='full_filter')
    type = django_filters.MultipleChoiceFilter(choices = TYPE_CHOICES)
    subcontractor_role = django_filters.MultipleChoiceFilter(choices = ROLE_CHOICES)
    subcontractor = django_filters.CharFilter(lookup_expr='name__icontains')
    mycompany = django_filters.CharFilter(lookup_expr='name__icontains')
    signed_at = django_filters.DateFromToRangeFilter()
    validity_period_from = django_filters.DateFromToRangeFilter()
    validity_period_till = django_filters.DateFromToRangeFilter()
    prolongation_type = django_filters.MultipleChoiceFilter(choices = PROLONGATION_TYPE_CHOICES)
    prolongation_alert = django_filters.DateFromToRangeFilter()
    status = django_filters.MultipleChoiceFilter(choices = STATUS_CHOICES)
    status_date = django_filters.DateFromToRangeFilter()
    description = django_filters.CharFilter(lookup_expr='icontains')
    project = django_filters.ModelMultipleChoiceFilter(queryset=app_project_models.Project.objects.all())
    transaction = django_filters.ModelMultipleChoiceFilter(queryset=app_transaction_models.Transaction.objects.all())
    report = django_filters.ModelMultipleChoiceFilter(queryset=app_project_models.Report.objects.all())
    doc_template = django_filters.ModelMultipleChoiceFilter(queryset=app_docs_models.DocTemplateFile.objects.all())
    parent = django_filters.CharFilter(method='parent_filter')
    has_attachment = django_filters.ChoiceFilter(choices = BOOL_CHOICES_YESNO, method='has_attachment_filter')
    has_scan = django_filters.ChoiceFilter(choices = BOOL_CHOICES_YESNO, method='has_scan_filter')
    has_src = django_filters.ChoiceFilter(choices = BOOL_CHOICES_YESNO, method='has_src_filter')


    def parent_filter(self, queryset, name, value):
        q = Q()
        for qs in queryset:
            if qs.parent is not None:
                print(qs.parent, qs)
                parent = app_docs_models.Agreement.objects.get(pk=qs.parent)
                if value in str(parent):
                    q = q | Q(pk=qs.pk)
        queryset = queryset.filter(q)

        if len(q) == 0:
            return queryset.none()
        return queryset


    def has_attachment_filter(self, queryset, name, value):
        q = Q()
        for qs in queryset:
            if len(qs.agreement_files.all()) != 0:
                q = q | Q(pk=qs.pk)
        if value == 'True':
            queryset = queryset.filter(q)
        else:
            queryset = queryset.exclude(q)
        return queryset

    def has_scan_filter(self, queryset, name, value):
        q = Q()
        for qs in queryset:
            if len(qs.agreement_files.all()) != 0:
                for file in qs.agreement_files.all():
                    if file.type == 1:
                        q = q | Q(pk=qs.pk)
        if value == 'True':
            queryset = queryset.filter(q)
        else:
            queryset = queryset.exclude(q)
        return queryset

    def has_src_filter(self, queryset, name, value):
        q = Q()
        for qs in queryset:
            if len(qs.agreement_files.all()) != 0:
                for file in qs.agreement_files.all():
                    if file.type == 2:
                        q = q | Q(pk=qs.pk)
        if value == 'True':
            queryset = queryset.filter(q)
        else:
            queryset = queryset.exclude(q)
        return queryset



    def full_filter(self, queryset, name, value):
        all = app_docs_models.Agreement.objects.all()
        req = {'{0}__{1}'.format('name', 'icontains'): value}
        qs = queryset.filter(**req)

        if len(qs) == 0:
            return queryset.none()
        else:
            q = Q()
            for item in qs:
                parents_list = [item,]
                list = lister(all, parents_list, False)
                for parent in list:
                    q = q | Q(pk=parent.pk)

            queryset = all.filter(q)
        return queryset


    class Meta:
        model = app_docs_models.Agreement
        fields = []






def filterMain(qs, filter_param, is_json = True):
    qs = qs.annotate(
            has_attachment=Case(
                When(agreement_files=None, then=False),
                default=True,
                output_field=BooleanField(),
            )).annotate(
                scans=Count(Case(When(agreement_files__type=1, then=1)))
            ).annotate(
                srcs=Count(Case(When(agreement_files__type=2, then=1)))
            ).annotate(
            has_scan=Case(
                When(scans=0, then=False),
                default=True,
                output_field=BooleanField(),
            )).annotate(
            has_src=Case(
                When(srcs=0, then=False),
                default=True,
                output_field=BooleanField(),
            ))

    # for q in qs:
    #     print('=========')
    #     print('scans',q.name)
    #     print('scans',q.scans)
    #     print('srcs',q.srcs)
    #     print('has_scan',q.has_scan)
    #     print('has_src',q.has_src)

    if is_json:
        filter_param = json.loads(filter_param)

    query = Q()
    with_children = False

    for (key, value) in filter_param.items():

        if key == 'name' and len(value) > 0:
            query &= Q(name__icontains=value)

        elif key == 'type' and len(str(value)) > 0 and value is not None:
            query &= Q(type=value)

        elif key == 'dateSignFrom' and len(value) > 0:
            query &= Q(signed_at__gte=value)

        elif key == 'dateSignTo' and len(value) > 0:
            query &= Q(signed_at__lte=value)

        elif key == 'status' and len(value)>0:
            query &= Q(status__in=value)

        elif key == 'dateStatusFrom' and len(value) > 0:
            query &= Q(status_date__gte=value)

        elif key == 'dateStatusTo' and len(value) > 0:
            query &= Q(status_date__lte=value)

        elif key == 'subcontractor' and len(str(value)) > 0 and value is not None:
            query &= Q(subcontractor=value)

        elif key == 'subcontractorRole' and len(value)>0:
            query &= Q(subcontractor_role__in=value)

        elif key == 'mycompany' and len(value)>0:
            query &= Q(mycompany__in=value)

        elif key == 'datePeriodStartFrom' and len(value) > 0:
            query &= Q(validity_period_from__gte=value)

        elif key == 'datePeriodStartTo' and len(value) > 0:
            query &= Q(validity_period_from__lte=value)

        elif key == 'datePeriodEndFrom' and len(value) > 0:
            query &= Q(validity_period_till__gte=value)

        elif key == 'datePeriodEndTo' and len(value) > 0:
            query &= Q(validity_period_till__lte=value)

        elif key == 'prolongationType' and len(value)>0:
            query &= Q(prolongation_type__in=value)

        elif key == 'dateProlongationAlertFrom' and len(value) > 0:
            query &= Q(prolongation_alert__gte=value)

        elif key == 'dateProlongationAlertTo' and len(value) > 0:
            query &= Q(prolongation_alert__lte=value)

        elif key == 'description' and len(value) > 0:
            query &= Q(description__icontains=value)


        elif key == 'project' and len(str(value)) > 0 and value is not None:
            query &= Q(project=value)

        elif key == 'transaction' and len(str(value)) > 0 and value is not None:
            query &= Q(transaction=value)

        elif key == 'report' and len(str(value)) > 0 and value is not None:
            query &= Q(report=value)

        elif key == 'template' and len(str(value)) > 0 and value is not None:
            query &= Q(doc_template=value)

        elif key == 'hasAttachments' and len(value)>0:
            newval=[]
            for v in value:
                newval.append(bool(int(v)))
            query &= Q(has_attachment__in=newval)

        elif key == 'hasScan'  and len(value)>0:
            newval=[]
            for v in value:
                newval.append(bool(int(v)))
            print(newval)
            query &= Q(has_scan__in=newval)

        elif key == 'hasSrc'  and len(value)>0:
            newval=[]
            for v in value:
                newval.append(bool(int(v)))
            query &= Q(has_src__in=newval)

        elif key == 'namefull' and value is True:
            with_children = True

    filtered = qs.filter(query)

    if with_children and len(filtered) != 0:
        all = app_docs_models.Agreement.objects.all()
        q = Q()
        for item in filtered:
            parents_list = [item, ]
            list = child_finder(all, parents_list, False)
            for child in list:
                q = q | Q(pk=child.pk)

        filtered = all.filter(q)
    return filtered




def filterAccounting(qs, filter_param, is_json = True):
    qs = qs.annotate(
            has_attachment=Case(
                When(accounting_docs_files=None, then=False),
                default=True,
                output_field=BooleanField(),
            )).annotate(
                scans=Count(Case(When(accounting_docs_files__type=1, then=1)))
            ).annotate(
                srcs=Count(Case(When(accounting_docs_files__type=2, then=1)))
            ).annotate(
            has_scan=Case(
                When(scans=0, then=False),
                default=True,
                output_field=BooleanField(),
            )).annotate(
            has_src=Case(
                When(srcs=0, then=False),
                default=True,
                output_field=BooleanField(),
            ))


    if is_json:
        filter_param = json.loads(filter_param)

    query = Q()

    for (key, value) in filter_param.items():

        if key == 'name' and len(value) > 0:
            query &= Q(name__icontains=value)

        elif key == 'type' and len(str(value)) > 0 and value is not None:
            query &= Q(type=value)

        elif key == 'dateSignFrom' and len(value) > 0:
            query &= Q(signed_at__gte=value)

        elif key == 'dateSignTo' and len(value) > 0:
            query &= Q(signed_at__lte=value)

        elif key == 'status' and len(value)>0:
            query &= Q(status__in=value)

        elif key == 'dateStatusFrom' and len(value) > 0:
            query &= Q(status_date__gte=value)

        elif key == 'dateStatusTo' and len(value) > 0:
            query &= Q(status_date__lte=value)

        elif key == 'subcontractor' and len(str(value)) > 0 and value is not None:
            query &= Q(subcontractor=value)

        elif key == 'mycompany' and len(value)>0:
            query &= Q(mycompany__in=value)

        elif key == 'datePeriodStartFrom' and len(value) > 0:
            query &= Q(covered_period_from__gte=value)

        elif key == 'datePeriodStartTo' and len(value) > 0:
            query &= Q(covered_period_from__lte=value)

        elif key == 'datePeriodEndFrom' and len(value) > 0:
            query &= Q(covered_period_till__gte=value)

        elif key == 'datePeriodEndTo' and len(value) > 0:
            query &= Q(covered_period_till__lte=value)

        elif key == 'agreement'  and len(str(value)) > 0 and value is not None:
            query &= Q(agreement=value)

        elif key == 'project' and len(str(value)) > 0 and value is not None:
            query &= Q(project=value)

        elif key == 'transaction' and len(str(value)) > 0 and value is not None:
            query &= Q(transaction=value)

        elif key == 'report' and len(str(value)) > 0 and value is not None:
            query &= Q(report=value)

        elif key == 'hasAttachments' and len(value)>0:
            newval=[]
            for v in value:
                newval.append(bool(int(v)))
            query &= Q(has_attachment__in=newval)

        elif key == 'hasScan'  and len(value)>0:
            newval=[]
            for v in value:
                newval.append(bool(int(v)))
            print(newval)
            query &= Q(has_scan__in=newval)

        elif key == 'hasSrc'  and len(value)>0:
            newval=[]
            for v in value:
                newval.append(bool(int(v)))
            query &= Q(has_src__in=newval)



    filtered = qs.filter(query)

    return filtered