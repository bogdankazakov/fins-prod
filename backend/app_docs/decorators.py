from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render, redirect
from subdomains.utils import reverse as sub_reverse
from django.urls import reverse
from functools import wraps
from app_signup.models import User
from django.http import HttpResponseRedirect
from django.conf import settings
from app_transaction import models as app_transaction_models
from app_docs import models as app_docs_models
from app_setup import models as app_setup_models
from django.db.models import Q


def access_app_docs_section(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to Docs section
        '''

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_docs
        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain, scheme='https'))
        else:
            return function(request, *args, **kwargs)
    return wrap

def access_app_agr_list(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to Project section and returns get all function
        '''

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_docs

        if 'pk' in kwargs:
            pk = kwargs['pk']
        else:
            pk = None

        if access_level == 0:
            return redirect(
                sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain, scheme='https'))

        elif access_level == 1:
            def get_access_all():
                all = app_docs_models.Agreement.objects.filter(project__teamprojects_in_project__user=dbuser).order_by('-pk')
                return all
            all = get_access_all().values_list('pk', flat=True)
            all = list(all)
            if pk is not None:
                if int(pk) in all:
                    return function(request, get_access_all, *args, **kwargs)
                else:
                    return redirect(
                        sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain, scheme='https'))
            else:
                return function(request, get_access_all, *args, **kwargs)

        elif access_level == 3:
            def get_access_all():
                all = app_docs_models.Agreement.objects.filter(project__teamprojects_in_project__user__unit=dbuser.unit).order_by('-pk')
                return all

            all = get_access_all().values_list('pk', flat=True)
            all = list(all)

            if pk is not None:
                if int(pk) in all:
                    return function(request, get_access_all, *args, **kwargs)
                else:
                    return redirect(
                        sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain, scheme='https'))
            else:
                return function(request, get_access_all, *args, **kwargs)

        else:
            def get_access_all():
                all = app_docs_models.Agreement.objects.all()
                return all

            return function(request, get_access_all, *args, **kwargs)



    return wrap

def access_app_ad_list(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to Project section and returns get all function
        '''

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_docs

        if 'pk' in kwargs:
            pk = kwargs['pk']
        else:
            pk = None
        if access_level == 0:
            return redirect(
                sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain, scheme='https'))

        elif access_level == 1:
            def get_access_all():
                all = app_docs_models.Agreement.objects.filter(project__teamprojects_in_project__user=dbuser).order_by('-pk')
                return all

            all = app_docs_models.AccountingDocs.objects.filter(agreement__project__teamprojects_in_project__user=dbuser).values_list('pk', flat=True)
            all = list(all)
            if pk is not None:
                if int(pk) in all:
                    return function(request, get_access_all, *args, **kwargs)
                else:
                    return redirect(
                        sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain, scheme='https'))
            else:
                return function(request, get_access_all, *args, **kwargs)

        elif access_level == 3:
            def get_access_all():
                all = app_docs_models.Agreement.objects.filter(project__teamprojects_in_project__user__unit=dbuser.unit).order_by('-pk')
                return all

            all = app_docs_models.AccountingDocs.objects.filter(agreement__project__teamprojects_in_project__user__unit=dbuser.unit).values_list('pk', flat=True)
            all = list(all)

            if pk is not None:
                if int(pk) in all:
                    return function(request, get_access_all, *args, **kwargs)
                else:
                    return redirect(
                        sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain, scheme='https'))
            else:
                return function(request, get_access_all, *args, **kwargs)

        else:
            def get_access_all():
                all = app_docs_models.Agreement.objects.all()
                return all

            return function(request, get_access_all, *args, **kwargs)



    return wrap