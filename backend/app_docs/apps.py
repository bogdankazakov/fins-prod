from django.apps import AppConfig


class AppDocsConfig(AppConfig):
    name = 'app_docs'
