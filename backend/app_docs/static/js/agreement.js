$(function () {
  var parent_conditions = function(){
      var id_mycompany = $(".modal-content #id_mycompany");
      var id_subcontractor_role = $(".modal-content #id_subcontractor_role");
      var id_subcontractor = $(".modal-content #id_subcontractor");
      var id_parent = $(".modal-content #id_parent");

      if (!id_parent.val() === false){
          id_subcontractor.prop('hidden', true);
          id_subcontractor_role.prop('hidden', true);
          id_mycompany.prop('hidden', true);

          var url = $(".agreement-form").attr("data-parent");
          console.log('url',url)
          $.ajax({
                      url: url,
                      data: {'pk' :id_parent.val()},
                      type: 'get',
                      dataType: 'json',
                      success: function (data) {
                          id_subcontractor.val(data.parent_sub);
                          id_subcontractor_role.val(data.parent_sub_type);
                          id_mycompany.val(data.parent_mycompany);
                      },
                      error: function () {
                          console.log("error")
                        }
            });

      }
      else{
           id_subcontractor.prop('hidden', false);
          id_subcontractor_role.prop('hidden', false);
          id_mycompany.prop('hidden', false)
      }
  };

    var prolongation_conditions = function(){
        var id_prolongation_type = $(".modal-content #id_prolongation_type");
        var id_prolongation_alert = $(".modal-content #id_prolongation_alert");

        if (id_prolongation_type.val() === '1'){
             id_prolongation_alert.prop('hidden', false)
         }
         else{
             id_prolongation_alert.prop('hidden', true)
         }
    };

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-docs").modal("show");
      },
      success: function (data) {
        $("#modal-docs .modal-content").html(data.html_form);
        parent_conditions();
        prolongation_conditions();
          $(".modal-content #id_parent").on("change", parent_conditions);
          $(".modal-content #id_prolongation_type").on("change", prolongation_conditions);
      }
    });
  };

  var saveForm = function () {

    var form = $(this);
    var formData = new FormData(form[0]);
    var link = new URL(window.location.href);
    var page = link.searchParams.get("page");

    formData.append('page', page);

    $.ajax({
      url: form.attr("action"),
      data: formData,
      type: form.attr("method"),
      dataType: 'json',
      async: true,
      cache: false,
      contentType: false,
      enctype: form.attr("enctype"),
      processData: false,

      success: function (data) {
      if (data.form_is_valid) {
        $("#docs_list tbody").html(data.html_list);
        $("#pagination").html(data.html_pagination);
        $("#messages").html(data.html_partial_project_messages);

        $("#modal-docs").modal("hide");

      }
      else {
        $("#modal-docs .modal-content").html(data.html_form);
      }
      },
      error: function () {
          console.log("error")
        }
    });
    return false;
  };


  // Create agreement
  $(".js-create-agreement").click(loadForm);
  $("#modal-docs").on("submit", ".js-agreement-create-form", saveForm);

  // Update agreement
  $("#docs_list").on("click", ".js-update-agreement", loadForm);
  $("#modal-docs").on("submit", ".js-agreement-update-form", saveForm);

  // Delete agreement
  $("#docs_list").on("click", ".js-delete-agreement", loadForm);
  $("#modal-docs").on("submit", ".js-agreement-delete-form", saveForm);





    var gen = function () {
        var btn = $(this);
        window.location.href = btn.attr("data-url")
  };


  // Gen agreement
  $("#docs_list").on("click", ".js-gen-agreement", gen);






});