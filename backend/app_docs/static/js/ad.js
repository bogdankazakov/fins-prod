$(function () {

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-docs").modal("show");
      },
      success: function (data) {
        $("#modal-docs .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    var formData = new FormData(form[0]);
    var link = new URL(window.location.href);
    var page = link.searchParams.get("page");

    formData.append('page', page);

    $.ajax({
      url: form.attr("action"),
      data: formData,
      type: form.attr("method"),
      dataType: 'json',
      async: true,
      cache: false,
      contentType: false,
      enctype: form.attr("enctype"),
      processData: false,
      success: function (data) {
        if (data.form_is_valid) {
          $("#docs_list tbody").html(data.html_list);
          $("#messages").html(data.html_partial_project_messages);

          $("#modal-docs").modal("hide");
        }
        else {
          $("#modal-docs .modal-content").html(data.html_form);
        }
      },
      error: function () {
          console.log("error")
        }
    });
    return false;
  };


  // Create agreement
  $(".js-create-ad").click(loadForm);
  $("#modal-docs").on("submit", ".js-ad-create-form", saveForm);

  // Update agreement
  $("#docs_list").on("click", ".js-update-ad", loadForm);
  $("#modal-docs").on("submit", ".js-ad-update-form", saveForm);

  // Delete agreement
  $("#docs_list").on("click", ".js-delete-ad", loadForm);
  $("#modal-docs").on("submit", ".js-ad-delete-form", saveForm);



});