$(function () {
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });

  var loadList = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-docs").modal("show");
      },
      success: function (data) {
        $("#modal-docs .modal-content").html(data.html_form);
      }
    });
  };

    // Show history
  $(".js-history-agreement").click(loadList);

});