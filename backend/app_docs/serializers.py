from rest_framework import serializers
from app_transaction import models as app_transaction_models
from app_docs import models as app_docs_models
from app_project import models as app_project_models
from app_setup import models as app_setup_models

def field_creation(model, exclude_fields, extra_fields):
    all_fields = [f.name for f in model._meta.get_fields()]
    result = [item for item in all_fields if item not in exclude_fields] + extra_fields
    return result

class AgreementFileSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_docs_models.AgreementFile
        fields = field_creation(
            app_docs_models.AgreementFile,
            [
                'file'
            ],
            [
                'filename',
                'path',
             ] )


class AgreementSerializer(serializers.ModelSerializer):
    type_display = serializers.ReadOnlyField(source='get_type_display', read_only=True)
    status_display = serializers.ReadOnlyField(source='get_status_display', read_only=True)
    subcontractor_name = serializers.ReadOnlyField(source='subcontractor.name', read_only=True)
    subcontractor_role_display = serializers.ReadOnlyField(source='get_subcontractor_role_display', read_only=True)
    mycompany_display = serializers.ReadOnlyField(source='mycompany.name', read_only=True)
    prolongation_type_display = serializers.ReadOnlyField(source='get_prolongation_type_display', read_only=True)
    project_display = serializers.ReadOnlyField(source='project.full_name', read_only=True)
    transaction_display = serializers.ReadOnlyField(source='transaction.name', read_only=True)
    report_display = serializers.ReadOnlyField(source='report.name', read_only=True)
    doc_template_display = serializers.ReadOnlyField(source='doc_template.name', read_only=True)
    idx = serializers.ReadOnlyField(read_only=True)
    # agreement_files = AgreementFileSerializer(many=True)

    class Meta:
        model = app_docs_models.Agreement
        fields = field_creation(
            app_docs_models.Agreement,
            [
                'history',
                'account_docs',
                'agreement_files',
            ],
            [
                'full_name',
                'type_display',
                'status_display',
                'subcontractor_name',
                'subcontractor_role_display',
                'mycompany_display',
                'prolongation_type_display',
                'project_display',
                'transaction_display',
                'doc_template_display',
                'report_display',
                'has_attachment_display',
                'has_scan_display',
                'has_src_display',
                'idx',
                # 'agreement_files',
             ] )

class SubcontractorSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_transaction_models.Subcontractor
        fields = '__all__'

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = app_project_models.Project
        fields = ('id', 'full_name', 'name')

class TransactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_transaction_models.Transaction
        fields = ('id', 'name')

class ReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_project_models.Report
        fields = ('id', 'name')

class DocTemplateSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_docs_models.DocTemplateFile
        fields = ('id', 'name')


class AgreementShortSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_docs_models.Agreement
        fields = ('id', 'name', 'full_name_with_parents')









class AccountingFileSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_docs_models.ADFile
        fields = field_creation(
            app_docs_models.ADFile,
            [
                'file'
            ],
            [
                'filename',
                'path',
             ] )

class AccountingSerializer(serializers.ModelSerializer):
    type_display = serializers.ReadOnlyField(source='get_type_display', read_only=True)
    agreement_display = serializers.ReadOnlyField(source='agreement.full_name_with_parents', read_only=True)
    status_display = serializers.ReadOnlyField(source='get_status_display', read_only=True)
    subcontractor_display = serializers.ReadOnlyField(source='subcontractor.name', read_only=True)
    mycompany_display = serializers.ReadOnlyField(source='mycompany.name', read_only=True)

    project_display = serializers.ReadOnlyField(source='project.full_name', read_only=True)
    transaction_display = serializers.ReadOnlyField(source='transaction.name', read_only=True)
    report_display = serializers.ReadOnlyField(source='report.name', read_only=True)

    class Meta:
        model = app_docs_models.AccountingDocs
        fields = field_creation(
            app_docs_models.AccountingDocs,
            [
                'history',
                'accounting_docs_files',
            ],
            [
                'type_display',
                'full_name',
                'status_display',
                'agreement_display',
                'has_attachment_display',
                'has_scan_display',
                'has_src_display',
                'subcontractor_display',
                'mycompany_display',
                'project_display',
                'transaction_display',
                'report_display',

             ] )





