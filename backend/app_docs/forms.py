from django.forms import ModelForm
from app_signup import models as app_signup_models
from app_transaction import models as app_transaction_models
from app_docs import models as app_docs_models
from django import forms
from django.forms import modelformset_factory, formset_factory, inlineformset_factory, BaseInlineFormSet
from django.db.models import Q


def lister(all, parents_list, flag):
    '''
    creates a list of dics with its parents
    '''
    for p in parents_list:
        for item in all:
            if item.parent == p.id:
                parents_list.append(item)
                all = all.exclude(pk=item.pk)
                flag = True

    if flag is True:
        return lister(all, parents_list, False)
    else:
        return parents_list


class AgreementForm(ModelForm):

        # if 'using' in kwargs:
        #     print('tut')
        #     db = kwargs.pop('using')
        #     print(db)
        #     # self.fields['parent'].choices = app_docs_models.Agreement.objects.using(db).all()

    class Meta:
        model = app_docs_models.Agreement

        fields = (
            'mycompany',
            'subcontractor_role',

            'subcontractor',
            'name',
            'signed_at',
            'type',
            'validity_period_from',
            'validity_period_till',
            'prolongation_type',
            'prolongation_alert',
            'status',
            'parent',
            'description',
            'project',
            'transaction',
            'report',
            'doc_template',

        )

        widgets = {
            'signed_at': forms.TextInput(attrs={'type': 'date'}),
            'validity_period_from': forms.TextInput(attrs={'type': 'date'}),
            'validity_period_till': forms.TextInput(attrs={'type': 'date'}),
            'prolongation_alert': forms.TextInput(attrs={'type': 'date'}),
            'description': forms.Textarea(),

        }

    def __init__(self, *args, **kwargs):

        using = kwargs.pop('using')
        all = app_docs_models.Agreement.objects.using(using).all()

        try:
            instance = kwargs['instance']
            # prevent editing doc to be in the list of parent docs
            all = all.exclude(id=instance.id)

            #protection against inifinite recursion

            parents_list = [instance,]
            for el in all:
                if el.parent is not None and el.parent == instance.parent:
                    parents_list.append(el)

            list = lister(all, parents_list, False)
            q = Q()
            for parent in list:
                q = q | Q(pk=parent.pk)
            all = all.exclude(q)

        except Exception:
            pass


        PARENT_CHOICES = [('', '-----------'),]
        for el in all:
            choice = (el.id, el.name)
            PARENT_CHOICES.append(choice)
        PARENT_CHOICES = tuple(PARENT_CHOICES)

        super(AgreementForm, self).__init__(*args, **kwargs)



        self.fields['parent'].widget = forms.Select(choices=PARENT_CHOICES)

        sub = app_transaction_models.Subcontractor.objects.all()

        MYCOMPANY_CHOICES = [('', '-----------'),]
        for el in sub.filter(is_my_company=True):
            choice = (el.id, el.name)
            MYCOMPANY_CHOICES.append(choice)
        MYCOMPANY_CHOICES = tuple(MYCOMPANY_CHOICES)
        self.fields['mycompany'].widget = forms.Select(choices=MYCOMPANY_CHOICES)

        SUB_CHOICES = [('', '-----------'),]
        for el in sub.exclude(is_my_company=True):
            choice = (el.id, el.name)
            SUB_CHOICES.append(choice)
        SUB_CHOICES = tuple(SUB_CHOICES)
        self.fields['subcontractor'].widget = forms.Select(choices=SUB_CHOICES)



class AccountingDocsForm(ModelForm):

    class Meta:
        model = app_docs_models.AccountingDocs
        fields = (
            'type',
            'name',
            'signed_at',
            'covered_period_from',
            'covered_period_till',
            'status',
            'status_date',
            'agreement',

        )

        widgets = {
            'signed_at': forms.TextInput(attrs={'type': 'date'}),
            'covered_period_from': forms.TextInput(attrs={'type': 'date'}),
            'covered_period_till': forms.TextInput(attrs={'type': 'date'}),
            'status_date': forms.TextInput(attrs={'type': 'date'}),

        }

# class AgreementFileForm(forms.ModelForm):
#     class Meta:
#         model = app_docs_models.AgreementFile
#         fields = (
#             'file',
#             'comment',
#         )
#

EXTRA = 1

AgreementFileFormSet = modelformset_factory(
    app_docs_models.AgreementFile,
    fields=('file', 'comment', 'type'),
    can_delete=True,
    extra=EXTRA,

)


AccountingFileFormSet = modelformset_factory(
    app_docs_models.ADFile,
    fields=('file', 'comment', 'type'),
    can_delete=True,
    extra=EXTRA,

)