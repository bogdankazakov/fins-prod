from django.contrib import admin
from django.urls import path, include, re_path
from django.contrib.auth import views as auth_views
from app_setup.views import subcontractor, account, subcontractortypes, unit, \
    paypurpose, legalform, taxetype, projectcategory, projectsubcategory, brand, nbsource, \
    nbformat, productcategory, projectproduct, currency, team,profile
from .routers import router

app_name = 'app_setup'
urlpatterns = [

    path('api/subcontractors/', subcontractor.list),
    path('api/subcontractors/<int:pk>/', subcontractor.detail),
    path('api/subcontractors/history/', subcontractor.history),
    path('api/subcontractors/extra/', subcontractor.extra),

    path('api/subcontractortypes/', subcontractortypes.list),
    path('api/subcontractortypes/<int:pk>/', subcontractortypes.detail),
    path('api/subcontractortypes/history/', subcontractortypes.history),

    path('api/units/', unit.list),
    path('api/units/<int:pk>/', unit.detail),
    path('api/units/history/', unit.history),

    path('api/paypurposes/', paypurpose.list),
    path('api/paypurposes/<int:pk>/', paypurpose.detail),
    path('api/paypurposes/history/', paypurpose.history),

    path('api/currencies/', currency.list),
    path('api/currencies/<int:pk>/', currency.detail),
    path('api/currencies/history/', currency.history),
    path('api/rate/<int:pk>/', currency.rate),
    path('api/currencies/default/', currency.default),
    path('api/convert/', currency.convert),

    path('api/accounts/', account.list),
    path('api/accounts/<int:pk>/', account.detail),
    path('api/accounts/history/', account.history),
    path('api/accounts/extra/', account.extra),

    path('api/legalforms/', legalform.list),
    path('api/legalforms/<int:pk>/', legalform.detail),
    path('api/legalforms/history/', legalform.history),

    path('api/taxetypes/', taxetype.list),
    path('api/taxetypes/<int:pk>/', taxetype.detail),
    path('api/taxetypes/history/', taxetype.history),

    path('api/projectcats/', projectcategory.list),
    path('api/projectcats/<int:pk>/', projectcategory.detail),
    path('api/projectcats/history/', projectcategory.history),

    path('api/projectsubcats/', projectsubcategory.list),
    path('api/projectsubcats/<int:pk>/', projectsubcategory.detail),
    path('api/projectsubcats/history/', projectsubcategory.history),
    path('api/projectsubcats/extra/', projectsubcategory.extra),

    path('api/brands/', brand.list),
    path('api/brands/<int:pk>/', brand.detail),
    path('api/brands/history/', brand.history),

    path('api/nbsources/', nbsource.list),
    path('api/nbsources/<int:pk>/', nbsource.detail),
    path('api/nbsources/history/', nbsource.history),

    path('api/nbformats/', nbformat.list),
    path('api/nbformats/<int:pk>/', nbformat.detail),
    path('api/nbformats/history/', nbformat.history),

    path('api/productcategories/', productcategory.list),
    path('api/productcategories/<int:pk>/', productcategory.detail),
    path('api/productcategories/history/', productcategory.history),

    path('api/projectproducts/', projectproduct.list),
    path('api/projectproducts/<int:pk>/', projectproduct.detail),
    path('api/projectproducts/history/', projectproduct.history),
    path('api/projectproducts/extra/', projectproduct.extra),

    path('api/team/', team.list),
    path('api/team/<int:pk>/', team.detail),
    path('api/team/extra/', team.extra),
    path('api/team/resend/<int:pk>/', team.user_resend),
    path('join/<subdomain>/<uidb64>/<token>/', team.join, name='join'),
    # path('users/create/', views.user_create, name='user_create'),
    # path('users/save/', views.user_save, name='user_save'),
    # path('users/update/<pk>', views.user_update, name='user_update'),
    # path('users/delete/<pk>', views.user_delete, name='user_delete'),

    path('api/profile/', profile.detail),
    path('api/profile/main/', profile.detail_main),


    #
    #
    # path('', views.index, name='index'),
    # path('docs_download/', views.docs_download, name='docs_download'),
    # path('users/', views.users_list, name='users'),
    # path('users/create/', views.user_create, name='user_create'),
    # path('users/save/', views.user_save, name='user_save'),
    # path('users/update/<pk>', views.user_update, name='user_update'),
    # path('users/delete/<pk>', views.user_delete, name='user_delete'),
    # path('join/<subdomain>/<uidb64>/<token>/', views.join, name='join'),
    # path('users/user_resend/<pk>', views.user_resend, name='user_resend'),
    #
    # path('profile/', views.profile, name='profile'),
    # path('profile/update', views.profile_update, name='profile_update'),
    #
    #
    #
    #
    #
    # path('units/', views.unit_list, name='units'),
    # path('units/create/', views.unit_create, name='unit_create'),
    # path('units/save/', views.unit_save, name='unit_save'),
    # path('units/update/<pk>', views.unit_update, name='unit_update'),
    # path('units/delete/<pk>', views.unit_delete, name='unit_delete'),
    # # path('units/history', views.unit_history, name='unit_history'),
    #
    # path('purposes/', views.purpose_list, name='purposes'),
    # path('purposes/create/', views.purpose_create, name='purpose_create'),
    # path('purposes/save/', views.purpose_save, name='purpose_save'),
    # path('purposes/update/<pk>', views.purpose_update, name='purpose_update'),
    # path('purposes/delete/<pk>', views.purpose_delete, name='purpose_delete'),
    # path('purposes/history', views.purpose_history, name='purpose_history'),
    #
    # path('currencies/', views.currency_list, name='currencies'),
    # path('currencies/create/', views.currency_create, name='currency_create'),
    # path('currencies/save/', views.currency_save, name='currency_save'),
    # path('currencies/update/<pk>', views.currency_update, name='currency_update'),
    # path('currencies/delete/<pk>', views.currency_delete, name='currency_delete'),
    # path('currencies/history', views.currency_history, name='currency_history'),
    # path('currencies/default', views.currency_default, name='currency_default'),
    # path('currencies/conversion', views.currency_conversion, name='currency_conversion'),
    #
    # path('rate/save/', views.rate_save, name='rate_save'),
    # path('rate/update/<pk>', views.rate_update, name='rate_update'),
    #
    # path('accounts/', views.account_list, name='accounts'),
    # path('accounts/create/', views.account_create, name='account_create'),
    # path('accounts/save/', views.account_save, name='account_save'),
    # path('accounts/update/<pk>', views.account_update, name='account_update'),
    # path('accounts/delete/<pk>', views.account_delete, name='account_delete'),
    # path('accounts/history', views.account_history, name='account_history'),
    #
    # path('legalforms/', views.legalform_list, name='legalforms'),
    # path('legalforms/create/', views.legalform_create, name='legalform_create'),
    # path('legalforms/save/', views.legalform_save, name='legalform_save'),
    # path('legalform/updates/<pk>', views.legalform_update, name='legalform_update'),
    # path('legalform/deletes/<pk>', views.legalform_delete, name='legalform_delete'),
    # path('legalforms/history', views.legalform_history, name='legalform_history'),
    #
    # path('taxetypes/', views.taxetype_list, name='taxetypes'),
    # path('taxetypes/create/', views.taxetype_create, name='taxetype_create'),
    # path('taxetypes/save/', views.taxetype_save, name='taxetype_save'),
    # path('taxetypes/updates/<pk>', views.taxetype_update, name='taxetype_update'),
    # path('taxetypes/deletes/<pk>', views.taxetype_delete, name='taxetype_delete'),
    # path('taxetypes/history', views.taxetype_history, name='taxetype_history'),
    #
    # path('subcontractortypes/', views.subcontractortype_list, name='subcontractortypes'),
    # path('subcontractortypes/create/', views.subcontractortype_create, name='subcontractortype_create'),
    # path('subcontractortypes/save/', views.subcontractortype_save, name='subcontractortype_save'),
    # path('subcontractortypes/updates/<pk>', views.subcontractortype_update, name='subcontractortype_update'),
    # path('subcontractortypes/deletes/<pk>', views.subcontractortype_delete, name='subcontractortype_delete'),
    # path('subcontractortypes/history', views.subcontractortype_history, name='subcontractortype_history'),
    #
    # path('nbsources/', views.nbsource_list, name='nbsources'),
    # path('nbsources/create/', views.nbsource_create, name='nbsource_create'),
    # path('nbsources/save/', views.nbsource_save, name='nbsource_save'),
    # path('nbsources/updates/<pk>', views.nbsource_update, name='nbsource_update'),
    # path('nbsources/deletes/<pk>', views.nbsource_delete, name='nbsource_delete'),
    # path('nbsources/history', views.nbsource_history, name='nbsource_history'),
    #
    # path('nbformats/', views.nbformat_list, name='nbformats'),
    # path('nbformats/create/', views.nbformat_create, name='nbformat_create'),
    # path('nbformats/save/', views.nbformat_save, name='nbformat_save'),
    # path('nbformats/updates/<pk>', views.nbformat_update, name='nbformat_update'),
    # path('nbformats/deletes/<pk>', views.nbformat_delete, name='nbformat_delete'),
    # path('nbformats/history', views.nbformat_history, name='nbformat_history'),
    #
    # path('productcategories/', views.productcategory_list, name='productcategories'),
    # path('productcategories/create/', views.productcategory_create, name='productcategory_create'),
    # path('productcategories/save/', views.productcategory_save, name='productcategory_save'),
    # path('productcategories/updates/<pk>', views.productcategory_update, name='productcategory_update'),
    # path('productcategories/deletes/<pk>', views.productcategory_delete, name='productcategory_delete'),
    # path('productcategories/history', views.productcategory_history, name='productcategory_history'),
    #
    # path('projectproducts/', views.projectproduct_list, name='projectproducts'),
    # path('projectproducts/create/', views.projectproduct_create, name='projectproduct_create'),
    # path('projectproducts/save/', views.projectproduct_save, name='projectproduct_save'),
    # path('projectproducts/updates/<pk>', views.projectproduct_update, name='projectproduct_update'),
    # path('projectproducts/deletes/<pk>', views.projectproduct_delete, name='projectproduct_delete'),
    # path('projectproducts/history', views.projectproduct_history, name='projectproduct_history'),
    #
    # path('projectcats/', views.projectcat_list, name='projectcats'),
    # path('projectcats/create/', views.projectcat_create, name='projectcat_create'),
    # path('projectcats/save/', views.projectcat_save, name='projectcat_save'),
    # path('projectcats/updates/<pk>', views.projectcat_update, name='projectcat_update'),
    # path('projectcats/deletes/<pk>', views.projectcat_delete, name='projectcat_delete'),
    # path('projectcats/history', views.projectcat_history, name='projectcat_history'),
    #
    # path('projectsubcats/', views.projectsubcat_list, name='projectsubcats'),
    # path('projectsubcats/create/', views.projectsubcat_create, name='projectsubcat_create'),
    # path('projectsubcats/save/', views.projectsubcat_save, name='projectsubcat_save'),
    # path('projectsubcats/updates/<pk>', views.projectsubcat_update, name='projectsubcat_update'),
    # path('projectsubcats/deletes/<pk>', views.projectsubcat_delete, name='projectsubcat_delete'),
    # path('projectsubcats/history', views.projectsubcat_history, name='projectsubcat_history'),
    #
    # path('brands/', views.brand_list, name='brands'),
    # path('brands/create/', views.brand_create, name='brand_create'),
    # path('brands/save/', views.brand_save, name='brand_save'),
    # path('brands/updates/<pk>', views.brand_update, name='brand_update'),
    # path('brands/deletes/<pk>', views.brand_delete, name='brand_delete'),
    # path('brands/history', views.brand_history, name='brand_history'),
    #
    # path('doc_template/', views.doc_template_list, name='doc_templates'),
    # path('doc_template/create/', views.doc_template_create, name='doc_template_create'),
    # path('doc_template/save/', views.doc_template_save, name='doc_template_save'),
    # path('doc_template/update/<pk>', views.doc_template_update, name='doc_template_update'),
    # path('doc_template/delete/<pk>', views.doc_template_delete, name='doc_template_delete'),
    # path('doc_template/history', views.doc_template_history, name='doc_template_history'),


    # path('api/', include(router.urls)),

]


