from django.test import TestCase, Client, RequestFactory
from app_signup import models as app_signup_models
from app_transaction import models as app_transaction_models
from app_setup import views as app_setup_views
from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
from django.conf import settings
from fins.threadlocal import thread_local
from django.contrib.messages.storage.fallback import FallbackStorage
import psycopg2
from .test_global import get_db_name_test
from app_setup.tests.test_tools import Setup



class SubcontractorsTest(Setup):


    def test_subcontractors_CRUD(self):

        self.client.login(username='b@b.ru', password='123')

        #create
        link = sub_reverse('app_setup:subcontractor_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestSub'],
                 'type': [1],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response
        response = tread(request)

        created = app_transaction_models.Subcontractor.objects.using(get_db_name_test()).get(name = 'TestSub')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(created)

        #update
        link = sub_reverse('app_setup:subcontractor_update', kwargs={'pk': created.pk} ,subdomain='mysubdomain')
        data = {
                 'name': ['TestSub'],
                 'type': [2],
                 'taxe_type': [1],
                 'legal_form': [1],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.subcontractor_update(request, created.pk)
            return response

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)

        response = tread(request)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(app_transaction_models.Subcontractor.objects.using(get_db_name_test()).get(name='TestSub').type.pk, 2)


        #delete
        link = sub_reverse('app_setup:subcontractor_delete',kwargs={'pk': created.pk}, subdomain='mysubdomain')
        data = {}
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.subcontractor_delete(request, created.pk)
            return response
        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        response = tread(request)

        deleted = app_transaction_models.Subcontractor.objects.using(get_db_name_test()).filter(name = 'TestSub')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)

