from django.test import TestCase, Client, RequestFactory
from app_signup import models as app_signup_models
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from app_setup import views as app_setup_views
from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
from django.conf import settings
from fins.threadlocal import thread_local
from django.contrib.messages.storage.fallback import FallbackStorage
import psycopg2
from .test_global import get_db_name_test
from app_setup.tests.test_tools import Setup



class ProjectSubcatsTest(Setup):


    def test_projectsubcats_CRUD(self):

        self.client.login(username='b@b.ru', password='123')

        #create cat
        link = sub_reverse('app_setup:projectsubcat_create',subdomain='mysubdomain')
        data = {
                 'name': ['projectcat'],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.projectcat_create(request)
            return response
        response = tread(request)


        #create subcat
        link = sub_reverse('app_setup:projectsubcat_create',subdomain='mysubdomain')
        data = {
                 'name': ['projectsubcat'],
                 'category': [int(app_project_models.ProjectCategory.objects.using(get_db_name_test()).all()[0].pk)],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.projectsubcat_create(request)
            return response
        response = tread(request)

        created = app_project_models.ProjectSubcategory.objects.using(get_db_name_test()).get(name = 'projectsubcat')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(created)

        #update
        link = sub_reverse('app_setup:projectsubcat_update', kwargs={'pk': created.pk} ,subdomain='mysubdomain')
        data = {
                 'name': ['projectsubcat2'],
                 'category': [int(app_project_models.ProjectCategory.objects.using(get_db_name_test()).all()[0].pk)],

        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.projectsubcat_update(request, created.pk)
            return response

        response = tread(request)

        update = app_project_models.ProjectSubcategory.objects.using(get_db_name_test()).get(name='projectsubcat2')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(update)


        #delete
        link = sub_reverse('app_setup:projectsubcat_delete',kwargs={'pk': created.pk}, subdomain='mysubdomain')
        data = {}
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.projectsubcat_delete(request, created.pk)
            return response
        response = tread(request)

        deleted = app_project_models.ProjectSubcategory.objects.using(get_db_name_test()).filter(name = 'projectsubcat2')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)

