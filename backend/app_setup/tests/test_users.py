from django.test import TestCase, Client, RequestFactory, modify_settings, override_settings
from django.urls import reverse
from app_signup import models as app_signup_models
from app_setup import views as app_setup_views
from app_transaction import models as app_transaction_models

from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from app_setup.tokens import account_activation_token
from fins.threadlocal import thread_local
from django.contrib.messages.storage.fallback import FallbackStorage
import psycopg2
from .test_global import get_db_name_test
from app_setup import models as app_setup_models
from app_setup.tests.test_tools import Setup




class UsersTest(Setup):


    def test_app_setup_users_invite(self):
        """
        test invite functionality for user that is not in system
        """

        self.client.login(username='b@b.ru', password='123')

        # 1. User is created and invited
        link = sub_reverse('app_setup:user_create', subdomain='mysubdomain')
        data = {
            'first_name': ['first'],
            'last_name': ['last'],
            'email': ['invited@email.com'],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.user_create(request)
            return response

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)

        response = tread(request)

        self.assertEqual(response.status_code, 200)
        invited_user = app_signup_models.User.objects.get(email='invited@email.com')
        self.assertTrue(invited_user)
        self.assertFalse(invited_user.password) #check that password doesn't exists

        self.assertTrue(
            app_transaction_models.DBUser.objects.using(get_db_name_test()).get(
                user_id=invited_user.id)) #check that signal works when user is created

        #2 invite link is correct
        uid = force_text(urlsafe_base64_encode(force_bytes(invited_user.pk)))
        token = account_activation_token.make_token(invited_user)
        subdomain = request.account.subdomain
        link = 'http://'+ get_current_site(request).domain + reverse('app_setup:join', kwargs={
            'uidb64':uid, 'token': token, 'subdomain':subdomain, })
        response = self.client.get(link, follow=True)
        self.assertEqual(response.status_code, 200) #check that link is correct

        #3 User joins the account

        data = {
            'password1': ['firstFIRST11'],
            'password2': ['firstFIRST11'],
        }
        response = self.client.post(reverse('app_setup:join', kwargs={
            'uidb64':uid, 'token': token, 'subdomain':subdomain }), data, follow=True)


        invited_user = app_signup_models.User.objects.get(email='invited@email.com')
        self.assertTrue(invited_user.password) #check that password was created

        request = response.wsgi_request
        self.assertTrue(request.user.is_authenticated)  # check that user is loged in afer redirect

        self.assertRedirects(response,
                             sub_reverse(settings.LOGIN_REDIRECT_URL, subdomain='mysubdomain'),
                             status_code=302, target_status_code=200,
                             msg_prefix='', fetch_redirect_response=True) #check the redirect after password save

    def test_app_setup_users_in_db_invite(self):
        """
        test invite functionality for user that IS IN system but not in the account
        """

        # we create another user related to another company
        c1 = app_signup_models.Account(name='MyCompany2', subdomain='mysubdomain2')
        c1.save()
        c1.create_database()
        c1.fill_database()

        user2 = app_signup_models.User(
            first_name='UserFirst2',
            last_name='UserLast2',
            email='b2@b2.ru')
        user2.set_password('123')
        user2.created_account_pk = c1.pk
        user2.save()
        user2.account.add(c1)


        self.client.login(username='b@b.ru', password='123')

        # User is attached to the inviting account

        self.assertFalse(self.account in user2.account.all()) # we check that invited account isn't related to inviting company

        link = sub_reverse('app_setup:user_create', subdomain='mysubdomain')
        data = {
            'first_name': ['first'],
            'last_name': ['last'],
            'email': ['b2@b2.ru'],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        def get_db_name_test():
            q = app_signup_models.Account.objects.all().order_by('-pk')[0]
            name = 'account_' + str(q.pk - 1)
            return name

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.user_create(request)
            return response

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)

        response = tread(request)

        self.assertTrue(self.account in user2.account.all()) # we check that invited account IS  related to inviting company

        #we try to attach user that is already attached to the account
        link = sub_reverse('app_setup:user_create', subdomain='mysubdomain')
        data = {
            'first_name': ['first'],
            'last_name': ['last'],
            'email': ['b2@b2.ru'],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        def get_db_name_test():
            q = app_signup_models.Account.objects.all().order_by('-pk')[0]
            name = 'account_' + str(q.pk - 1)
            return name

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.user_create(request)
            return response

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)

        response = tread(request)




        # print(response.context['messages'])


