from django.test import TestCase, Client, RequestFactory
from app_signup import models as app_signup_models
from app_transaction import models as app_transaction_models
from app_setup import views as app_setup_views
from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
from django.conf import settings
from fins.threadlocal import thread_local
from django.contrib.messages.storage.fallback import FallbackStorage
import psycopg2
from .test_global import get_db_name_test
import os
from app_docs import models as app_docs_models
import shutil
from app_setup.tests.test_tools import Setup



class DocTemplateTest(Setup):


    def test_doc_template_CRUD(self):

        #create template and load file
        path = os.path.join(settings.BASE_DIR, 'fins/static/docs/template_for_test.docx' )
        myfile = open(path, 'rb')

        #create doc template
        link = sub_reverse('app_setup:doc_template_create',subdomain='mysubdomain')
        data = {
                 'name': ['TestTemplate'],
                 'file': myfile,
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.doc_template_create(request)
            return response


        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)

        response = tread(request)

        template_created = app_docs_models.DocTemplateFile.objects.using(get_db_name_test()).get(name = 'TestTemplate')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(template_created)


        file_path = os.path.join(settings.MEDIA_ROOT, str(template_created.file) )
        try:
            open(file_path, 'r')
            exist = True
        except Exception:
            exist = False
        self.assertTrue(exist)




        #update doc template
        link = sub_reverse('app_setup:doc_template_update', kwargs={'pk': template_created.pk}, subdomain='mysubdomain')
        data = {
                 'name': ['TestTemplateUpdate'],
                }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.doc_template_update(request, template_created.pk)
            return response


        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)

        response = tread(request)

        template_updated = app_docs_models.DocTemplateFile.objects.using(get_db_name_test()).get(name = 'TestTemplateUpdate')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(template_updated)


        #delete doc template
        link = sub_reverse('app_setup:doc_template_delete', kwargs={'pk': template_created.pk},subdomain='mysubdomain')
        data = {}
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_setup_views.doc_template_delete(request,template_created.pk)
            return response


        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)

        response = tread(request)

        template_deleted = app_docs_models.DocTemplateFile.objects.using(get_db_name_test()).filter(name = 'TestTemplate')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(template_deleted), 0)


        file_path = os.path.join(settings.MEDIA_ROOT, str(template_created.file) )
        try:
            open(file_path, 'r')
            exist = True
        except Exception:
            exist = False
        self.assertFalse(exist)



