from rest_framework import routers
from app_setup import viewsets


router = routers.DefaultRouter()
router.register(r'subcontractors', viewsets.SubcontractorViewSet)