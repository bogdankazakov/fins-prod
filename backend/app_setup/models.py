from django.db import models
from django.utils.translation import ugettext_lazy as _
# from app_signup.models import User
from fins.global_functions import IsDeletableMixin, BOOL_CHOICES_YESNO
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import transaction
# from app_transaction.models import Unit as Ui
from app_signup import models as app_signup_models
from simple_history.models import HistoricalRecords
from django.conf import settings
import psycopg2


class Unit(models.Model, IsDeletableMixin):
    name = models.CharField(_('Юнит'), max_length=100, unique=True)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add = True)
    # created_by = models.ForeignKey(DBUser, related_name='unit_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    # modified_by = models.ForeignKey(DBUser, related_name='unit_modified_by', on_delete=models.PROTECT, null=True)
    # history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('юнит')
        verbose_name_plural = _('юниты')

    def __str__(self):
        return self.name


class DBUser(models.Model):

    PERMISSION_TYPE_CHOICES = (
        (0, 'Доступ запрещен'),
        (1, 'Все проекты, где я участник'),
        (2, 'Все'),
        (3, 'Все проекты, где участник с моим юнитом'),
    )

    PERMISSION_TYPE_CHOICES2 = (
        (1, 'Все проекты, где участник'),
        (2, 'Все'),
        (3, 'Все проекты, где мой юнит и участник'),
    )

    PERMISSION_TYPE_CHOICES0 = (
        (2, 'Все'),
    )

    PERMISSION_TYPE_CHOICES3 = (
        (0, 'Да'),
        (1, 'Нет'),
    )

    user_id = models.SmallIntegerField()
    account_id = models.SmallIntegerField(blank=True, null=True)

    access_app_dashboard = models.PositiveSmallIntegerField(_('Доступ к Dashboard'), choices=PERMISSION_TYPE_CHOICES0, default=2)
    access_app_docs = models.PositiveSmallIntegerField(_('Доступ к Документам'), choices=PERMISSION_TYPE_CHOICES, default=1)
    access_app_setup = models.PositiveSmallIntegerField(_('Доступ к Настройкам'), choices=PERMISSION_TYPE_CHOICES3, default=1)
    access_app_project = models.PositiveSmallIntegerField(_('Доступ к Проектам и транзакциям'), choices=PERMISSION_TYPE_CHOICES, default=1)
    access_app_reports = models.PositiveSmallIntegerField(_('Доступ к Отчетам'), choices=PERMISSION_TYPE_CHOICES, default=1)
    access_users = models.PositiveSmallIntegerField(_('Доступ к Пользователям'), choices=PERMISSION_TYPE_CHOICES3, default=1)
    access_can_block = models.PositiveSmallIntegerField(_('Может блокировать редактирование и скрывать'), choices=PERMISSION_TYPE_CHOICES3, default=1)

    is_active = models.BooleanField(_('Активен'), choices = BOOL_CHOICES_YESNO, default=True)
    is_owner = models.BooleanField(_('Владелец'), choices = BOOL_CHOICES_YESNO, default=False)

    unit = models.ForeignKey(Unit, verbose_name = 'Юнит', on_delete=models.PROTECT, blank=True, null=True)

    invited_at = models.DateTimeField(_('Когда приглашен'), null=True)
    invited_by = models.SmallIntegerField(_('Кто пригласил'), null=True)
    joined_at = models.DateTimeField(_('Когда присоединился'), null=True)
    is_joined = models.BooleanField(_('Присоединился'), default=False, choices = BOOL_CHOICES_YESNO)


    def __str__(self):
        # return ''
        return str(app_signup_models.User.objects.get(pk=self.user_id).get_full_name())
    def get_name(self):
        return str(app_signup_models.User.objects.get(pk=self.user_id).get_full_name())

    @receiver(post_save, sender=app_signup_models.User)
    def create_user_profile(sender, instance, created, using, **kwargs):
        if created:
            try:
                with transaction.atomic():
                    DBUser.objects.create(
                        user_id=instance.pk
                    )
            except Exception:
                if instance.is_superuser:
                    pass
                else:
                    db = 'account_' + str(instance.created_account_pk)
                    with transaction.atomic():
                        DBUser.objects.using(db).create(
                            user_id=instance.pk,
                            account_id=instance.created_account_pk
                        )



    def get_main_user(self):
        return app_signup_models.Account.objects.get(pk=self.account_id).users.all().get(pk=self.user_id)


    @property
    def email(self):
        return self.get_main_user().email
    @property
    def first_name(self):
        return self.get_main_user().first_name
    @property
    def last_name(self):
        return self.get_main_user().last_name
    @property
    def invited_by_full(self):
        return app_signup_models.Account.objects.get(pk=self.account_id).users.all().get(pk=self.invited_by).get_full_name()


    # @receiver(post_save, sender=app_signup_models.User)
    # def save_user_profile(sender, instance, **kwargs):
    #     instance.dbuser.save()


