from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
import logging
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from app_setup.serializers import DBUserSerializer, UserSerializer
import math
from app_transaction.serializers import UnitSerializer
from app_signup.forms import JoinSignUpForm
from app_setup.decorators import *
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from app_setup.tokens import account_activation_token
from datetime import datetime
import pytz
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import login
from subdomains.utils import reverse as sub_reverse
from app_setup import models as app_setup_models
from django.db.models import Q

# ===Logger====

logger = logging.getLogger(__name__)


# ===SETUP====


Serializer = DBUserSerializer

model = app_setup_models.DBUser
def get_items():
    qs = app_setup_models.DBUser.objects.all().exclude(is_owner=True).order_by('pk')
    return qs
def get_item(pk):
    qs = app_setup_models.DBUser.objects.get(pk=pk)
    return qs
def get_history():
    qs = app_setup_models.DBUser.history.all()
    return qs

instance_name = 'Команда'
sex = False
# ===========


@api_view(['GET', 'POST'])
@login_required
@access_company_level
@access_users
def list(request):
    """
    List all items or create a new items.
    """
    if request.method == 'GET':


        offset = int(request.GET.get('offset'))
        page = int(request.GET.get('page'))
        searchquery = request.GET.get('searchquery')

        all = get_items()
        # all = all.filter(Q(name__icontains=searchquery) | Q(name__icontains=searchquery))


        start = (page - 1) * offset
        end = page * offset
        items = all[start:end]
        total_pages = math.ceil(all.count() / offset)

        data = {
            'total_pages': total_pages,
            'list': Serializer(items, many=True).data,
        }

        return Response(data)

    elif request.method == 'POST':

        email = request.data['email']
        users = app_signup_models.User.objects.filter(email=email)
        if len(users) == 1:  # works if user is already in systeme
            instance = app_signup_models.User.objects.get(email=email)
            acc_users = app_setup_models.DBUser.objects.using('account_' + str(request.account.pk)).filter(
                user_id=instance.id)
            if len(acc_users) == 1:  # works if user is already in the inviting account
                error={
                    'code': 1,
                    'description': 'User is already invited in this account'
                }
                return Response(error, status=status.HTTP_400_BAD_REQUEST)
            else:  # works for the user who is in the system but not in the account
                instance = app_signup_models.User.objects.get(email=email)
                instance.account.add(request.account)
                app_setup_models.DBUser.objects.using('account_' + str(request.account.pk)).create(
                    user_id=instance.id,
                    invited_at=pytz.utc.localize(datetime.utcnow()),
                    invited_by=request.user.pk)


                subject = 'Activate Your MySite Account'
                message = render_to_string('app_setup/users/user_in_db_invite_email.html', {
                    'user': instance,
                    'link': sub_reverse(settings.LOGIN_REDIRECT_URL, subdomain=request.account.subdomain, scheme='https'),
                })
                instance.email_user(subject, message)

                message={
                    'code': 1,
                    'description': 'User is already in the system and is successfully invited to join your team'
                }

                return Response(message, status=status.HTTP_200_OK)

        else:
            serializer = UserSerializer(data=request.data)
            if serializer.is_valid():
                instance = serializer.save()
                instance.account.add(request.account)
                dbuser = app_setup_models.DBUser.objects.using('account_' + str(request.account.pk)).get(
                    user_id=instance.id)
                dbuser.invited_at = pytz.utc.localize(datetime.utcnow())
                dbuser.account_id = request.account.pk
                dbuser.invited_by = request.user.pk
                dbuser.save(using=('account_' + str(request.account.pk)))
                subject = 'Activate Your MySite Account'
                message = render_to_string('app_setup/users/user_invite_email.html', {
                    'user': instance,
                    'domain': get_current_site(request).domain,
                    'uid': force_text(urlsafe_base64_encode(force_bytes(instance.pk))),
                    'token': account_activation_token.make_token(instance),
                    'subdomain': request.account.subdomain,
                })
                instance.email_user(subject, message)

                message={
                    'code': 2,
                    'description': 'User is successfully invited to join your team'
                }

                return Response(message, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




@api_view(['GET', 'PUT', 'DELETE'])
@login_required
@access_company_level
@access_users
def detail(request, pk):
    """
    Retrieve, update or delete.
    """
    try:
        item = get_item(pk)
    except model.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = Serializer(item)

        new_serializer = {'deletable': True}
        new_serializer.update(serializer.data)
        return Response(new_serializer)

    elif request.method == 'PUT':
        serializer = Serializer(item, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)






@api_view(['GET', ])
@login_required
@access_company_level
@access_users
def extra(request):

    if request.method == 'GET':

        choice_1 = []
        for stat in app_setup_models.DBUser.PERMISSION_TYPE_CHOICES:
            choice_1.append({
                'id': stat[0],
                'name': stat[1],
            })
        choice_2 = []
        for stat in app_setup_models.DBUser.PERMISSION_TYPE_CHOICES2:
            choice_2.append({
                'id': stat[0],
                'name': stat[1],
            })
        choice_3 = []
        for stat in app_setup_models.DBUser.PERMISSION_TYPE_CHOICES0:
            choice_3.append({
                'id': stat[0],
                'name': stat[1],
            })
        choice_4 = []
        for stat in app_setup_models.DBUser.PERMISSION_TYPE_CHOICES3:
            choice_4.append({
                'id': stat[0],
                'name': stat[1],
            })

        units = app_setup_models.Unit.objects.all()

        extra = {
            'units': UnitSerializer(units, many=True).data,
            'choice_1': choice_1,
            'choice_2': choice_2,
            'choice_3': choice_3,
            'choice_4': choice_4,
        }
        data = {'extra': extra, }
        return Response(data)



@api_view(['POST', ])
@login_required
@access_company_level
@access_users
def user_resend(request, pk):
    dbuser = get_object_or_404(app_setup_models.DBUser, pk=pk)
    instance = get_object_or_404(app_signup_models.User, pk=dbuser.user_id)
    if request.method == 'POST':
        subject = 'RECAP Activate Your MySite Account'
        message = render_to_string('app_setup/users/user_invite_email.html', {
            'user': instance,
            'domain': get_current_site(request).domain,
            'uid': force_text(urlsafe_base64_encode(force_bytes(instance.pk))),
            'token': account_activation_token.make_token(instance),
            'subdomain': request.account.subdomain,
        })
        instance.email_user(subject, message)
        dbuser.invited_at = pytz.utc.localize(datetime.utcnow())
        dbuser.save()
        return Response(status=status.HTTP_200_OK)


def join(request, uidb64, token, subdomain):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = app_signup_models.User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, app_signup_models.User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        if request.method == 'POST':
            pass_form = JoinSignUpForm(request.POST, instance=user)
            if pass_form.is_valid():
                instance = pass_form.save(commit=False)
                pass_form.save()
                account = app_signup_models.Account.objects.get(subdomain=subdomain)
                dbuser = app_setup_models.DBUser.objects.using('account_' + str(account.pk)).get(
                    user_id=instance.id)
                dbuser.joined_at = pytz.utc.localize(datetime.utcnow())
                dbuser.is_joined = True
                dbuser.save(using=('account_' + str(account.pk)))
                login(request, user)
                return redirect(sub_reverse(settings.LOGIN_REDIRECT_URL, subdomain=subdomain, scheme='https'))
        else:
            pass_form = JoinSignUpForm(instance=user)
        return render(request, 'app_setup/users/user_join_password.html',
                      {'pass_form': pass_form})
    else:
        return render(request, 'app_setup/users/account_activation_invalid.html')



from app_signup import models as app_signup_models
from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required

from django.http import JsonResponse

from django.contrib import messages
import logging

from django.db import transaction






#
# @login_required
# @access_company_level
# @access_users
# def user_save(request, form_user, template_name):
#     data = dict()
#     if request.method == 'POST':
#         email = request.POST.get('email')
#         users = app_signup_models.User.objects.filter(email=email)
#         if len(users) == 1: # works if user is already in systeme
#                 instance = app_signup_models.User.objects.get(email=email)
#                 acc_users = app_setup_models.DBUser.objects.using('account_' + str(request.account.pk)).filter(
#                     user_id=instance.id)
#                 if len(acc_users) == 1:# works if user is already in the inviting account
#                     messages.error(request, 'Пользователь уже приглашен в аккаунт')
#                 else:  # works for the user who is in the system but not in the account
#                     instance = app_signup_models.User.objects.get(email=email)
#                     instance.account.add(request.account)
#                     app_setup_models.DBUser.objects.using('account_' + str(request.account.pk)).create(
#                         user_id=instance.id,
#                         invited_at=pytz.utc.localize(datetime.utcnow()),
#                         invited_by=request.user.pk)
#
#                     data['form_is_valid'] = True
#                     context = {
#                         'users_list': get_all_user(request),
#                         'wording': wording,
#                     }
#                     data['html_users_list'] = render_to_string('app_setup/users/partial_users_list.html', context)
#                     subject = 'Activate Your MySite Account'
#                     message = render_to_string('app_setup/users/user_in_db_invite_email.html', {
#                         'user': instance,
#                         'link': sub_reverse(settings.LOGIN_REDIRECT_URL, subdomain=request.account.subdomain),
#                     })
#                     instance.email_user(subject, message)
#
#         else:
#
#             if form_user.is_valid():
#                 instance = form_user.save(commit=False)
#                 instance.save()
#                 instance.account.add(request.account)
#                 dbuser = app_setup_models.DBUser.objects.using('account_' + str(request.account.pk)).get(
#                     user_id=instance.id)
#                 dbuser.invited_at = pytz.utc.localize(datetime.utcnow())
#                 dbuser.invited_by = request.user.pk
#                 dbuser.save(using=('account_' + str(request.account.pk)))
#                 data['form_is_valid'] = True
#                 context = {
#                     'users_list': get_all_user(request),
#                     'wording': wording,
#                 }
#                 data['html_users_list'] = render_to_string('app_setup/users/partial_users_list.html', context)
#                 subject = 'Activate Your MySite Account'
#                 message = render_to_string('app_setup/users/user_invite_email.html', {
#                     'user': instance,
#                     'domain': get_current_site(request).domain,
#                     'uid': force_text(urlsafe_base64_encode(force_bytes(instance.pk))),
#                     'token': account_activation_token.make_token(instance),
#                     'subdomain': request.account.subdomain,
#                 })
#                 instance.email_user(subject, message)
#             else:
#                 messages.error(request, 'Ошибка валидации формы')
#
#     if request.method == 'GET':
#         data['form_is_valid'] = False
#     context = {'form_user': form_user}
#     data['html_form'] = render_to_string(template_name, context, request=request)
#     return JsonResponse(data)










#
# @login_required
# @access_company_level
# @access_users
# def user_delete(request, pk):
#     data = dict()
#     usr = get_object_or_404(app_setup_models.DBUser, pk=pk)
#     if request.method == 'POST':
#         usr.delete()
#         data['form_is_valid'] = True
#         context = {
#             'users_list':  get_all_user(request),
#             'wording': wording,
#
#         }
#         data['html_users_list'] = render_to_string('app_setup/users/partial_users_list.html', context)
#     if request.method == 'GET':
#
#         context = {'usr': usr}
#         data['html_form'] = render_to_string('app_setup/users/partial_user_delete.html',
#                                              context,
#                                              request=request,
#                                              )
#     return JsonResponse(data)




