from app_project import models as app_project_models
from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_setup.decorators import *
from app_setup.forms import *
from django.shortcuts import get_object_or_404, redirect, render
import logging
from fins.global_functions import history_action_verbose, history_record
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from app_setup.serializers import SubcontractorSerializer
from datetime import datetime
import pytz
import math
from rest_framework import serializers


# ===Logger====

logger = logging.getLogger(__name__)


# ===SETUP====

class ProjectCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = app_project_models.ProjectCategory
        fields = '__all__'

Serializer = ProjectCategorySerializer

model = app_project_models.ProjectCategory
def get_items():
    qs = app_project_models.ProjectCategory.objects.all()
    return qs
def get_item(pk):
    qs = app_project_models.ProjectCategory.objects.get(pk=pk)
    return qs
def get_history():
    qs = app_project_models.ProjectCategory.history.all()
    return qs

instance_name = 'Категория проекта'
sex = False
# ===========


@api_view(['GET', 'POST'])
@login_required
@access_company_level
@access_app_setup
def list(request):
    """
    List all items or create a new items.
    """
    if request.method == 'GET':


        offset = int(request.GET.get('offset'))
        page = int(request.GET.get('page'))
        searchquery = request.GET.get('searchquery')

        all = get_items()
        all = all.filter(name__icontains=searchquery)

        start = (page - 1) * offset
        end = page * offset
        items = all[start:end]
        total_pages = math.ceil(all.count() / offset)

        data = {
            'total_pages': total_pages,
            'list': Serializer(items, many=True).data,
        }

        return Response(data)

    elif request.method == 'POST':
        serializer = Serializer(data=request.data)
        if serializer.is_valid():
            serializer.save(
                created_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
@login_required
@access_company_level
@access_app_setup
def detail(request, pk):
    """
    Retrieve, update or delete.
    """
    try:
        item = get_item(pk)
    except model.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = Serializer(item)

        deletable, related_obj = item.can_delete()
        new_serializer = {'deletable': deletable}
        new_serializer.update(serializer.data)
        return Response(new_serializer)

    elif request.method == 'PUT':
        serializer = Serializer(item, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', ])
@login_required
@access_company_level
@access_app_setup
def history(request):
    """
    Retrieve
    """
    if request.method == 'GET':
        history = get_history()[:35]
        records = history_record(history, instance_name, sex)
        data = {'records': records}
        return Response(data)



