from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_setup.decorators import *
from app_setup.forms import *
from app_docs import models as app_docs_models
from django.shortcuts import get_object_or_404, redirect, render
import logging
import os
import zipfile
from io import BytesIO
from app_setup.tools import *
from django.http import HttpResponse

logger = logging.getLogger(__name__)


@login_required
@access_company_level
@access_app_setup
def index(request):
    context = {}
    return render(request, 'app_setup/index.html', context)



@login_required
@access_company_level
@access_app_setup
def docs_download(request):
    # Files (local path) to put in the .zip
    #if 0 = arg if 1 = ad
    filenames=[]
    qs = app_docs_models.AgreementFile.objects.all()
    for q in qs:
        link = os.path.join(settings.MEDIA_ROOT, str(q.file))
        doc_id = q.agreement_id
        filenames.append((link,doc_id,0))
    qs = app_docs_models.ADFile.objects.all()
    for q in qs:
        link = os.path.join(settings.MEDIA_ROOT, str(q.file))
        doc_id = q.ad_id
        filenames.append((link,doc_id,1))

    # Folder name in ZIP archive which contains the above files
    # E.g [thearchive.zip]/somefiles/file2.txt
    zip_subdir = "DocsArchive"
    zip_filename = "%s.zip" % zip_subdir

    # Open StringIO to grab in-memory ZIP contents
    in_memory = BytesIO()

    # The zip compressor
    zf = zipfile.ZipFile(in_memory, "w")



    for fpath in filenames:
        # Calculate path for file in zip
        fdir, fname = os.path.split(fpath[0])
        if fpath[2] == 0:
            agr_all = app_docs_models.Agreement.objects.all()
            agr_linked = agr_all.get(pk=fpath[1])
        if fpath[2] == 1:
            ad_all = app_docs_models.AccountingDocs.objects.all()
            ad_linked = ad_all.get(pk=fpath[1])
            agr_linked = ad_linked.agreement

        folder_role = agr_linked.get_subcontractor_role_display()
        folder_sub = str(agr_linked.subcontractor.name) + ', ' + str(agr_linked.subcontractor.legal_form)
        path_list = [zip_subdir, folder_role, folder_sub]

        # putting doc in parents folders
        list = lister(agr_all, [], agr_linked)
        folder_agr(list,path_list)

        path_list.append(fname)

        zip_path = os.path.join(*path_list)

        # Add file, at correct path
        zf.write(fpath[0], zip_path)

    # Must close zip for all contents to be written
    zf.close()


    # Grab ZIP file from in-memory, make response with correct MIME-type
    resp = HttpResponse(in_memory.getvalue(), content_type = "application/x-zip-compressed")
    # ..and correct content-disposition
    resp['Content-Disposition'] = 'attachment; filename=%s' % zip_filename

    return resp
    # context = {}
    # return render(request, 'app_setup/index.html', context)