from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
import logging
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from app_setup.serializers import DBUserSerializer, UserSerializer
import math
from app_transaction.serializers import UnitSerializer
from app_signup.forms import JoinSignUpForm
from app_setup.decorators import *
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from app_setup.tokens import account_activation_token
from datetime import datetime
import pytz
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import login
from subdomains.utils import reverse as sub_reverse
from app_setup import models as app_setup_models
from app_signup import models as app_app_signup
from django.db.models import Q

# ===Logger====

logger = logging.getLogger(__name__)


# ===SETUP====


Serializer = DBUserSerializer

model = app_setup_models.DBUser
def get_items():
    qs = app_setup_models.DBUser.objects.all().exclude(is_owner=True).order_by('pk')
    return qs
def get_item(pk):
    qs = app_setup_models.DBUser.objects.get(user_id=pk)
    return qs
def get_history():
    qs = app_setup_models.DBUser.history.all()
    return qs

instance_name = 'Мой профиль'
sex = True
# ===========




@api_view(['GET',])
@login_required
@access_company_level
@access_users
def detail(request):
    """
    Retrieve, update or delete.
    """
    try:
        item = get_item(request.user.pk)
    except model.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = Serializer(item)
        return Response(serializer.data)

@api_view(['GET', 'PUT'])
@login_required
@access_company_level
@access_users
def detail_main(request):
    """
    Retrieve, update or delete.
    """
    try:
        item = app_app_signup.User.objects.get(pk = request.user.pk)
    except app_app_signup.User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = UserSerializer(item)
        return Response(serializer.data)
    elif request.method == 'PUT':
        serializer = UserSerializer(item, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)






