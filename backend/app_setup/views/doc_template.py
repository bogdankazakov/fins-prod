from app_transaction import models as app_transaction_models
from app_docs import models as app_docs_models
from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_setup.decorators import *
from django.http import JsonResponse
from app_setup.forms import *
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib import messages
import logging
from app_setup.tools import log_data2, form_file_enrich
from django.urls import reverse_lazy
from fins.global_functions import history_action_verbose, history_record
from fins.global_functions import get_db_name
import os


logger = logging.getLogger(__name__)

# ===SETUP====



Form = DocTemplateForm
wording = {
    'btn_create': 'Создать',
    'btn_add': 'Добавить шаблон ',
    'btn_delete': 'Удалить',
    'btn_cancel': 'Отмена',
    'btn_update': 'Редактировать',
    'btn_save': 'Сохранить',
    'btn_history': 'История',
    'h1_list': 'Список шаблонов',
    'h1_create': 'Создать шаблон',
    'h1_update': 'Редактировать шаблон',
    'h1_delete': 'Удалить шаблон',
    'h1_history': 'История изменений шаблонов',
    'validation_error': 'Ошибка валидации',
    'not_deletable': 'Мы не можем удалить элемент - он используется в системе.',
    'approve_delete': ' Вы уверены, что хотите удалить шаблон ',
}
templates = {
    'list': 'app_setup/doc_templates/doc_templates.html',
    'partial_list': 'app_setup/doc_templates/partial_list.html',
    'create': 'app_setup/doc_templates/partial_create.html',
    'update': 'app_setup/doc_templates/partial_update.html',
    'delete': 'app_setup/doc_templates/partial_delete.html',
    'history': 'app_setup/doc_templates/partial_history.html',
}

js = {
    'jscreate': 'js-create-doc_template',
    'jshistory': 'js-history-doc_template',

}
url = {
    'create': reverse_lazy('app_setup:doc_template_create'),
    'history': reverse_lazy('app_setup:doc_template_history'),
}
def get_all():
    qs = app_docs_models.DocTemplateFile.objects.all()
    return qs

def get_item(pk):
    item = get_object_or_404(app_docs_models.DocTemplateFile, pk=pk)
    return item


# ===========


@login_required
@access_company_level
@access_app_setup
def doc_template_list(request):
    context = {
        'list': get_all(),
        'wording': wording,
        'templates': templates,
        'js': js,
        'url': url,
    }
    return render(request, templates['list'], context)




@login_required
@access_company_level
@access_app_setup
def doc_template_create(request):
    if request.method == 'POST':
        form = Form(request.POST, request.FILES)
    else:
        form = Form()
    return doc_template_save(request, form, templates['create'])



@login_required
@access_company_level
@access_app_setup
def doc_template_save(request, form, template_name):
    data = dict()
    form_file_enrich(form)
    if request.method == 'POST':
        if form.is_valid():
            if form.cleaned_data['file'] != None:
                file = form.save(commit=False)
                log_data2(file, request)
                file.account = request.account
                file.save()

            data['form_is_valid'] = True
            context = {
                'list': get_all(),
                'wording': wording,
                'js': js,
                'url': url,
            }
            data['html_list'] = render_to_string(templates['partial_list'], context)
        else:
            messages.error(request, wording['validation_error'])

    if request.method == 'GET':
        data['form_is_valid'] = False
    context = {
                'form': form,
                'wording': wording,
                'js': js,
                'url': url,
               }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
@access_company_level
@access_app_setup
def doc_template_update(request, pk):
    item = get_item(pk)
    if request.method == 'POST':
        form = Form(request.POST, request.FILES, instance=item)
    else:
        form = Form(instance=item)
    return doc_template_save(request, form, templates['update'])

@login_required
@access_company_level
@access_app_setup
def doc_template_delete(request, pk):
    data = dict()
    item = get_item(pk)
    if request.method == 'POST':
        os.remove(os.path.join(settings.MEDIA_ROOT, str(item.file)))
        item.delete()
        data['form_is_valid'] = True
        context = {
            'list': get_all(),
            'wording': wording,
            'js': js,
            'url': url,
        }
        data['html_list'] = render_to_string(templates['partial_list'], context)
    if request.method == 'GET':
        context = {
            'item': item,
            'wording': wording,
            'js': js,
            'url': url,
        }
        data['html_form'] = render_to_string(templates['delete'],context,request=request,)
    return JsonResponse(data)




@login_required
@access_company_level
@access_app_setup
def doc_template_history(request):

    history = app_docs_models.DocTemplateFile.history.all()[:3]
    print(history)
    context = {
        'records': history_record(history, '', True),
        'wording':wording,
    }

    data = {}
    data['html_form'] = render_to_string(templates['history'], context, request=request)

    return JsonResponse(data)
