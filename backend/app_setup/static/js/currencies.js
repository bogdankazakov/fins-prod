$(function () {


  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-global").modal("show");
      },
      success: function (data) {
        $("#modal-global .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#currencies_list tbody").html(data.html_list);
          $("#rates_list tbody").html(data.html_list_rate);

          $("#modal-global").modal("hide");
        }
        else {
          $("#modal-global .modal-content").html(data.html_form);
        }
      },
      error: function () {
          console.log("error")
        }
    });
    return false;
  };


  // Create subcontractor
  $(".js-create-currency").click(loadForm);
  $("#modal-global").on("submit", ".js-currency-create-form", saveForm);

  // Update subcontractor
  $("#currencies_list").on("click", ".js-update-currency", loadForm);
  $("#modal-global").on("submit", ".js-currency-update-form", saveForm);

  // Delete subcontractor
  $("#currencies_list").on("click", ".js-delete-currency", loadForm);
  $("#modal-global").on("submit", ".js-currency-delete-form", saveForm);


  var loadList = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-global").modal("show");
      },
      success: function (data) {
        $("#modal-global .modal-content").html(data.html_form);
      }
    });
  };

  // Show history
  $(".js-history-currency").click(loadList);

  var def_saver = function (){
    $.ajax({
      url: $("#default_currency").attr("data-url"),
      data: {
        'value': $(this).val()
      },

      success: function (data) {

      }
    });

  };

    // default currency
  $("#id_default").change(def_saver);

    // currency converter
  var convert = function(event){
      var form = $("#conversion_form");
      $.ajax({
          url: form.attr("action"),
          data: form.serialize(),
          type: form.attr("method"),
          dataType: 'json',
          beforeSend: function () {
              event.preventDefault();
          },
          success: function (data) {
              $("#conversion_result").html(data.html);
          }
       });
  };

  $("#conversion_form").on("submit", convert);


    // rate
  var ratesaveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#rates_list tbody").html(data.html_list);
          $("#modal-global").modal("hide");
        }
        else {
          $("#modal-global .modal-content").html(data.html_form);
        }
      },
      error: function () {
          console.log("error")
        }
    });
    return false;
  };

  // Update rate
  $("#rates_list").on("click", ".js-update-rate", loadForm);
  $("#modal-global").on("submit", ".js-rate-update-form", ratesaveForm);


});