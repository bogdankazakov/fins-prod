$(function () {

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-global").modal("show");
      },
      success: function (data) {
        $("#modal-global .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#subcontractortypes_list tbody").html(data.html_list);
          $("#modal-global").modal("hide");
        }
        else {
          $("#modal-global .modal-content").html(data.html_form);
        }
      },
      error: function () {
          console.log("error")
        }
    });
    return false;
  };


  // Create subcontractor
  $(".js-create-subcontractortype").click(loadForm);
  $("#modal-global").on("submit", ".js-subcontractortype-create-form", saveForm);

  // Update subcontractor
  $("#subcontractortypes_list").on("click", ".js-update-subcontractortype", loadForm);
  $("#modal-global").on("submit", ".js-subcontractortype-update-form", saveForm);

  // Delete subcontractor
  $("#subcontractortypes_list").on("click", ".js-delete-subcontractortype", loadForm);
  $("#modal-global").on("submit", ".js-subcontractortype-delete-form", saveForm);


  var loadList = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-global").modal("show");
      },
      success: function (data) {
        $("#modal-global .modal-content").html(data.html_form);
      }
    });
  };

    // Show history
  $(".js-history-subcontractortype").click(loadList);

});