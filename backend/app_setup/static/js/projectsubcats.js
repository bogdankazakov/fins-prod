$(function () {

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-global").modal("show");
      },
      success: function (data) {
        $("#modal-global .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#projectsubcats_list tbody").html(data.html_list);
          $("#modal-global").modal("hide");
        }
        else {
          $("#modal-global .modal-content").html(data.html_form);
        }
      },
      error: function () {
          console.log("error")
        }
    });
    return false;
  };


  // Create subcontractor
  $(".js-create-projectsubcat").click(loadForm);
  $("#modal-global").on("submit", ".js-projectsubcat-create-form", saveForm);

  // Update subcontractor
  $("#projectsubcats_list").on("click", ".js-update-projectsubcat", loadForm);
  $("#modal-global").on("submit", ".js-projectsubcat-update-form", saveForm);

  // Delete subcontractor
  $("#projectsubcats_list").on("click", ".js-delete-projectsubcat", loadForm);
  $("#modal-global").on("submit", ".js-projectsubcat-delete-form", saveForm);


  var loadList = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-global").modal("show");
      },
      success: function (data) {
        $("#modal-global .modal-content").html(data.html_form);
      }
    });
  };

    // Show history
  $(".js-history-projectsubcat").click(loadList);

});