$(function () {

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-global").modal("show");
      },
      success: function (data) {
        $("#modal-global .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#projectcats_list tbody").html(data.html_list);
          $("#modal-global").modal("hide");
        }
        else {
          $("#modal-global .modal-content").html(data.html_form);
        }
      },
      error: function () {
          console.log("error")
        }
    });
    return false;
  };


  // Create subcontractor
  $(".js-create-projectcat").click(loadForm);
  $("#modal-global").on("submit", ".js-projectcat-create-form", saveForm);

  // Update subcontractor
  $("#projectcats_list").on("click", ".js-update-projectcat", loadForm);
  $("#modal-global").on("submit", ".js-projectcat-update-form", saveForm);

  // Delete subcontractor
  $("#projectcats_list").on("click", ".js-delete-projectcat", loadForm);
  $("#modal-global").on("submit", ".js-projectcat-delete-form", saveForm);


  var loadList = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-global").modal("show");
      },
      success: function (data) {
        $("#modal-global .modal-content").html(data.html_form);
      }
    });
  };

    // Show history
  $(".js-history-projectcat").click(loadList);

});