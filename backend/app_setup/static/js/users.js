$(function () {

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-user").modal("show");
      },
      success: function (data) {
        $("#modal-user .modal-content").html(data.html_form);
      }
    });
  };

  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#users_list").html(data.html_users_list);
          $("#modal-user").modal("hide");
        }
        else {
          $("#modal-user .modal-content").html(data.html_form);
        }
      },
      error: function () {
          console.log("error")
        }
    });
    return false;
  };


  // Create user
  $(".js-create-user").click(loadForm);
  $("#modal-user").on("submit", ".js-user-create-form", saveForm);

  // Update user
  $("#users_list").on("click", ".js-update-user", loadForm);
  $("#modal-user").on("submit", ".js-user-update-form", saveForm);

  // Delete user
  $("#users_list").on("click", ".js-delete-user", loadForm);
  $("#modal-user").on("submit", ".js-user-delete-form", saveForm);




  // Resend ========================
    var resend = function(event){
      var form = $("#sendagain");
      $.ajax({
          url: form.attr("action"),
          data: form.serialize(),
          type: form.attr("method"),
          dataType: 'json',
          beforeSend: function () {
              event.preventDefault();
          },
          success: function (data) {
              console.log('succccccc')
              $("#resend_message").html('Приглашение высланно повторно');

          }
       });
  };


  // Send invitation again
  $(".js-resend-user").click(resend);

});