from app_setup import models as app_setup_models
from app_transaction import models as app_transaction_models
import os
from django.conf import settings
import pytz
from datetime import datetime

def get_db_user(request):
    upk = request.user.pk
    db_user = app_setup_models.DBUser.objects.get(user_id=upk)
    return db_user

def log_data(instance, request):
    db_user = get_db_user(request)
    if instance.created_by is None:
        instance.created_by = db_user
    instance.modified_by = db_user
    instance.modified_at = pytz.utc.localize(datetime.utcnow())
    return instance, db_user


def log_data2(instance, request):
    db_user = get_db_user(request)
    instance.uploaded_by = db_user
    return instance, db_user

def lister(agr_all, array, agr_linked):

    '''
    creates a list of dics with its parents
    '''

    if agr_linked.parent is None:
        array.append(agr_linked)
        l = list(reversed(array))
        return l
    else:
        array.append(agr_linked)
        agr_linked_parent = agr_all.get(pk=agr_linked.parent)
        return lister(agr_all, array, agr_linked_parent)


def folder_agr(list, path_list):
    '''
    takes the list of doc with its parents and add it to pathlist
    '''
    for el in list:
        path_list.append(name(el))
    return path_list


def name(agr_linked):
    folder_agr_linked = str(agr_linked.get_type_display()) + ' №' + str(agr_linked.name) + ' от ' \
                        + str(agr_linked.signed_at)
    return folder_agr_linked

def currency_rate_builder(instance):
    currencies = app_transaction_models.Currency.objects.exclude(pk=instance.pk)
    for currency in currencies:
        app_transaction_models.DefaultRate(
            target=currency,
            source=instance
        ).save()

def currency_rate_killer(item):
    rates = app_transaction_models.DefaultRate.objects.all()
    for rate in rates:
        if rate.target == item or rate.source == item:
            rate.delete()

def delete_checker(item):
    for rel in item._meta.get_fields():
        if not item.is_deletable:
            return False, None
        try:
            # check if there is a relationship with at least one related object
            related = rel.related_model.objects.filter(**{rel.field.name: item})
            if related.exists():
                for el in related:
                    if el._meta.model.__name__ != 'DefaultRate':
                        # if there is return a Tuple of flag = False the related_model object
                        return False, related
        except AttributeError:  # an attribute error for field occurs when checking for AutoField
            pass  # just pass as we dont need to check for AutoField
    return True, None

def form_file_enrich(form):
    if form.instance.file:
        form.path = os.path.join(settings.MEDIA_URL, str(form.instance.file) )
        form.filename = str(form.instance.file).split('/', 2)[2]



