
$(function () {
    var modal = $("#modal-global");
    var projects_list = $("#projects_list");

    var autonum_init_estimation = function () {

        id_estimate_presale_outcome_ext = new AutoNumeric(document.getElementById("id_estimate_presale_outcome_ext"), autoNumericOptions);
        id_estimate_presale_outcome_int = new AutoNumeric(document.getElementById("id_estimate_presale_outcome_int"), autoNumericOptions);
        id_estimate_inprogress_income_before_taxes = new AutoNumeric(document.getElementById("id_estimate_inprogress_income_before_taxes"), autoNumericOptions);
        id_estimate_inprogress_gross_profit_before_taxes = new AutoNumeric(document.getElementById("id_estimate_inprogress_gross_profit_before_taxes"), autoNumericOptions);
        id_estimate_inprogress_project_profit_before_taxes = new AutoNumeric(document.getElementById("id_estimate_inprogress_project_profit_before_taxes"), autoNumericOptions);

    };



    var agreement_format_conditions = function () {
        var agreement_format = $(".modal-content #id_agreement_format");
        var agreement_format_label = $("label[for='" + agreement_format.attr('id') + "']");
        var commission = $(".modal-content #id_commission");
        var commission_label = $("label[for='" + commission.attr('id') + "']");

        if (agreement_format.val() === '1'){
            commission.prop('hidden', false);
            commission_label.prop('hidden', false);
            commission.prop('required', true);
        }
        else{
            commission.prop('hidden', true);
            commission_label.prop('hidden', true);
            commission.prop('required', false);
        }

    };

    var category_conditions = function () {
        var id_category = $(".modal-content #id_category");
        var id_subcategory = $(".modal-content #id_subcategory");
        var brand = $(".modal-content #id_brand");
        var brand_label = $("label[for='" + brand.attr('id') + "']");
        var argeement_type = $(".modal-content #id_agreement_format");
        var argeement_type_label = $("label[for='" + argeement_type.attr('id') + "']");
        var work_format = $(".modal-content #id_work_format");
        var work_format_label = $("label[for='" + work_format.attr('id') + "']");

        if (id_category.val() === '1'){
            brand.prop('required', true);
            brand.prop('hidden', false);
            brand_label.prop('hidden', false);
            argeement_type.prop('required', true);
            argeement_type.prop('hidden', false);
            argeement_type_label.prop('hidden', false);
            work_format.prop('required', true);
            work_format.prop('hidden', false);
            work_format_label.prop('hidden', false);
            modal.on("change", "#id_agreement_format", agreement_format_conditions);

                }
        else{
            brand.prop('required', false);
            brand.prop('hidden', true);
            brand_label.prop('hidden', true);
            argeement_type.prop('required', false);
            argeement_type.prop('hidden', true);
            argeement_type_label.prop('hidden', true);
            work_format.prop('required', false);
            work_format.prop('hidden', true);
            work_format_label.prop('hidden', true);
            }

        var url = $("#projectForm").attr("data-sub-url");
        var categoryId = id_category.val();
        $.ajax({
            url: url,
            data: {
                'category': categoryId
            },
            success: function (data) {
                id_subcategory.html(data);

            }
        });

    };




    var gross_profit_conditions = function () {
            var income = id_estimate_inprogress_income_before_taxes.getNumericString();
            var gross_profitability = $(".modal-content #id_estimate_inprogress_gross_profitability_before_taxes");
            var result = income * gross_profitability.val()  / 100;
            id_estimate_inprogress_gross_profit_before_taxes.set(result)
        };

    var project_profit_conditions = function () {
            var income = id_estimate_inprogress_income_before_taxes.getNumericString();
            var project_profitability = $(".modal-content #id_estimate_inprogress_project_profitability_before_taxes");
            var result = income * project_profitability.val()  / 100;
            id_estimate_inprogress_project_profit_before_taxes.set(result)
        };


   // modal load functions ================================

    var loadForm_project = function (event) {
        var btn = $(this);
        var url = btn.attr("data-url");
        $.ajax({
          url: url,
          type: 'get',
          dataType: 'json',
          beforeSend: function () {
            $("#modal-global").modal("show");
          },
          success: function (data) {
            $("#modal-global .modal-content").html(data.html_form);
            agreement_format_conditions();
            try {
              if (event.data.is_create === true){
                  category_conditions();
                  modal.on("change", "#id_category", category_conditions);}
            } catch (err) {}
            try {
              if (event.data.is_estimation === true){
                  autonum_init_estimation();


                    var gross_profit = $(".modal-content #id_estimate_inprogress_gross_profit_before_taxes");
                    var project_profit = $(".modal-content #id_estimate_inprogress_project_profit_before_taxes");
                    gross_profit.prop('readonly', true);
                    project_profit.prop('readonly', true);
                    modal.on("change", "#id_estimate_inprogress_gross_profitability_before_taxes", gross_profit_conditions);
                    modal.on("change", "#id_estimate_inprogress_income_before_taxes", gross_profit_conditions);
                    modal.on("change", "#id_estimate_inprogress_project_profitability_before_taxes", project_profit_conditions);
                    modal.on("change", "#id_estimate_inprogress_income_before_taxes", project_profit_conditions);

              }
            } catch (err) {}
          }
        });
    };

    var saveForm_project = function (event) {
        event.preventDefault()
        var form = $(this);
        var is_create = event.data.is_create;
        var is_update = event.data.is_update;
        var is_delete = event.data.is_delete;
        var is_estimation = event.data.is_estimation;
        var link = new URL(window.location.href);
        var page = link.searchParams.get("page");
        var url = form.attr("action");
        if (is_estimation){
           var url = form.attr("data-url-estimation");
        }
        // if (is_delete) {
        //     var redirect_url = form.attr("data-redirect");
        // }

        $.ajax({
          url: url,
          data: form.serialize() + "&page=" + page,
          type: form.attr("method"),
          dataType: 'json',
          success: function (data) {
                    window.modal = data.modal_info;
                    if (data.form_is_valid) {
                        if (is_create) {
                            $("#projects_list tbody").html(data.html_list);
                            $("#pagination").html(data.html_pagination);
                            $("#messages").html(data.html_partial_project_messages);


                        }
                        if (is_update){
                            if(is_estimation) {
                                $("#partial_project_estimation").html(data.html_partial_project_estimation);
                                $("#messages").html(data.html_partial_project_messages);

                            } else{
                                $("#partial_project").html(data.html_partial_project);
                                $("#project_name").html(data.html_partial_project_name);
                                $("#messages").html(data.html_partial_project_messages);
                            }

                        }
                        //  if (is_delete){
                        //     window.location.href = redirect_url;
                        // }
                        $("#modal-global").modal("hide");
                    }
                    else {
                      $("#modal-global .modal-content").html(data.html_form);
                    }
              },
              error: function () {
                  console.log("error")
                }
        });
        return false;
    };

    // Create project
    $(".js-create-project").click({is_create:true}, loadForm_project);
    modal.on("submit", ".js-project-create-form",{is_create:true}, saveForm_project);


    // Update project base
    $(".js-update-project").click({is_update:true}, loadForm_project);
    modal.on("submit", ".js-project-update-form", {is_estimation:false, is_update:true }, saveForm_project);


    // Update project estimation
    $(".js-update-estimation-project").click({is_estimation:true}, loadForm_project);
    modal.on("submit", ".js-project-update-estimation-form", {is_estimation:true, is_update:true }, saveForm_project);

    // Delete project
    $(".js-delete-project").click(loadForm_project);
    // modal.on("submit", ".js-project-delete-form", {is_delete: true } , saveForm_project);


       // CRUD services in project card ================================


    var loadForm_service = function (event) {
        var btn = $(this);
        var url = btn.attr("data-url");
        $.ajax({
          url: url,
          type: 'get',
          dataType: 'json',
          beforeSend: function () {
            $("#modal-global").modal("show");
          },
          success: function (data) {
            $("#modal-global .modal-content").html(data.html_form);

          }
        });
    };

    var saveForm_service = function (event) {
        var form = $(this);
        var url = form.attr("action");

        $.ajax({
          url: url,
          data: form.serialize(),
          type: form.attr("method"),
          dataType: 'json',
          success: function (data) {
                if (data.form_is_valid) {
                    $("#services").html(data.html_list);
                    $("#messages").html(data.html_partial_project_messages);

                    $(".js-update-service").click(loadForm_service);
                    $(".js-delete-service").click(loadForm_service);

                    $("#modal-global").modal("hide");

                }
              },
          error: function () {
              console.log("error")
            }
        });
        return false;
    };
    // Create service
    $(".js-create-service").click(loadForm_service);
    modal.on("submit", ".js-service-create-form", saveForm_service);

    // Update service
    $(".js-update-service").click(loadForm_service);
    modal.on("submit", ".js-service-update-form", saveForm_service);

    // Delete service
    $(".js-delete-service").click(loadForm_service);
    modal.on("submit", ".js-service-delete-form", saveForm_service);



       // CRUD team in project card ================================


    var loadForm_team = function (event) {
        var btn = $(this);
        var url = btn.attr("data-url");
        $.ajax({
          url: url,
          type: 'get',
          dataType: 'json',
          beforeSend: function () {
            $("#modal-global").modal("show");
          },
          success: function (data) {
            $("#modal-global .modal-content").html(data.html_form);

          }
        });
    };

    var saveForm_team = function (event) {
        var form = $(this);
        var url = form.attr("action");

        $.ajax({
          url: url,
          data: form.serialize(),
          type: form.attr("method"),
          dataType: 'json',
          success: function (data) {
                if (data.form_is_valid) {
                    $("#team").html(data.html_list);
                    $("#messages").html(data.html_partial_project_messages);

                    $(".js-update-team").click(loadForm_service);
                    $(".js-delete-team").click(loadForm_service);

                    $("#modal-global").modal("hide");

                }
                else {
                      $("#modal-global .modal-content").html(data.html_form);
                    }
              },
          error: function () {
              console.log("error")
            }
        });
        return false;
    };

    // Create team
    $(".js-create-team").click(loadForm_team);
    modal.on("submit", ".js-team-create-form", saveForm_team);

    // Update team
    $(".js-update-team").click(loadForm_team);
    modal.on("submit", ".js-team-update-form", saveForm_team);

    // Delete team
    $(".js-delete-team").click(loadForm_team);
    modal.on("submit", ".js-team-delete-form", saveForm_team);



    // Show project history
    var loadPList = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-global").modal("show");
      },
      success: function (data) {
        $("#modal-global .modal-content").html(data.html_form);
      }
    });
    };

    $(".js-history-project").click(loadPList);








});

