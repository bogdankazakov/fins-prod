from django.db import models
from django.utils.translation import ugettext_lazy as _
from fins.global_functions import IsDeletableMixin, BOOL_CHOICES_YESNO
from simple_history.models import HistoricalRecords
from django.conf import settings
from fins.gist import SpanningForeignKey
from app_signup import models as app_signup_models
from app_transaction import models as app_transaction_models
from app_setup.models import DBUser
from decimal import *
from django.db.models import Case, When, DateField
from app_project.tools_new import statistics

class ProjectCategory(models.Model, IsDeletableMixin):
    name = models.CharField(_('Категория проекта'), max_length=100, unique=True)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='project_cat_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='project_cat_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('категория проекта')
        verbose_name_plural = _('категории проекта')

    def __str__(self):
        return self.name

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class ProjectSubcategory(models.Model, IsDeletableMixin):
    name = models.CharField(_('Субкатегория проекта'), max_length=50, unique=True, blank=True)
    category = models.ForeignKey(ProjectCategory, verbose_name='Категория проекта', related_name="subcategories", on_delete=models.PROTECT, null=True)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='project_sub_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='project_sub_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('субкатегория проекта')
        verbose_name_plural = _('субкатегории проекта')

    def __str__(self):
        return self.name

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class Brand(models.Model, IsDeletableMixin):
    name = models.CharField(_('Бренд'), max_length=100, unique=True)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='brand_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='brand_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('бренд')
        verbose_name_plural = _('бренды')

    def __str__(self):
        return self.name

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class ProductCategory(models.Model, IsDeletableMixin):
    name = models.CharField(_('Категория продукта'), max_length = 50, unique = True)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='product_type_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='product_type_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('тип продукта')
        verbose_name_plural = _('типы продукта')

    def __str__(self):
        return self.name

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class ProjectProduct(models.Model, IsDeletableMixin):
    name = models.CharField(_('Продукт'), max_length=50, unique=True)
    type = models.ForeignKey(ProductCategory, verbose_name='Категория продукта', on_delete=models.PROTECT, null=True)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='product_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='product_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('продукт')
        verbose_name_plural = _('продукты')

    def __str__(self):
        return self.name

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class NBSource(models.Model, IsDeletableMixin):
    name = models.CharField(_('Источник лида'), max_length=50, unique=True)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='nbsource_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='nbsource_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('источник привлечения')
        verbose_name_plural = _('источники привлечения')

    def __str__(self):
        return self.name

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class NBFormat(models.Model, IsDeletableMixin):
    name = models.CharField(_('Формат получения договора'), max_length=50, unique=True, blank=True)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='nbformat_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='nbformat_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('формат получения проекта')
        verbose_name_plural = _('форматы получения проекта')

    def __str__(self):
        return self.name

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value



class Project(models.Model, IsDeletableMixin):

    WORK_FORMAT_CHOICES = (
        (0, 'Fix'),
        (1, 'Time&Materials'),
    )

    AGREEMENT_FORMAT_CHOICES = (
        (0, 'Договор подряда'),
        (1, 'Агентский договор'),
    )

    STAGES_CHOICES = (
        (0, 'Новый'),
        (1, 'Оценка'),
        (2, 'Продажа'),
        (3, 'Реализация'),
        (4, 'Завершен'),
        (5, 'Не продан'),
    )
    name = models.CharField(_('Номер проекта'), max_length=50)
    name_short = models.CharField(_('Короткое название проекта'), max_length=200)
    name_long = models.CharField(_('Полное название проекта'),max_length=200, blank=True, null=True)
    category = models.ForeignKey(ProjectCategory, verbose_name = 'Категория проекта', related_name="category_projects", on_delete=models.PROTECT, null=True)
    subcategory = models.ForeignKey(ProjectSubcategory, verbose_name = 'Субкатегория проекта', related_name="subcategory_projects" , on_delete=models.PROTECT, null=True, blank=True)

    work_format = models.SmallIntegerField(_('Формат оценки работы'), choices=WORK_FORMAT_CHOICES, default=0)
    agreement_format = models.SmallIntegerField(_('Формат договора'), choices=AGREEMENT_FORMAT_CHOICES, default=0)
    commission = models.SmallIntegerField(_('Агентская комиссия (%)'), blank=True, null=True)
    stage = models.SmallIntegerField(_('Стадия проекта'), choices=STAGES_CHOICES, default=0)

    project_start = models.DateField(_('Начало работы по проекту '),blank=True, null=True)
    project_end = models.DateField(_('Завершение работы по проекту'), blank=True, null=True)

    description = models.CharField(_('Описание проекта'),max_length=10000, blank=True, default='Нет описания')

    estimate_probability = models.IntegerField(_('Вероятность победы'),blank=True, default='0')
    estimate_presale_start = models.DateField(_('Дата начала продажи'), blank=True, null=True)
    estimate_presale_end = models.DateField(_('Дата завершения продажи'),blank=True, null=True)
    estimate_presale_outcome_ext = models.DecimalField(_('Внешние затраты на продажу'), max_digits=15, decimal_places=2, blank=True, default=Decimal('0.00'))
    estimate_presale_outcome_int = models.DecimalField(_('Внутренние затраты на продажу'), max_digits=15, decimal_places=2, blank=True, default=Decimal('0.00'))
    estimate_inprogress_start = models.DateField(_('Дата начала работ по проекту'), blank=True, null=True)
    estimate_inprogress_end = models.DateField(_('Дата завершения работ по проекту'), blank=True, null=True)
    estimate_inprogress_income_before_taxes = models.DecimalField(_('Выручка по проекту'), max_digits=15, decimal_places=2, blank=True, default=Decimal('0.00'))
    estimate_inprogress_gross_profitability_before_taxes = models.IntegerField(_('Рентабельность по валовой прибыли'), blank=True, default=Decimal('0.00'))
    estimate_inprogress_gross_profit_before_taxes = models.DecimalField(_('Валовая прибыль'), max_digits=15, decimal_places=2, blank=True, default=Decimal('0.00'))
    estimate_inprogress_project_profitability_before_taxes = models.IntegerField(_('Рентабельность по проектной прибыли'), blank=True, default=Decimal('0.00'))
    estimate_inprogress_project_profit_before_taxes = models.DecimalField(_('Проектная прибыль'), max_digits=15, decimal_places=2, blank=True, default=Decimal('0.00'))

    brand = models.ForeignKey(Brand, verbose_name = 'Бренд', on_delete=models.PROTECT, blank=True, null=True)
    nb_source = models.ForeignKey(NBSource, verbose_name = 'Источник лида', on_delete=models.PROTECT, blank=True, null=True)
    nb_format = models.ForeignKey(NBFormat, verbose_name = 'Формат получения договора', on_delete=models.PROTECT, blank=True, null=True)

    product_category = models.ForeignKey(ProductCategory, verbose_name = 'Категория продуктов',  on_delete=models.PROTECT, blank=True, null=True)
    project_products = models.ManyToManyField(ProjectProduct, verbose_name = 'Продукты проекта', blank=True)

    is_editable = models.BooleanField(_('Может быть отредактирован'), default=True, choices = BOOL_CHOICES_YESNO)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, verbose_name = 'Создан пользователем', related_name='projects_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, verbose_name = 'Изменен пользователем', related_name='projects_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    def __str__(self):
        return '#' + str(self.name) + '/' + str(self.brand.name) + '/' + self.name_short

    class Meta:
        ordering = ['name']
        verbose_name = _('номер проекта')
        verbose_name_plural = _('номера проектов')

        indexes = [
            models.Index(fields=['name']),
            models.Index(fields=['category']),
        ]

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

    def base_calcul(self):
        main_currency = app_transaction_models.Currency.objects.get(is_default=True)
        transactions_all = app_transaction_models.Transaction.objects.select_related(
            'unit',
            'account__currency',

        ).all()
        transactions = transactions_all.filter(project=self)
        all = transactions_all.annotate(
            date=Case(
                When(date_fact=None, then='date_doc'),
                default='date_fact',
                output_field=DateField(),
            )).order_by('date')

        report = statistics(transactions, main_currency, all, False)
        return report

    @property
    def income(self):
        report = self.base_calcul()
        return report[0]['income']

    @property
    def outcome_ext(self):
        report = self.base_calcul()
        return report[0]['outcome_ext']

    @property
    def gross_profit(self):
        report = self.base_calcul()
        return report[0]['gross_profit']

    @property
    def gross_profitability(self):
        report = self.base_calcul()
        return report[0]['gross_profitability']

    @property
    def units(self):
        all = self.project_transaction.all()

        unit_list = []
        for tr in all:
            if str(tr.unit) not in unit_list and tr.unit is not None:
                # print(tr.unit, unit_list)
                unit_list.append(str(tr.unit))
        units = ', '.join(unit_list)

        return units

    @property
    def full_name(self):
        if self.brand:
            return '#' + str(self.name) + '/' + str(self.brand.name) + '/' + self.name_short
        else:
            return '#' + str(self.name) + '/' + self.name_short

class ReportPeriod(models.Model, IsDeletableMixin):
    project = models.ForeignKey(Project, verbose_name='Проект', on_delete=models.PROTECT, null=True, blank=True)
    start = models.DateField(_('Начало работы '),blank=True, null=True)
    end = models.DateField(_('Завершение работы'), blank=True, null=True)

    is_deletable = models.BooleanField(_('Может быть удален'), default=True, choices=BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='period_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='period_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['start']
        verbose_name = _('период оказания услуг t&m')
        verbose_name_plural = _('периоды оказания услуг t&m')

    def __str__(self):
        return str(self.start) + ' — ' + str(self.end)

    @property
    def name(self):
        return str(self.start) + ' — ' + str(self.end)

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class Service(models.Model, IsDeletableMixin):
    name = models.CharField(_('Название'), max_length=50)
    rate_bt = models.DecimalField(_('Стоимость часа до налогов'), max_digits=15, decimal_places=2, blank=True, default='0.00')
    project = models.ForeignKey(Project, verbose_name='Проект', related_name='project_services', on_delete=models.PROTECT, null=True, blank=True)


    is_deletable = models.BooleanField(_('Может быть удален'), default=True, choices=BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='service_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='service_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('отчет t&m')
        verbose_name_plural = _('отчеты t&m')

    def __str__(self):
        return self.name

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class Report(models.Model, IsDeletableMixin):
    period = models.ForeignKey(ReportPeriod, verbose_name='Отчетный период', on_delete=models.PROTECT, null=True, blank=True)
    name = models.CharField(_('Название отчета'), max_length=50)

    is_deletable = models.BooleanField(_('Может быть удален'), default=True, choices=BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='report_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='report_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('отчет t&m')
        verbose_name_plural = _('отчеты t&m')

    def __str__(self):
        return str(self.name)

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class ServiceInReport(models.Model, IsDeletableMixin):
    report = models.ForeignKey(Report, verbose_name='Отчет', related_name='services_in_report', on_delete=models.PROTECT, null=True, blank=True)
    service = models.ForeignKey(Service, verbose_name='Услуга', on_delete=models.PROTECT, null=True, blank=True)
    hours = models.IntegerField(_('Количество затраченных часов'), default=0, null=True, blank=True)
    description = models.CharField(_('Описание работы'),max_length=10000, blank=True, default='Нет описания')

    is_deletable = models.BooleanField(_('Может быть удален'), default=True, choices=BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='serviceinreport_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='serviceinreport_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['report']
        verbose_name = _('стока отчет t&m')
        verbose_name_plural = _('строки в отчете t&m')

    def __str__(self):
        return 'Услуга' + str(self.service) + ' в отчете ' + str(self.report)

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class TeamProject(models.Model, IsDeletableMixin):

    ROLE_CHOICES = (
        (0, 'Участник'),
        (1, 'Лидер'),
        (2, 'Супервизор'),
    )

    role = models.SmallIntegerField(_('Роль пользователя в проекте'), choices=ROLE_CHOICES, default=0)
    user = models.ForeignKey(DBUser, verbose_name='Пользователь', on_delete=models.PROTECT, null=True, blank=True)
    project = models.ForeignKey(Project, verbose_name='Проект', related_name='teamprojects_in_project', on_delete=models.PROTECT, null=True, blank=True)

    is_deletable = models.BooleanField(_('Может быть удален'), default=True, choices=BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='teamproject_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='teamproject_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['project']
        verbose_name = _('Пользователь в проекте')
        verbose_name_plural = _('Пользователи в проекте')

    def __str__(self):
        return str(self.user)

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value