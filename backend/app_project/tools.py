from django.db.models import Sum
from decimal import *
from app_transaction import models as app_transaction_models
from app_transaction.converter import *
from app_transaction.tools import converter_float
from django.db.models import Case, When, DateField
from app_project import models as app_project_models
from app_setup.tools import get_db_user
from app_signup import models as app_signup_models


def add_user_to_team(instance, request):
    db_user = get_db_user(request)
    app_project_models.TeamProject(
        project=instance,
        role=1,
        user=db_user
    ).save()
    pass

def extra_data_pre(instance):


    return instance

def numgen(instance):
    """generate transaction number"""
    if instance.pk > 99999:
        instance.name = 'P' + '%012d' % instance.pk
    else:
        instance.name = 'P' + '%06d' % instance.pk
    return instance


def extra_data_post(instance):
    instance = numgen(instance)
    return instance


def statistics(transactions, main_currency, all, test):

    transactions = transactions.annotate(
        date=Case(
            When(date_fact=None, then='date_doc'),
            default='date_fact',
            output_field=DateField(),
        ))
    # getting list of used accounts
    account_list = []
    for t in transactions:
        if t.account not in account_list:
            account_list.append(t.account )
    # getting list of used currencies
    currency_list = []
    for a in account_list:
        if a.currency not in currency_list:
            currency_list.append(a.currency )

    # making report for currencies
    report = []
    for c in currency_list:
        trans = transactions.filter(account__currency=c)
        income = trans.filter(type=True).aggregate(Sum('sum_before_taxes'))['sum_before_taxes__sum'] or Decimal('0.00')
        outcome = trans.filter(type=False).aggregate(Sum('sum_before_taxes'))['sum_before_taxes__sum'] or Decimal('0.00')
        gross_profit = income - outcome
        try:
            gross_profitability = round((gross_profit / income), 2) * 100
            if gross_profitability < 0:
                gross_profitability = 0
        except Exception:
            gross_profitability= 0


        report.append(dict(
            currency=c,
            income=income,
            outcome_ext=outcome,
            gross_profit= gross_profit,
            gross_profitability= gross_profitability,
        ))


    # making report for main currency


    income_list = []
    outcome_list = []


    for currency in currency_list:
        trans = transactions.filter(account__currency=currency)
        for t in trans:

            if t.type == True:
                income_list.append(converter(t, main_currency, all, test))
            else:
                outcome_list.append(converter(t, main_currency, all, test))

    income = sum(income_list)
    outcome = sum(outcome_list)
    gross_profit = income - outcome
    try:
        gross_profitability = round((gross_profit / income), 2) * 100
        if gross_profitability < 0:
            gross_profitability = 0
    except Exception:
        gross_profitability = 0
    report = [dict(
        currency='Все с основной валюте (' + str(main_currency.name) + ')',
        income=income,
        outcome_ext=outcome,
        gross_profit=gross_profit,
        gross_profitability=gross_profitability,

    )] + report


    return report

def format_tool(project):
    if project.work_format == 0 and project.agreement_format == 0:
        format = 1 # subcontractor contract + fix
    elif project.work_format == 0 and project.agreement_format == 1:
        format = 2 # agency contract + fix
    elif project.work_format == 1 and project.agreement_format == 0:
        format = 3 # subcontractor contract + time&materials
    elif project.work_format == 1 and project.agreement_format == 1:
        format = 4 # agency contract + time&materials
    return format


def period_tool(project):
    periods = app_project_models.ReportPeriod.objects.filter(project=project)
    for period in periods:
        period.transactions = app_transaction_models.Transaction.objects.filter(period=period).annotate(
                    date=Case(
                        When(date_fact=None, then='date_doc'),
                        default='date_fact',
                        output_field=DateField(),
                    )).order_by('date')
        period.reports = app_project_models.Report.objects.filter(period=period)
    return periods


def form_converter(form):
    '''
    Т.к. значения сумм мы получаем в формат строки, то мы сначала конвертируем их в decimal.
    а копируем форму, чтобы можно было редактировать в ней значения
    :param form: форма
    :return: формсет с обновленными значениями сумм
    '''
    try:
        form.data = form.data.copy()
        form.data['estimate_presale_outcome_ext'] = converter_float(form.data['estimate_presale_outcome_ext'])
        form.data['estimate_presale_outcome_int'] = converter_float(form.data['estimate_presale_outcome_int'])
        form.data['estimate_inprogress_income_before_taxes'] = converter_float(form.data['estimate_inprogress_income_before_taxes'])
        form.data['estimate_inprogress_gross_profitability_before_taxes'] = converter_float(form.data['estimate_inprogress_gross_profitability_before_taxes'])
        form.data['estimate_inprogress_gross_profit_before_taxes'] = converter_float(form.data['estimate_inprogress_gross_profit_before_taxes'])
        form.data['estimate_inprogress_project_profitability_before_taxes'] = converter_float(form.data['estimate_inprogress_project_profitability_before_taxes'])
        form.data['estimate_inprogress_project_profit_before_taxes'] = converter_float(form.data['estimate_inprogress_project_profit_before_taxes'])
    except Exception:
        pass
    return form


def can_delete_report(item):
    for rel in item._meta.get_fields():
        if not item.is_deletable:
            return False, None
        try:
            # check if there is a relationship with at least one related object
            related = rel.related_model.objects.filter(**{rel.field.name: item})
            if related.exists():
                for el in related:
                    if el._meta.model.__name__ != 'ServiceInReport':
                        # if there is return a Tuple of flag = False the related_model object
                        return False, related
        except AttributeError:  # an attribute error for field occurs when checking for AutoField
            pass  # just pass as we dont need to check for AutoField
    return True, None


def can_delete_project(item):
    for rel in item._meta.get_fields():
        if not item.is_deletable:
            return False, None
        try:
            # check if there is a relationship with at least one related object
            related = rel.related_model.objects.filter(**{rel.field.name: item})
            if related.exists():
                for el in related:
                    if el._meta.model.__name__ != 'TeamProject':
                        # if there is return a Tuple of flag = False the related_model object
                        return False, related
        except AttributeError:  # an attribute error for field occurs when checking for AutoField
            pass  # just pass as we dont need to check for AutoField
    return True, None

def teamproject_killer(item):
    team_projects = item.teamprojects_in_project.all()
    for t in team_projects:
        t.delete()

def serviceinreport_killer(item):
    services = app_project_models.ServiceInReport.objects.filter(report=item)
    for service in services:
        service.delete()


def report_add_data(formset, services):
    for form in formset:
        form.service_label = services.get(pk=form['service'].value())
    return formset

def agency_checker(transactions, project):
    messages = []
    if project.agreement_format == 1:
        currencies = []
        for t in transactions:
            if t.account.currency not in currencies:
                currencies.append(t.account.currency)

        for currency in currencies:
            income = transactions.filter(account__currency=currency).filter(type=True).aggregate(Sum('sum_before_taxes'))['sum_before_taxes__sum']
            outcome = transactions.filter(account__currency=currency).filter(type=False).aggregate(Sum('sum_before_taxes'))['sum_before_taxes__sum']
            if income is None:
                income = Decimal('0.00')
            if outcome is None:
                outcome = Decimal('0.00')
            expected = round(float(outcome) * (1 + project.commission/100), 2)

            if expected != income:
                messages.append('Для счета с валютой ' + str(currency) + ' расход Х на агентскую коммиссию не равен указанному доходу!')

    return messages



def delete_checker_team(item, project):
    # get all the related object
    for rel in item._meta.get_fields():
        if not item.is_deletable:
            return False, None
        try:
            # check if there is a relationship with at least one related object
            related = rel.related_model.objects.filter(**{rel.field.name: item})
            if related.exists():
                # if there is return a Tuple of flag = False the related_model object
                return False, related
        except AttributeError:  # an attribute error for field occurs when checking for AutoField
            pass  # just pass as we dont need to check for AutoField



    if len(project.teamprojects_in_project.all()) <=1:
        return False, None

    return True, None

def filter_extra_data(filter):
    main_currency = app_transaction_models.Currency.objects.get(is_default=True)

    team_all = app_project_models.TeamProject.objects.select_related(
        'user',
    ).all()

    transactions_all = app_transaction_models.Transaction.objects.select_related(
        'unit',
        'account__currency',

    ).all()


    for project in filter.qs:
        #leader
        try:
            project.leader = team_all.get(project=project , role=1).user
        except Exception:
            project.leader = ''

        #team
        team = team_all.filter(project=project)
        team_list=[]
        for member in team:
            team_list.append(str(member.user))
        project.team = ', '.join(team_list)

        #units
        transactions = transactions_all.filter(project=project)

        unit_list = []
        for tr in transactions:
            if str(tr.unit) not in unit_list and tr.unit is not None:
                # print(tr.unit, unit_list)
                unit_list.append(str(tr.unit))
        project.units = ', '.join(unit_list)
    #
        #income
        all = transactions_all.annotate(
            date=Case(
                When(date_fact=None, then='date_doc'),
                default='date_fact',
                output_field=DateField(),
            )).order_by('date')
        report = statistics(transactions, main_currency, all, False)
        project.income = report[0]['income']
        project.outcome_ext = report[0]['outcome_ext']
        project.gross_profit = report[0]['gross_profit']
        project.gross_profitability = report[0]['gross_profitability']

        #products
        products_full = []
        for product in project.project_products.all():
            products_full.append(str(product))

        project.products_full = ', '.join(products_full)


    return filter

def extra_data_p_card(project):

    products_full = []
    for product in project.project_products.all():
        products_full.append(str(product))

    project.products_full = ', '.join(products_full)

    return project



def m2m (instance, form):
    if 'project_products' in form.cleaned_data:
        instance.project_products.clear()
        for product in form.cleaned_data['project_products']:
            instance.project_products.add(product)
    return instance

