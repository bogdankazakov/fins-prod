from app_project.tests.test_tools import create_project, create_service
from app_project.tests.test_main import Setup
from subdomains.utils import reverse as sub_reverse
from fins.global_functions import get_db_name_test
from app_project import views as app_project_views
from app_project import models as app_project_models
from fins.threadlocal import thread_local
from django.contrib.messages.storage.fallback import FallbackStorage


class ServiceTest(Setup):

    def test_service_CRUD(self):

        self.client.login(username='b@b.ru', password='123')
        # create project
        response, created_project = create_project(self.brand.pk, self.factory, self.my_user, self.account)

        #create service
        response, created = create_service(created_project, self.factory, self.my_user, self.account )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(created)


        #update project
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:service_update', kwargs=kwargs ,subdomain='mysubdomain')
        data = {
            'name': ['TestServiceUpdate'],
            'rate_bt': ['23'],

        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.service_update(request, **kwargs)
            return response

        response = tread(request)

        update = app_project_models.Service.objects.using(get_db_name_test()).get(name = 'TestServiceUpdate')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(update)

        #delete
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:service_delete',kwargs=kwargs, subdomain='mysubdomain')
        data = {
            'page': ['null']
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.service_delete(request, **kwargs)
            return response

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        response = tread(request)

        deleted = app_project_models.Service.objects.using(get_db_name_test()).filter(name = 'TestServiceUpdate')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)
