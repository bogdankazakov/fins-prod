from app_project.tests.test_tools import create_project, create_service, create_period
from app_project.tests.test_main import Setup
from subdomains.utils import reverse as sub_reverse
from fins.global_functions import get_db_name_test
from app_project import views as app_project_views
from app_project import models as app_project_models
from fins.threadlocal import thread_local
from django.contrib.messages.storage.fallback import FallbackStorage
from datetime import date
from app_transaction.tests import TransactionTest_CRUD
class PeriodTest(Setup):


    def test_period_CRUD(self):

        self.client.login(username='b@b.ru', password='123')
        # create project
        response, created_project = create_project(self.brand.pk, self.factory, self.my_user, self.account)

        #create period
        response, created = create_period(created_project, self.factory, self.my_user, self.account)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(created)

        #update period
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:update_period', kwargs=kwargs ,subdomain='mysubdomain')
        data = {
            'start': date(2017, 8, 14),
            'end': date(2017, 8, 28),

        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.update_period(request, **kwargs)
            return response

        response = tread(request)

        update = app_project_models.ReportPeriod.objects.using(get_db_name_test()).get(start = date(2017, 8, 14), end=date(2017, 8, 28))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(update)

        #delete period
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:delete_period',kwargs=kwargs, subdomain='mysubdomain')
        data = {
            'page': ['null']
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.delete_period(request, **kwargs)
            return response

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        response = tread(request)

        deleted = app_project_models.ReportPeriod.objects.using(get_db_name_test()).filter(start = date(2017, 8, 14), end=date(2017, 8, 28))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)

    def test_report_CRUD(self):
        self.client.login(username='b@b.ru', password='123')
        # create project
        response, created_project = create_project(self.brand.pk, self.factory, self.my_user, self.account)

        # create service
        response, created_service = create_service(created_project, self.factory, self.my_user, self.account)

        # create period
        response, created_period = create_period(created_project, self.factory, self.my_user, self.account)


        # create report
        kwargs = {'projectpk': created_project.pk, 'periodpk': created_period.pk}
        link = sub_reverse('app_project:report_create', kwargs=kwargs,
                           subdomain='mysubdomain')
        data = {
            'name': 'ReportTest',
            'form-TOTAL_FORMS': '1',
            'form-INITIAL_FORMS': '1',
            'form-MIN_NUM_FORMS': '0',
            'form-MAX_NUM_FORMS': '1000',
            'form-0-service': created_service.pk,
            'form-0-hours': '12',
            'form-3-description': '',
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.report_create(request, **kwargs)
            return response

        response = tread(request)
        created = app_project_models.Report.objects.using(get_db_name_test()).get(name='ReportTest')



        # update report
        kwargs = {'pk': created.pk, 'projectpk': created_project.pk}
        link = sub_reverse('app_project:report_update', kwargs=kwargs, subdomain='mysubdomain')
        data = {
            'name': 'ReportTestUpdated',
            'form-TOTAL_FORMS': '1',
            'form-INITIAL_FORMS': '1',
            'form-MIN_NUM_FORMS': '0',
            'form-MAX_NUM_FORMS': '1000',
            'form-0-service': created_service.pk,
            'form-0-hours': '14',
            'form-3-description': '',

        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.report_update(request, **kwargs)
            return response

        response = tread(request)

        update = app_project_models.Report.objects.using(get_db_name_test()).get(name='ReportTestUpdated')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(update)

        # delete report
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:report_delete', kwargs=kwargs, subdomain='mysubdomain')
        data = {
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.report_delete(request, **kwargs)
            return response

        # need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        response = tread(request)

        deleted = app_project_models.Report.objects.using(get_db_name_test()).filter(name='ReportTestUpdated')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)

