from app_project.tests.test_main import Setup
from subdomains.utils import reverse as sub_reverse
from fins.global_functions import get_db_name_test
from app_project import views as app_project_views
from app_project import models as app_project_models
from fins.threadlocal import thread_local
from django.contrib.sessions.middleware import SessionMiddleware


class ExcelTest(Setup):

    def test_basic(self):

        self.client.login(username='b@b.ru', password='123')

        l = []
        for q in app_project_models.Project.objects.using(get_db_name_test()).all():
            l.append(q.pk)

        link = sub_reverse('app_project:excel_export',subdomain='mysubdomain')
        data = {}
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        request.session['project_filter_qs'] = l
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.excel_export(request)
            return response

        response = tread(request)
        self.assertEqual(response.status_code, 200)