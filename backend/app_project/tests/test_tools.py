from subdomains.utils import reverse as sub_reverse
from fins.threadlocal import thread_local
from app_project import models as app_project_models
from app_project import views as app_project_views
from fins.global_functions import get_db_name_test
from datetime import date

def create_project(brand_pk, factory, my_user, account):
    #create
    link = sub_reverse('app_project:project_create', subdomain='mysubdomain')
    data = {
             'category': ['1'],
             'brand': brand_pk,
             'name_short': ['TestProjectName'],
             'work_format': ['0'],
             'agreement_format': ['0'],
             'page': ['null'],
            }
    request = factory.post(link, data)
    request.user = my_user
    request.account = account
    @thread_local(using_db=get_db_name_test())
    def tread(request):
        response = app_project_views.project_create(request)
        return response
    response = tread(request)

    created = app_project_models.Project.objects.using(get_db_name_test()).get(name_short='TestProjectName')
    return response, created

def create_service(created_project, factory, my_user, account):
    kwargs = {'projectpk': created_project.pk}
    link = sub_reverse('app_project:service_create', kwargs=kwargs, subdomain='mysubdomain')
    data = {
        'name': ['TestService'],
        'rate_bt': ['23'],
    }
    request = factory.post(link, data)
    request.user = my_user
    request.account = account

    @thread_local(using_db=get_db_name_test())
    def tread(request):
        response = app_project_views.service_create(request, **kwargs)
        return response

    response = tread(request)

    created = app_project_models.Service.objects.using(get_db_name_test()).get(name='TestService')
    return response, created

def create_period(created_project, factory, my_user, account):
    kwargs = {'projectpk': created_project.pk}
    link = sub_reverse('app_project:create_period', kwargs=kwargs,
                       subdomain='mysubdomain')
    data = {
        'start': date(2017, 8, 14),
        'end': date(2017, 8, 24),
    }
    request = factory.post(link, data)
    request.user = my_user
    request.account = account

    @thread_local(using_db=get_db_name_test())
    def tread(request):
        response = app_project_views.create_period(request, **kwargs)
        return response

    response = tread(request)

    created = app_project_models.ReportPeriod.objects.using(get_db_name_test()).get(start=date(2017, 8, 14),
                                                                                    end=date(2017, 8, 24))
    return response, created
