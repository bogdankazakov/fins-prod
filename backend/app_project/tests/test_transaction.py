from app_project.tests.test_tools import create_project, create_service, create_period
from app_project.tests.test_main import Setup
from subdomains.utils import reverse as sub_reverse
from fins.global_functions import get_db_name_test
from app_project import views as app_project_views
from app_project import models as app_project_models
from fins.threadlocal import thread_local
from django.contrib.messages.storage.fallback import FallbackStorage
from datetime import date
from app_transaction.tests import TransactionTest_CRUD
from app_transaction import models as app_transaction_models
from app_transaction import views as app_transaction_views
from app_transaction.tests import basic_data
from app_project.tools import statistics
from django.db.models import Case, When, DateField
from decimal import *


class TransactionTest(TransactionTest_CRUD):

    def setUp(self):
        TransactionTest_CRUD.setUp(self)
        self.brand = app_project_models.Brand(name='Бренд - пример')
        self.brand.save(using=get_db_name_test())

    def test_transaction_CRUD(self):

        self.client.login(username='b@b.ru', password='123')

        # create project
        response, created_project = create_project(self.brand.pk, self.factory, self.my_user, self.account)


        # create transaction
        kwargs = {'projectpk': created_project.pk}
        link = sub_reverse('app_transaction:transaction_create_project', kwargs=kwargs, subdomain='mysubdomain')
        data = basic_data(self.project_1, self.sub_1, self.puprose_outcome, self.ac1)
        data.pop('project', None)

        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_create_project(request, **kwargs)
            return response

        response = tread(request)

        created = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000001')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(created)

        # update
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_transaction:transaction_update_project', kwargs=kwargs, subdomain='mysubdomain')
        data = basic_data(self.project_1, self.sub_1, self.puprose_outcome, self.ac1)
        data.pop('project', None)
        data['sum_before_taxes'] = '100,00'

        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_update_project(request, **kwargs)
            return response

        response = tread(request)

        updated = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000001')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            str(updated.sum_before_taxes), '100.00')

        # delete
        kwargs = {'pk': updated.pk}
        link = sub_reverse('app_transaction:transaction_delete_project', kwargs=kwargs, subdomain='mysubdomain')
        data = {}
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_delete_project(request, **kwargs)
            return response

        response = tread(request)

        deleted = app_transaction_models.Transaction.objects.using(get_db_name_test()).filter(name='T000001')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)

    def test_transaction_inperiod_CRUD(self):

        self.client.login(username='b@b.ru', password='123')

        # create project
        response, created_project = create_project(self.brand.pk, self.factory, self.my_user, self.account)

        # create period
        response, created_period = create_period(created_project, self.factory, self.my_user, self.account)

        # create transaction
        kwargs = {'projectpk': created_project.pk, 'periodpk': created_period.pk}
        link = sub_reverse('app_transaction:transaction_create_project_period', kwargs=kwargs, subdomain='mysubdomain')
        data = basic_data(self.project_1, self.sub_1, self.puprose_outcome, self.ac1)
        data.pop('project', None)

        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_create_project_period(request, **kwargs)
            return response

        response = tread(request)

        created = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000001')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(created)

        # update
        kwargs = {'pk': created.pk, 'projectpk': created_project.pk}
        link = sub_reverse('app_transaction:transaction_update_project_period', kwargs=kwargs, subdomain='mysubdomain')
        data = basic_data(self.project_1, self.sub_1, self.puprose_outcome, self.ac1)
        data.pop('project', None)
        data['sum_before_taxes'] = '100,00'

        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_update_project_period(request, **kwargs)
            return response

        response = tread(request)

        updated = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000001')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            str(updated.sum_before_taxes), '100.00')

        # delete
        kwargs = {'pk': updated.pk}
        link = sub_reverse('app_transaction:transaction_delete_project_period', kwargs=kwargs, subdomain='mysubdomain')
        data = {}
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_delete_project_period(request, **kwargs)
            return response

        response = tread(request)

        deleted = app_transaction_models.Transaction.objects.using(get_db_name_test()).filter(name='T000001')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)

    def test_startistics(self):

        self.client.login(username='b@b.ru', password='123')

        # create project ==================
        response, created_project = create_project(self.brand.pk, self.factory, self.my_user, self.account)


        # create transaction RUB ==================
        kwargs = {'projectpk': created_project.pk}
        link = sub_reverse('app_transaction:transaction_create_project', kwargs=kwargs, subdomain='mysubdomain')
        data = basic_data(self.project_1, self.sub_1, self.puprose_outcome, self.ac1)
        data.pop('project', None)
        data['sum_before_taxes'] = '100,00'
        data['account'] = [str(self.ac1.pk)]

        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_create_project(request, **kwargs)
            return response

        response = tread(request)
        created_1 = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000001')


        # create transaction RUB ==================
        kwargs = {'projectpk': created_project.pk}
        link = sub_reverse('app_transaction:transaction_create_project', kwargs=kwargs,
                           subdomain='mysubdomain')
        data = basic_data(self.project_1, self.sub_1, self.puprose_outcome, self.ac1)
        data.pop('project', None)
        data['sum_before_taxes'] = '200,00'
        data['account'] = [str(self.ac1.pk)]

        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_create_project(request, **kwargs)
            return response

        response = tread(request)
        created_2 = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000002')

        # create transaction USD ==================
        kwargs = {'projectpk': created_project.pk}
        link = sub_reverse('app_transaction:transaction_create_project', kwargs=kwargs,
                           subdomain='mysubdomain')
        data = basic_data(self.project_1, self.sub_1, self.puprose_outcome, self.ac1)
        data.pop('project', None)
        data['sum_before_taxes'] = '100,00'
        data['account'] = [str(self.ac3.pk)]

        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_create_project(request, **kwargs)
            return response

        response = tread(request)
        created_3 = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000003')

        # making report
        transactions =  app_transaction_models.Transaction.objects.using(get_db_name_test()).filter(project=created_project)
        main_currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(is_default=True)
        all = app_transaction_models.Transaction.objects.using(get_db_name_test()).all().annotate(
            date=Case(
                When(date_fact=None, then='date_doc'),
                default='date_fact',
                output_field=DateField(),
            )).order_by('date')

        report = statistics(transactions, main_currency, all, True)
        expected = [
            {'currency': 'Все с основной валюте (Rub)', 'income': 0, 'outcome_ext': Decimal('550.00'), 'gross_profit': Decimal('-550.00'), 'gross_profitability': 0},
            {'currency': self.ac1.currency, 'income': Decimal('0.00'), 'outcome_ext': Decimal('300.00'), 'gross_profit': Decimal('-300.00'), 'gross_profitability': 0},
            {'currency': self.ac3.currency, 'income': Decimal('0.00'), 'outcome_ext': Decimal('100.00'), 'gross_profit': Decimal('-100.00'), 'gross_profitability': 0}
        ]


        self.assertEqual(expected, report)



