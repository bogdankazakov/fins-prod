from django.test import TestCase, Client, RequestFactory
from app_signup import models as app_signup_models
from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
from django.conf import settings
from app_auth.tests import load_url_pattern_names
from fins import urls  as fins_url
import psycopg2
from fins.threadlocal import thread_local
from app_setup import views as app_setup_views
from app_transaction import models as app_transaction_models
from django.contrib.messages.storage.fallback import FallbackStorage
from app_setup import models as app_setup_models
from fins.global_functions import get_db_name_test as get_db_name
from app_project import models as app_project_models
from app_project import views as app_project_views
from fins.global_functions import get_db_name_test
from app_project.tests.test_tools import create_project, create_service
from django.contrib.sessions.middleware import SessionMiddleware
#
class AccessPermissionsTest(TestCase):


    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()

        site = Site.objects.get(pk=1)
        site.domain = 'testserver'
        site.save()

        c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
        c.save()
        c.create_database()
        c.fill_database()

        user = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b@b.ru')
        user.set_password('123')
        user.created_account_pk = c.pk
        user.save()
        user.account.add(c)


        self.my_user = user
        self.account = c
        self.dbuser = app_setup_models.DBUser.objects.using(get_db_name()).get(
            user_id=user.pk)

        #fill db ... stupidly)

        self.client.login(username='b@b.ru', password='123')
        link = sub_reverse('app_dashboard:index',subdomain='mysubdomain')
        request = self.factory.get(link)
        request.user = self.my_user
        request.account = self.account
        db_name_1 = get_db_name()

        @thread_local(using_db=db_name_1)
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response

    def tearDown(self):

        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

    def test_access_permission(self):
        """
        test app_setup access and redirect if denied
        Exceptions are given in exception list

        """

        Exeption_app_list_default = ['admin', ]
        Exeption_app_list_no_login = ['app_about', 'app_auth', 'app_signup',]
        Exeption_name_list_default = []
        Exeption_name_list_no_login = [
            'project',
            'project_update',
            'project_update_estimation',
            'project_delete',
            'create_period',
            'update_period',
            'delete_period',
            'service_create',
            'service_update',
            'service_delete',
            'report_create',
            'report_update',
            'report_delete',
            'team_create',
            'team_update',
            'team_delete',
        ]
        App_name = ['app_project']


        for item in load_url_pattern_names(fins_url.urlpatterns,
                                           Exeption_app_list_default, Exeption_app_list_no_login,
                                           Exeption_name_list_default, Exeption_name_list_no_login, App_name):
            self.client.login(username='b@b.ru', password='123')

            # tests access permited
            self.dbuser.access_app_project = True
            self.dbuser.save(using=get_db_name())
            response = self.client.get(
                str(item), HTTP_HOST='mysubdomain.testserver')
            print(str(item) + ' - start access level 2')
            self.assertEqual(response.status_code, 200)
            print(str(item) + ' - checked access level 2')

            # tests access level denied
            self.dbuser.access_app_project = False
            self.dbuser.save(using=get_db_name())
            response = self.client.get(
                str(item), HTTP_HOST='mysubdomain.testserver', follow=True)
            print(str(item) + ' - start access level 0')
            self.assertRedirects(response,
                                 sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain='mysubdomain'),
                                 status_code=302, target_status_code=200,
                                 msg_prefix='', fetch_redirect_response=True)
            print(str(item) + ' - checked access level 0')

class Setup(TestCase):
    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()

        site = Site.objects.get(pk=1)
        site.domain = 'testserver'
        site.save()

        c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
        c.save()
        c.create_database()
        c.fill_database()

        unit1 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[0]
        unit2 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[1]
        user = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b@b.ru')
        user.set_password('123')
        user.created_account_pk = c.pk
        user.save()
        user.account.add(c)

        userdb = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=user.pk)
        userdb.access_app_project = 2
        userdb.unit = unit1
        userdb.save(using=get_db_name_test())

        self.user2 = app_signup_models.User(
            first_name='UserFirst2',
            last_name='UserLast2',
            email='a@a.ru')
        self.user2.set_password('123')
        self.user2.created_account_pk = c.pk
        self.user2.save()
        self.user2.account.add(c)

        userdb2 = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=self.user2.pk)
        userdb2.access_app_project = 1
        userdb2.unit = unit1
        userdb2.save(using=get_db_name_test())

        self.user3 = app_signup_models.User(
            first_name='UserFirst3',
            last_name='UserLast3',
            email='c@c.ru')
        self.user3.set_password('123')
        self.user3.created_account_pk = c.pk
        self.user3.save()
        self.user3.account.add(c)

        userdb3 = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=self.user3.pk)
        userdb3.access_app_project = 3
        userdb3.unit = unit2
        userdb3.save(using=get_db_name_test())

        self.user4 = app_signup_models.User(
            first_name='UserFirst4',
            last_name='UserLast4',
            email='d@d.ru')
        self.user4.set_password('123')
        self.user4.created_account_pk = c.pk
        self.user4.save()
        self.user4.account.add(c)

        userdb4 = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=self.user4.pk)
        userdb4.access_app_project = 0
        userdb4.unit = unit1
        userdb4.save(using=get_db_name_test())

        self.brand = app_project_models.Brand(name='Бренд - пример')
        self.brand.save(using=get_db_name_test())

        self.my_user = user
        self.account = c

    def tearDown(self):

        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

class ProjectTest(Setup):

    def test_project_and_estimation_CRUD_2(self):

        """
        user have all rights
        :return:
        """
        self.client.login(username='b@b.ru', password='123')

        # create
        response, created = create_project(self.brand.pk, self.factory, self.my_user, self.account)


        self.assertEqual(response.status_code, 200)
        self.assertTrue(created)

        #test teamproject creation
        team_projects = created.teamprojects_in_project.all()
        self.assertNotEqual(len(team_projects), 0)

        #update project
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:project_update', kwargs=kwargs ,subdomain='mysubdomain')
        data = {
                'name_short': ['TestProjectName_2'],
                'stage': ['0'],
                'brand': self.brand.pk,
                'page': ['null'],
                'is_editable': ['True'],

        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.project_update(request, **kwargs)
            return response

        response = tread(request)

        update = app_project_models.Project.objects.using(get_db_name_test()).get(name_short = 'TestProjectName_2')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(update)


        #update estimation
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:project_update_estimation', kwargs=kwargs ,subdomain='mysubdomain')
        data = {
                'page': ['null'],

        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.project_update_estimation(request, **kwargs)
            return response

        response = tread(request)

        update = app_project_models.Project.objects.using(get_db_name_test()).get(name_short = 'TestProjectName_2')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(update)

        #delete
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:project_delete',kwargs=kwargs, subdomain='mysubdomain')
        data = {
            'page': ['null']
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.project_delete(request, **kwargs)
            return response

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        response = tread(request)

        deleted = app_project_models.Project.objects.using(get_db_name_test()).filter(name_short = 'TestProjectName_2')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)

        #test teamproject DELETE
        team_projects = created.teamprojects_in_project.all()
        self.assertEqual(len(team_projects), 0)

    def test_project_and_estimation_CRUD_1_created_NOT_by_me(self):

        """
        user can see only projects where he is a part of the team. He is NOT member of created project
        """
        self.client.login(username='b@b.ru', password='123')

        # create
        response, created = create_project(self.brand.pk, self.factory, self.my_user, self.account)


        self.client.login(username='a@a.ru', password='123')

        #visit project list
        link = sub_reverse('app_project:index',subdomain='mysubdomain')
        data = {}
        request = self.factory.get(link, data)
        request.user = self.user2
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.index(request)
            return response

        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        response = tread(request)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('TestProjectName' not in response.content.decode("utf-8")) #checks that project is not dispalyed in the list



        #visit project card
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:project', kwargs=kwargs ,subdomain='mysubdomain')
        data = {}
        request = self.factory.get(link, data)
        request.user = self.user2
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.project(request, **kwargs)
            return response
        response = tread(request)
        self.assertEqual(response.status_code, 302)


        #update project
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:project_update', kwargs=kwargs ,subdomain='mysubdomain')
        data = {
                'name_short': ['TestProjectName_2'],
                'stage': ['0'],
                'brand': self.brand.pk,
                'page': ['null'],
                'is_editable': ['True'],

        }
        request = self.factory.post(link, data)
        request.user = self.user2
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.project_update(request, **kwargs)
            return response

        response = tread(request)

        update = app_project_models.Project.objects.using(get_db_name_test()).filter(name_short = 'TestProjectName_2')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(len(update), 0)


        #update estimation
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:project_update_estimation', kwargs=kwargs ,subdomain='mysubdomain')
        data = {
                'page': ['null'],

        }
        request = self.factory.post(link, data)
        request.user = self.user2
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.project_update_estimation(request, **kwargs)
            return response

        response = tread(request)

        update = app_project_models.Project.objects.using(get_db_name_test()).filter(name_short = 'TestProjectName_2')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(len(update), 0)


        #delete
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:project_delete',kwargs=kwargs, subdomain='mysubdomain')
        data = {
            'page': ['null']
        }
        request = self.factory.post(link, data)
        request.user = self.user2
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.project_delete(request, **kwargs)
            return response

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        response = tread(request)

        deleted = app_project_models.Project.objects.using(get_db_name_test()).filter(name_short = 'TestProjectName_2')
        self.assertEqual(response.status_code, 302)

    def test_project_and_estimation_CRUD_1_created_BY_me(self):

        """
        user can see only projects where he is a part of team. He IS member of created project
        """

        self.client.login(username='a@a.ru', password='123')

        # create
        response, created = create_project(self.brand.pk, self.factory, self.user2, self.account)

        #visit project list
        link = sub_reverse('app_project:index',subdomain='mysubdomain')
        data = {}
        request = self.factory.get(link, data)
        request.user = self.user2
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.index(request)
            return response

        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        response = tread(request)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('TestProjectName' in response.content.decode("utf-8")) #checks that project is not dispalyed in the list



        #visit project card
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:project', kwargs=kwargs ,subdomain='mysubdomain')
        data = {}
        request = self.factory.get(link, data)
        request.user = self.user2
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.project(request, **kwargs)
            return response
        response = tread(request)
        self.assertEqual(response.status_code, 200)

        #update project
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:project_update', kwargs=kwargs ,subdomain='mysubdomain')
        data = {
                'name_short': ['TestProjectName_2'],
                'stage': ['0'],
                'brand': self.brand.pk,
                'page': ['null'],
                'is_editable': ['True'],

        }
        request = self.factory.post(link, data)
        request.user = self.user2
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.project_update(request, **kwargs)
            return response

        response = tread(request)

        update = app_project_models.Project.objects.using(get_db_name_test()).filter(name_short = 'TestProjectName_2')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(update), 1)


        #update estimation
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:project_update_estimation', kwargs=kwargs ,subdomain='mysubdomain')
        data = {
                'page': ['null'],

        }
        request = self.factory.post(link, data)
        request.user = self.user2
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.project_update_estimation(request, **kwargs)
            return response

        response = tread(request)

        update = app_project_models.Project.objects.using(get_db_name_test()).filter(name_short = 'TestProjectName_2')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(update), 1)


        #delete
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:project_delete',kwargs=kwargs, subdomain='mysubdomain')
        data = {
            'page': ['null']
        }
        request = self.factory.post(link, data)
        request.user = self.user2
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.project_delete(request, **kwargs)
            return response

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        response = tread(request)

        deleted = app_project_models.Project.objects.using(get_db_name_test()).filter(name_short = 'TestProjectName_2')
        self.assertEqual(response.status_code, 200)

