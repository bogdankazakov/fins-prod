from app_project.tests.test_tools import create_project, create_service
from app_project.tests.test_main import Setup
from subdomains.utils import reverse as sub_reverse
from fins.global_functions import get_db_name_test
from app_project import views as app_project_views
from app_project import models as app_project_models
from fins.threadlocal import thread_local
from django.contrib.messages.storage.fallback import FallbackStorage
from app_project.tools import delete_checker_team
from django.core.exceptions import ValidationError


class TeamTest(Setup):

    def test_team_CRUD(self):

        self.client.login(username='b@b.ru', password='123')

        # create
        response, created_project = create_project(self.brand.pk, self.factory, self.my_user, self.account)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(created_project)

        #test teamproject basic creation
        team_projects = created_project.teamprojects_in_project.all()
        self.assertNotEqual(len(team_projects), 0)


        #create another team member
        kwargs = {'projectpk': created_project.pk}
        link = sub_reverse('app_project:team_create', kwargs=kwargs ,subdomain='mysubdomain')
        data = {
                'role': ['0'], #role = member
                'user': self.user2.pk,
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.team_create(request, **kwargs)
            return response

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        response = tread(request)

        # print('content', response.content)
        # print('content', response.content)
        # try:
        #
        # except ValidationError:
        #     print (err)
        # print('ValidationError', ValidationError, err)
        # self.assertRaises(ValidationError)

        created = app_project_models.TeamProject.objects.using(get_db_name_test()).get(project=created_project.pk, user=self.user2.pk, role=0)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(created)


        #update team member
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:team_update', kwargs=kwargs ,subdomain='mysubdomain')
        data = {
                'role': ['2'], #role = supervisor
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.team_update(request, **kwargs)
            return response

        response = tread(request)

        update = app_project_models.TeamProject.objects.using(get_db_name_test()).get(project=created_project.pk, user=self.user2.pk, role=2)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(update)



        #update team member try to make 2 leader in one project
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:team_update', kwargs=kwargs ,subdomain='mysubdomain')
        data = {
                'role': ['1'], #role = leader
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.team_update(request, **kwargs)
            return response
        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        response = tread(request)


        update = app_project_models.TeamProject.objects.using(get_db_name_test()).filter(project=created_project.pk, role=1)
        self.assertTrue(len(update),1) #checks that seconde leader wasn't created


        #delete created member
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_project:team_delete',kwargs=kwargs, subdomain='mysubdomain')
        data = {

        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account
        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_project_views.team_delete(request, **kwargs)
            return response

        #need this to make messages work
        setattr(request, 'session', 'session')
        messages = FallbackStorage(request)
        setattr(request, '_messages', messages)
        response = tread(request)

        deleted = app_project_models.TeamProject.objects.using(get_db_name_test()).filter(project=created_project.pk, user=self.user2.pk)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)


        #delete basic member
        basic = app_project_models.TeamProject.objects.using(get_db_name_test()).get(project=created_project.pk, user=self.my_user.pk)
        deletable, related_obj = delete_checker_team(basic, created_project)
        self.assertEqual(deletable, False) #check that basic can't be deleted