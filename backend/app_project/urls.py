from django.contrib import admin
from django.urls import path, include, re_path
from django.contrib.auth import views as auth_views
from . import views

app_name = 'app_project'
urlpatterns = [
    # path('', views.index, name='index'),
    # path('project/<pk>/', views.project, name='project'),
    path('api/', views.project_list),
    path('api/<int:pk>/', views.project_detail),
    path('api/extra/', views.project_extra),
    path('api/statistics/<int:pk>/', views.project_statistics),
    path('api/history/', views.project_history),
    path('api/excel/', views.excel_export),

    path('api/team_list/<int:pk>/', views.project_team_list),
    path('api/team/<int:pk>/', views.project_team_detail),

    path('api/service_list/<int:pk>/', views.project_service_list),
    path('api/service/<int:pk>/', views.project_service_detail),

    path('api/period_list/<int:pk>/', views.project_period_list),
    path('api/period/<int:pk>/', views.project_period_detail),

    path('api/report_list/<int:pk>/', views.project_report_list),
    path('api/report/<int:pk>/', views.project_report_detail),





    # path('project/<pk>/', views.project, name='project'),
    # path('project_create/', views.project_create, name='project_create'),
    # path('project_update/<pk>/', views.project_update, name='project_update'),
    # path('project_update_estimation/<pk>/', views.project_update_estimation, name='project_update_estimation'),
    # path('project_delete/<pk>/', views.project_delete, name='project_delete'),
    #
    # path('project/history', views.project_history, name='project_history'),
    #
    # path('ajax/load-subcat/', views.load_subcat, name='ajax_load_subcat'),
    #
    # path('create_period/<projectpk>/', views.create_period, name='create_period'),
    # path('update_period/<pk>/', views.update_period, name='update_period'),
    # path('delete_period/<pk>/', views.delete_period, name='delete_period'),
    #
    # path('service_create/<projectpk>/', views.service_create, name='service_create'),
    # path('service_update/<pk>/', views.service_update, name='service_update'),
    # path('service_delete/<pk>/', views.service_delete, name='service_delete'),
    #
    # path('report_create/<projectpk>/<periodpk>', views.report_create, name='report_create'),
    # path('report_update/<pk>/<projectpk>', views.report_update, name='report_update'),
    # path('report_delete/<pk>/', views.report_delete, name='report_delete'),
    #
    # path('team_create/<projectpk>/', views.team_create, name='team_create'),
    # path('team_update/<pk>/', views.team_update, name='team_update'),
    # path('team_delete/<pk>/', views.team_delete, name='team_delete'),
    #
    # path('excel_export/', views.excel_export, name='excel_export'),

]


