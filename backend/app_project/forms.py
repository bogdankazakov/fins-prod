from django.forms import ModelForm, Form
from app_project import models as app_project_models
from app_transaction import models as app_transaction_models
from django import forms
from django.forms import formset_factory, modelformset_factory


class CreateProjectForm(ModelForm):

    class Meta:
        model = app_project_models.Project
        fields = (
            'category',
            'subcategory',
            'brand',
            'name_short',
            'work_format',
            'agreement_format',
            'commission',

        )

        widgets = {
            'commission': forms.TextInput(attrs={'hidden': 'true', 'type':'number'}),
            'brand': forms.Select(attrs={'hidden': 'true', }),
            'work_format': forms.Select(attrs={'hidden': 'true'}),
            'agreement_format': forms.Select(attrs={'hidden': 'true'}),

        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['subcategory'].queryset = app_project_models.ProjectSubcategory.objects.none()
        print(self.fields['brand'].label)




        if 'category' in self.data:
            try:
                category_id = int(self.data.get('category'))
                self.fields['subcategory'].queryset = app_project_models.ProjectSubcategory.objects.filter(category_id=category_id).order_by('name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['subcategory'].queryset = self.instance.category.subcategory_set.order_by('name')

class ProjectBasicForm(ModelForm):

    class Meta:
        model = app_project_models.Project
        fields = (
            'name_short',
            'name_long',
            'subcategory',
            'stage',
            'project_start',
            'project_end',
            'brand',
            'nb_source',
            'nb_format',
            'project_products',
            'description',
            'commission',
            'is_editable',
        )

        widgets = {
            'description': forms.Textarea(attrs={'rows':4}),
            'project_start': forms.TextInput(attrs={'type': 'date'}),
            'project_end': forms.TextInput(attrs={'type': 'date'}),
            # 'project_products': forms.ModelMultipleChoiceField(
            #     widget=forms.CheckboxSelectMultiple(),
            #     queryset=qs)

        }

    def __init__(self, *args, **kwargs):
        self.using = kwargs.pop('using')
        qs = app_project_models.ProjectProduct.objects.using(self.using).all()

        CHOICES = []
        for el in qs:
            choice = (el.id, el.name)
            CHOICES.append(choice)
        CHOICES = tuple(CHOICES)

        super(ProjectBasicForm, self).__init__(*args, **kwargs)

        self.fields['project_products'].widget = forms.CheckboxSelectMultiple(
            choices=CHOICES,
            attrs={'class': 'form-check-input'},
        )

class ProjectBaseForm(ProjectBasicForm):

    class Meta(ProjectBasicForm.Meta):
        exclude = (
            'commission',
        )

class ProjectAgencyForm(ProjectBasicForm):
    pass


class ProjectEstimationForm(ModelForm):

    class Meta:
        model = app_project_models.Project
        fields = (
            'estimate_probability',
            'estimate_presale_start',
            'estimate_presale_end',
            'estimate_presale_outcome_ext',
            'estimate_presale_outcome_int',
            'estimate_inprogress_start',
            'estimate_inprogress_end',
            'estimate_inprogress_income_before_taxes',
            'estimate_inprogress_gross_profitability_before_taxes',
            'estimate_inprogress_gross_profit_before_taxes',
            'estimate_inprogress_project_profitability_before_taxes',
            'estimate_inprogress_project_profit_before_taxes',

        )

        NUMBER_CHOICES = (
            ('0', '0%'),
            ('5', '5%'),
            ('10', '10%'),
            ('15', '15%'),
            ('20', '20%'),
            ('25', '25%'),
            ('30', '30%'),
            ('35', '35%'),
            ('40', '40%'),
            ('45', '45%'),
            ('50', '50%'),
            ('55', '55%'),
            ('60', '60%'),
            ('65', '65%'),
            ('70', '70%'),
            ('75', '75%'),
            ('80', '80%'),
            ('85', '85%'),
            ('90', '90%'),
            ('95', '95%'),
            ('100', '100%'),
        )

        widgets = {
            'estimate_presale_start': forms.TextInput(attrs={'type': 'date'}),
            'estimate_presale_end': forms.TextInput(attrs={'type': 'date'}),
            'estimate_inprogress_start': forms.TextInput(attrs={'type': 'date'}),
            'estimate_inprogress_end': forms.TextInput(attrs={'type': 'date'}),
            'estimate_probability':forms.Select(choices=NUMBER_CHOICES),
            'estimate_inprogress_gross_profitability_before_taxes':forms.Select(choices=NUMBER_CHOICES),
            'estimate_inprogress_project_profitability_before_taxes':forms.Select(choices=NUMBER_CHOICES),

            'estimate_presale_outcome_ext': forms.TextInput(attrs={'type': 'text'}),
            'estimate_presale_outcome_int': forms.TextInput(attrs={'type': 'text'}),
            'estimate_inprogress_income_before_taxes': forms.TextInput(attrs={'type': 'text'}),
            'estimate_inprogress_gross_profit_before_taxes': forms.TextInput(attrs={'type': 'text'}),
            'estimate_inprogress_project_profit_before_taxes': forms.TextInput(attrs={'type': 'text'}),

        }

class CreatePeriodForm(ModelForm):

    class Meta:
        model = app_project_models.ReportPeriod
        fields = (
            'start',
            'end',
        )

        widgets = {
            'start': forms.TextInput(attrs={'type': 'date', 'required':'true'}),
            'end': forms.TextInput(attrs={'type': 'date', 'required':'true'}),

        }

class ServiceForm(ModelForm):
    class Meta:
        model = app_project_models.Service
        fields = (
            'name',
            'rate_bt',
        )



class ReportForm(ModelForm):
    class Meta:
        model = app_project_models.Report
        fields = (
            'name',
        )





class ServiceInReportForm(ModelForm):
    class Meta:
        model = app_project_models.ServiceInReport
        fields = (
            'service',
            'hours',
            'description',
        )

        widgets = {
            'service': forms.Select(attrs={'hidden': 'true'}),
        }


ServiceInReportFormSet = formset_factory(ServiceInReportForm, extra=0)

ServiceInReportUpdateFormSet = modelformset_factory(
    app_project_models.ServiceInReport, fields=('service', 'hours', 'description',), extra=0,
    widgets = {'service': forms.Select(attrs={'hidden': 'true'}), }
)


class TeamForm(ModelForm):
    class Meta:
        model = app_project_models.TeamProject
        fields = (
            'role',
            'user',
        )
        widgets = {
            'role': forms.Select(attrs={'required': 'true'}),
            'user': forms.Select(attrs={'required': 'true'}),
        }

    #
    # def clean(self):
    #     form_data = self.cleaned_data
    #     user = form_data['user']
    #     app_project_models.TeamProject.objects.filter(project)
    #     self._errors["user"] = ["Password do not match"]
    #     return form_data


class TeamUpdateForm(ModelForm):
    class Meta:
        model = app_project_models.TeamProject
        fields = (
            'role',
        )
        widgets = {
            'role': forms.Select(attrs={'required': 'true'}),
        }