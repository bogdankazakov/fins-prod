from django.contrib.auth.models import User
import django_filters
from .models import *
from django.db.models import Q
from django.http.request import HttpRequest
from django import forms
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from app_setup import models as app_setup_models
from django_filters import Filter
from django_filters.fields import Lookup
import json
from django.db.models import Q, F
from django.db.models import Case, When, DateField, DecimalField, ExpressionWrapper

from django.core.validators import EMPTY_VALUES
from app_project.tools import statistics

BOOL_CHOICES_YESNO = (
    (True,'Да'),
    (False,'Нет'),
)

BOOL_CHOICES_INCOME = (
    (True,'Доход'),
    (False,'Расход'),
)

WORK_FORMAT_CHOICES = (
    (0, 'Fix'),
    (1, 'Time&Materials'),
)

AGREEMENT_FORMAT_CHOICES = (
    (0, 'Договор подряда'),
    (1, 'Агентский договор'),
)

STAGES_CHOICES = (
        (0, 'Новый'),
        (1, 'Оценка'),
        (2, 'Продажа'),
        (3, 'Реализация'),
        (4, 'Завершен'),
        (5, 'Не продан'),
    )

# создан специальный фильтр чтобы определять что строка пустая
class EmptyStringFilter(django_filters.BooleanFilter):
    def filter(self, qs, value):
        if value in EMPTY_VALUES:
            return qs

        exclude = self.exclude ^ (value is True)
        method = qs.exclude if exclude else qs.filter

        return method(**{self.name: ""})



class ProjectsFilter(django_filters.FilterSet):
    # widget = forms.TextInput(attrs={'id': 'text'})
    name = django_filters.CharFilter(lookup_expr='icontains')
    name_short = django_filters.CharFilter(lookup_expr='icontains')
    name_long = django_filters.CharFilter(lookup_expr='icontains')
    category = django_filters.ModelMultipleChoiceFilter(queryset=app_project_models.ProjectCategory.objects.all())
    subcategory = django_filters.ModelMultipleChoiceFilter(queryset=app_project_models.ProjectSubcategory.objects.all())
    brand = django_filters.ModelMultipleChoiceFilter(queryset=app_project_models.Brand.objects.all())
    work_format = django_filters.MultipleChoiceFilter(choices = WORK_FORMAT_CHOICES)
    agreement_format = django_filters.MultipleChoiceFilter(choices = AGREEMENT_FORMAT_CHOICES)
    stage = django_filters.MultipleChoiceFilter(choices = STAGES_CHOICES)
    project_start = django_filters.DateFromToRangeFilter()
    project_end = django_filters.DateFromToRangeFilter()
    nb_source = django_filters.ModelMultipleChoiceFilter(queryset=app_project_models.NBSource.objects.all())
    nb_format = django_filters.ModelMultipleChoiceFilter(queryset=app_project_models.NBFormat.objects.all())
    product_category = django_filters.ModelMultipleChoiceFilter(queryset=app_project_models.ProductCategory.objects.all())
    project_products = django_filters.ModelMultipleChoiceFilter(queryset=app_project_models.ProjectProduct.objects.all())
    created_at = django_filters.DateFromToRangeFilter()
    created_by = django_filters.ModelMultipleChoiceFilter(queryset=app_setup_models.DBUser.objects.all())
    modified_at = django_filters.DateFromToRangeFilter()
    modified_by = django_filters.ModelMultipleChoiceFilter(queryset=app_setup_models.DBUser.objects.all())
    team= django_filters.ModelMultipleChoiceFilter(queryset=app_setup_models.DBUser.objects.all(),method='team_filter',)
    leader= django_filters.ModelMultipleChoiceFilter(queryset=app_setup_models.DBUser.objects.all(),method='leader_filter',)
    units= django_filters.ModelMultipleChoiceFilter(queryset=app_transaction_models.Unit.objects.all(),method='units_filter',)

    income= django_filters.RangeFilter(method='income_filter')
    outcome_ext= django_filters.RangeFilter(method='outcome_ext_filter')
    gross_profit= django_filters.RangeFilter(method='gross_profit_filter')
    gross_profitability= django_filters.RangeFilter(method='gross_profitability_filter')

    is_editable = django_filters.MultipleChoiceFilter(choices = BOOL_CHOICES_YESNO)


    def statictics_filter(self, value, queryset, param):
        transactions = app_transaction_models.Transaction.objects.all()
        projects = app_project_models.Project.objects.all()
        main_currency = app_transaction_models.Currency.objects.get(is_default=True)

        q = Q()
        for p in projects:
            tr = transactions.filter(project=p)
            report = statistics(tr, main_currency, transactions, False)
            income = report[0][param]

            if value.start is not None and value.stop is not None:
                if income >= Decimal(value.start) and income <= Decimal(value.stop):
                    q = q | Q(pk=p.pk)
            elif value.start is None and value.stop is not None:
                if income <= Decimal(value.stop):
                    q = q | Q(pk=p.pk)
            else:
                if income >= Decimal(value.start):
                    q = q | Q(pk=p.pk)

        if len(q) == 0:
            return queryset.none()
        else:
            return queryset.filter(q)


    def income_filter(self, queryset, name, value):
        queryset = self.statictics_filter(value, queryset, 'income')
        return queryset

    def outcome_ext_filter(self, queryset, name, value):
        queryset = self.statictics_filter(value, queryset, 'outcome_ext')
        return queryset

    def gross_profit_filter(self, queryset, name, value):
        queryset = self.statictics_filter(value, queryset, 'gross_profit')
        return queryset

    def gross_profitability_filter(self, queryset, name, value):
        queryset = self.statictics_filter(value, queryset, 'gross_profitability')
        return queryset

    def units_filter(self, queryset, name, value):
        project_list=[]
        q = Q()
        for val in value:
            for el in app_transaction_models.Transaction.objects.filter(unit=val):
                if el.project not in project_list:
                    q = q | Q(pk=el.project.pk)
                    project_list.append(el.project)

        return queryset.filter(q)

    def leader_filter(self, queryset, name, value):
        project_list=[]
        q = Q()
        for val in value:
            for el in app_project_models.TeamProject.objects.filter(user=val, role=1):
                if el.project not in project_list:
                    q = q | Q(pk=el.project.pk)
                    project_list.append(el.project)
        return queryset.filter(q)

    def team_filter(self, queryset, name, value):
        project_list=[]
        q = Q()
        for val in value:
            for el in app_project_models.TeamProject.objects.filter(user=val):
                if el.project not in project_list:
                    q = q | Q(pk=el.project.pk)
                    project_list.append(el.project)
        return queryset.filter(q)

    class Meta:
        model = app_project_models.Project
        fields = [ ]


    def __init__(self, data, *args, **kwargs):
        if  len(data) == 0:
            data = data.copy()
            data.update({'stage': '0'})
            data.update({'stage': '1'})
            data.update({'stage': '2'})
            data.update({'stage': '3'})


        super().__init__(data, *args, **kwargs)



def filterMain(qs, filter_param, is_json = True):
    print(filter_param)


    if is_json:
        filter_param = json.loads(filter_param)

    query = Q()
    statistic_filter = False

    for (key, value) in filter_param.items():

        if key == 'name' and len(value) > 0:
            query &= Q(name__icontains=value)

        if key == 'name_short' and len(value) > 0:
            query &= Q(name_short__icontains=value)

        elif key == 'category' and len(value)>0:
            query &= Q(category__in=value)

        elif key == 'subcategory' and len(value)>0:
            query &= Q(subcategory__in=value)

        elif key == 'brand' and len(value)>0:
            query &= Q(brand__in=value)

        elif key == 'work_format' and len(value) > 0:
            query &= Q(work_format__in=value)

        elif key == 'agreement_format' and len(value) > 0:
            query &= Q(agreement_format__in=value)

        elif key == 'stage' and len(value) > 0:
            query &= Q(stage__in=value)



        elif key == 'dateStartFrom' and len(value) > 0:
            query &= Q(project_start__gte=value)

        elif key == 'dateStartTo' and len(value) > 0:
            query &= Q(project_start__lte=value)


        elif key == 'dateEndFrom' and len(value) > 0:
            query &= Q(project_end__gte=value)

        elif key == 'dateEndTo' and len(value) > 0:
            query &= Q(project_end__lte=value)


        elif key == 'nbsource' and len(value) > 0:
            query &= Q(nbsource__in=value)

        elif key == 'nbformat' and len(value) > 0:
            query &= Q(nbformat__in=value)

        # elif key == 'products' and len(value) > 0:
        #     query &= Q(project_products__in=value)


        elif key == 'createdAtFrom' and len(value) > 0:
            query &= Q(created_at__gte=value)

        elif key == 'createdAtTo' and len(value) > 0:
            query &= Q(created_at__lte=value)

        elif key == 'createdby' and len(str(value)) > 0:
            query &= Q(modified_by=value)

        elif key == 'modifiedAtFrom' and len(value) > 0:
            query &= Q(modified_at__gte=value)

        elif key == 'modifiedAtTo' and len(value) > 0:
            query &= Q(modified_at__lte=value)

        elif key == 'modifiedby' and len(str(value)) > 0:
            query &= Q(modified_by=value)


        # elif key == 'project_leader' and len(value) > 0:
        #     query &= Q(project_leader__in=value)
        #
        # elif key == 'project_team' and len(value) > 0:
        #     query &= Q(project_team__in=value)
        #
        elif key == 'units' and len(value) > 0:
            query &= Q(project_transaction__unit__in=value)

        elif key == 'isEditable' and len(value) > 0:
            query &= Q(is_editable__in=value)

        elif key in [
            'incomeFrom',
            'incomeTo',
            'outcomeFrom',
            'outcomeTo',
            'grossProfitFrom',
            'grossProfitTo',
            'grossProfitabilityFrom',
            'grossProfitabilityTo',
        ]:
            statistic_filter =  True



    filtered = qs.filter(query)

    if  statistic_filter:
        transactions_all = app_transaction_models.Transaction.objects.select_related(
            'unit',
            'account__currency',

        ).all()
        main_currency = app_transaction_models.Currency.objects.get(is_default=True)

        for project in filtered:
            print(type(project))
            transactions = transactions_all.filter(project=project)

            all = transactions_all.annotate(
                date=Case(
                    When(date_fact=None, then='date_doc'),
                    default='date_fact',
                    output_field=DateField(),
                )).order_by('date')
            report = statistics(transactions, main_currency, all, False)
            project.income_filter = report[0]['income']
            project.outcome_ext_filter = report[0]['outcome_ext']
            project.gross_profit_filter = report[0]['gross_profit']
            project.gross_profitability_filter = report[0]['gross_profitability']



        for (key, value) in filter_param.items():

            if key == 'incomeFrom' and len(value) > 0:
                new = []
                for project in filtered:
                    if project.income_filter >= float(value):
                        new.append(project)
                filtered = new

            elif key == 'incomeTo' and len(value) > 0:
                new = []
                for project in filtered:
                    if project.income_filter <= float(value):
                        new.append(project)
                filtered = new

            elif key == 'outcomeFrom' and len(value) > 0:
                new = []
                for project in filtered:
                    if project.outcome_ext_filter >= float(value):
                        new.append(project)
                filtered = new

            elif key == 'outcomeTo' and len(value) > 0:
                new = []
                for project in filtered:
                    if project.outcome_ext_filter <= float(value):
                        new.append(project)
                filtered = new

            elif key == 'grossProfitFrom' and len(value) > 0:
                new = []
                for project in filtered:
                    if project.gross_profit_filter >= float(value):
                        new.append(project)
                filtered = new

            elif key == 'grossProfitTo' and len(value) > 0:
                new = []
                for project in filtered:
                    if project.gross_profit_filter <= float(value):
                        new.append(project)
                filtered = new

            elif key == 'grossProfitabilityFrom' and len(value) > 0:
                new = []
                for project in filtered:
                    if project.gross_profitability_filter >= float(value):
                        new.append(project)
                filtered = new

            elif key == 'grossProfitabilityTo' and len(value) > 0:
                new = []
                for project in filtered:
                    if project.gross_profitability_filter <= float(value):
                        new.append(project)
                filtered = new

    return filtered, statistic_filter