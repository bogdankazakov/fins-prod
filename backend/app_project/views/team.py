from app_project.decorators import *
from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_project import models as app_project_models
from app_project.forms import TeamForm, TeamUpdateForm
from app_project.tools import delete_checker_team
from app_setup.tools import log_data
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.contrib import messages
from app_project.views.main import wording
from django.template import Context, Template, RequestContext





@login_required
@access_company_level
@access_app_project_team_C
def team_create(request, projectpk):
    if request.method == 'POST':
        form = TeamForm(request.POST)
    else:
        form = TeamForm()
    return team_save(request, form, 'app_project/team_CRUD/partial_create_team.html', projectpk)

@login_required
@access_company_level
@access_app_project_team_RUD
def team_update(request, pk):
    item = app_project_models.TeamProject.objects.get(pk=pk)
    projectpk = item.project.pk

    if request.method == 'POST':
        form = TeamUpdateForm(request.POST, instance=item)
    else:
        form = TeamUpdateForm(instance=item)

    return team_save(request, form, 'app_project/team_CRUD/partial_update_team.html', projectpk)

@login_required
@access_company_level
@access_app_project_section
def team_save(request, form, template_name, projectpk):
    data = dict()

    if request.method == 'POST':
        if form.is_valid():
            project = app_project_models.Project.objects.get(pk=projectpk)
            try:
                user = form.cleaned_data['user']
                raise_error = False
                if app_project_models.TeamProject.objects.filter(project=project, user=user).exists():
                    messages.error(request, wording['validation_error'])
                    form._errors["user"] = ["Пользователь уже добавлен в этот проект"]
                    raise_error = True
            except Exception: # means we create team member
                raise_error = False
                if form.cleaned_data['role'] == 1 and app_project_models.TeamProject.objects.filter(project=project, role=1).exists():
                    messages.error(request, wording['validation_error'])
                    form._errors["role"] = ["Лидер на проекте может быть только один"]

                    raise_error = True

            if raise_error:
                pass
            else:
                instance = form.save(commit=False)
                log_data(instance, request)
                instance.save()
                instance.project = project
                instance.save()

                data['form_is_valid'] = True
                if 'partial_update_team' in template_name:
                    messages.add_message(request, messages.SUCCESS, 'Роль пользователья обновлена')
                else:
                    messages.add_message(request, messages.SUCCESS, 'Пользователь добавлен')

                context = {
                    'services': app_project_models.Service.objects.filter(project=project),
                    'wording': wording,
                    'project': project,
                    'team': app_project_models.TeamProject.objects.filter(project=project),
                    'messages': request._messages,
                }
                data['html_list'] = render_to_string('app_project/project_card/partial_team.html', context)
                data['html_partial_project_messages'] = render_to_string('app_project/project_card/partial_project_messages.html', context)
        else:
            messages.error(request, wording['validation_error'])

    if request.method == 'GET':
        data['form_is_valid'] = False
    context = {
            'form': form,
            'wording': wording,
            'projectpk': projectpk,
           }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
@access_company_level
@access_app_project_team_RUD
def team_delete(request, pk):
    data = dict()
    item = app_project_models.TeamProject.objects.get(pk=pk)
    project = item.project
    if request.method == 'POST':
        item.delete()
        data['form_is_valid'] = True
        messages.add_message(request, messages.SUCCESS, 'Пользователь удален из команды проекта')

        context = {
            'team': app_project_models.TeamProject.objects.filter(project=project),
            'wording': wording,
            'project': project,
            'messages': request._messages,
        }
        data['html_list'] = render_to_string('app_project/project_card/partial_team.html', context)
        data['html_partial_project_messages'] = render_to_string('app_project/project_card/partial_project_messages.html',
                                                             context)

    if request.method == 'GET':


        deletable, related_obj = delete_checker_team(item, project)


        context = {
            'item': item,
            'deletable':deletable,
            'wording': wording,
        }
        data['html_form'] = render_to_string('app_project/team_CRUD/partial_delete_team.html',context,request=request,)
    return JsonResponse(data)
