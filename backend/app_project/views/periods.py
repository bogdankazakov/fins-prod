from app_project.decorators import *
from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_project import models as app_project_models
from app_transaction import models as app_transaction_models
from app_project.forms import CreatePeriodForm
from app_project.tools import period_tool
from app_setup.tools import log_data
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.contrib import messages
from app_project.views.main import wording


@login_required
@access_company_level
@access_app_project_periods_C
def create_period(request, projectpk):
    if request.method == 'POST':
        form = CreatePeriodForm(request.POST)
    else:
        form = CreatePeriodForm()
    return period_save(request, form, 'app_project/period_CRUD/partial_create_period.html', projectpk)

@login_required
@access_company_level
@access_app_project_periods_RUD
def update_period(request, pk):
    item = app_project_models.ReportPeriod.objects.get(pk=pk)
    projectpk = item.project.pk

    if request.method == 'POST':
        form = CreatePeriodForm(request.POST, instance=item)
    else:
        form = CreatePeriodForm(instance=item)

    return period_save(request, form, 'app_project/period_CRUD/partial_update_period.html', projectpk)

@login_required
@access_company_level
@access_app_project_section
def period_save(request, form, template_name, projectpk):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            project = app_project_models.Project.objects.get(pk=projectpk)
            instance = form.save(commit=False)
            log_data(instance, request)
            instance.save()
            instance.project = project
            instance.save()

            data['form_is_valid'] = True

            if 'partial_update_period' in template_name:
                messages.add_message(request, messages.SUCCESS, 'Период обновлен')
            else:
                messages.add_message(request, messages.SUCCESS, 'Период добавлен')

            context = {
                'periods': period_tool(project),
                'wording': wording,
                'project': project,
                'obj': app_transaction_models.Transaction,
                'messages': request._messages,

            }
            data['html_list'] = render_to_string('app_project/project_card/partial_list_periods.html', context)
            data['html_partial_project_messages'] = render_to_string(
                'app_project/project_card/partial_project_messages.html',
                context)
        else:
            messages.error(request, wording['validation_error'])

    if request.method == 'GET':
        data['form_is_valid'] = False
    context = {
            'form': form,
            'wording': wording,
            'projectpk': projectpk,
           }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
@access_company_level
@access_app_project_periods_RUD
def delete_period(request, pk):
    data = dict()
    item = app_project_models.ReportPeriod.objects.get(pk=pk)
    project = item.project
    if request.method == 'POST':
        item.delete()
        data['form_is_valid'] = True
        messages.add_message(request, messages.SUCCESS, 'Период {0} удален'.format(item))

        context = {
            'periods': period_tool(project),
            'wording': wording,
            'project': project,
            'obj': app_transaction_models.Transaction,
            'messages': request._messages,

        }
        data['html_list'] = render_to_string('app_project/project_card/partial_list_periods.html', context)
        data['html_partial_project_messages'] = render_to_string('app_project/project_card/partial_project_messages.html',
                                                             context)

    if request.method == 'GET':
        deletable, related_obj = item.can_delete()


        context = {
            'item': item,
            'deletable':deletable,
            'wording': wording,
        }
        data['html_form'] = render_to_string('app_project/period_CRUD/partial_delete_period.html',context,request=request,)
    return JsonResponse(data)

