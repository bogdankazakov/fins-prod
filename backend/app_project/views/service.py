from app_project.decorators import *
from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_project import models as app_project_models
from app_project.forms import ServiceForm
from app_setup.tools import log_data
from django.template.loader import render_to_string
from django.http import JsonResponse
from django.contrib import messages
from app_project.views.main import wording

@login_required
@access_company_level
@access_app_project_services_C
def service_create(request, projectpk):
    if request.method == 'POST':
        form = ServiceForm(request.POST)
    else:
        form = ServiceForm()
    return service_save(request, form, 'app_project/service_CRUD/partial_create_service.html', projectpk)

@login_required
@access_company_level
@access_app_project_services_RUD
def service_update(request, pk):
    item = app_project_models.Service.objects.get(pk=pk)
    projectpk = item.project.pk

    if request.method == 'POST':
        form = ServiceForm(request.POST, instance=item)
    else:
        form = ServiceForm(instance=item)

    return service_save(request, form, 'app_project/service_CRUD/partial_update_service.html', projectpk)

@login_required
@access_company_level
@access_app_project_section
def service_save(request, form, template_name, projectpk):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            project = app_project_models.Project.objects.get(pk=projectpk)
            instance = form.save(commit=False)
            log_data(instance, request)
            instance.save()
            instance.project = project
            instance.save()

            data['form_is_valid'] = True

            if 'partial_update_service' in template_name:
                messages.add_message(request, messages.SUCCESS, 'Услуга обновлена')
            else:
                messages.add_message(request, messages.SUCCESS, 'Услуга добавлен')


            context = {
                'services': app_project_models.Service.objects.filter(project=project),
                'wording': wording,
                'project': project,
                'messages': request._messages,

            }
            data['html_list'] = render_to_string('app_project/project_card/partial_list_services.html', context)
            data['html_partial_project_messages'] = render_to_string(
                'app_project/project_card/partial_project_messages.html',
                context)
        else:
            messages.error(request, wording['validation_error'])

    if request.method == 'GET':
        data['form_is_valid'] = False
    context = {
            'form': form,
            'wording': wording,
            'projectpk': projectpk,
           }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
@access_company_level
@access_app_project_services_RUD
def service_delete(request, pk):
    data = dict()
    item = app_project_models.Service.objects.get(pk=pk)
    project = item.project
    if request.method == 'POST':
        item.delete()
        data['form_is_valid'] = True
        messages.add_message(request, messages.SUCCESS, 'Услуга {0} удалена'.format(item))

        context = {
            'services': app_project_models.Service.objects.filter(project=project),
            'wording': wording,
            'project': project,
            'messages': request._messages,

        }
        data['html_list'] = render_to_string('app_project/project_card/partial_list_services.html', context)
        data['html_partial_project_messages'] = render_to_string('app_project/project_card/partial_project_messages.html',
                                                             context)

    if request.method == 'GET':
        deletable, related_obj = item.can_delete()


        context = {
            'item': item,
            'deletable':deletable,
            'wording': wording,
        }
        data['html_form'] = render_to_string('app_project/service_CRUD/partial_delete_service.html',context,request=request,)
    return JsonResponse(data)





