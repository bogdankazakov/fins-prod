from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from app_auth.decorators import access_company_level
import logging
from app_project.decorators import *
from app_signup import models as app_signup_models
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from app_setup import models as app_setup_models
from app_project.forms import *
from django.template.loader import render_to_string
from django.http import JsonResponse
from app_setup.tools import log_data
from app_project.tools import *
from django.contrib import messages
from app_project.filters import ProjectsFilter
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect
import xlwt
import datetime
from django.contrib.sites.models import Site
from fins.global_functions import get_db_name
from fins.global_functions import history_record




logger = logging.getLogger(__name__)

wording = {
    'btn_create': 'Создать',
    'btn_add': 'Добавить транзакцию',
    'btn_add2': 'Добавить отчетный период',
    'btn_add3': 'Добавить услугу',
    'btn_add4': 'Добавить отчет',
    'btn_add5': 'Добавить члена команды',
    'btn_add_p': 'Добавить проект',
    'btn_delete': 'Удалить транзакцию',
    'btn_delete2': 'Удалить период',
    'btn_delete3': 'Удалить услугу',
    'btn_delete4': 'Удалить отчет',
    'btn_delete5': 'Удалить из команды',
    'btn_generate_report': 'Сгенерировать отчет',
    'btn_generate_assigment': 'Сгенерировать задание',
    'btn_delete_p': 'Удалить проект',
    'btn_cancel': 'Отмена',
    'btn_docs': 'Документы',
    'btn_update': 'Редактировать транзакцию',
    'btn_update2': 'Редактировать период',
    'btn_update3': 'Редактировать услугу',
    'btn_update4': 'Редактировать отчет',
    'btn_update5': 'Редактировать статус',
    'btn_update_p': 'Редактировать проект',
    'btn_est': 'Редактировать оценку проекта',
    'btn_save': 'Сохранить',
    'btn_history': 'История',
    'h1_list': 'Список транзакций',
    'h1_service': 'Список Услуг',
    'h1_list_p': 'Список проектов',
    'h1_period': 'Отчетные периоды',
    'h1_create': 'Создать транзакцию',
    'h1_create_p': 'Создать проект',
    'h1_create_service': 'Создать сервис',
    'h1_create_team': 'Добавить в команду',
    'h1_create_period': 'Создать отчетный период',
    'h1_create_report': 'Создать отчет',
    'h1_update': 'Редактировать',
    'h1_delete': 'Удалить транзакцию',
    'h1_delete2': 'Удалить период',
    'h1_delete3': 'Удалить услугу',
    'h1_delete4': 'Удалить отчет',
    'h1_delete5': 'Удалить из команды',
    'h1_delete_p': 'Удалить проект',
    'h1_history': 'История изменений проектов',
    'h1_docs': 'Список связанных документов',
    'validation_error': 'Ошибка валидации',
    'not_deletable_system': 'Мы не можем удалить проект, т.к. он является системным',
    'not_deletable': 'Мы не можем удалить проект. Сначала удалите все транзакции.',
    'not_deletable_team': 'Мы не можем удалить пользователя. Он остался один в проекте.',
    'approve_delete': ' Вы уверены, что хотите удалить проект ',
    'approve_delete2': ' Вы уверены, что хотите удалить период ',
    'approve_delete3': ' Вы уверены, что хотите удалить услугу ',
    'approve_delete4': ' Вы уверены, что хотите удалить отчет ',
    'approve_delete5': ' Вы уверены, что хотите удалить из команды пользователя ',
    'empty_str': '———',
    'none': '———',
}
def default_get_all():
    all = app_project_models.Project.objects.all().order_by('-pk')
    return all

def get_all_transactions():
    all = app_transaction_models.Transaction.objects.all().annotate(
                date=Case(
                    When(date_fact=None, then='date_doc'),
                    default='date_fact',
                    output_field=DateField(),
                )).order_by('date', 'pk')
    return all


def filter_data(request, page, queryset):
    filter = ProjectsFilter(request.GET, queryset=queryset)
    filter = filter_extra_data(filter)
    obj = app_project_models.Project
    obj2 = app_project_models.TeamProject

    if page is not None:
        request.GET = request.GET.copy()
        request.GET['page'] = page

    page = request.GET.get('page', 1)
    paginator = Paginator(filter.qs, 30)
    try:
        results = paginator.page(page)
    except PageNotAnInteger:
        results = paginator.page(1)
    except EmptyPage:
        results = paginator.page(paginator.num_pages)

    return filter, obj, results, obj2

@login_required
@access_company_level
@access_app_projects_list
def index(request, get_access_all):

    filter, obj, results, obj2 = filter_data(request, None, get_access_all())
    context = {
        'list': results,
        'list_num': range(1, int(results.paginator.num_pages) + 1),
        # 'sum': summ(filter.qs),
        'wording': wording,
        'filter': filter,
        'obj': obj,
        'obj2': obj2,
    }

    # saving request for excel export
    l = []
    for q in filter.qs:
        l.append(q.pk)
    request.session['project_filter_qs'] = l

    # from django.db import connection
    #
    # print(len(connection.queries))
    #
    # from django.db import reset_queries
    #
    # reset_queries()

    return render(request, 'app_project/list/projects.html', context)


@login_required
@access_company_level
@access_app_project_card
def project(request, pk):
    project = app_project_models.Project.objects.get(pk=pk)
    transactions = get_all_transactions().filter(project=project)
    messages = agency_checker(transactions, project)

    all = app_transaction_models.Transaction.objects.all().annotate(
            date=Case(
                When(date_fact=None, then='date_doc'),
                default='date_fact',
                output_field=DateField(),
            )).order_by('date')
    main_currency = app_transaction_models.Currency.objects.get(is_default=True)
    report = statistics(transactions, main_currency, all, False)

    obj = app_transaction_models.Transaction
    context = {
        'project': extra_data_p_card(project),
        'messages': messages,
        'list': transactions,
        'obj': obj,
        'wording': wording,
        'pk':pk,
        'report':report,
        'services': app_project_models.Service.objects.filter(project=project),
        'team': app_project_models.TeamProject.objects.filter(project=project),
        'agrs': project.project_agreements.all(),
        'user_can_block': app_setup_models.DBUser.objects.get(user_id=request.user.id).access_can_block,

    }

    if project.work_format == 1:
        context.update({
            'periods': period_tool(project),
        })
    return render(request, 'app_project/project_card/project.html', context)





@login_required
@access_company_level
@access_app_project_section
def project_create(request):
    if request.method == 'POST':
        form = CreateProjectForm(request.POST)
    else:
        form = CreateProjectForm()
    return project_save(request, form, 'app_project/project_CRUD/partial_create.html', True)

@login_required
@access_company_level
@access_app_project_card
def project_update(request, pk):
    item = app_project_models.Project.objects.get(pk=pk)
    if item.agreement_format == 1:
        if request.method == 'POST':
            form = ProjectAgencyForm(request.POST, instance=item, using=get_db_name(request))
        else:
            form = ProjectAgencyForm(instance=item, using=get_db_name(request))
    else:
        if request.method == 'POST':
            form = ProjectBaseForm(request.POST, instance=item, using=get_db_name(request))
        else:
            form = ProjectBaseForm(instance=item, using=get_db_name(request))
            print(form)

    return project_save(request, form, 'app_project/project_CRUD/partial_update.html', False)

@login_required
@access_company_level
@access_app_project_card
def project_update_estimation(request, pk):
    item = app_project_models.Project.objects.get(pk=pk)
    if request.method == 'POST':
        form = ProjectEstimationForm(request.POST, instance=item)
    else:
        form = ProjectEstimationForm(instance=item)

    return project_save(request, form, 'app_project/project_CRUD/partial_update_estimation.html', False)

@login_required
@access_company_level
@access_app_projects_list
def project_save(request, get_access_all, form, template_name, is_creation):
    data = dict()
    if request.method == 'POST':
        page = request.POST.get('page')

        form_converter(form)
        if form.is_valid():
            instance = form.save(commit=False)
            log_data(instance, request)


            instance.save()
            extra_data_post(instance)
            instance.save()

            data['form_is_valid'] = True

            if is_creation == True:
                add_user_to_team(instance, request)
                filter, obj, results, obj2 = filter_data(request, page, get_access_all())
                messages.add_message(request, messages.SUCCESS, 'Проект {0} создан'.format(form.instance.name))

                context = {
                    'list': results,
                    'wording': wording,
                    'messages': request._messages,

                }
                data['html_list'] = render_to_string('app_project/list/partial_list.html', context)
                data['html_pagination'] = render_to_string('app_project/list/pagination.html', context)
                data['html_partial_project_messages'] = render_to_string('app_project/project_card/partial_project_messages.html', context)

            else:
                m2m(instance, form)
                project =  app_project_models.Project.objects.get(pk=form.instance.pk)
                if 'partial_update_estimation' in template_name:
                    message = 'Предпродажная оценка проекта {0} отредактирована'.format(form.instance.name)
                    messages.add_message(request, messages.SUCCESS, message)

                else:
                    message = 'Базовые характеристики проекта {0} отредактированы'.format(form.instance.name)
                    messages.add_message(request, messages.SUCCESS, message)

                context = {
                    'project': extra_data_p_card(project),
                    'wording': wording,
                    'messages': request._messages,
                }
                data['html_partial_project'] = render_to_string('app_project/project_card/partial_project.html', context)
                data['html_partial_project_name'] = render_to_string('app_project/project_card/partial_project_name.html', context)
                data['html_partial_project_estimation'] = render_to_string('app_project/project_card/partial_project_estimation.html', context)
                data['html_partial_project_messages'] = render_to_string('app_project/project_card/partial_project_messages.html', context)


        else:
            messages.error(request, wording['validation_error'])
    if request.method == 'GET':
        data['form_is_valid'] = False
    context = {
            'form': form,
            'wording': wording,
           }
    data['html_form'] = render_to_string(template_name, context, request=request)
    # print('data', data['form_is_valid'])

    return JsonResponse(data)

@login_required
@access_company_level
@access_app_project_card
def project_delete(request, pk):
    data = dict()
    item = app_project_models.Project.objects.get(pk=pk)
    if request.method == 'POST':
        teamproject_killer(item)
        item.delete()
        data['form_is_valid'] = True
        messages.add_message(request, messages.SUCCESS, 'Проект {0} удален'.format(item.name))
        return redirect(reverse('app_project:index'))
    if request.method == 'GET':
        deletable, related_obj = can_delete_project(item)


        context = {
            'item': item,
            'deletable':deletable,
            'wording': wording,
        }
        data['html_form'] = render_to_string('app_project/project_CRUD/partial_delete.html',context,request=request,)
    return JsonResponse(data)

@login_required
@access_company_level
@access_app_project_section
def load_subcat(request):
    try:
        category_id = int(request.GET.get('category'))
        subcategory = app_project_models.ProjectSubcategory.objects.filter(category_id=category_id).order_by('name')
    except Exception:
        subcategory = None
    return render(request, 'app_project/project_CRUD/subcategory_dropdown_list_options.html', {'subcategory': subcategory})



@login_required
@access_company_level
@access_app_project_section
def project_history(request):

    history_project = app_project_models.Project.history.all()[:3]
    history_period = app_project_models.ReportPeriod.history.all()[:3]
    history_report = app_project_models.Report.history.all()[:3]
    history_service = app_project_models.Service.history.all()[:3]
    history_service_in_report = app_project_models.ServiceInReport.history.all()[:3]
    history_team = app_project_models.TeamProject.history.all()[:3]



    context = {
        'projects': history_record(history_project, 'Проект', True),
        'periods': history_record(history_period, 'Период', True),
        'reports': history_record(history_report, 'Отчет', True),
        'services': history_record(history_service, 'Услуга', True),
        'services_in_report': history_record(history_service_in_report, 'Услуга в отчете', True),
        'teams': history_record(history_team, 'Участник команды', True),
        'wording':wording,
    }

    data = {}
    data['html_form'] = render_to_string('app_project/list/partial_history.html', context, request=request)

    return JsonResponse(data)

