from app_project.decorators import *
from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_project import models as app_project_models
from app_project.tools import filter_extra_data
from app_project.filters import filterMain
from django.http import HttpResponse
import xlwt
import datetime
from django.contrib.sites.models import Site
from rest_framework.decorators import api_view

@api_view(['POST', ])
@login_required
@access_company_level
@access_app_projects_list
def excel_export(request, get_access_all):
    filter_param = request.data['filter']

    qs = get_access_all()
    all, use_dict = filterMain(qs, filter_param, is_json=False)

    response = HttpResponse(content_type='application/ms-excel')
    filename = str(Site.objects.all()[0]) + '_projects_' + str(datetime.datetime.now())
    response['Content-Disposition'] = 'attachment; filename=' + filename + '.xls'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet("Проекты")

    #table header
    row_num = 0
    model = app_project_models.Project
    columns = [
        (str(model._meta.get_field('name').verbose_name), 4000),
        (str(model._meta.get_field('name_short').verbose_name), 4000),
        (str(model._meta.get_field('brand').verbose_name), 4000),
        (str(model._meta.get_field('category').verbose_name), 4000),
        (str(model._meta.get_field('subcategory').verbose_name), 4000),
        (str(model._meta.get_field('work_format').verbose_name), 4000),
        (str(model._meta.get_field('agreement_format').verbose_name), 4000),
        (str(model._meta.get_field('commission').verbose_name), 4000),
        (str(model._meta.get_field('stage').verbose_name), 4000),
        (str(model._meta.get_field('project_start').verbose_name), 4000),
        (str(model._meta.get_field('project_end').verbose_name), 4000),
        # ("Лидер проекта", 4000),
        # ("Команда проекта", 4000),
        ("Юниты", 4000),
        ("Выручка проекта (в основной валюте)", 4000),
        ("Внешние расходы проекта (в основной валюте)", 4000),
        ("Валовая прибыль проекта (в основной валюте)", 4000),
        ("РВП проекта (в основной валюте)", 4000),

        (str(model._meta.get_field('nb_source').verbose_name), 4000),
        (str(model._meta.get_field('nb_format').verbose_name), 4000),
        (str(model._meta.get_field('estimate_probability').verbose_name), 4000),
        (str(model._meta.get_field('estimate_presale_start').verbose_name), 4000),
        (str(model._meta.get_field('estimate_presale_end').verbose_name), 4000),
        (str(model._meta.get_field('estimate_presale_outcome_ext').verbose_name), 4000),
        (str(model._meta.get_field('estimate_presale_outcome_int').verbose_name), 4000),
        (str(model._meta.get_field('estimate_inprogress_start').verbose_name), 4000),
        (str(model._meta.get_field('estimate_inprogress_end').verbose_name), 4000),
        (str(model._meta.get_field('estimate_inprogress_income_before_taxes').verbose_name), 4000),
        (str(model._meta.get_field('estimate_inprogress_gross_profitability_before_taxes').verbose_name), 4000),
        (str(model._meta.get_field('estimate_inprogress_gross_profit_before_taxes').verbose_name), 4000),
        (str(model._meta.get_field('estimate_inprogress_project_profitability_before_taxes').verbose_name), 4000),
        (str(model._meta.get_field('estimate_inprogress_project_profit_before_taxes').verbose_name), 4000),



        (str(model._meta.get_field('created_at').verbose_name), 4000),
        (str(model._meta.get_field('created_by').verbose_name), 4000),
        (str(model._meta.get_field('modified_at').verbose_name), 4000),
        (str(model._meta.get_field('modified_by').verbose_name), 4000),



    ]
    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    #rows
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num][0], font_style)
        # set column width
        ws.col(col_num).width = columns[col_num][1]

    font_style = xlwt.XFStyle()
    font_style.alignment.wrap = 1

    print('all', all)

    for obj in all:

        print('obj', obj)

        if obj.category == None:
            category = ''
        else:
            category = obj.category.name

        if obj.subcategory == None:
            subcategory = ''
        else:
            subcategory = obj.subcategory.name


        if obj.work_format == None:
            work_format = ''
        else:
            work_format = obj.get_work_format_display()

        if obj.agreement_format == None:
            agreement_format = ''
        else:
            agreement_format = obj.get_agreement_format_display()

        if obj.stage == None:
            stage = ''
        else:
            stage = obj.get_stage_display()

        if obj.brand == None:
            brand = ''
        else:
            brand = obj.brand.name

        if obj.project_start == None:
            project_start = ''
        else:
            project_start = obj.project_start.strftime("%d/%m/%Y")

        if obj.project_end == None:
            project_end = ''
        else:
            project_end = obj.project_end.strftime("%d/%m/%Y")


        # if obj.leader == None:
        #     leader = ''
        # else:
        #     leader = str(obj.leader)
        #
        # if obj.team == None:
        #     team = ''
        # else:
        #     team = str(obj.team)

        if obj.units == None:
            units = ''
        else:
            units = str(obj.units)



        if obj.nb_source == None:
            nb_source = ''
        else:
            nb_source = obj.nb_source.name

        if obj.nb_format == None:
            nb_format = ''
        else:
            nb_format = obj.nb_format.name

        if obj.estimate_probability == None:
            estimate_probability = ''
        else:
            estimate_probability = obj.estimate_probability

        if obj.estimate_presale_start == None:
            estimate_presale_start = ''
        else:
            estimate_presale_start = obj.estimate_presale_start.strftime("%d/%m/%Y")

        if obj.estimate_presale_end == None:
            estimate_presale_end = ''
        else:
            estimate_presale_end = obj.estimate_presale_end.strftime("%d/%m/%Y")

        if obj.estimate_presale_outcome_ext == None:
            estimate_presale_outcome_ext = ''
        else:
            estimate_presale_outcome_ext = obj.estimate_presale_outcome_ext

        if obj.estimate_presale_outcome_int == None:
            estimate_presale_outcome_int = ''
        else:
            estimate_presale_outcome_int = obj.estimate_presale_outcome_int

        if obj.estimate_inprogress_start == None:
            estimate_inprogress_start = ''
        else:
            estimate_inprogress_start = obj.estimate_inprogress_start.strftime("%d/%m/%Y")

        if obj.estimate_inprogress_end == None:
            estimate_inprogress_end = ''
        else:
            estimate_inprogress_end = obj.estimate_inprogress_end.strftime("%d/%m/%Y")

        if obj.estimate_inprogress_income_before_taxes == None:
            estimate_inprogress_income_before_taxes = ''
        else:
            estimate_inprogress_income_before_taxes = obj.estimate_inprogress_income_before_taxes

        if obj.estimate_inprogress_gross_profitability_before_taxes == None:
            estimate_inprogress_gross_profitability_before_taxes = ''
        else:
            estimate_inprogress_gross_profitability_before_taxes = obj.estimate_inprogress_gross_profitability_before_taxes

        if obj.estimate_inprogress_gross_profit_before_taxes == None:
            estimate_inprogress_gross_profit_before_taxes = ''
        else:
            estimate_inprogress_gross_profit_before_taxes = obj.estimate_inprogress_gross_profit_before_taxes

        if obj.estimate_inprogress_project_profitability_before_taxes == None:
            estimate_inprogress_project_profitability_before_taxes = ''
        else:
            estimate_inprogress_project_profitability_before_taxes = obj.estimate_inprogress_project_profitability_before_taxes

        if obj.estimate_inprogress_project_profit_before_taxes == None:
            estimate_inprogress_project_profit_before_taxes = ''
        else:
            estimate_inprogress_project_profit_before_taxes = obj.estimate_inprogress_project_profit_before_taxes


        if obj.created_at == None:
            created_at = ''
        else:
            created_at = obj.created_at.replace(tzinfo=None).strftime("%d/%m/%Y")

        if obj.created_by == None:
            created_by = ''
        else:
            created_by = str(obj.created_by)

        if obj.modified_at == None:
            modified_at = ''
        else:
            modified_at = obj.modified_at.replace(tzinfo=None).strftime("%d/%m/%Y")

        if obj.modified_by == None:
            modified_by = ''
        else:
            modified_by = str(obj.modified_by)









        row_num += 1
        row = [
            obj.name,
            obj.name_short,
            brand,
            category,
            subcategory,
            work_format,
            agreement_format,
            obj.commission,
            stage,
            project_start,
            project_end,
            # leader,
            # team,
            units,
            obj.income,
            obj.outcome_ext,
            obj.gross_profit,
            obj.gross_profitability,




            nb_source,
            nb_format,
            estimate_probability,
            estimate_presale_start,
            estimate_presale_end,
            estimate_presale_outcome_ext,
            estimate_presale_outcome_int,
            estimate_inprogress_start,
            estimate_inprogress_end,
            estimate_inprogress_income_before_taxes,
            estimate_inprogress_gross_profitability_before_taxes,
            estimate_inprogress_gross_profit_before_taxes,
            estimate_inprogress_project_profitability_before_taxes,
            estimate_inprogress_project_profit_before_taxes,


            created_at,
            created_by,
            modified_at,
            modified_by,
        ]


        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response


