from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from app_auth.decorators import access_company_level
import logging
from app_project.decorators import *
from app_signup import models as app_signup_models
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from app_setup import models as app_setup_models
from app_project.forms import *
from django.template.loader import render_to_string
from django.http import JsonResponse
from app_setup.tools import log_data
from app_project.tools_new import *
from django.contrib import messages
from app_project.filters import filterMain
from app_project.serializers import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect
import xlwt
# import datetime
from django.contrib.sites.models import Site
from fins.global_functions import get_db_name
from fins.global_functions import history_record
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
import math
import pytz
from datetime import datetime



logger = logging.getLogger(__name__)

wording = {
    'btn_create': 'Создать',
    'btn_add': 'Добавить транзакцию',
    'btn_add2': 'Добавить отчетный период',
    'btn_add3': 'Добавить услугу',
    'btn_add4': 'Добавить отчет',
    'btn_add5': 'Добавить члена команды',
    'btn_add_p': 'Добавить проект',
    'btn_delete': 'Удалить транзакцию',
    'btn_delete2': 'Удалить период',
    'btn_delete3': 'Удалить услугу',
    'btn_delete4': 'Удалить отчет',
    'btn_delete5': 'Удалить из команды',
    'btn_generate_report': 'Сгенерировать отчет',
    'btn_generate_assigment': 'Сгенерировать задание',
    'btn_delete_p': 'Удалить проект',
    'btn_cancel': 'Отмена',
    'btn_docs': 'Документы',
    'btn_update': 'Редактировать транзакцию',
    'btn_update2': 'Редактировать период',
    'btn_update3': 'Редактировать услугу',
    'btn_update4': 'Редактировать отчет',
    'btn_update5': 'Редактировать статус',
    'btn_update_p': 'Редактировать проект',
    'btn_est': 'Редактировать оценку проекта',
    'btn_save': 'Сохранить',
    'btn_history': 'История',
    'h1_list': 'Список транзакций',
    'h1_service': 'Список Услуг',
    'h1_list_p': 'Список проектов',
    'h1_period': 'Отчетные периоды',
    'h1_create': 'Создать транзакцию',
    'h1_create_p': 'Создать проект',
    'h1_create_service': 'Создать сервис',
    'h1_create_team': 'Добавить в команду',
    'h1_create_period': 'Создать отчетный период',
    'h1_create_report': 'Создать отчет',
    'h1_update': 'Редактировать',
    'h1_delete': 'Удалить транзакцию',
    'h1_delete2': 'Удалить период',
    'h1_delete3': 'Удалить услугу',
    'h1_delete4': 'Удалить отчет',
    'h1_delete5': 'Удалить из команды',
    'h1_delete_p': 'Удалить проект',
    'h1_history': 'История изменений проектов',
    'h1_docs': 'Список связанных документов',
    'validation_error': 'Ошибка валидации',
    'not_deletable_system': 'Мы не можем удалить проект, т.к. он является системным',
    'not_deletable': 'Мы не можем удалить проект. Сначала удалите все транзакции.',
    'not_deletable_team': 'Мы не можем удалить пользователя. Он остался один в проекте.',
    'approve_delete': ' Вы уверены, что хотите удалить проект ',
    'approve_delete2': ' Вы уверены, что хотите удалить период ',
    'approve_delete3': ' Вы уверены, что хотите удалить услугу ',
    'approve_delete4': ' Вы уверены, что хотите удалить отчет ',
    'approve_delete5': ' Вы уверены, что хотите удалить из команды пользователя ',
    'empty_str': '———',
    'none': '———',
}
# def default_get_all():
#     all = app_project_models.Project.objects.all().order_by('-pk')
#     return all
#
# def get_all_transactions():
#     all = app_transaction_models.Transaction.objects.all().annotate(
#                 date=Case(
#                     When(date_fact=None, then='date_doc'),
#                     default='date_fact',
#                     output_field=DateField(),
#                 )).order_by('date', 'pk')
#     return all


# def filter_data(request, page, queryset):
#     filter = ProjectsFilter(request.GET, queryset=queryset)
#     filter = filter_extra_data(filter)
#     obj = app_project_models.Project
#     obj2 = app_project_models.TeamProject
#
#     if page is not None:
#         request.GET = request.GET.copy()
#         request.GET['page'] = page
#
#     page = request.GET.get('page', 1)
#     paginator = Paginator(filter.qs, 30)
#     try:
#         results = paginator.page(page)
#     except PageNotAnInteger:
#         results = paginator.page(1)
#     except EmptyPage:
#         results = paginator.page(paginator.num_pages)
#
#     return filter, obj, results, obj2

@login_required
@access_company_level
@access_app_projects_list
def index(request, get_access_all):
    context = {
        'obj': app_project_models.Project,
        'obj2': app_project_models.TeamProject,
    }

    return render(request, 'app_project/new/index.html', context)





@api_view(['GET', ])
@login_required
@access_company_level
@access_app_projects_list
def project_extra(request,get_access_all):

    if request.method == 'GET':

        work_format = []
        for el in app_project_models.Project.WORK_FORMAT_CHOICES:
            work_format.append({
                'id': el[0],
                'name': el[1],
            })

        agreement_format = []
        for el in app_project_models.Project.AGREEMENT_FORMAT_CHOICES:
            agreement_format.append({
                'id': el[0],
                'name': el[1],
            })

        stage = []
        for el in app_project_models.Project.STAGES_CHOICES:
            stage.append({
                'id': el[0],
                'name': el[1],
            })

        team_role = []
        for el in app_project_models.TeamProject.ROLE_CHOICES:
            team_role.append({
                'id': el[0],
                'name': el[1],
            })

        data = {
            'extra': {
                'categories':ProjectCategorySerializer(app_project_models.ProjectCategory.objects.all(), many=True).data,
                'subcategories':ProjectSubcategorySerializer(app_project_models.ProjectSubcategory.objects.all(), many=True).data,
                'brand':BrandSerializer(app_project_models.Brand.objects.all(), many=True).data,
                'work_format':work_format,
                'agreement_format':agreement_format,
                'stage':stage,
                'team_role':team_role,
                'nbsource': NBSourceSerializer(app_project_models.NBSource.objects.all(), many=True).data,
                'nbformat': NBFormatSerializer(app_project_models.NBFormat.objects.all(), many=True).data,
                'products': ProjectProductSerializer(app_project_models.ProjectProduct.objects.all(), many=True).data,
                'users': UserSerializer(app_setup_models.DBUser.objects.all(), many=True).data,
                'units': UnitSerializer(app_transaction_models.Unit.objects.all(), many=True).data,
            },
        }

        return Response(data)




@api_view(['GET', ])
@login_required
@access_company_level
@access_app_projects_list
def project_statistics(request,get_access_all,pk):

    if request.method == 'GET':

        project = app_project_models.Project.objects.get(pk=pk)
        transactions = app_transaction_models.Transaction.objects.all().annotate(
                date=Case(
                    When(date_fact=None, then='date_doc'),
                    default='date_fact',
                    output_field=DateField(),
                )).order_by('date', 'pk').filter(project=project)
        all = app_transaction_models.Transaction.objects.all().annotate(
            date=Case(
                When(date_fact=None, then='date_doc'),
                default='date_fact',
                output_field=DateField(),
            )).order_by('date')
        main_currency = app_transaction_models.Currency.objects.get(is_default=True)

        report = statistics(transactions, main_currency, all, False)
        data = {
            'report':report
        }

        return Response(data)





@api_view(['GET', 'POST'])
@login_required
@access_company_level
@access_app_projects_list
def project_list(request, get_access_all):
    """
    List all items or create a new items.
    """
    if request.method == 'GET':
        offset =  int(request.GET.get('offset'))
        page =  int(request.GET.get('page'))
        filter_param =  request.GET.get('filter')

        qs = get_access_all()
        all, use_dict = filterMain(qs, filter_param)

        start = (page - 1) * offset
        end =  page * offset
        items = all[start:end]

        if use_dict:
            total_pages = math.ceil(len(all) / offset)
        else:
            total_pages = math.ceil(all.count() / offset)

        serializer = ProjectSerializer(items, many=True)

        data = {
            'total_pages': total_pages,
            'list': serializer.data,
        }

        return Response(data)

    elif request.method == 'POST':
        serializer = ProjectSerializer(data=request.data)
        if request.data['work_format'] is None or request.data['agreement_format'] is None:
            request.data['work_format'] = 0
            request.data['agreement_format'] = 0

        if serializer.is_valid():

            instance = serializer.save(
                name='EMPTY',
                created_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            numgen(instance)
            instance.save()

            app_project_models.TeamProject(
                project=instance,
                role=1,
                user=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            ).save()


            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




@api_view(['GET', 'PUT', 'DELETE'])
def project_detail(request, pk):
    """
    Retrieve, update or delete a items.
    """
    try:
        item = app_project_models.Project.objects.get(pk=pk)
    except app_project_models.Project.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ProjectSerializer(item)
        deletable, related_obj = can_delete_project(item)
        new_serializer = {'deletable': deletable}
        new_serializer.update(serializer.data)
        return Response(new_serializer)


    elif request.method == 'PUT':
        serializer = ProjectSerializer(item, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        teamproject_killer(item)
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)











@login_required
@access_company_level
@access_app_project_card
def project(request, pk):
    context={'pk':pk}
    return render(request, 'app_project/new/index_project.html', context)


@api_view(['GET', 'POST'])
@login_required
@access_company_level
@access_app_projects_list
def project_team_list(request, get_access_all, pk):
    """
    List all items or create a new items.
    """
    try:
        project = app_project_models.Project.objects.get(pk=pk)
    except app_project_models.Project.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        items = app_project_models.TeamProject.objects.filter(project=project)
        serializer = TeamProjectSerializer(items, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = TeamProjectSerializer(data=request.data)


        if serializer.is_valid():

            instance = serializer.save(
                project=project,
                created_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            instance.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




@api_view(['GET', 'PUT', 'DELETE'])
def project_team_detail(request, pk):
    """
    Retrieve, update or delete a items.
    """
    try:
        item = app_project_models.TeamProject.objects.get(pk=pk)
    except app_project_models.TeamProject.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = TeamProjectSerializer(item)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = TeamProjectSerializer(item, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)






@api_view(['GET', 'POST'])
@login_required
@access_company_level
@access_app_projects_list
def project_service_list(request, get_access_all, pk):
    """
    List all items or create a new items.
    """
    try:
        project = app_project_models.Project.objects.get(pk=pk)
    except app_project_models.Project.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        items = app_project_models.Service.objects.filter(project=project)
        serializer = ServiceSerializer(items, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ServiceSerializer(data=request.data)


        if serializer.is_valid():

            instance = serializer.save(
                project=project,
                created_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            instance.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




@api_view(['GET', 'PUT', 'DELETE'])
def project_service_detail(request, pk):
    """
    Retrieve, update or delete a items.
    """
    try:
        item = app_project_models.Service.objects.get(pk=pk)
    except app_project_models.Service.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ServiceSerializer(item)
        deletable, related_obj = can_delete_project(item)
        new_serializer = {'deletable': deletable}
        new_serializer.update(serializer.data)
        return Response(new_serializer)

    elif request.method == 'PUT':
        serializer = ServiceSerializer(item, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)





@api_view(['GET', 'POST'])
@login_required
@access_company_level
@access_app_projects_list
def project_period_list(request, get_access_all, pk):
    """
    List all items or create a new items.
    """
    try:
        project = app_project_models.Project.objects.get(pk=pk)
    except app_project_models.Project.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        items = app_project_models.ReportPeriod.objects.filter(project=project)
        serializer = PeriodSerializer(items, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = PeriodSerializer(data=request.data)


        if serializer.is_valid():

            instance = serializer.save(
                project=project,
                created_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            instance.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




@api_view(['GET', 'PUT', 'DELETE'])
def project_period_detail(request, pk):
    """
    Retrieve, update or delete a items.
    """
    try:
        item = app_project_models.ReportPeriod.objects.get(pk=pk)
    except app_project_models.ReportPeriod.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = PeriodSerializer(item)
        deletable, related_obj = item.can_delete()
        new_serializer = {'deletable': deletable}
        new_serializer.update(serializer.data)
        return Response(new_serializer)

    elif request.method == 'PUT':
        serializer = PeriodSerializer(item, data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)




@api_view(['GET', 'POST'])
@login_required
@access_company_level
@access_app_projects_list
def project_report_list(request, get_access_all, pk):
    """
    List all items or create a new items.
    """
    try:
        period = app_project_models.ReportPeriod.objects.get(pk=pk)
    except app_project_models.ReportPeriod.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        items = app_project_models.Report.objects.filter(period=period)
        serializer = ReportSerializer(items, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        report_serializer = ReportSerializer(data=request.data)
        if report_serializer.is_valid():
            instance_report = report_serializer.save(
                period=period,
                created_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )
            instance_report.save()

            for key,value in request.data['projectServices'].items():
                service_serializer = ServiceInReportSerializer(data=value)
                if service_serializer.is_valid():
                    instance_service_in_report = service_serializer.save(
                        report=instance_report,
                        service=app_project_models.Service.objects.get(pk=key),
                        created_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk),
                        modified_at=pytz.utc.localize(datetime.utcnow()),
                        modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
                    )
                    instance_service_in_report.save()
                else:
                    return Response(service_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            return Response(report_serializer.data, status=status.HTTP_201_CREATED)
        return Response(report_serializer.errors, status=status.HTTP_400_BAD_REQUEST)




@api_view(['GET', 'PUT', 'DELETE'])
def project_report_detail(request, pk):
    """
    Retrieve, update or delete a items.
    """
    try:
        item = app_project_models.Report.objects.get(pk=pk)
    except app_project_models.Report.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ReportWithServicesSerializer(item)
        deletable, related_obj = can_delete_report(item)
        new_serializer = {'deletable': deletable}
        new_serializer.update(serializer.data)
        return Response(new_serializer)

    elif request.method == 'PUT':
        report_serializer = ReportWithServicesSerializer(item, data=request.data)
        if report_serializer.is_valid(raise_exception=True):
            report_serializer.save(
                modified_at=pytz.utc.localize(datetime.utcnow()),
                modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
            )

            for service in request.data['services_in_report']:
                item = app_project_models.ServiceInReport.objects.get(pk=service['id'])
                service_serializer = ServiceInReportSerializer(item, data=service)
                if service_serializer.is_valid():
                    instance_service_in_report = service_serializer.save(
                        modified_at=pytz.utc.localize(datetime.utcnow()),
                        modified_by=app_setup_models.DBUser.objects.get(user_id=request.user.pk)
                    )
                    instance_service_in_report.save()
                else:
                    return Response(service_serializer.errors, status=status.HTTP_400_BAD_REQUEST)



            return Response(report_serializer.data)
        return Response(report_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':

        services = app_project_models.ServiceInReport.objects.filter(report=item)
        for service in services:
            service.delete()

        item.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)








@api_view(['GET', ])
@login_required
@access_company_level
@access_app_project_section
def project_history(request):
    """
    Retrieve,
    """
    if request.method == 'GET':
        history_project = app_project_models.Project.history.all()[:3]
        history_period = app_project_models.ReportPeriod.history.all()[:3]
        history_report = app_project_models.Report.history.all()[:3]
        history_service = app_project_models.Service.history.all()[:3]
        history_service_in_report = app_project_models.ServiceInReport.history.all()[:3]
        history_team = app_project_models.TeamProject.history.all()[:3]

        data = {
            'projects': history_record(history_project, 'Проект', True),
            'periods': history_record(history_period, 'Период', True),
            'reports': history_record(history_report, 'Отчет', True),
            'services': history_record(history_service, 'Услуга', True),
            'services_in_report': history_record(history_service_in_report, 'Услуга в отчете', True),
            'teams': history_record(history_team, 'Участник команды', True),
        }

        return Response(data)