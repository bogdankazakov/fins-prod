from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from app_auth.decorators import access_company_level
import logging
from app_project.decorators import *
from app_signup import models as app_signup_models
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from app_setup import models as app_setup_models
from app_project.forms import *
from django.template.loader import render_to_string
from django.http import JsonResponse
from app_setup.tools import log_data
from app_project.tools import *
from django.contrib import messages
from app_project.filters import ProjectsFilter
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, HttpResponseRedirect
import xlwt
import datetime
from django.contrib.sites.models import Site

logger = logging.getLogger(__name__)

wording = {
    'btn_create': 'Создать',
    'btn_add': 'Добавить транзакцию',
    'btn_add2': 'Добавить отчетный период',
    'btn_add3': 'Добавить услугу',
    'btn_add4': 'Добавить отчет',
    'btn_add5': 'Добавить члена команды',
    'btn_add_p': 'Добавить проект',
    'btn_delete': 'Удалить транзакцию',
    'btn_delete2': 'Удалить период',
    'btn_delete3': 'Удалить услугу',
    'btn_delete4': 'Удалить отчет',
    'btn_delete5': 'Удалить из команды',
    'btn_delete_p': 'Удалить проект',
    'btn_cancel': 'Отмена',
    'btn_update': 'Редактировать транзакцию',
    'btn_update2': 'Редактировать период',
    'btn_update3': 'Редактировать услугу',
    'btn_update4': 'Редактировать отчет',
    'btn_update5': 'Редактировать статус',
    'btn_update_p': 'Редактировать проект',
    'btn_est': 'Редактировать оценку проекта',
    'btn_save': 'Сохранить',
    'btn_history': 'История',
    'h1_list': 'Список транзакций',
    'h1_service': 'Список Услуг',
    'h1_list_p': 'Список проектов',
    'h1_period': 'Отчетные периоды',
    'h1_create': 'Создать транзакцию',
    'h1_create_p': 'Создать проект',
    'h1_create_service': 'Создать сервис',
    'h1_create_team': 'Добавить в команду',
    'h1_create_period': 'Создать отчетный период',
    'h1_create_report': 'Создать отчет',
    'h1_update': 'Редактировать',
    'h1_delete': 'Удалить транзакцию',
    'h1_delete2': 'Удалить период',
    'h1_delete3': 'Удалить услугу',
    'h1_delete4': 'Удалить отчет',
    'h1_delete5': 'Удалить из команды',
    'h1_delete_p': 'Удалить проект',
    'h1_history': 'История изменений проектов',
    'validation_error': 'Ошибка валидации',
    'not_deletable_system': 'Мы не можем удалить проект, т.к. он является системным',
    'not_deletable': 'Мы не можем удалить проект. Сначала удалите все транзакции.',
    'not_deletable_team': 'Мы не можем удалить пользователя. Он остался один в проекте.',
    'approve_delete': ' Вы уверены, что хотите удалить транзакции ',
    'approve_delete2': ' Вы уверены, что хотите удалить период ',
    'approve_delete3': ' Вы уверены, что хотите удалить услугу ',
    'approve_delete4': ' Вы уверены, что хотите удалить отчет ',
    'approve_delete5': ' Вы уверены, что хотите удалить из команды пользователя ',
    'empty_str': '———',
    'none': '———',
}
def get_all():
    all = app_project_models.Project.objects.all().order_by('-pk')
    return all


def filter_data(request, page):
    filter = ProjectsFilter(request.GET, queryset=app_project_models.Project.objects.all().order_by('-pk'))
    filter = filter_extra_data(filter)
    obj = app_project_models.Project
    obj2 = app_project_models.TeamProject

    if page is not None:
        request.GET = request.GET.copy()
        request.GET['page'] = page

    page = request.GET.get('page', 1)
    paginator = Paginator(filter.qs, 10)
    try:
        results = paginator.page(page)
    except PageNotAnInteger:
        results = paginator.page(1)
    except EmptyPage:
        results = paginator.page(paginator.num_pages)

    return filter, obj, results, obj2

@login_required
@access_company_level
@access_app_project
def index(request):

    filter, obj, results, obj2 = filter_data(request, None)




    context = {
        'list': results,
        # 'sum': summ(filter.qs),
        'wording': wording,
        'filter': filter,
        'obj': obj,
        'obj2': obj2,
    }

    # saving request for excel export
    l = []
    for q in filter.qs:
        l.append(q.pk)

    request.session['project_filter_qs'] = l
    return render(request, 'app_project/list/projects.html', context)


@login_required
@access_company_level
@access_app_project
def project(request, pk):
    project = app_project_models.Project.objects.get(pk=pk)
    transactions = app_transaction_models.Transaction.objects.filter(project=project)
    messages = agency_checker(transactions, project)
    report = statistics(transactions)
    obj = app_transaction_models.Transaction
    context = {
        'project': project,
        'messages': messages,
        'list': transactions,
        'obj': obj,
        'wording': wording,
        'pk':pk,
        'report':report,
        'services': app_project_models.Service.objects.filter(project=project),
        'team': app_project_models.TeamProject.objects.filter(project=project),

    }

    if project.work_format == 1:
        context.update({
            'periods': period_tool(project),
        })
    return render(request, 'app_project/project_card/project.html', context)





@login_required
@access_company_level
@access_app_project
def project_create(request):
    if request.method == 'POST':
        form = CreateProjectForm(request.POST)
    else:
        form = CreateProjectForm()
    return project_save(request, form, 'app_project/project_CRUD/partial_create.html', True)

@login_required
@access_company_level
@access_app_project
def project_update(request, pk):
    item = app_project_models.Project.objects.get(pk=pk)
    if item.agreement_format == 1:
        if request.method == 'POST':
            form = ProjectAgencyForm(request.POST, instance=item)
        else:
            form = ProjectAgencyForm(instance=item)
    else:
        if request.method == 'POST':
            form = ProjectBaseForm(request.POST, instance=item)
        else:
            form = ProjectBaseForm(instance=item)

    return project_save(request, form, 'app_project/project_CRUD/partial_update.html', False)

@login_required
@access_company_level
@access_app_project
def project_update_estimation(request, pk):
    item = app_project_models.Project.objects.get(pk=pk)
    if request.method == 'POST':
        form = ProjectEstimationForm(request.POST, instance=item)
    else:
        form = ProjectEstimationForm(instance=item)

    return project_save(request, form, 'app_project/project_CRUD/partial_update_estimation.html', False)

@login_required
@access_company_level
@access_app_project
def project_save(request, form, template_name, is_creation):
    data = dict()
    if request.method == 'POST':
        page = request.POST.get('page')

        form_converter(form)
        if form.is_valid():
            instance = form.save(commit=False)
            log_data(instance, request)
            instance.save()
            extra_data_post(instance)
            instance.save()

            data['form_is_valid'] = True

            if is_creation == True:
                add_user_to_team(instance, request)
                filter, obj, results = filter_data(request, page)
                context = {
                    'list': results,
                    'wording': wording,
                    'messages': ['Проект {0} создан'.format(form.instance.name)]
                }
                data['html_list'] = render_to_string('app_project/list/partial_list.html', context)
                data['html_pagination'] = render_to_string('app_project/list/pagination.html', context)
            else:
                context = {
                    'project': app_project_models.Project.objects.get(pk=form.instance.pk),
                    'wording': wording,
                    'messages': ['Проект {0} отредактирован'.format(form.instance.name)]
                }
                data['html_partial_project'] = render_to_string('app_project/project_card/partial_project.html', context)
                data['html_partial_project_name'] = render_to_string('app_project/project_card/partial_project_name.html', context)
                data['html_partial_project_estimation'] = render_to_string('app_project/project_card/partial_project_estimation.html', context)

        else:
            messages.error(request, wording['validation_error'])

    if request.method == 'GET':
        data['form_is_valid'] = False
    context = {
            'form': form,
            'wording': wording,
           }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
@access_company_level
@access_app_project
def project_delete(request, pk):
    data = dict()
    item = app_project_models.Project.objects.get(pk=pk)
    if request.method == 'POST':
        item.delete()
        data['form_is_valid'] = True
        messages.success(request, 'Проект {0} удален'.format(item.name))
    if request.method == 'GET':
        deletable, related_obj = item.can_delete()


        context = {
            'item': item,
            'deletable':deletable,
            'wording': wording,
        }
        data['html_form'] = render_to_string('app_project/project_CRUD/partial_delete.html',context,request=request,)
    return JsonResponse(data)

@login_required
@access_company_level
@access_app_project
def load_subcat(request):
    category_id = request.GET.get('category')
    subcategory = app_project_models.ProjectSubcategory.objects.filter(category_id=category_id).order_by('name')
    return render(request, 'app_project/project_CRUD/subcategory_dropdown_list_options.html', {'subcategory': subcategory})







@login_required
@access_company_level
@access_app_project
def create_period(request, projectpk):
    if request.method == 'POST':
        form = CreatePeriodForm(request.POST)
    else:
        form = CreatePeriodForm()
    return period_save(request, form, 'app_project/period_CRUD/partial_create_period.html', projectpk)

@login_required
@access_company_level
@access_app_project
def update_period(request, pk):
    item = app_project_models.ReportPeriod.objects.get(pk=pk)
    projectpk = item.project.pk

    if request.method == 'POST':
        form = CreatePeriodForm(request.POST, instance=item)
    else:
        form = CreatePeriodForm(instance=item)

    return period_save(request, form, 'app_project/period_CRUD/partial_update_period.html', projectpk)

@login_required
@access_company_level
@access_app_project
def period_save(request, form, template_name, projectpk):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            project = app_project_models.Project.objects.get(pk=projectpk)
            instance = form.save(commit=False)
            log_data(instance, request)
            instance.save()
            instance.project = project
            instance.save()

            data['form_is_valid'] = True

            context = {
                'periods': period_tool(project),
                'wording': wording,
                'project': project
            }
            data['html_list'] = render_to_string('app_project/project_card/partial_list_periods.html', context)
        else:
            messages.error(request, wording['validation_error'])

    if request.method == 'GET':
        data['form_is_valid'] = False
    context = {
            'form': form,
            'wording': wording,
            'projectpk': projectpk,
           }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
@access_company_level
@access_app_project
def delete_period(request, pk):
    data = dict()
    item = app_project_models.ReportPeriod.objects.get(pk=pk)
    project = item.project
    if request.method == 'POST':
        item.delete()
        data['form_is_valid'] = True
        messages.success(request, 'Период {0} удален'.format(item))

        context = {
            'periods': period_tool(project),
            'wording': wording,
            'project': project
        }
        data['html_list'] = render_to_string('app_project/project_card/partial_list_periods.html', context)


    if request.method == 'GET':
        deletable, related_obj = item.can_delete()


        context = {
            'item': item,
            'deletable':deletable,
            'wording': wording,
        }
        data['html_form'] = render_to_string('app_project/period_CRUD/partial_delete_period.html',context,request=request,)
    return JsonResponse(data)








@login_required
@access_company_level
@access_app_project
def service_create(request, projectpk):
    if request.method == 'POST':
        form = ServiceForm(request.POST)
    else:
        form = ServiceForm()
    return service_save(request, form, 'app_project/service_CRUD/partial_create_service.html', projectpk)

@login_required
@access_company_level
@access_app_project
def service_update(request, pk):
    item = app_project_models.Service.objects.get(pk=pk)
    projectpk = item.project.pk

    if request.method == 'POST':
        form = ServiceForm(request.POST, instance=item)
    else:
        form = ServiceForm(instance=item)

    return service_save(request, form, 'app_project/service_CRUD/partial_update_service.html', projectpk)

@login_required
@access_company_level
@access_app_project
def service_save(request, form, template_name, projectpk):
    data = dict()
    if request.method == 'POST':
        if form.is_valid():
            project = app_project_models.Project.objects.get(pk=projectpk)
            instance = form.save(commit=False)
            log_data(instance, request)
            instance.save()
            instance.project = project
            instance.save()

            data['form_is_valid'] = True

            context = {
                'services': app_project_models.Service.objects.filter(project=project),
                'wording': wording,
                'project': project
            }
            data['html_list'] = render_to_string('app_project/project_card/partial_list_services.html', context)
        else:
            messages.error(request, wording['validation_error'])

    if request.method == 'GET':
        data['form_is_valid'] = False
    context = {
            'form': form,
            'wording': wording,
            'projectpk': projectpk,
           }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
@access_company_level
@access_app_project
def service_delete(request, pk):
    data = dict()
    item = app_project_models.Service.objects.get(pk=pk)
    project = item.project
    if request.method == 'POST':
        item.delete()
        data['form_is_valid'] = True
        messages.success(request, 'Период {0} удален'.format(item))

        context = {
            'services': app_project_models.Service.objects.filter(project=project),
            'wording': wording,
            'project': project
        }
        data['html_list'] = render_to_string('app_project/project_card/partial_list_services.html', context)


    if request.method == 'GET':
        deletable, related_obj = item.can_delete()


        context = {
            'item': item,
            'deletable':deletable,
            'wording': wording,
        }
        data['html_form'] = render_to_string('app_project/service_CRUD/partial_delete_service.html',context,request=request,)
    return JsonResponse(data)







@login_required
@access_company_level
@access_app_project
def report_create(request, projectpk, periodpk):
    project = app_project_models.Project.objects.get(pk=projectpk)
    services = app_project_models.Service.objects.filter(project=project).order_by('name')
    initial = []
    for service in services:
        initial.append({
            'service': service.pk,
        })

    if request.method == 'POST':
        form = ReportForm(request.POST)
        formset = ServiceInReportFormSet(request.POST)

    else:
        form = ReportForm()
        formset = ServiceInReportFormSet(initial=initial)
        formset = report_add_data(formset, services)
    return report_save(request, form, formset, 'app_project/report_CRUD/partial_create_report.html', projectpk, periodpk)

@login_required
@access_company_level
@access_app_project
def report_update(request, pk, projectpk):
    item = app_project_models.Report.objects.get(pk=pk)
    periodpk = item.period.pk

    servicesIR = app_project_models.ServiceInReport.objects.filter(report=item)
    initial = []
    for service in servicesIR:
        initial.append({
            'service': service.service,
            'hours': service.hours,
        })
    services = app_project_models.Service.objects.filter(project_id=projectpk).order_by('name')



    if request.method == 'POST':
        form = ReportForm(request.POST, instance=item)
        formset = ServiceInReportUpdateFormSet(request.POST, queryset=servicesIR)
    else:
        form = ReportForm(instance=item)
        formset = ServiceInReportUpdateFormSet(queryset=servicesIR)
        formset = report_add_data(formset, services)


    return report_save(request, form, formset, 'app_project/report_CRUD/partial_update_report.html', projectpk, periodpk)

@login_required
@access_company_level
@access_app_project
def report_save(request, form, formset, template_name, projectpk, periodpk):
    project = app_project_models.Project.objects.get(pk=projectpk)
    period = app_project_models.ReportPeriod.objects.get(pk=periodpk)
    data = dict()
    if request.method == 'POST':
        if form.is_valid():

            report = form.save(commit=False)
            log_data(report, request)
            report.save()
            report.period = period
            report.save()

            data['form_is_valid'] = True

            context = {
                'periods': period_tool(project),
                'wording': wording,
                'project': project
            }
            data['html_list'] = render_to_string('app_project/project_card/partial_list_periods.html', context)
        else:
            messages.error(request, wording['validation_error'])
        for form in formset:
            if form.is_valid():
                if form.cleaned_data['hours'] != 0:
                    instance = form.save(commit=False)
                    instance.report = report
                    instance.save()

    if request.method == 'GET':
        data['form_is_valid'] = False
    context = {
            'form': form,
            'formset':formset,
            'wording': wording,
            'projectpk': projectpk,
            'periodpk': periodpk,
           }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
@access_company_level
@access_app_project
def report_delete(request, pk):
    data = dict()
    item = app_project_models.Report.objects.get(pk=pk)
    project = item.period.project
    if request.method == 'POST':
        serviceinreport_killer(item)
        item.delete()
        data['form_is_valid'] = True
        messages.success(request, 'Период {0} удален'.format(item))

        context = {
            'periods': period_tool(project),
            'wording': wording,
            'project': project
        }
        data['html_list'] = render_to_string('app_project/project_card/partial_list_periods.html', context)

    if request.method == 'GET':


        deletable, related_obj = can_delete_report(item)


        context = {
            'item': item,
            'deletable':deletable,
            'wording': wording,
        }
        data['html_form'] = render_to_string('app_project/report_CRUD/partial_delete_report.html',context,request=request,)
    return JsonResponse(data)









@login_required
@access_company_level
@access_app_project
def team_create(request, projectpk):

    if request.method == 'POST':
        form = TeamForm(request.POST)
    else:
        form = TeamForm()
    return team_save(request, form, 'app_project/team_CRUD/partial_create_team.html', projectpk)

@login_required
@access_company_level
@access_app_project
def team_update(request, pk):
    item = app_project_models.TeamProject.objects.get(pk=pk)
    projectpk = item.project.pk

    if request.method == 'POST':
        form = TeamUpdateForm(request.POST, instance=item)
    else:
        form = TeamUpdateForm(instance=item)

    return team_save(request, form, 'app_project/team_CRUD/partial_update_team.html', projectpk)

@login_required
@access_company_level
@access_app_project
def team_save(request, form, template_name, projectpk):
    data = dict()

    if request.method == 'POST':
        if form.is_valid():
            project = app_project_models.Project.objects.get(pk=projectpk)


            try:
                user = form.cleaned_data['user']
                raise_error = False
                if app_project_models.TeamProject.objects.filter(project=project, user=user).exists():
                    messages.error(request, wording['validation_error'])
                    form._errors["user"] = ["Пользователь уже добавлен в этот проект"]
                    raise_error = True
            except Exception: # means we create team member
                raise_error = False
                if form.cleaned_data['role'] == 1 and app_project_models.TeamProject.objects.filter(project=project, role=1).exists():
                    messages.error(request, wording['validation_error'])
                    form._errors["role"] = ["Лидер на проекте может быть только один"]
                    raise_error = True

            if raise_error:
                pass
            else:
                instance = form.save(commit=False)
                log_data(instance, request)
                instance.save()
                instance.project = project
                instance.save()

                data['form_is_valid'] = True

                context = {
                    'services': app_project_models.Service.objects.filter(project=project),
                    'wording': wording,
                    'project': project,
                    'team': app_project_models.TeamProject.objects.filter(project=project),

                }
                data['html_list'] = render_to_string('app_project/project_card/partial_team.html', context)
        else:
            messages.error(request, wording['validation_error'])

    if request.method == 'GET':
        data['form_is_valid'] = False
    context = {
            'form': form,
            'wording': wording,
            'projectpk': projectpk,
           }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
@access_company_level
@access_app_project
def team_delete(request, pk):
    data = dict()
    item = app_project_models.TeamProject.objects.get(pk=pk)
    project = item.project
    if request.method == 'POST':
        item.delete()
        data['form_is_valid'] = True
        messages.success(request, 'Пользователь {0} удален из команды проекта'.format(item.user))

        context = {
            'team': app_project_models.TeamProject.objects.filter(project=project),
            'wording': wording,
            'project': project
        }
        data['html_list'] = render_to_string('app_project/project_card/partial_team.html', context)


    if request.method == 'GET':


        deletable, related_obj = delete_checker_team(item, project)


        context = {
            'item': item,
            'deletable':deletable,
            'wording': wording,
        }
        data['html_form'] = render_to_string('app_project/team_CRUD/partial_delete_team.html',context,request=request,)
    return JsonResponse(data)

@login_required
@access_company_level
@access_app_project
def excel_export(request):
    project_filter_qs = request.session['project_filter_qs']
    queryset = app_project_models.Project.objects.filter(pk__in=project_filter_qs).order_by('-name')

    filter = ProjectsFilter(request.GET, queryset=queryset)
    filter = filter_extra_data(filter)

    response = HttpResponse(content_type='application/ms-excel')
    filename = str(Site.objects.all()[0]) + '_projects_' + str(datetime.datetime.now())
    response['Content-Disposition'] = 'attachment; filename=' + filename + '.xls'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet("Проекты")

    #table header
    row_num = 0
    model = app_project_models.Project
    columns = [
        (str(model._meta.get_field('name').verbose_name), 4000),
        (str(model._meta.get_field('name_short').verbose_name), 4000),
        (str(model._meta.get_field('brand').verbose_name), 4000),
        (str(model._meta.get_field('category').verbose_name), 4000),
        (str(model._meta.get_field('subcategory').verbose_name), 4000),
        (str(model._meta.get_field('work_format').verbose_name), 4000),
        (str(model._meta.get_field('agreement_format').verbose_name), 4000),
        (str(model._meta.get_field('commission').verbose_name), 4000),
        (str(model._meta.get_field('stage').verbose_name), 4000),
        (str(model._meta.get_field('project_start').verbose_name), 4000),
        (str(model._meta.get_field('project_end').verbose_name), 4000),
        ("Лидер проекта", 4000),
        ("Команда проекта", 4000),
        ("Юниты", 4000),
        ("Выручка проекта (в основной валюте)", 4000),
        ("Внешние расходы проекта (в основной валюте)", 4000),
        ("Валовая прибыль проекта (в основной валюте)", 4000),
        ("РВП проекта (в основной валюте)", 4000),

        (str(model._meta.get_field('nb_source').verbose_name), 4000),
        (str(model._meta.get_field('nb_format').verbose_name), 4000),
        (str(model._meta.get_field('estimate_probability').verbose_name), 4000),
        (str(model._meta.get_field('estimate_presale_start').verbose_name), 4000),
        (str(model._meta.get_field('estimate_presale_end').verbose_name), 4000),
        (str(model._meta.get_field('estimate_presale_outcome_ext').verbose_name), 4000),
        (str(model._meta.get_field('estimate_presale_outcome_int').verbose_name), 4000),
        (str(model._meta.get_field('estimate_inprogress_start').verbose_name), 4000),
        (str(model._meta.get_field('estimate_inprogress_end').verbose_name), 4000),
        (str(model._meta.get_field('estimate_inprogress_income_before_taxes').verbose_name), 4000),
        (str(model._meta.get_field('estimate_inprogress_gross_profitability_before_taxes').verbose_name), 4000),
        (str(model._meta.get_field('estimate_inprogress_gross_profit_before_taxes').verbose_name), 4000),
        (str(model._meta.get_field('estimate_inprogress_project_profitability_before_taxes').verbose_name), 4000),
        (str(model._meta.get_field('estimate_inprogress_project_profit_before_taxes').verbose_name), 4000),



        (str(model._meta.get_field('created_at').verbose_name), 4000),
        (str(model._meta.get_field('created_by').verbose_name), 4000),
        (str(model._meta.get_field('modified_at').verbose_name), 4000),
        (str(model._meta.get_field('modified_by').verbose_name), 4000),



    ]
    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    #rows
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num][0], font_style)
        # set column width
        ws.col(col_num).width = columns[col_num][1]

    font_style = xlwt.XFStyle()
    font_style.alignment.wrap = 1



    for obj in filter.qs:


        if obj.category == None:
            category = ''
        else:
            category = obj.category.name

        if obj.subcategory == None:
            subcategory = ''
        else:
            subcategory = obj.subcategory.name


        if obj.work_format == None:
            work_format = ''
        else:
            work_format = obj.get_work_format_display()

        if obj.agreement_format == None:
            agreement_format = ''
        else:
            agreement_format = obj.get_agreement_format_display()

        if obj.stage == None:
            stage = ''
        else:
            stage = obj.get_stage_display()

        if obj.brand == None:
            brand = ''
        else:
            brand = obj.brand.name

        if obj.project_start == None:
            project_start = ''
        else:
            project_start = obj.project_start.strftime("%d/%m/%Y")

        if obj.project_end == None:
            project_end = ''
        else:
            project_end = obj.project_end.strftime("%d/%m/%Y")


        if obj.leader == None:
            leader = ''
        else:
            leader = str(obj.leader)

        if obj.team == None:
            team = ''
        else:
            team = str(obj.team)

        if obj.units == None:
            units = ''
        else:
            units = str(obj.units)



        if obj.nb_source == None:
            nb_source = ''
        else:
            nb_source = obj.nb_source.name

        if obj.nb_format == None:
            nb_format = ''
        else:
            nb_format = obj.nb_format.name

        if obj.estimate_probability == None:
            estimate_probability = ''
        else:
            estimate_probability = obj.estimate_probability

        if obj.estimate_presale_start == None:
            estimate_presale_start = ''
        else:
            estimate_presale_start = obj.estimate_presale_start.strftime("%d/%m/%Y")

        if obj.estimate_presale_end == None:
            estimate_presale_end = ''
        else:
            estimate_presale_end = obj.estimate_presale_end.strftime("%d/%m/%Y")

        if obj.estimate_presale_outcome_ext == None:
            estimate_presale_outcome_ext = ''
        else:
            estimate_presale_outcome_ext = obj.estimate_presale_outcome_ext

        if obj.estimate_presale_outcome_int == None:
            estimate_presale_outcome_int = ''
        else:
            estimate_presale_outcome_int = obj.estimate_presale_outcome_int

        if obj.estimate_inprogress_start == None:
            estimate_inprogress_start = ''
        else:
            estimate_inprogress_start = obj.estimate_inprogress_start.strftime("%d/%m/%Y")

        if obj.estimate_inprogress_end == None:
            estimate_inprogress_end = ''
        else:
            estimate_inprogress_end = obj.estimate_inprogress_end.strftime("%d/%m/%Y")

        if obj.estimate_inprogress_income_before_taxes == None:
            estimate_inprogress_income_before_taxes = ''
        else:
            estimate_inprogress_income_before_taxes = obj.estimate_inprogress_income_before_taxes

        if obj.estimate_inprogress_gross_profitability_before_taxes == None:
            estimate_inprogress_gross_profitability_before_taxes = ''
        else:
            estimate_inprogress_gross_profitability_before_taxes = obj.estimate_inprogress_gross_profitability_before_taxes

        if obj.estimate_inprogress_gross_profit_before_taxes == None:
            estimate_inprogress_gross_profit_before_taxes = ''
        else:
            estimate_inprogress_gross_profit_before_taxes = obj.estimate_inprogress_gross_profit_before_taxes

        if obj.estimate_inprogress_project_profitability_before_taxes == None:
            estimate_inprogress_project_profitability_before_taxes = ''
        else:
            estimate_inprogress_project_profitability_before_taxes = obj.estimate_inprogress_project_profitability_before_taxes

        if obj.estimate_inprogress_project_profit_before_taxes == None:
            estimate_inprogress_project_profit_before_taxes = ''
        else:
            estimate_inprogress_project_profit_before_taxes = obj.estimate_inprogress_project_profit_before_taxes


        if obj.created_at == None:
            created_at = ''
        else:
            created_at = obj.created_at.replace(tzinfo=None).strftime("%d/%m/%Y")

        if obj.created_by == None:
            created_by = ''
        else:
            created_by = str(obj.created_by)

        if obj.modified_at == None:
            modified_at = ''
        else:
            modified_at = obj.modified_at.replace(tzinfo=None).strftime("%d/%m/%Y")

        if obj.modified_by == None:
            modified_by = ''
        else:
            modified_by = str(obj.modified_by)









        row_num += 1
        row = [
            obj.name,
            obj.name_short,
            brand,
            category,
            subcategory,
            work_format,
            agreement_format,
            obj.commission,
            stage,
            project_start,
            project_end,
            leader,
            team,
            units,
            obj.income,
            obj.outcome_ext,
            obj.gross_profit,
            obj.gross_profitability,




            nb_source,
            nb_format,
            estimate_probability,
            estimate_presale_start,
            estimate_presale_end,
            estimate_presale_outcome_ext,
            estimate_presale_outcome_int,
            estimate_inprogress_start,
            estimate_inprogress_end,
            estimate_inprogress_income_before_taxes,
            estimate_inprogress_gross_profitability_before_taxes,
            estimate_inprogress_gross_profit_before_taxes,
            estimate_inprogress_project_profitability_before_taxes,
            estimate_inprogress_project_profit_before_taxes,


            created_at,
            created_by,
            modified_at,
            modified_by,
        ]


        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response