from rest_framework import serializers
from app_transaction import models as app_transaction_models
from app_docs import models as app_docs_models
from app_project import models as app_project_models
from app_setup import models as app_setup_models



class ProjectCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = app_project_models.ProjectCategory
        fields = ('id', 'name')



class ProjectSubcategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = app_project_models.ProjectSubcategory
        fields = ('id', 'name', 'category')


class BrandSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_project_models.Brand
        fields = ('id', 'name')

class NBFormatSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_project_models.NBFormat
        fields = ('id', 'name')

class NBSourceSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_project_models.NBSource
        fields = ('id', 'name')

class ProjectProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_project_models.ProjectProduct
        fields = ('id', 'name')

class UnitSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_transaction_models.Unit
        fields = ('id', 'name')

class UserSerializer(serializers.ModelSerializer):
    user_display = serializers.ReadOnlyField(source='get_name', read_only=True)

    class Meta:
        model = app_setup_models.DBUser
        fields = (
            'id',
            'user_id',
            'user_display',
        )


def field_creation(model, exclude_fields, extra_fields):
    all_fields = [f.name for f in model._meta.get_fields()]
    result = [item for item in all_fields if item not in exclude_fields] + extra_fields
    return result


class ProjectSerializer(serializers.ModelSerializer):
    category_display = serializers.ReadOnlyField(source='category.name', read_only=True)
    subcategory_display = serializers.ReadOnlyField(source='subcategory.name', read_only=True)
    brand_display = serializers.ReadOnlyField(source='brand.name', read_only=True)
    work_format_display = serializers.ReadOnlyField(source='get_work_format_display', read_only=True)
    agreement_format_display = serializers.ReadOnlyField(source='get_agreement_format_display', read_only=True)
    stage_display = serializers.ReadOnlyField(source='get_stage_display', read_only=True)
    created_by_display = serializers.ReadOnlyField(source='created_by.get_name', read_only=True)
    modified_by_display = serializers.ReadOnlyField(source='created_by.get_name', read_only=True)
    is_editable_display = serializers.ReadOnlyField(source='get_is_editable_display', read_only=True)
    name = serializers.ReadOnlyField(read_only=True)

    class Meta:
        model = app_project_models.Project
        fields = field_creation(
            app_project_models.Project,
            [
                'transaction',
                'reportperiod',
                'project_agreements',
                'project_accounting_docs',
                'project_services',
                'project_transaction',
                'teamprojects_in_project',

            ],
            [
                'category_display',
                'subcategory_display',
                'brand_display',
                'work_format_display',
                'agreement_format_display',
                'stage_display',
                'created_by_display',
                'modified_by_display',
                'is_editable_display',
                'income',
                'outcome_ext',
                'gross_profit',
                'gross_profitability',
                'units',

            ])

class TeamProjectSerializer(serializers.ModelSerializer):
    role_display = serializers.ReadOnlyField(source='get_role_display', read_only=True)
    user_display = serializers.ReadOnlyField(source='user.get_name', read_only=True)

    class Meta:
        model = app_project_models.TeamProject
        fields = field_creation(
            app_project_models.TeamProject,
            [


            ],
            [
                'user_display',
                'role_display',


            ])

class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = app_project_models.Service
        fields = '__all__'


class PeriodSerializer(serializers.ModelSerializer):

    # class Meta:
    #     model = app_project_models.ReportPeriod
    #     fields = '__all__'

    class Meta:
        model = app_project_models.ReportPeriod
        fields = field_creation(
            app_project_models.ReportPeriod,
            [
                'transaction',
                'report',
            ],
            [
                'name'
            ])


class ReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_project_models.Report
        fields = '__all__'

class ReportWithServicesSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_project_models.Report
        fields = [
            'id',
            'period',
            'name',
            'is_deletable',
            'services_in_report',
        ]
        depth=1

class ServiceInReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_project_models.ServiceInReport
        fields = '__all__'