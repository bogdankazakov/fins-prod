from app_transaction.decorators import *
from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_project import models as app_project_models
from app_transaction.tools import filter_extra_data
from app_transaction.filters import TransactionsFilter
from django.db.models import Case, When, DateField
from app_transaction import models as app_transaction_models
from django.http import HttpResponse
import xlwt
import datetime
from django.contrib.sites.models import Site
from app_transaction.views.main import filter_data

@login_required
@access_company_level
@access_app_transactions_list
def excel_export(request, get_access_all):
    transaction_filter_qs = request.session['transaction_filter_qs']
    queryset = get_access_all().filter(pk__in=transaction_filter_qs)
    filter, obj, results, formset = filter_data(request, None, queryset)
    filter = filter_extra_data(filter,formset)

    response = HttpResponse(content_type='application/ms-excel')
    filename = str(Site.objects.all()[0]) + '_transactions_' + str(datetime.datetime.now())
    response['Content-Disposition'] = 'attachment; filename=' + filename + '.xls'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet("Транзакции")

    #table header
    row_num = 0
    model = app_transaction_models.Transaction
    columns = [
        (str(model._meta.get_field('name').verbose_name), 4000),
        (str(model._meta.get_field('type').verbose_name), 4000),
        (str(model._meta.get_field('status').verbose_name), 4000),
        (str(model._meta.get_field('doc_request').verbose_name), 4000),
        (str(model._meta.get_field('subcontractor').verbose_name), 4000),
        ('Тип контрагента', 4000),
        (str(model._meta.get_field('purpose').verbose_name), 4000),
        ('Системная дата', 4000),
        (str(model._meta.get_field('date_fact').verbose_name), 4000),
        (str(model._meta.get_field('date_doc').verbose_name), 4000),
        (str(model._meta.get_field('sum_before_taxes').verbose_name), 4000),
        ("Сумма после налогов", 4000),
        ("Размер налога", 4000),
        ("Тип налога", 4000),
        (str(model._meta.get_field('agreement_start_date').verbose_name), 4000),
        (str(model._meta.get_field('agreement_end_date').verbose_name), 4000),
        (str(model._meta.get_field('pay_delay').verbose_name), 4000),
        (str(model._meta.get_field('delayed').verbose_name), 4000),
        (str(model._meta.get_field('pay_model').verbose_name), 4000),
        (str(model._meta.get_field('act_date').verbose_name), 4000),
        (str(model._meta.get_field('comment').verbose_name), 4000),
        (str(model._meta.get_field('estimation_link').verbose_name), 4000),
        (str(model._meta.get_field('project').verbose_name), 4000),
        ('Категория проекта', 4000),
        ('Субкатегория проекта', 4000),
        ('Бренд', 4000),
        ('Короткое название', 4000),
        ('Этап', 4000),
        (str(model._meta.get_field('unit').verbose_name), 4000),
        (str(model._meta.get_field('account').verbose_name), 4000),
        (str(model._meta.get_field('created_at').verbose_name), 4000),
        (str(model._meta.get_field('created_by').verbose_name), 4000),
        (str(model._meta.get_field('modified_at').verbose_name), 4000),
        (str(model._meta.get_field('modified_by').verbose_name), 4000),
        (str(model._meta.get_field('is_transfer').verbose_name), 4000),
        (str(model._meta.get_field('hidden').verbose_name), 4000),



    ]
    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    #rows
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num][0], font_style)
        # set column width
        ws.col(col_num).width = columns[col_num][1]

    font_style = xlwt.XFStyle()
    font_style.alignment.wrap = 1



    for obj in filter.qs:


        if obj.status == None:
            status = ''
        else:
            status = obj.get_status_display()


        if obj.type == None:
            type = ''
        else:
            type = obj.get_type_display()



        if obj.doc_request == None:
            doc_request = ''
        else:
            doc_request = obj.get_doc_request_display()


        if obj.subcontractor == None:
            subcontractor = ''
        else:
            subcontractor = str(obj.subcontractor)

        if obj.subcontractor.type == None:
            subcontractor_type = ''
        else:
            subcontractor_type = str(obj.subcontractor.type)


        if obj.purpose == None:
            purpose = ''
        else:
            purpose = str(obj.purpose)



        if obj.date == None:
            date = ''
        else:
            date = obj.date.strftime("%d/%m/%Y")

        if obj.date_fact == None:
            date_fact = ''
        else:
            date_fact = obj.date_fact.strftime("%d/%m/%Y")

        if obj.date_doc == None:
            date_doc = ''
        else:
            date_doc = obj.date_doc.strftime("%d/%m/%Y")

        if obj.subcontractor.taxe_type == None:
            subcontractor_taxe_type = ''
        else:
            subcontractor_taxe_type = str(obj.subcontractor.taxe_type)


        if obj.agreement_start_date == None:
            agreement_start_date = ''
        else:
            agreement_start_date = obj.agreement_start_date.strftime("%d/%m/%Y")

        if obj.agreement_end_date == None:
            agreement_end_date = ''
        else:
            agreement_end_date = obj.agreement_end_date.strftime("%d/%m/%Y")



        if obj.pay_model == None:
            pay_model = ''
        else:
            pay_model = obj.get_pay_model_display()



        if obj.act_date == None:
            act_date = ''
        else:
            act_date = obj.act_date.strftime("%d/%m/%Y")



        if obj.project == None:
            project = ''
        else:
            project = str(obj.project)


        if obj.project.name_short == None:
            project_nameshort = ''
        else:
            project_nameshort = str(obj.project.name_short)

        if obj.project.category == None:
            project_category = ''
        else:
            project_category = str(obj.project.category)

        if obj.project.subcategory == None:
            project_subcategory = ''
        else:
            project_subcategory = str(obj.project.subcategory)

        if obj.project.brand == None:
            project_brand = ''
        else:
            project_brand = str(obj.project.brand)

        if obj.project.stage == None:
            project_stage = ''
        else:
            project_stage = str(obj.project.get_stage_display())


        if obj.unit == None:
            unit = ''
        else:
            unit = str(obj.unit)

        if obj.account == None:
            account = ''
        else:
            account = str(obj.account)

        if obj.created_at == None:
            created_at = ''
        else:
            created_at = obj.created_at.replace(tzinfo=None).strftime("%d/%m/%Y")

        if obj.created_by == None:
            created_by = ''
        else:
            created_by = str(obj.created_by)

        if obj.modified_at == None:
            modified_at = ''
        else:
            modified_at = obj.modified_at.replace(tzinfo=None).strftime("%d/%m/%Y")

        if obj.modified_by == None:
            modified_by = ''
        else:
            modified_by = str(obj.modified_by)

        if obj.is_transfer == None:
            is_transfer = ''
        else:
            is_transfer = str(obj.get_is_transfer_display())

        if obj.hidden == None:
            hidden = ''
        else:
            hidden = str(obj.get_hidden_display())









        row_num += 1
        row = [
            obj.name,
            type,
            status,
            doc_request,
            subcontractor,
            subcontractor_type,
            purpose,
            date,
            date_fact,
            date_doc,
            obj.sum_before_taxes,
            obj.sat,
            obj.taxe,
            subcontractor_taxe_type,
            agreement_start_date,
            agreement_end_date,
            obj.pay_delay,
            obj.delayed,
            pay_model,
            act_date,
            obj.comment,
            obj.estimation_link,
            project,
            project_category,
            project_subcategory,
            project_brand,
            project_nameshort,

            project_stage,
            unit,
            account,

            created_at,
            created_by,
            modified_at,
            modified_by,
            is_transfer,
            hidden,
        ]


        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response