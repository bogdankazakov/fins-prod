from app_transaction import models as app_transaction_models
from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_transaction.decorators import *
from django.http import JsonResponse
from app_transaction.forms import TransferFormSet, TransferCompanyForm
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404
from django.contrib import messages
from app_setup.tools import log_data
from app_transaction.tools import formset_converter, extra_data_pre, transfer_extra_data_post
from django.db.models import Q
from app_transaction.views.main import templates, wording, filter_data, get_item


@login_required
@access_company_level
@access_app_transaction_section
def transfer_create(request):
    queryset = app_transaction_models.Transaction.objects.none()
    if request.method == 'POST':
        formset = TransferFormSet(request.POST, queryset=queryset)
        form_company = TransferCompanyForm(request.POST, user=request.user)
        pk = None
    else:
        formset = TransferFormSet(queryset=queryset)
        formset.extra = 2
        form_company = TransferCompanyForm(user=request.user)
        pk = None
    return transfer_save(request, formset, form_company, templates['create2'], pk)

@login_required
@access_company_level
@access_app_transactions_list
def transfer_save(request, get_access_all, formset, form_company, template_name, pk):
    data = dict()
    if request.method == 'POST':
        page = request.POST.get('page')
        formset_converter(formset)
        if formset.is_valid() and form_company.is_valid():
            form_src = formset[0]
            form_rec = formset[1]

            instance_src = form_src.save(commit=False)
            log_data(instance_src, request)
            extra_data_pre(instance_src, False)
            instance_src.save()

            instance_rec = form_rec.save(commit=False)
            log_data(instance_rec, request)
            extra_data_pre(instance_rec, True)
            instance_rec.save()

            transfer_extra_data_post(instance_src, instance_rec, form_company)
            instance_src.save()
            instance_rec.save()


            data['form_is_valid'] = True
            data['modal_info'] = 1  # means transfer form
            filter, obj, results, formset2 = filter_data(request, page, get_access_all())
            context = {
                'list': results,
                'wording': wording,
                'formset': formset2,
                'user_can_block': app_setup_models.DBUser.objects.get(user_id=request.user.id).access_can_block,

            }
            data['html_list'] = render_to_string(templates['partial_list'], context)
            data['html_pagination'] = render_to_string('app_transaction/pagination.html', context)
        else:
            messages.error(request, wording['validation_error'])

    if request.method == 'GET':
        data['form_is_valid'] = False
        data['modal_info'] = 1  # means transfer form
    context = {
                'formset': formset,
                'form_company': form_company,
                'wording': wording,
                'pk': pk,
               }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

@login_required
@access_company_level
@access_app_transaction_project_RUD
def transfer_update(request, pk):
    item = get_object_or_404(app_transaction_models.Transaction, pk=pk)
    queryset = app_transaction_models.Transaction.objects.filter(Q(pk=pk) | Q(pk=item.transfer))
    initial_form = {'companies': item.account.company_owner}
    if request.method == 'POST':
        formset = TransferFormSet(request.POST, queryset=queryset)
        form_company = TransferCompanyForm(request.POST, initial=initial_form, user=request.user)
    else:
        formset = TransferFormSet(queryset=queryset)
        formset.extra = 0
        form_company = TransferCompanyForm(initial=initial_form, user=request.user)

    return transfer_save(request, formset, form_company, templates['update2'], pk)

@login_required
@access_company_level
@access_app_transactions_list
def transfer_delete(request, get_access_all,  pk):
    data = dict()
    item_1 = get_item(pk)
    item_2 = get_object_or_404(app_transaction_models.Transaction, pk=item_1.transfer)
    if request.method == 'POST':
        item_1.delete()
        item_2.delete()

        data['form_is_valid'] = True
        data['modal_info'] = 0  # means transaction form
        page = request.POST.get('page')
        filter, obj, results, formset = filter_data(request, page, get_access_all())
        context = {
            'list': results,
            'wording': wording,
        }
        data['html_list'] = render_to_string(templates['partial_list'], context)
        data['html_pagination'] = render_to_string('app_transaction/pagination.html', context)

    if request.method == 'GET':
        deletable1, related_obj1 = item_1.can_delete()
        deletable2, related_obj2 = item_2.can_delete()
        if deletable1 and deletable2:
            deletable = True
        else:
            deletable = False
        context = {
            'item': item_1,
            'deletable':deletable,
            'wording': wording,
        }
        data['html_form'] = render_to_string(templates['delete2'],context,request=request,)
    return JsonResponse(data)







