from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_transaction.decorators import *
from django.http import JsonResponse
from app_transaction.forms import *
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404, render
from django.contrib import messages
import logging
from app_setup.tools import log_data
from app_transaction.tools import *
from app_transaction.filters import TransactionsFilter
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Case, When, DateField
from app_project import models as app_project_models
from app_project.tools import period_tool, statistics
from app_project.views import wording as app_project_wording
from fins.global_functions import history_record


logger = logging.getLogger(__name__)




# ===SETUP====

Form = TransactionForm
Form2 = TransactionProjectForm
# try:
#     wording_db = app_signup_models.Wording.objects.all()
# except Exception:
#     wording_db
wording = {
    'btn_create': 'Создать',
    'btn_add': 'Добавить транзакцию',
    'btn_add2': 'Добавить перевод',
    'btn_delete': 'Удалить',
    'btn_cancel': 'Отмена',
    'btn_update': 'Редактировать',
    'btn_save': 'Сохранить',
    'btn_history': 'История',
    'h1_list': 'Список транзакций',
    'h1_create': 'Создать транзакцию',
    'h1_create1': 'Создать перевод между счетами',
    'h1_update': 'Редактировать транзакцию',
    'h1_update1': 'Редактировать перевод',
    'h1_delete': 'Удалить транзакцию',
    'h1_delete1': 'Удалить перевод',
    'h1_history': 'История изменений транзакций',
    'validation_error': 'Ошибка валидации',
    'not_deletable': 'Мы не можем удалить элемент - он используется в системе. Сначала удалите его из всех транзакций.',
    'approve_delete': ' Вы уверены, что хотите удалить транзакцию ',
    'approve_delete1': ' Вы уверены, что хотите удалить перевод ',
    'empty_str': '———',
    'none': '———',
}
templates = {
    'list': 'app_transaction/transactions.html',
    'partial_list': 'app_transaction/partial_list.html',
    'delete': 'app_transaction/transaction/partial_delete.html',
    'history': 'app_transaction/partial_history.html',
    'create2': 'app_transaction/transfer/partial_create.html',
    'update2': 'app_transaction/transfer/partial_update.html',
    'delete2': 'app_transaction/transfer/partial_delete.html',
}





def get_item(pk):
    item = get_object_or_404(app_transaction_models.Transaction, pk=pk)
    return item


# ===========

# def filter_data(request, page, queryset):
#     filter = TransactionsFilter(request.GET, queryset=queryset)
#     formset = TransactionListFormSet(queryset=filter.qs)
#     filter = filter_extra_data(filter, formset)
#
#     obj = app_transaction_models.Transaction
#
#     if page is not None:
#         request.GET = request.GET.copy()
#         request.GET['page'] = page
#
#     page = request.GET.get('page', 1)
#     paginator = Paginator(filter.qs, 30)
#     try:
#         results = paginator.page(page)
#     except PageNotAnInteger:
#         results = paginator.page(1)
#     except EmptyPage:
#         results = paginator.page(paginator.num_pages)
#
#     return filter, obj, results, formset
#
#
# @login_required
# @access_company_level
# @access_app_transactions_list
# def transaction_list(request, get_access_all):
#     filter, obj, results, formset = filter_data(request, None, get_access_all())
#
#     context = {
#         'list': results,
#         'list_num': range(1, int(results.paginator.num_pages) + 1),
#         'sum': summ(filter.qs),
#         'wording': wording,
#         'templates': templates,
#         'filter': filter,
#         'obj': obj,
#         'formset': formset,
#         'user_can_block': app_setup_models.DBUser.objects.get(user_id=request.user.id).access_can_block,
#     }
#
#     # saving request for excel export
#     l = []
#     for q in filter.qs:
#         l.append(q.pk)
#     request.session['transaction_filter_qs'] = l
#
#     return render(request, templates['list'], context)
#

#
#
# @login_required
# @access_company_level
# @access_app_transaction_section
# def transaction_create(request):
#     if request.method == 'POST':
#         form = Form(request.POST, user=request.user)
#     else:
#         form = Form(user=request.user)
#     return transaction_save(request, form, 'app_transaction/transaction/partial_create.html', 0, None, None)

@login_required
@access_company_level
@access_app_transaction_project_C
def transaction_create_project(request, projectpk):
    project = app_project_models.Project.objects.get(pk=projectpk)
    if project.agreement_format == 1:
        if request.method == 'POST':
            form = TransactionAgencyProjectForm(request.POST, user=request.user)
        else:
            form = TransactionAgencyProjectForm(user=request.user)
    else:
        if request.method == 'POST':
            form = Form2(request.POST, user=request.user)
        else:
            form = Form2(user=request.user)
    return transaction_save(request, form, 'app_transaction/transaction/partial_create.html', 1, projectpk, None)


@login_required
@access_company_level
@access_app_transaction_project_C
def transaction_create_project_period(request, projectpk, periodpk):
    project = app_project_models.Project.objects.get(pk=projectpk)
    if project.agreement_format == 1:
        if request.method == 'POST':
            form = TransactionAgencyProjectForm(request.POST, user=request.user)
        else:
            form = TransactionAgencyProjectForm(user=request.user)
    else:
        if request.method == 'POST':
            form = Form2(request.POST, user=request.user)
        else:
            form = Form2(user=request.user)
    return transaction_save(request, form, 'app_transaction/transaction/partial_create.html', 2, projectpk, periodpk)






#
# @login_required
# @access_company_level
# @access_app_transaction_project_RUD
# def transaction_update(request, pk):
#     item = get_item(pk)
#     if request.method == 'POST':
#         form = Form2(request.POST, instance=item, user=request.user)
#     else:
#         form = Form2(instance=item, user=request.user)
#     return transaction_save(request, form, 'app_transaction/transaction/partial_update.html', 0, None, None)

@login_required
@access_company_level
@access_app_transaction_project_RUD
def transaction_update_project(request, pk):
    item = get_item(pk)
    if request.method == 'POST':
        form = Form2(request.POST, instance=item, user=request.user)
    else:
        form = Form2(instance=item, user=request.user)
    return transaction_save(request, form, 'app_transaction/transaction/partial_update.html', 1, None, None)


@login_required
@access_company_level
@access_app_transaction_project_RUD
def transaction_update_project_period(request, pk, projectpk):
    item = get_item(pk)
    if request.method == 'POST':
        form = Form2(request.POST, instance=item, user=request.user)
    else:
        form = Form2(instance=item, user=request.user)
    return transaction_save(request, form, 'app_transaction/transaction/partial_update.html', 2, projectpk, None)






# @login_required
# @access_company_level
# @access_app_transactions_list
# def transaction_delete(request, get_access_all, pk):
#     data = dict()
#     item = get_item(pk)
#     if request.method == 'POST':
#         item.delete()
#         data['form_is_valid'] = True
#         data['modal_info'] = 0  # means transaction form
#         page = request.POST.get('page')
#         filter, obj, results, formset = filter_data(request, page, get_access_all())
#         context = {
#             'list': results,
#             'formset': formset,
#             'wording': wording,
#             'user_can_block': app_setup_models.DBUser.objects.get(user_id=request.user.id).access_can_block,
#
#         }
#         data['html_list'] = render_to_string(templates['partial_list'], context)
#         data['html_pagination'] = render_to_string('app_transaction/pagination.html', context)
#
#     if request.method == 'GET':
#         deletable, related_obj = item.can_delete()
#         context = {
#             'item': item,
#             'deletable':deletable,
#             'wording': wording,
#             'url': reverse('app_transaction:transaction_delete', kwargs={'pk': item.pk}),
#             'class_delete': 'js-transaction-delete-form',
#         }
#         data['html_form'] = render_to_string(templates['delete'],context,request=request,)
#     return JsonResponse(data)

@login_required
@access_company_level
@access_app_transaction_project_RUD
def transaction_delete_project(request, pk):
    data = dict()
    item = get_item(pk)
    if request.method == 'POST':
        item.delete()
        data['form_is_valid'] = True
        data['modal_info'] = 0  # means transaction form
        project = item.project
        transactions = app_transaction_models.Transaction.objects.filter(project=project)
        main_currency = app_transaction_models.Currency.objects.get(is_default=True)
        all = app_transaction_models.Transaction.objects.all().annotate(
            date=Case(
                When(date_fact=None, then='date_doc'),
                default='date_fact',
                output_field=DateField(),
            )).order_by('date')
        report = statistics(transactions, main_currency, all, False)
        context = {
            'list': app_transaction_models.Transaction.objects.filter(project=item.project),
            'wording': wording,
            'project': project,
            'report': report,
            'user_can_block': app_setup_models.DBUser.objects.get(user_id=request.user.id).access_can_block,


        }
        data['html_list'] = render_to_string('app_transaction/in_project/partial_list_in_projects.html', context)
        data['html_list_statistics'] = render_to_string('app_project/project_card/partial_statistics.html', context)

    if request.method == 'GET':
        deletable, related_obj = item.can_delete()
        context = {
            'item': item,
            'deletable':deletable,
            'wording': wording,
            'url': reverse('app_transaction:transaction_delete_project', kwargs={'pk': item.pk}),
            'class_delete': 'js-transaction-delete-form',
        }
        data['html_form'] = render_to_string(templates['delete'],context,request=request,)
    return JsonResponse(data)


@login_required
@access_company_level
@access_app_transaction_project_RUD
def transaction_delete_project_period(request, pk):
    data = dict()
    item = get_item(pk)
    if request.method == 'POST':
        item.delete()
        data['form_is_valid'] = True
        data['modal_info'] = 0  # means transaction form

        project = item.project
        context = {
            'periods': period_tool(project),
            'wording': app_project_wording,
            'project': project,
            'obj': app_transaction_models.Transaction,
            'user_can_block': app_setup_models.DBUser.objects.get(user_id=request.user.id).access_can_block,

        }
        data['html_list'] = render_to_string('app_project/project_card/partial_list_periods.html', context)

    if request.method == 'GET':
        deletable, related_obj = item.can_delete()
        context = {
            'item': item,
            'deletable':deletable,
            'wording': wording,
            'url': reverse('app_transaction:transaction_delete_project_period', kwargs={'pk': item.pk}),
            'class_delete': 'js-transaction-delete-fromperiod-form',
        }
        data['html_form'] = render_to_string(templates['delete'],context,request=request,)
    return JsonResponse(data)






@login_required
@access_company_level
@access_app_transactions_list
def transaction_save(request, get_access_all, form, template_name, source, projectpk, periodpk):
    """
    source 0 - from the list
    source 1 - from project
    source 2 - from project periods (used in t&m agreements)
    modal info 0 - transaction form
    modal info 1 - transfer form
    """
    data = dict()
    if request.method == 'POST':
        page = request.POST.get('page')
        form_converter(form)
        if form.is_valid():
            instance = form.save(commit=False)
            log_data(instance, request)
            instance.save()
            extra_data_post(instance, source, projectpk, request, periodpk)
            instance.save()

            data['form_is_valid'] = True
            data['modal_info'] = 0
            if source == 0:
                filter, obj, results, formset = filter_data(request, page, get_access_all())
                message = 'Все прошло успешно'
                messages.add_message(request, messages.SUCCESS, message)
                context = {
                    'list': results,
                    'wording': wording,
                    'formset': formset,
                    'user_can_block': app_setup_models.DBUser.objects.get(user_id=request.user.id).access_can_block,
                    'messages': request._messages,

                }
                data['html_list'] = render_to_string(templates['partial_list'], context)
                data['html_pagination'] = render_to_string('app_transaction/pagination.html', context)
                data['html_partial_project_messages'] = render_to_string('app_project/project_card/partial_project_messages.html', context)

            elif source == 1:
                try:
                    if form.cleaned_data['create_income']:
                        agency_income_creator(instance, projectpk, source)
                except Exception:
                    pass
                project = instance.project
                all = get_access_all()
                results = all.filter(project=form.instance.project)
                transactions = app_transaction_models.Transaction.objects.filter(project=project)
                main_currency = app_transaction_models.Currency.objects.get(is_default=True)

                report = statistics(transactions, main_currency, all, False)
                message = 'Все прошло успешно  '
                messages.add_message(request, messages.SUCCESS, message)
                context = {
                    'list': results,
                    'wording': wording,
                    'project': project,
                    'report': report,
                    'user_can_block': app_setup_models.DBUser.objects.get(user_id=request.user.id).access_can_block,
                    'messages': request._messages,

                }
                data['html_list'] = render_to_string('app_transaction/in_project/partial_list_in_projects.html', context)
                data['html_list_statistics'] = render_to_string('app_project/project_card/partial_statistics.html', context)
                data['html_partial_project_messages'] = render_to_string('app_project/project_card/partial_project_messages.html', context)

            else:
                try:
                    if form.cleaned_data['create_income']:
                        agency_income_creator(instance, projectpk, source)
                except Exception:
                    pass
                project = instance.project
                message = 'Все прошло успешно'
                messages.add_message(request, messages.SUCCESS, message)
                context = {
                    'periods': period_tool(project),
                    'wording': app_project_wording,
                    'project': project,
                    'obj': app_transaction_models.Transaction,
                    'user_can_block': app_setup_models.DBUser.objects.get(user_id=request.user.id).access_can_block,
                    'messages': request._messages,

                }
                data['html_list'] = render_to_string('app_project/project_card/partial_list_periods.html', context)
                data['html_partial_project_messages'] = render_to_string('app_project/project_card/partial_project_messages.html', context)



        else:
            messages.error(request, wording['validation_error'])

    if request.method == 'GET':
        data['form_is_valid'] = False
        data['modal_info'] = 0 #means transaction form

    if source == 0:
        url_update = reverse('app_transaction:transaction_update', kwargs={'pk': form.instance.pk} )
        url_create = reverse('app_transaction:transaction_create' )
        class_create = 'js-transaction-create-form'
        class_update = 'js-transaction-update-form'
    elif source == 1:
        url_update = reverse('app_transaction:transaction_update_project', kwargs={'pk': form.instance.pk})
        url_create = reverse('app_transaction:transaction_create_project', kwargs={'projectpk': projectpk})
        class_create = 'js-transaction-create-form'
        class_update = 'js-transaction-update-form'
    else:
        url_update = reverse('app_transaction:transaction_update_project_period', kwargs={'pk': form.instance.pk, 'projectpk': projectpk})
        url_create = reverse('app_transaction:transaction_create_project_period', kwargs={'projectpk': projectpk, 'periodpk': periodpk} )
        class_create = 'js-transaction-create-fromperiod-form'
        class_update = 'js-transaction-update-fromperiod-form'

    context = {
            'form': form,
            'wording': wording,
            'url_update': url_update,
            'url_create': url_create,
            'class_create': class_create,
            'class_update': class_update,
           }
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


#
#
# @login_required
# @access_company_level
# @access_app_transaction_section
# def transaction_history(request):
#     history = app_transaction_models.Transaction.history.all()[:3]
#     context = {
#         'records': history_record(history, 'Транзакция', True),
#         'wording':wording,
#     }
#
#     data = {}
#     data['html_form'] = render_to_string(templates['history'], context, request=request)
#
#     return JsonResponse(data)