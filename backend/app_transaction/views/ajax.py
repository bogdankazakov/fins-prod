from app_transaction import models as app_transaction_models
from app_auth.decorators import access_company_level
from django.contrib.auth.decorators import login_required
from app_transaction.decorators import *
from django.http import JsonResponse
from django.template.loader import render_to_string
from app_transaction.views.main import templates, wording
from app_setup.tools import log_data


@login_required
@access_company_level
@access_app_transaction_section
def load_purpose(request):
    type = request.GET.get('type')
    purposes = app_transaction_models.Purpose.objects.filter(type=type).order_by('name')
    return render(request, 'app_transaction/purpose_dropdown_list_options.html', {'purposes': purposes})

@login_required
@access_company_level
@access_app_transaction_section
def subcontractor_info(request):
    data = {}
    pk = request.GET.get('pk')
    if pk:
        subcontractor = app_transaction_models.Subcontractor.objects.get(pk=pk)
        data['legalform'] = str(subcontractor.legal_form)
        data['taxetype'] = str(subcontractor.taxe_type)
        data['taxevalue'] = str(subcontractor.taxe_type.multiplicator)
        data['paydelay'] = str(subcontractor.pay_delay)
    else:
        data['legalform'] = None
        data['taxetype'] = None
        data['taxevalue'] = None
        data['paydelay'] = None
    return JsonResponse(data)


@login_required
@access_company_level
@access_app_transaction_section
def mycompany_info(request):
    data = {}
    pk = request.GET.get('pk')
    if pk:
        mycompany = app_transaction_models.Subcontractor.objects.get(pk=pk)
        accounts = mycompany.subs_accounts.all()
        data['dropdown'] = render(request, 'app_transaction/accounts_dropdown_list_options.html', {'accounts': accounts}).content.decode('utf-8')
        data['taxevalue'] = str(mycompany.taxe_type.multiplicator)

    else:
        data['dropdown'] = None
        data['taxevalue'] = None

    return JsonResponse(data)

@login_required
@access_company_level
@access_app_transaction_section
def account_info(request):
    data = {}
    pk = request.GET.get('pk')
    if pk:
        account = app_transaction_models.Account.objects.get(pk=pk)
        data['currency'] = str(account.currency)
        data['taxevalue'] = str(account.company_owner.taxe_type.multiplicator)
        data['taxe'] = str(account.company_owner.taxe_type.name)

    else:
        data['currency'] = None
        data['taxevalue'] = None
        data['taxe'] = None

    return JsonResponse(data)

@login_required
@access_company_level
@access_app_transaction_lister
def list_saver(request):
    data = {}
    print('request.GET', request.GET)
    name = request.GET.get('name')
    status = request.GET.get('status')
    date_fact = request.GET.get('date_fact')

    transaction = app_transaction_models.Transaction.objects.get(name=name)

    transaction.status = status

    if status == '3':
        transaction.is_editable = False
        data['editable'] = False
    else:
        transaction.is_editable = True
        data['editable'] = True
        data['user_can_block'] = app_setup_models.DBUser.objects.get(user_id=request.user.id).access_can_block

    if date_fact != '':
        transaction.date_fact = date_fact
    log_data(transaction, request)

    transaction.save()

    return JsonResponse(data)


@login_required
@access_company_level
@access_app_transaction_section
def transaction_docs(request,pk):
    data={}
    transaction = app_transaction_models.Transaction.objects.get(pk=pk)

    wording = {
        'btn_cancel': 'Отмена',
        'h1_docs': 'Список связанных документов',
    }


    context = {
        'wording': wording,
        'agrs': transaction.transaction_agreements.all(),
    }
    data['html'] = render_to_string('app_transaction/transaction/partial_docs.html', context)
    return JsonResponse(data)