from django.test import TestCase, Client, RequestFactory
from app_signup import models as app_signup_models
from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
import psycopg2
from fins.threadlocal import thread_local
from app_transaction import views as app_transaction_views
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from selenium.webdriver.support.ui import Select
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import os
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.conf import settings
from django.contrib.auth import (
    SESSION_KEY, BACKEND_SESSION_KEY, HASH_SESSION_KEY,
    get_user_model
)
from django.contrib.sessions.backends.db import SessionStore
from fins.global_functions import get_db_name_test
from app_transaction.converter import *
from app_transaction.tests.main import basic_data
from datetime import date
from app_setup import models as app_setup_models
from django.contrib.sessions.middleware import SessionMiddleware
from app_project import views as app_project_views


class TransactionTest_CRUD(TestCase):

    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()

        site = Site.objects.get(pk=1)
        site.domain = 'testserver'
        site.save()

        c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
        c.save()
        c.create_database()
        c.fill_database()

        unit1 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[0]
        unit2 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[1]

        user = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b@b.ru')
        user.set_password('123')
        user.created_account_pk = c.pk
        user.save()
        user.account.add(c)

        userdb = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=user.pk)
        userdb.access_app_project = 2
        userdb.unit = unit1
        userdb.save(using=get_db_name_test())

        self.my_user = user
        self.account = c

        self.user2 = app_signup_models.User(
            first_name='UserFirst2',
            last_name='UserLast2',
            email='a@a.ru')
        self.user2.set_password('123')
        self.user2.created_account_pk = c.pk
        self.user2.save()
        self.user2.account.add(c)

        userdb2 = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=self.user2.pk)
        userdb2.access_app_project = 1
        userdb2.unit = unit1
        userdb2.save(using=get_db_name_test())

        # self.user3 = app_signup_models.User(
        #     first_name='UserFirst3',
        #     last_name='UserLast3',
        #     email='c@c.ru')
        # self.user3.set_password('123')
        # self.user3.created_account_pk = c.pk
        # self.user3.save()
        # self.user3.account.add(c)
        #
        # userdb3 = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=self.user3.pk)
        # userdb3.access_app_project = 3
        # userdb3.unit = unit2
        # userdb3.save(using=get_db_name_test())
        #
        # self.user4 = app_signup_models.User(
        #     first_name='UserFirst4',
        #     last_name='UserLast4',
        #     email='d@d.ru')
        # self.user4.set_password('123')
        # self.user4.created_account_pk = c.pk
        # self.user4.save()
        # self.user4.account.add(c)
        #
        # userdb4 = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=self.user4.pk)
        # userdb4.access_app_project = 0
        # userdb4.unit = unit1
        # userdb4.save(using=get_db_name_test())


        self.tt_1 = app_transaction_models.TaxeType(name='Налог1', multiplicator=1, is_deletable=False)
        self.tt_1.save(using=get_db_name_test())
        self.tt_2 = app_transaction_models.TaxeType(name='Налог2', multiplicator=2, is_deletable=False)
        self.tt_2.save(using=get_db_name_test())
        self.tt_3 = app_transaction_models.TaxeType(name='Налог3', multiplicator=3, is_deletable=False)
        self.tt_3.save(using=get_db_name_test())
        self.tt_4 = app_transaction_models.TaxeType(name='Налог4', multiplicator=4, is_deletable=False)
        self.tt_4.save(using=get_db_name_test())

        self.sub_1 = app_transaction_models.Subcontractor(
            name='Подрядчик_1',
            is_deletable=False,
        )
        self.sub_1.save(using=get_db_name_test())
        self.sub_1.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_1.taxe_type = self.tt_1
        self.sub_1.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_1.is_my_company = False
        self.sub_1.save(using=get_db_name_test())

        self.sub_2 = app_transaction_models.Subcontractor(
            name='Подрядчик_2',
            is_deletable=False,
        )
        self.sub_2.save(using=get_db_name_test())
        self.sub_2.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_2.taxe_type = self.tt_2
        self.sub_2.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_2.is_my_company = False
        self.sub_2.save(using=get_db_name_test())

        self.sub_3 = app_transaction_models.Subcontractor(
            name='Подрядчик_3',
            is_deletable=False,
        )
        self.sub_3.save(using=get_db_name_test())
        self.sub_3.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_3.taxe_type = self.tt_3
        self.sub_3.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_3.is_my_company = False
        self.sub_3.save(using=get_db_name_test())

        self.sub_4 = app_transaction_models.Subcontractor(
            name='Подрядчик_4',
            is_deletable=False,
        )
        self.sub_4.save(using=get_db_name_test())
        self.sub_4.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_4.taxe_type = self.tt_4
        self.sub_4.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_4.is_my_company = False
        self.sub_4.save(using=get_db_name_test())

        self.mycompany_1 = app_transaction_models.Subcontractor(name='Тестовая компания 1', is_deletable=False)
        self.mycompany_1.save(using=get_db_name_test())
        self.mycompany_1.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.mycompany_1.taxe_type = app_transaction_models.TaxeType.objects.using(get_db_name_test()).get(name='НДС')
        self.mycompany_1.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.mycompany_1.is_my_company = True
        self.mycompany_1.save(using=get_db_name_test())

        self.ac1 = app_transaction_models.Account(name='Банк', type=1)
        self.ac1.save(using=get_db_name_test())
        self.ac1.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='Rub')
        self.ac1.company_owner = self.mycompany_1
        self.ac1.save(using=get_db_name_test())

        self.ac2 = app_transaction_models.Account(name='Касса', type=1)
        self.ac2.save(using=get_db_name_test())
        self.ac2.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='Rub')
        self.ac2.company_owner = self.mycompany_1
        self.ac2.save(using=get_db_name_test())

        self.ac3 = app_transaction_models.Account(name='Сейф', type=1)
        self.ac3.save(using=get_db_name_test())
        self.ac3.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='USD')
        self.ac3.company_owner = self.mycompany_1
        self.ac3.save(using=get_db_name_test())

        self.mycompany_2 = app_transaction_models.Subcontractor(name='Тестовая компания 2', is_deletable=False)
        self.mycompany_2.save(using=get_db_name_test())
        self.mycompany_2.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.mycompany_2.taxe_type = app_transaction_models.TaxeType.objects.using(get_db_name_test()).get(name='НДФЛ')
        self.mycompany_2.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.mycompany_2.is_my_company = True
        self.mycompany_2.save(using=get_db_name_test())

        self.ac4 = app_transaction_models.Account(name='Заначка', type=1)
        self.ac4.save(using=get_db_name_test())
        self.ac4.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='USD')
        self.ac4.company_owner = self.mycompany_2
        self.ac4.save(using=get_db_name_test())

        self.project_1 = app_project_models.Project(name='P00001')
        self.project_1.save(using=get_db_name_test())
        self.project_1.category = app_project_models.ProjectCategory.objects.using(get_db_name_test()).get(
            name='Проекты')
        self.project_1.save(using=get_db_name_test())
        team = app_project_models.TeamProject()
        team.save(using=get_db_name_test())
        team.user = userdb
        team.project = self.project_1
        team.save(using=get_db_name_test())

        self.project_tech = app_project_models.Project.objects.using(get_db_name_test()).get(
            name_short='Внутренние переводы')

        self.project_2 = app_project_models.Project(name='P00002')
        self.project_2.save(using=get_db_name_test())
        self.project_2.category = app_project_models.ProjectCategory.objects.using(get_db_name_test()).get(
            name='Проекты')
        self.project_2.save(using=get_db_name_test())
        team = app_project_models.TeamProject()
        team.save(using=get_db_name_test())
        team.user = userdb2
        team.project = self.project_2
        team.save(using=get_db_name_test())

        self.puprose_income = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(name='Оплата')
        self.puprose_outcome = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(name='Расход')

    def tearDown(self):
        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

    def test_transaction_CRUD_2(self):
        print('sub_1', self.sub_1, self.sub_1.taxe_type)
        print('sub_2', self.sub_2, self.sub_2.taxe_type)
        print('sub_3', self.sub_3, self.sub_3.taxe_type)
        print('sub_4', self.sub_4, self.sub_4.taxe_type)
        print('mycompany_1', self.mycompany_1.taxe_type)
        print('ac1', self.ac1, self.ac1.currency, self.ac1.company_owner)
        print('ac2', self.ac2, self.ac2.currency, self.ac2.company_owner)
        print('ac3', self.ac3, self.ac3.currency, self.ac3.company_owner)
        print('mycompany_2', self.mycompany_2.taxe_type)
        print('ac4', self.ac4, self.ac4.currency, self.ac4.company_owner)
        print('project_1', self.project_1)
        print('project_tech', self.project_tech)
        print('puprose_income', self.puprose_income)
        print('puprose_outcome', self.puprose_outcome)

        self.client.login(username='b@b.ru', password='123')

        # create transaction
        link = sub_reverse('app_transaction:transaction_create', subdomain='mysubdomain')
        data = basic_data(self.project_1, self.sub_1, self.puprose_outcome, self.ac1)
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_create(request)
            return response

        response = tread(request)

        created = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000001')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(created)

        # update
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_transaction:transaction_update', kwargs=kwargs, subdomain='mysubdomain')
        data = {
            'project': [str(self.project_1.pk)],
            'type': ['False'],
            'subcontractor': [str(self.sub_1.pk)],
            'purpose': [str(self.puprose_outcome.pk)],
            'unit': '',
            'account': [str(self.ac1.pk)],
            'date_fact': '',
            'date_doc': ['2019-03-02'],
            'sum_before_taxes': ['100,00'],
            'sum_after_taxes': ['0,00'],
            'agreement_start_date': ['2019-03-02'],
            'agreement_end_date': ['2019-03-02'],
            'pay_model': ['0'],
            'pay_delay': ['0'],
            'description': [''],
            'comment': [''],
            'estimation_link': [''],
            'act_date': [''],
            'status': ['0'],
            'doc_request': ['0'],
            'parent': [''],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_update(request, **kwargs)
            return response

        response = tread(request)

        updated = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000001')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            str(updated.sum_before_taxes), '100.00')

        # delete
        kwargs = {'pk': updated.pk}
        link = sub_reverse('app_transaction:transaction_delete', kwargs=kwargs, subdomain='mysubdomain')
        data = {}
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_delete(request, **kwargs)
            return response

        response = tread(request)

        deleted = app_transaction_models.Transaction.objects.using(get_db_name_test()).filter(name='T000001')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)


    def test_transaction_CRUD_1_created_NOT_in_my_project(self):

        self.client.login(username='b@b.ru', password='123')

        # create transaction
        link = sub_reverse('app_transaction:transaction_create', subdomain='mysubdomain')
        data = basic_data(self.project_1, self.sub_1, self.puprose_outcome, self.ac1)
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_create(request)
            return response

        response = tread(request)
        created = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000001')


        # visit project list
        link = sub_reverse('app_transaction:index', subdomain='mysubdomain')
        data = {}
        request = self.factory.get(link, data)
        request.user = self.user2
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_list(request)
            return response

        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        response = tread(request)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('T000' not in response.content.decode(
            "utf-8"))  # checks that project is not dispalyed in the list


        # update
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_transaction:transaction_update', kwargs=kwargs, subdomain='mysubdomain')
        data = basic_data(self.project_1, self.sub_1, self.puprose_outcome, self.ac1)
        request = self.factory.post(link, data)
        request.user = self.user2
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_update(request, **kwargs)
            return response

        response = tread(request)

        self.assertEqual(response.status_code, 302)


        # delete
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_transaction:transaction_delete', kwargs=kwargs, subdomain='mysubdomain')
        data = {}
        request = self.factory.post(link, data)
        request.user = self.user2
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_delete(request, **kwargs)
            return response

        response = tread(request)

        self.assertEqual(response.status_code, 302)

    def test_transaction_CRUD_1_created_IN_my_project(self):

        self.client.login(username='b@b.ru', password='123')

        # create transaction
        link = sub_reverse('app_transaction:transaction_create', subdomain='mysubdomain')
        data = basic_data(self.project_2, self.sub_1, self.puprose_outcome, self.ac1)
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_create(request)
            return response

        response = tread(request)
        created = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000001')

        self.client.login(username='a@a.ru', password='123')

        # visit project list
        link = sub_reverse('app_transaction:index', subdomain='mysubdomain')
        data = {}
        request = self.factory.get(link, data)
        request.user = self.user2
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_list(request)
            return response

        middleware = SessionMiddleware()
        middleware.process_request(request)
        request.session.save()

        response = tread(request)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('T000' in response.content.decode(
            "utf-8"))  # checks that project is not dispalyed in the list


        # update
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_transaction:transaction_update', kwargs=kwargs, subdomain='mysubdomain')
        data = basic_data(self.project_2, self.sub_1, self.puprose_outcome, self.ac1)
        request = self.factory.post(link, data)
        request.user = self.user2
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_update(request, **kwargs)
            return response

        response = tread(request)

        self.assertEqual(response.status_code, 200)


        # delete
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_transaction:transaction_delete', kwargs=kwargs, subdomain='mysubdomain')
        data = {}
        request = self.factory.post(link, data)
        request.user = self.user2
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_delete(request, **kwargs)
            return response

        response = tread(request)

        self.assertEqual(response.status_code, 200)

    def test_transaction_list_access(self):


        self.client.login(username='b@b.ru', password='123')

        # create transaction
        link = sub_reverse('app_transaction:transaction_create', subdomain='mysubdomain')
        data = basic_data(self.project_1, self.sub_1, self.puprose_outcome, self.ac1)
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_create(request)
            return response

        response = tread(request)

        created = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000001')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(created)

        # update
        kwargs = {'pk': created.pk}
        link = sub_reverse('app_transaction:transaction_update', kwargs=kwargs, subdomain='mysubdomain')
        data = {
            'project': [str(self.project_1.pk)],
            'type': ['False'],
            'subcontractor': [str(self.sub_1.pk)],
            'purpose': [str(self.puprose_outcome.pk)],
            'unit': '',
            'account': [str(self.ac1.pk)],
            'date_fact': '',
            'date_doc': ['2019-03-02'],
            'sum_before_taxes': ['100,00'],
            'sum_after_taxes': ['0,00'],
            'agreement_start_date': ['2019-03-02'],
            'agreement_end_date': ['2019-03-02'],
            'pay_model': ['0'],
            'pay_delay': ['0'],
            'description': [''],
            'comment': [''],
            'estimation_link': [''],
            'act_date': [''],
            'status': ['0'],
            'doc_request': ['0'],
            'parent': [''],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_update(request, **kwargs)
            return response

        response = tread(request)

        updated = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000001')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            str(updated.sum_before_taxes), '100.00')

        # delete
        kwargs = {'pk': updated.pk}
        link = sub_reverse('app_transaction:transaction_delete', kwargs=kwargs, subdomain='mysubdomain')
        data = {}
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_delete(request, **kwargs)
            return response

        response = tread(request)

        deleted = app_transaction_models.Transaction.objects.using(get_db_name_test()).filter(name='T000001')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(deleted), 0)


class TransactionTestJSCreate(StaticLiveServerTestCase):

    def create_session_cookie(self, user):
        session = SessionStore()
        session[SESSION_KEY] = user.pk
        session[BACKEND_SESSION_KEY] = settings.AUTHENTICATION_BACKENDS[0]
        session[HASH_SESSION_KEY] = user.get_session_auth_hash()
        session.save()
        cookie = {
            'name': settings.SESSION_COOKIE_NAME,
            'value': session.session_key,
            'secure': False,
            'path': '/',
        }
        return cookie

    def loggin(self):
        session_cookie = self.create_session_cookie(app_signup_models.User.objects.get(email='b@b.ru'))
        link = sub_reverse('app_dashboard:index', subdomain='mysubdomain')
        self.selenium.get(link)
        self.selenium.add_cookie(session_cookie)
        self.selenium.refresh()
        self.selenium.get(link)

    def graber_transactoion_modal(self):
        modal = self.selenium.find_element_by_id("modal-global")

        self.id_project_select = Select(modal.find_element_by_id("id_project"))
        self.id_project = modal.find_element_by_id("id_project")
        self.id_type_select = Select(modal.find_element_by_id("id_type"))
        self.id_type = modal.find_element_by_id("id_type")
        self.id_subcontractor_select = Select(modal.find_element_by_id("id_subcontractor"))
        self.id_subcontractor = modal.find_element_by_id("id_subcontractor")
        self.id_sub_taxetype = modal.find_element_by_id("id_sub_taxetype")
        self.id_sub_legalform = modal.find_element_by_id("id_sub_legalform")
        self.id_purpose_select = Select(modal.find_element_by_id("id_purpose"))
        self.id_purpose = modal.find_element_by_id("id_purpose")
        self.id_unit_select = Select(modal.find_element_by_id("id_unit"))
        self.id_unit = modal.find_element_by_id("id_unit")
        self.id_account_select = Select(modal.find_element_by_id("id_account"))
        self.id_account = modal.find_element_by_id("id_account")
        self.id_currency = modal.find_element_by_id("id_currency")
        self.id_date_fact = modal.find_element_by_id("id_date_fact")
        self.id_date_doc = modal.find_element_by_id("id_date_doc")
        self.id_sum_before_taxes = modal.find_element_by_id("id_sum_before_taxes")
        self.id_sum_after_taxes = modal.find_element_by_id("id_sum_after_taxes")
        self.id_sum_taxes = modal.find_element_by_id("id_sum_taxes")
        self.id_agreement_start_date = modal.find_element_by_id("id_agreement_start_date")
        self.id_agreement_end_date = modal.find_element_by_id("id_agreement_end_date")
        self.id_pay_model_select = Select(modal.find_element_by_id("id_pay_model"))
        self.id_pay_model = modal.find_element_by_id("id_pay_model")
        self.id_pay_delay = modal.find_element_by_id("id_pay_delay")
        self.id_description = modal.find_element_by_id("id_description")
        self.id_comment = modal.find_element_by_id("id_comment")
        self.id_estimation_link = modal.find_element_by_id("id_estimation_link")
        self.id_act_date = modal.find_element_by_id("id_act_date")
        self.id_status_select = Select(modal.find_element_by_id("id_status"))
        self.id_status = modal.find_element_by_id("id_status")
        self.id_doc_request_select = Select(modal.find_element_by_id("id_doc_request"))
        self.id_doc_request = modal.find_element_by_id("id_doc_request")
        self.id_hidden = modal.find_element_by_id("id_hidden")
        self.id_parent = modal.find_element_by_id("id_parent")

    def options_to_list(self, options):
        list = []
        for el in options:
            list.append(el.get_attribute("innerHTML"))
        return list

    def link(self):
        link = sub_reverse('app_transaction:index', subdomain='mysubdomain')
        return link

    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()

        site = Site.objects.get(pk=1)
        # site.domain = 'testserver'
        fdir, fname = os.path.split(self.live_server_url)
        site.domain = fname
        site.save()

        c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
        c.save()
        c.create_database()
        c.fill_database()

        user = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b@b.ru')
        user.set_password('qweqwe111')
        user.created_account_pk = c.pk
        user.save()
        user.account.add(c)

        unit1 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[0]
        userdb = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=user.pk)
        userdb.access_app_project = 2
        userdb.unit = unit1
        userdb.save(using=get_db_name_test())

        self.my_user = user
        self.account = c

        self.tt_1 = app_transaction_models.TaxeType(name='Налог1', multiplicator=1, is_deletable=False)
        self.tt_1.save(using=get_db_name_test())
        self.tt_2 = app_transaction_models.TaxeType(name='Налог2', multiplicator=2, is_deletable=False)
        self.tt_2.save(using=get_db_name_test())
        self.tt_3 = app_transaction_models.TaxeType(name='Налог3', multiplicator=3, is_deletable=False)
        self.tt_3.save(using=get_db_name_test())
        self.tt_4 = app_transaction_models.TaxeType(name='Налог4', multiplicator=4, is_deletable=False)
        self.tt_4.save(using=get_db_name_test())

        self.sub_1 = app_transaction_models.Subcontractor(
            name='Подрядчик_1',
            is_deletable=False,
        )
        self.sub_1.save(using=get_db_name_test())
        self.sub_1.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_1.taxe_type = self.tt_1
        self.sub_1.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_1.is_my_company = False
        self.sub_1.save(using=get_db_name_test())

        self.sub_2 = app_transaction_models.Subcontractor(
            name='Подрядчик_2',
            is_deletable=False,
        )
        self.sub_2.save(using=get_db_name_test())
        self.sub_2.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_2.taxe_type = self.tt_2
        self.sub_2.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_2.is_my_company = False
        self.sub_2.save(using=get_db_name_test())

        self.sub_3 = app_transaction_models.Subcontractor(
            name='Подрядчик_3',
            is_deletable=False,
        )
        self.sub_3.save(using=get_db_name_test())
        self.sub_3.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_3.taxe_type = self.tt_3
        self.sub_3.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_3.is_my_company = False
        self.sub_3.save(using=get_db_name_test())

        self.sub_4 = app_transaction_models.Subcontractor(
            name='Подрядчик_4',
            is_deletable=False,
        )
        self.sub_4.save(using=get_db_name_test())
        self.sub_4.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_4.taxe_type = self.tt_4
        self.sub_4.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_4.is_my_company = False
        self.sub_4.save(using=get_db_name_test())

        self.mycompany_1 = app_transaction_models.Subcontractor(name='Тестовая компания 1',
                                                                is_deletable=False)
        self.mycompany_1.save(using=get_db_name_test())
        self.mycompany_1.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[
            0]
        self.mycompany_1.taxe_type = app_transaction_models.TaxeType.objects.using(get_db_name_test()).get(
            name='НДС')
        self.mycompany_1.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.mycompany_1.is_my_company = True
        self.mycompany_1.save(using=get_db_name_test())

        self.ac1 = app_transaction_models.Account(name='Банк', type=1)
        self.ac1.save(using=get_db_name_test())
        self.ac1.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='Rub')
        self.ac1.company_owner = self.mycompany_1
        self.ac1.save(using=get_db_name_test())

        self.ac2 = app_transaction_models.Account(name='Касса', type=1)
        self.ac2.save(using=get_db_name_test())
        self.ac2.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='Rub')
        self.ac2.company_owner = self.mycompany_1
        self.ac2.save(using=get_db_name_test())

        self.ac3 = app_transaction_models.Account(name='Сейф', type=1)
        self.ac3.save(using=get_db_name_test())
        self.ac3.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='USD')
        self.ac3.company_owner = self.mycompany_1
        self.ac3.save(using=get_db_name_test())

        self.mycompany_2 = app_transaction_models.Subcontractor(name='Тестовая компания 2',
                                                                is_deletable=False)
        self.mycompany_2.save(using=get_db_name_test())
        self.mycompany_2.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[
            0]
        self.mycompany_2.taxe_type = app_transaction_models.TaxeType.objects.using(get_db_name_test()).get(
            name='НДФЛ')
        self.mycompany_2.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.mycompany_2.is_my_company = True
        self.mycompany_2.save(using=get_db_name_test())

        self.ac4 = app_transaction_models.Account(name='Заначка', type=1)
        self.ac4.save(using=get_db_name_test())
        self.ac4.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='USD')
        self.ac4.company_owner = self.mycompany_2
        self.ac4.save(using=get_db_name_test())

        self.project_1 = app_project_models.Project(name='P00001', work_format=0, agreement_format=0)
        self.project_1.save(using=get_db_name_test())
        self.project_1.category = app_project_models.ProjectCategory.objects.using(get_db_name_test()).get(
            name='Проекты')
        self.project_1.save(using=get_db_name_test())

        self.project_tech = app_project_models.Project.objects.using(get_db_name_test()).get(
            name_short='Внутренние переводы')

        self.puprose_income = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(name='Оплата')
        self.puprose_outcome = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(
            name='Расход')

        chromedriver_path = '/Users/bogdan/PycharmProjects/app/Fins_App/fins/app_transaction/tests/chromedriver'
        self.selenium = webdriver.Chrome(chromedriver_path)

        self.loggin()
        link = self.link()
        self.selenium.get(link)
        wait = WebDriverWait(self.selenium, 2)
        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, "js-create-transaction")))
        btn_create_transaction = self.selenium.find_element_by_class_name("js-create-transaction")
        btn_create_transaction.click()
        time.sleep(2)
        self.graber_transactoion_modal()

    def tearDown(self):
        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

        self.selenium.quit()

    def test_required(self):

        self.assertEqual(self.id_project.get_attribute("required"), 'true')
        self.assertEqual(self.id_type.get_attribute("required"), None)
        self.assertEqual(self.id_subcontractor.get_attribute("required"), 'true')
        self.assertEqual(self.id_sub_taxetype.get_attribute("required"), None)
        self.assertEqual(self.id_sub_legalform.get_attribute("required"), None)
        self.assertEqual(self.id_purpose.get_attribute("required"), 'true')
        self.assertEqual(self.id_unit.get_attribute("required"), None)
        self.assertEqual(self.id_account.get_attribute("required"), 'true')
        self.assertEqual(self.id_currency.get_attribute("required"), None)
        self.assertEqual(self.id_date_fact.get_attribute("required"), None)
        self.assertEqual(self.id_date_doc.get_attribute("required"), 'true')
        self.assertEqual(self.id_sum_before_taxes.get_attribute("required"), 'true')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_sum_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_agreement_start_date.get_attribute("required"), 'true')
        self.assertEqual(self.id_agreement_end_date.get_attribute("required"), 'true')
        self.assertEqual(self.id_pay_model.get_attribute("required"), None)
        self.assertEqual(self.id_pay_delay.get_attribute("required"), 'true')
        self.assertEqual(self.id_description.get_attribute("required"), None)
        self.assertEqual(self.id_comment.get_attribute("required"), None)
        self.assertEqual(self.id_estimation_link.get_attribute("required"), None)
        self.assertEqual(self.id_act_date.get_attribute("required"), None)
        self.assertEqual(self.id_status.get_attribute("required"), None)
        self.assertEqual(self.id_doc_request.get_attribute("required"), None)
        self.assertEqual(self.id_hidden.get_attribute("required"), None)
        self.assertEqual(self.id_parent.get_attribute("required"), None)

    def test_type_conditions(self):
        self.id_type_select.select_by_value('True')
        self.id_type_select.select_by_value('False')
        self.id_type_select.select_by_value('True')
        time.sleep(1)

        self.assertEqual(self.id_unit.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_unit_select.all_selected_options, [])
        list = self.options_to_list(self.id_purpose_select.options)
        self.assertTrue('Оплата' in list)
        self.assertTrue('Расход' not in list)

        self.id_type_select.select_by_value('False')
        time.sleep(1)

        self.assertEqual(self.id_unit.get_attribute("disabled"), None)
        list = self.options_to_list(self.id_purpose_select.options)
        self.assertTrue('Оплата' not in list)
        self.assertTrue('Расход' in list)

    def test_subcontractor_conditions(self):

        self.assertEqual(self.id_sum_before_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_sub_taxetype.get_attribute("value"), '')
        self.assertEqual(self.id_sub_legalform.get_attribute("value"), '')

        self.id_subcontractor_select.select_by_visible_text('Подрядчик_1')
        time.sleep(1)
        self.assertEqual(self.id_sum_before_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_sum_after_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_sub_taxetype.get_attribute("value"), 'Налог1')
        self.assertEqual(self.id_sub_legalform.get_attribute("value"), 'ИП')

        self.id_sum_before_taxes.click()
        self.id_sum_before_taxes.send_keys('100')
        self.id_pay_delay.send_keys('')
        self.id_subcontractor_select.select_by_visible_text('---------')
        time.sleep(1)
        self.assertEqual(self.id_sum_before_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_sub_taxetype.get_attribute("value"), '')
        self.assertEqual(self.id_sub_legalform.get_attribute("value"), '')
        self.assertEqual(self.id_sum_before_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_sum_taxes.get_attribute("value"), '0,00')

        self.id_subcontractor_select.select_by_visible_text('Подрядчик_1')
        time.sleep(1)
        self.id_sum_before_taxes.click()
        self.id_sum_before_taxes.send_keys('100')
        self.id_pay_delay.send_keys('')
        time.sleep(1)
        self.assertEqual(self.id_sum_before_taxes.get_attribute("value"), '100,00')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("value"), '100,00')
        self.assertEqual(self.id_sum_taxes.get_attribute("value"), '0,00')
        self.id_subcontractor_select.select_by_visible_text('Подрядчик_2')
        time.sleep(1)
        self.assertEqual(self.id_sum_before_taxes.get_attribute("value"), '100,00')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("value"), '200,00')
        self.assertEqual(self.id_sum_taxes.get_attribute("value"), '100,00')

    def test_account_conditions(self):
        self.id_account_select.select_by_visible_text('Банк')
        time.sleep(1)
        self.assertEqual(self.id_currency.get_attribute("value"), 'Rub')

        self.id_account_select.select_by_visible_text('---------')
        time.sleep(1)
        self.assertEqual(self.id_currency.get_attribute("value"), '')

    def test_sbt_sat_conditions(self):

        self.id_subcontractor_select.select_by_visible_text('Подрядчик_2')
        time.sleep(1)
        self.id_sum_before_taxes.click()
        self.id_sum_before_taxes.send_keys('100')
        self.id_pay_delay.send_keys('')
        time.sleep(1)
        self.assertEqual(self.id_sum_before_taxes.get_attribute("value"), '100,00')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("value"), '200,00')
        self.assertEqual(self.id_sum_taxes.get_attribute("value"), '100,00')
        self.id_sum_after_taxes.click()
        self.id_sum_after_taxes.send_keys('400')
        self.id_pay_delay.send_keys('')
        time.sleep(1)
        self.assertEqual(self.id_sum_before_taxes.get_attribute("value"), '200,00')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("value"), '400,00')
        self.assertEqual(self.id_sum_taxes.get_attribute("value"), '200,00')

    def test_date_conditions(self):
        self.id_pay_delay.clear()
        self.id_pay_delay.click()
        self.id_pay_delay.send_keys('3')
        self.id_comment.send_keys('')
        self.id_agreement_start_date.send_keys('13082018')
        self.id_comment.send_keys('')
        self.id_agreement_end_date.send_keys('20082018')
        self.id_comment.send_keys('')
        self.id_pay_model_select.select_by_visible_text('Постоплата')
        time.sleep(1)
        self.assertEqual(self.id_date_doc.get_attribute("value"), '2018-08-23')

        self.id_pay_model_select.select_by_visible_text('Предоплата')
        time.sleep(1)
        self.assertEqual(self.id_date_doc.get_attribute("value"), '2018-08-10')

        self.id_pay_delay.clear()
        self.id_pay_delay.click()
        self.id_pay_delay.send_keys('5')
        self.id_comment.send_keys('')
        time.sleep(1)
        self.assertEqual(self.id_date_doc.get_attribute("value"), '2018-08-08')

        self.id_agreement_start_date.send_keys('10082018')
        self.id_comment.send_keys('')
        time.sleep(1)
        self.assertEqual(self.id_date_doc.get_attribute("value"), '2018-08-05')

        self.id_pay_model_select.select_by_visible_text('Постоплата')
        self.id_agreement_end_date.send_keys('22082018')
        self.id_comment.send_keys('')
        time.sleep(1)
        self.assertEqual(self.id_date_doc.get_attribute("value"), '2018-08-27')


class TransactionTestJSUpdate(StaticLiveServerTestCase):

    def create_session_cookie(self, user):
        session = SessionStore()
        session[SESSION_KEY] = user.pk
        session[BACKEND_SESSION_KEY] = settings.AUTHENTICATION_BACKENDS[0]
        session[HASH_SESSION_KEY] = user.get_session_auth_hash()
        session.save()
        cookie = {
            'name': settings.SESSION_COOKIE_NAME,
            'value': session.session_key,
            'secure': False,
            'path': '/',
        }
        return cookie

    def loggin(self):
        session_cookie = self.create_session_cookie(app_signup_models.User.objects.get(email='b@b.ru'))
        link = sub_reverse('app_dashboard:index', subdomain='mysubdomain')
        self.selenium.get(link)
        self.selenium.add_cookie(session_cookie)
        self.selenium.refresh()
        self.selenium.get(link)

    def graber_transactoion_modal(self):
        modal = self.selenium.find_element_by_id("modal-global")


        self.id_type_select = Select(modal.find_element_by_id("id_type"))
        self.id_type = modal.find_element_by_id("id_type")
        self.id_subcontractor_select = Select(modal.find_element_by_id("id_subcontractor"))
        self.id_subcontractor = modal.find_element_by_id("id_subcontractor")
        self.id_sub_taxetype = modal.find_element_by_id("id_sub_taxetype")
        self.id_sub_legalform = modal.find_element_by_id("id_sub_legalform")
        self.id_purpose_select = Select(modal.find_element_by_id("id_purpose"))
        self.id_purpose = modal.find_element_by_id("id_purpose")
        self.id_unit_select = Select(modal.find_element_by_id("id_unit"))
        self.id_unit = modal.find_element_by_id("id_unit")
        self.id_account_select = Select(modal.find_element_by_id("id_account"))
        self.id_account = modal.find_element_by_id("id_account")
        self.id_currency = modal.find_element_by_id("id_currency")
        self.id_date_fact = modal.find_element_by_id("id_date_fact")
        self.id_date_doc = modal.find_element_by_id("id_date_doc")
        self.id_sum_before_taxes = modal.find_element_by_id("id_sum_before_taxes")
        self.id_sum_after_taxes = modal.find_element_by_id("id_sum_after_taxes")
        self.id_sum_taxes = modal.find_element_by_id("id_sum_taxes")
        self.id_agreement_start_date = modal.find_element_by_id("id_agreement_start_date")
        self.id_agreement_end_date = modal.find_element_by_id("id_agreement_end_date")
        self.id_pay_model_select = Select(modal.find_element_by_id("id_pay_model"))
        self.id_pay_model = modal.find_element_by_id("id_pay_model")
        self.id_pay_delay = modal.find_element_by_id("id_pay_delay")
        self.id_description = modal.find_element_by_id("id_description")
        self.id_comment = modal.find_element_by_id("id_comment")
        self.id_estimation_link = modal.find_element_by_id("id_estimation_link")
        self.id_act_date = modal.find_element_by_id("id_act_date")
        self.id_status_select = Select(modal.find_element_by_id("id_status"))
        self.id_status = modal.find_element_by_id("id_status")
        self.id_doc_request_select = Select(modal.find_element_by_id("id_doc_request"))
        self.id_doc_request = modal.find_element_by_id("id_doc_request")
        self.id_hidden = modal.find_element_by_id("id_hidden")
        self.id_parent = modal.find_element_by_id("id_parent")

    def options_to_list(self, options):
        list = []
        for el in options:
            list.append(el.get_attribute("innerHTML"))
        return list

    def create_transaction(self):
        self.client.login(username='b@b.ru', password='123')

        # create transaction
        link = sub_reverse('app_transaction:transaction_create', subdomain='mysubdomain')
        data = {
            'project': [str(self.project_1.pk)],
            'type': ['True'],
            'subcontractor': [str(self.sub_2.pk)],
            'purpose': [str(self.puprose_income.pk)],
            'unit': '',
            'account': [str(self.ac3.pk)],
            'date_fact': '',
            'date_doc': ['2017-08-23'],
            'sum_before_taxes': ['100,00'],
            'sum_after_taxes': ['200,00'],
            'agreement_start_date': ['2017-08-13'],
            'agreement_end_date': ['2017-08-20'],
            'pay_model': ['1'],
            'pay_delay': ['3'],
            'description': [''],
            'comment': [''],
            'estimation_link': [''],
            'act_date': [''],
            'status': ['0'],
            'doc_request': ['0'],
            'parent': [''],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_create(request)
            return response

        tread(request)

    def link(self):
        link = sub_reverse('app_transaction:index', subdomain='mysubdomain')
        return link

    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()

        site = Site.objects.get(pk=1)
        # site.domain = 'testserver'
        fdir, fname = os.path.split(self.live_server_url)
        site.domain = fname
        site.save()

        c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
        c.save()
        c.create_database()
        c.fill_database()

        user = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b@b.ru')
        user.set_password('qweqwe111')
        user.created_account_pk = c.pk
        user.save()
        user.account.add(c)

        unit1 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[0]
        userdb = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=user.pk)
        userdb.access_app_project = 2
        userdb.unit = unit1
        userdb.save(using=get_db_name_test())

        self.my_user = user
        self.account = c

        self.tt_1 = app_transaction_models.TaxeType(name='Налог1', multiplicator=1, is_deletable=False)
        self.tt_1.save(using=get_db_name_test())
        self.tt_2 = app_transaction_models.TaxeType(name='Налог2', multiplicator=2, is_deletable=False)
        self.tt_2.save(using=get_db_name_test())
        self.tt_3 = app_transaction_models.TaxeType(name='Налог3', multiplicator=3, is_deletable=False)
        self.tt_3.save(using=get_db_name_test())
        self.tt_4 = app_transaction_models.TaxeType(name='Налог4', multiplicator=4, is_deletable=False)
        self.tt_4.save(using=get_db_name_test())

        self.sub_1 = app_transaction_models.Subcontractor(
            name='Подрядчик_1',
            is_deletable=False,
        )
        self.sub_1.save(using=get_db_name_test())
        self.sub_1.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_1.taxe_type = self.tt_1
        self.sub_1.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_1.is_my_company = False
        self.sub_1.save(using=get_db_name_test())

        self.sub_2 = app_transaction_models.Subcontractor(
            name='Подрядчик_2',
            is_deletable=False,
        )
        self.sub_2.save(using=get_db_name_test())
        self.sub_2.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_2.taxe_type = self.tt_2
        self.sub_2.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_2.is_my_company = False
        self.sub_2.save(using=get_db_name_test())

        self.sub_3 = app_transaction_models.Subcontractor(
            name='Подрядчик_3',
            is_deletable=False,
        )
        self.sub_3.save(using=get_db_name_test())
        self.sub_3.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_3.taxe_type = self.tt_3
        self.sub_3.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_3.is_my_company = False
        self.sub_3.save(using=get_db_name_test())

        self.sub_4 = app_transaction_models.Subcontractor(
            name='Подрядчик_4',
            is_deletable=False,
        )
        self.sub_4.save(using=get_db_name_test())
        self.sub_4.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_4.taxe_type = self.tt_4
        self.sub_4.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_4.is_my_company = False
        self.sub_4.save(using=get_db_name_test())

        self.mycompany_1 = app_transaction_models.Subcontractor(name='Тестовая компания 1',
                                                                is_deletable=False)
        self.mycompany_1.save(using=get_db_name_test())
        self.mycompany_1.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[
            0]
        self.mycompany_1.taxe_type = app_transaction_models.TaxeType.objects.using(get_db_name_test()).get(
            name='НДС')
        self.mycompany_1.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.mycompany_1.is_my_company = True
        self.mycompany_1.save(using=get_db_name_test())

        self.ac1 = app_transaction_models.Account(name='Банк', type=1)
        self.ac1.save(using=get_db_name_test())
        self.ac1.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='Rub')
        self.ac1.company_owner = self.mycompany_1
        self.ac1.save(using=get_db_name_test())

        self.ac2 = app_transaction_models.Account(name='Касса', type=1)
        self.ac2.save(using=get_db_name_test())
        self.ac2.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='Rub')
        self.ac2.company_owner = self.mycompany_1
        self.ac2.save(using=get_db_name_test())

        self.ac3 = app_transaction_models.Account(name='Сейф', type=1)
        self.ac3.save(using=get_db_name_test())
        self.ac3.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='USD')
        self.ac3.company_owner = self.mycompany_1
        self.ac3.save(using=get_db_name_test())

        self.mycompany_2 = app_transaction_models.Subcontractor(name='Тестовая компания 2',
                                                                is_deletable=False)
        self.mycompany_2.save(using=get_db_name_test())
        self.mycompany_2.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[
            0]
        self.mycompany_2.taxe_type = app_transaction_models.TaxeType.objects.using(get_db_name_test()).get(
            name='НДФЛ')
        self.mycompany_2.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.mycompany_2.is_my_company = True
        self.mycompany_2.save(using=get_db_name_test())

        self.ac4 = app_transaction_models.Account(name='Заначка', type=1)
        self.ac4.save(using=get_db_name_test())
        self.ac4.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='USD')
        self.ac4.company_owner = self.mycompany_2
        self.ac4.save(using=get_db_name_test())

        self.project_1 = app_project_models.Project(name='P00001')
        self.project_1.save(using=get_db_name_test())
        self.project_1.category = app_project_models.ProjectCategory.objects.using(get_db_name_test()).get(
            name='Проекты')
        self.project_1.save(using=get_db_name_test())

        self.project_tech = app_project_models.Project.objects.using(get_db_name_test()).get(
            name_short='Внутренние переводы')

        self.puprose_income = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(name='Оплата')
        self.puprose_outcome = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(
            name='Расход')

        chromedriver_path = '/Users/bogdan/PycharmProjects/app/Fins_App/fins/app_transaction/tests/chromedriver'
        self.selenium = webdriver.Chrome(chromedriver_path)
        self.create_transaction()
        self.loggin()
        link = self.link()
        self.selenium.get(link)
        wait = WebDriverWait(self.selenium, 2)
        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, "js-update-transaction")))
        btn_create_transaction = self.selenium.find_element_by_class_name("js-update-transaction")
        btn_create_transaction.click()
        time.sleep(3)
        self.graber_transactoion_modal()

    def tearDown(self):
        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

        self.selenium.quit()

    def test_required(self):

        self.assertEqual(self.id_type.get_attribute("required"), None)
        self.assertEqual(self.id_subcontractor.get_attribute("required"), 'true')
        self.assertEqual(self.id_sub_taxetype.get_attribute("required"), None)
        self.assertEqual(self.id_sub_legalform.get_attribute("required"), None)
        self.assertEqual(self.id_purpose.get_attribute("required"), 'true')
        self.assertEqual(self.id_unit.get_attribute("required"), None)
        self.assertEqual(self.id_account.get_attribute("required"), 'true')
        self.assertEqual(self.id_currency.get_attribute("required"), None)
        self.assertEqual(self.id_date_fact.get_attribute("required"), None)
        self.assertEqual(self.id_date_doc.get_attribute("required"), 'true')
        self.assertEqual(self.id_sum_before_taxes.get_attribute("required"), 'true')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_sum_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_agreement_start_date.get_attribute("required"), 'true')
        self.assertEqual(self.id_agreement_end_date.get_attribute("required"), 'true')
        self.assertEqual(self.id_pay_model.get_attribute("required"), None)
        self.assertEqual(self.id_pay_delay.get_attribute("required"), 'true')
        self.assertEqual(self.id_description.get_attribute("required"), None)
        self.assertEqual(self.id_comment.get_attribute("required"), None)
        self.assertEqual(self.id_estimation_link.get_attribute("required"), None)
        self.assertEqual(self.id_act_date.get_attribute("required"), None)
        self.assertEqual(self.id_status.get_attribute("required"), None)
        self.assertEqual(self.id_doc_request.get_attribute("required"), None)
        self.assertEqual(self.id_hidden.get_attribute("required"), None)
        self.assertEqual(self.id_parent.get_attribute("required"), None)

    def test_type_conditions(self):
        self.assertEqual(self.id_type_select.first_selected_option.text, 'Доход')

        self.id_type_select.select_by_value('True')
        self.id_type_select.select_by_value('False')
        self.id_type_select.select_by_value('True')
        time.sleep(1)

        self.assertEqual(self.id_unit.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_unit_select.all_selected_options, [])
        list = self.options_to_list(self.id_purpose_select.options)
        self.assertTrue('Оплата' in list)
        self.assertTrue('Расход' not in list)

        self.id_type_select.select_by_value('False')
        time.sleep(1)

        self.assertEqual(self.id_unit.get_attribute("disabled"), None)
        list = self.options_to_list(self.id_purpose_select.options)
        self.assertTrue('Оплата' not in list)
        self.assertTrue('Расход' in list)

    def test_subcontractor_conditions(self):

        self.assertEqual(self.id_subcontractor_select.first_selected_option.text, 'Подрядчик_2')

        self.assertEqual(self.id_sum_before_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_sum_after_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_sub_taxetype.get_attribute("value"), 'Налог2')
        self.assertEqual(self.id_sub_legalform.get_attribute("value"), 'ИП')

        self.id_subcontractor_select.select_by_visible_text('---------')
        time.sleep(1)
        self.assertEqual(self.id_sum_before_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_sub_taxetype.get_attribute("value"), '')
        self.assertEqual(self.id_sub_legalform.get_attribute("value"), '')
        self.assertEqual(self.id_sum_before_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_sum_taxes.get_attribute("value"), '0,00')

        self.id_subcontractor_select.select_by_visible_text('Подрядчик_3')
        time.sleep(1)
        self.id_sum_before_taxes.click()
        self.id_sum_before_taxes.send_keys('100')
        self.id_pay_delay.send_keys('')
        time.sleep(1)
        self.assertEqual(self.id_sum_before_taxes.get_attribute("value"), '100,00')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("value"), '300,00')
        self.assertEqual(self.id_sum_taxes.get_attribute("value"), '200,00')
        self.id_subcontractor_select.select_by_visible_text('Подрядчик_4')
        time.sleep(1)
        self.assertEqual(self.id_sum_before_taxes.get_attribute("value"), '100,00')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("value"), '400,00')
        self.assertEqual(self.id_sum_taxes.get_attribute("value"), '300,00')

    def test_account_conditions(self):
        self.assertEqual(self.id_account_select.first_selected_option.text, 'Сейф')
        self.assertEqual(self.id_currency.get_attribute("value"), 'USD')

        self.id_account_select.select_by_visible_text('---------')
        time.sleep(1)
        self.assertEqual(self.id_currency.get_attribute("value"), '')

        self.id_account_select.select_by_visible_text('Банк')
        time.sleep(1)
        self.assertEqual(self.id_currency.get_attribute("value"), 'Rub')

    def test_sbt_sat_conditions(self):

        self.id_sum_before_taxes.click()
        self.id_sum_before_taxes.send_keys('300')
        self.id_pay_delay.send_keys('')
        time.sleep(1)
        self.assertEqual(self.id_sum_before_taxes.get_attribute("value"), '300,00')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("value"), '600,00')
        self.assertEqual(self.id_sum_taxes.get_attribute("value"), '300,00')
        self.id_sum_after_taxes.click()
        self.id_sum_after_taxes.send_keys('800')
        self.id_pay_delay.send_keys('')
        time.sleep(1)
        self.assertEqual(self.id_sum_before_taxes.get_attribute("value"), '400,00')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("value"), '800,00')
        self.assertEqual(self.id_sum_taxes.get_attribute("value"), '400,00')

    def test_date_conditions(self):
        self.assertEqual(self.id_agreement_start_date.get_attribute("value"), '2017-08-13')
        self.assertEqual(self.id_agreement_end_date.get_attribute("value"), '2017-08-20')
        self.assertEqual(self.id_pay_delay.get_attribute("value"), '3')
        self.assertEqual(self.id_pay_model_select.first_selected_option.text, 'Постоплата')
        self.assertEqual(self.id_date_doc.get_attribute("value"), '2017-08-23')

        self.id_pay_model_select.select_by_visible_text('Предоплата')
        time.sleep(1)
        self.assertEqual(self.id_date_doc.get_attribute("value"), '2017-08-10')

        self.id_pay_delay.clear()
        self.id_pay_delay.click()
        self.id_pay_delay.send_keys('5')
        self.id_comment.send_keys('')
        time.sleep(1)
        self.assertEqual(self.id_date_doc.get_attribute("value"), '2017-08-08')

        self.id_agreement_start_date.send_keys('10082017')
        self.id_comment.send_keys('')
        time.sleep(1)
        self.assertEqual(self.id_date_doc.get_attribute("value"), '2017-08-05')

        self.id_pay_model_select.select_by_visible_text('Постоплата')
        self.id_agreement_end_date.send_keys('22082017')
        self.id_comment.send_keys('')
        time.sleep(1)
        self.assertEqual(self.id_date_doc.get_attribute("value"), '2017-08-27')


class TransactionTestJSCreateFromProject(TransactionTestJSCreate):
    def link(self):
        link = sub_reverse('app_project:project', kwargs={'pk': self.project_1.pk}, subdomain='mysubdomain')
        return link

    def graber_transactoion_modal(self):
        modal = self.selenium.find_element_by_id("modal-global")

        self.id_type_select = Select(modal.find_element_by_id("id_type"))
        self.id_type = modal.find_element_by_id("id_type")
        self.id_subcontractor_select = Select(modal.find_element_by_id("id_subcontractor"))
        self.id_subcontractor = modal.find_element_by_id("id_subcontractor")
        self.id_sub_taxetype = modal.find_element_by_id("id_sub_taxetype")
        self.id_sub_legalform = modal.find_element_by_id("id_sub_legalform")
        self.id_purpose_select = Select(modal.find_element_by_id("id_purpose"))
        self.id_purpose = modal.find_element_by_id("id_purpose")
        self.id_unit_select = Select(modal.find_element_by_id("id_unit"))
        self.id_unit = modal.find_element_by_id("id_unit")
        self.id_account_select = Select(modal.find_element_by_id("id_account"))
        self.id_account = modal.find_element_by_id("id_account")
        self.id_currency = modal.find_element_by_id("id_currency")
        self.id_date_fact = modal.find_element_by_id("id_date_fact")
        self.id_date_doc = modal.find_element_by_id("id_date_doc")
        self.id_sum_before_taxes = modal.find_element_by_id("id_sum_before_taxes")
        self.id_sum_after_taxes = modal.find_element_by_id("id_sum_after_taxes")
        self.id_sum_taxes = modal.find_element_by_id("id_sum_taxes")
        self.id_agreement_start_date = modal.find_element_by_id("id_agreement_start_date")
        self.id_agreement_end_date = modal.find_element_by_id("id_agreement_end_date")
        self.id_pay_model_select = Select(modal.find_element_by_id("id_pay_model"))
        self.id_pay_model = modal.find_element_by_id("id_pay_model")
        self.id_pay_delay = modal.find_element_by_id("id_pay_delay")
        self.id_description = modal.find_element_by_id("id_description")
        self.id_comment = modal.find_element_by_id("id_comment")
        self.id_estimation_link = modal.find_element_by_id("id_estimation_link")
        self.id_act_date = modal.find_element_by_id("id_act_date")
        self.id_status_select = Select(modal.find_element_by_id("id_status"))
        self.id_status = modal.find_element_by_id("id_status")
        self.id_doc_request_select = Select(modal.find_element_by_id("id_doc_request"))
        self.id_doc_request = modal.find_element_by_id("id_doc_request")
        self.id_hidden = modal.find_element_by_id("id_hidden")
        self.id_parent = modal.find_element_by_id("id_parent")

    def test_required(self):
        self.assertEqual(self.id_type.get_attribute("required"), None)
        self.assertEqual(self.id_subcontractor.get_attribute("required"), 'true')
        self.assertEqual(self.id_sub_taxetype.get_attribute("required"), None)
        self.assertEqual(self.id_sub_legalform.get_attribute("required"), None)
        self.assertEqual(self.id_purpose.get_attribute("required"), 'true')
        self.assertEqual(self.id_unit.get_attribute("required"), None)
        self.assertEqual(self.id_account.get_attribute("required"), 'true')
        self.assertEqual(self.id_currency.get_attribute("required"), None)
        self.assertEqual(self.id_date_fact.get_attribute("required"), None)
        self.assertEqual(self.id_date_doc.get_attribute("required"), 'true')
        self.assertEqual(self.id_sum_before_taxes.get_attribute("required"), 'true')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_sum_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_agreement_start_date.get_attribute("required"), 'true')
        self.assertEqual(self.id_agreement_end_date.get_attribute("required"), 'true')
        self.assertEqual(self.id_pay_model.get_attribute("required"), None)
        self.assertEqual(self.id_pay_delay.get_attribute("required"), 'true')
        self.assertEqual(self.id_description.get_attribute("required"), None)
        self.assertEqual(self.id_comment.get_attribute("required"), None)
        self.assertEqual(self.id_estimation_link.get_attribute("required"), None)
        self.assertEqual(self.id_act_date.get_attribute("required"), None)
        self.assertEqual(self.id_status.get_attribute("required"), None)
        self.assertEqual(self.id_doc_request.get_attribute("required"), None)
        self.assertEqual(self.id_hidden.get_attribute("required"), None)
        self.assertEqual(self.id_parent.get_attribute("required"), None)


class TransactionTestJSUpdateFromProject(TransactionTestJSUpdate):
    def link(self):
        link = sub_reverse('app_project:project', kwargs={'pk': self.project_1.pk}, subdomain='mysubdomain')
        return link

    def graber_transactoion_modal(self):
        modal = self.selenium.find_element_by_id("modal-global")

        self.id_type_select = Select(modal.find_element_by_id("id_type"))
        self.id_type = modal.find_element_by_id("id_type")
        self.id_subcontractor_select = Select(modal.find_element_by_id("id_subcontractor"))
        self.id_subcontractor = modal.find_element_by_id("id_subcontractor")
        self.id_sub_taxetype = modal.find_element_by_id("id_sub_taxetype")
        self.id_sub_legalform = modal.find_element_by_id("id_sub_legalform")
        self.id_purpose_select = Select(modal.find_element_by_id("id_purpose"))
        self.id_purpose = modal.find_element_by_id("id_purpose")
        self.id_unit_select = Select(modal.find_element_by_id("id_unit"))
        self.id_unit = modal.find_element_by_id("id_unit")
        self.id_account_select = Select(modal.find_element_by_id("id_account"))
        self.id_account = modal.find_element_by_id("id_account")
        self.id_currency = modal.find_element_by_id("id_currency")
        self.id_date_fact = modal.find_element_by_id("id_date_fact")
        self.id_date_doc = modal.find_element_by_id("id_date_doc")
        self.id_sum_before_taxes = modal.find_element_by_id("id_sum_before_taxes")
        self.id_sum_after_taxes = modal.find_element_by_id("id_sum_after_taxes")
        self.id_sum_taxes = modal.find_element_by_id("id_sum_taxes")
        self.id_agreement_start_date = modal.find_element_by_id("id_agreement_start_date")
        self.id_agreement_end_date = modal.find_element_by_id("id_agreement_end_date")
        self.id_pay_model_select = Select(modal.find_element_by_id("id_pay_model"))
        self.id_pay_model = modal.find_element_by_id("id_pay_model")
        self.id_pay_delay = modal.find_element_by_id("id_pay_delay")
        self.id_description = modal.find_element_by_id("id_description")
        self.id_comment = modal.find_element_by_id("id_comment")
        self.id_estimation_link = modal.find_element_by_id("id_estimation_link")
        self.id_act_date = modal.find_element_by_id("id_act_date")
        self.id_status_select = Select(modal.find_element_by_id("id_status"))
        self.id_status = modal.find_element_by_id("id_status")
        self.id_doc_request_select = Select(modal.find_element_by_id("id_doc_request"))
        self.id_doc_request = modal.find_element_by_id("id_doc_request")
        self.id_hidden = modal.find_element_by_id("id_hidden")
        self.id_parent = modal.find_element_by_id("id_parent")

    def test_required(self):
        self.assertEqual(self.id_type.get_attribute("required"), None)
        self.assertEqual(self.id_subcontractor.get_attribute("required"), 'true')
        self.assertEqual(self.id_sub_taxetype.get_attribute("required"), None)
        self.assertEqual(self.id_sub_legalform.get_attribute("required"), None)
        self.assertEqual(self.id_purpose.get_attribute("required"), 'true')
        self.assertEqual(self.id_unit.get_attribute("required"), None)
        self.assertEqual(self.id_account.get_attribute("required"), 'true')
        self.assertEqual(self.id_currency.get_attribute("required"), None)
        self.assertEqual(self.id_date_fact.get_attribute("required"), None)
        self.assertEqual(self.id_date_doc.get_attribute("required"), 'true')
        self.assertEqual(self.id_sum_before_taxes.get_attribute("required"), 'true')
        self.assertEqual(self.id_sum_after_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_sum_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_agreement_start_date.get_attribute("required"), 'true')
        self.assertEqual(self.id_agreement_end_date.get_attribute("required"), 'true')
        self.assertEqual(self.id_pay_model.get_attribute("required"), None)
        self.assertEqual(self.id_pay_delay.get_attribute("required"), 'true')
        self.assertEqual(self.id_description.get_attribute("required"), None)
        self.assertEqual(self.id_comment.get_attribute("required"), None)
        self.assertEqual(self.id_estimation_link.get_attribute("required"), None)
        self.assertEqual(self.id_act_date.get_attribute("required"), None)
        self.assertEqual(self.id_status.get_attribute("required"), None)
        self.assertEqual(self.id_doc_request.get_attribute("required"), None)
        self.assertEqual(self.id_hidden.get_attribute("required"), None)
        self.assertEqual(self.id_parent.get_attribute("required"), None)


class TransactionTestList(StaticLiveServerTestCase):

    def create_session_cookie(self, user):
        session = SessionStore()
        session[SESSION_KEY] = user.pk
        session[BACKEND_SESSION_KEY] = settings.AUTHENTICATION_BACKENDS[0]
        session[HASH_SESSION_KEY] = user.get_session_auth_hash()
        session.save()
        cookie = {
            'name': settings.SESSION_COOKIE_NAME,
            'value': session.session_key,
            'secure': False,
            'path': '/',
        }
        return cookie

    def loggin(self):
        session_cookie = self.create_session_cookie(app_signup_models.User.objects.get(email='b@b.ru'))
        link = sub_reverse('app_dashboard:index', subdomain='mysubdomain')
        self.selenium.get(link)
        self.selenium.add_cookie(session_cookie)
        self.selenium.refresh()
        self.selenium.get(link)

    def graber_transactoion_modal(self):
        modal = self.selenium.find_element_by_id("modal-global")

        self.id_project_select = Select(modal.find_element_by_id("id_project"))
        self.id_project = modal.find_element_by_id("id_project")
        self.id_type_select = Select(modal.find_element_by_id("id_type"))
        self.id_type = modal.find_element_by_id("id_type")
        self.id_subcontractor_select = Select(modal.find_element_by_id("id_subcontractor"))
        self.id_subcontractor = modal.find_element_by_id("id_subcontractor")
        self.id_sub_taxetype = modal.find_element_by_id("id_sub_taxetype")
        self.id_sub_legalform = modal.find_element_by_id("id_sub_legalform")
        self.id_purpose_select = Select(modal.find_element_by_id("id_purpose"))
        self.id_purpose = modal.find_element_by_id("id_purpose")
        self.id_unit_select = Select(modal.find_element_by_id("id_unit"))
        self.id_unit = modal.find_element_by_id("id_unit")
        self.id_account_select = Select(modal.find_element_by_id("id_account"))
        self.id_account = modal.find_element_by_id("id_account")
        self.id_currency = modal.find_element_by_id("id_currency")
        self.id_date_fact = modal.find_element_by_id("id_date_fact")
        self.id_date_doc = modal.find_element_by_id("id_date_doc")
        self.id_sum_before_taxes = modal.find_element_by_id("id_sum_before_taxes")
        self.id_sum_after_taxes = modal.find_element_by_id("id_sum_after_taxes")
        self.id_sum_taxes = modal.find_element_by_id("id_sum_taxes")
        self.id_agreement_start_date = modal.find_element_by_id("id_agreement_start_date")
        self.id_agreement_end_date = modal.find_element_by_id("id_agreement_end_date")
        self.id_pay_model_select = Select(modal.find_element_by_id("id_pay_model"))
        self.id_pay_model = modal.find_element_by_id("id_pay_model")
        self.id_pay_delay = modal.find_element_by_id("id_pay_delay")
        self.id_description = modal.find_element_by_id("id_description")
        self.id_comment = modal.find_element_by_id("id_comment")
        self.id_estimation_link = modal.find_element_by_id("id_estimation_link")
        self.id_act_date = modal.find_element_by_id("id_act_date")
        self.id_status_select = Select(modal.find_element_by_id("id_status"))
        self.id_status = modal.find_element_by_id("id_status")
        self.id_doc_request_select = Select(modal.find_element_by_id("id_doc_request"))
        self.id_doc_request = modal.find_element_by_id("id_doc_request")
        self.id_hidden = modal.find_element_by_id("id_hidden")
        self.id_parent = modal.find_element_by_id("id_parent")

    def options_to_list(self, options):
        list = []
        for el in options:
            list.append(el.get_attribute("innerHTML"))
        return list

    def create_transaction(self):
        self.client.login(username='b@b.ru', password='123')

        # create transaction
        link = sub_reverse('app_transaction:transaction_create', subdomain='mysubdomain')
        data = {
            'project': [str(self.project_1.pk)],
            'type': ['True'],
            'subcontractor': [str(self.sub_2.pk)],
            'purpose': [str(self.puprose_income.pk)],
            'unit': '',
            'account': [str(self.ac3.pk)],
            'date_fact': '',
            'date_doc': ['2017-08-23'],
            'sum_before_taxes': ['100,00'],
            'sum_after_taxes': ['200,00'],
            'agreement_start_date': ['2017-08-13'],
            'agreement_end_date': ['2017-08-20'],
            'pay_model': ['1'],
            'pay_delay': ['3'],
            'description': [''],
            'comment': [''],
            'estimation_link': [''],
            'act_date': [''],
            'status': ['0'],
            'doc_request': ['0'],
            'parent': [''],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transaction_create(request)
            return response

        tread(request)

    def link(self):
        link = sub_reverse('app_transaction:index', subdomain='mysubdomain')
        return link

    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()

        site = Site.objects.get(pk=1)
        # site.domain = 'testserver'
        fdir, fname = os.path.split(self.live_server_url)
        site.domain = fname
        site.save()

        c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
        c.save()
        c.create_database()
        c.fill_database()

        user = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b@b.ru')
        user.set_password('qweqwe111')
        user.created_account_pk = c.pk
        user.save()
        user.account.add(c)

        unit1 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[0]
        userdb = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=user.pk)
        userdb.access_app_project = 2
        userdb.unit = unit1
        userdb.save(using=get_db_name_test())

        self.my_user = user
        self.account = c

        self.tt_1 = app_transaction_models.TaxeType(name='Налог1', multiplicator=1, is_deletable=False)
        self.tt_1.save(using=get_db_name_test())
        self.tt_2 = app_transaction_models.TaxeType(name='Налог2', multiplicator=2, is_deletable=False)
        self.tt_2.save(using=get_db_name_test())
        self.tt_3 = app_transaction_models.TaxeType(name='Налог3', multiplicator=3, is_deletable=False)
        self.tt_3.save(using=get_db_name_test())
        self.tt_4 = app_transaction_models.TaxeType(name='Налог4', multiplicator=4, is_deletable=False)
        self.tt_4.save(using=get_db_name_test())

        self.sub_1 = app_transaction_models.Subcontractor(
            name='Подрядчик_1',
            is_deletable=False,
        )
        self.sub_1.save(using=get_db_name_test())
        self.sub_1.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_1.taxe_type = self.tt_1
        self.sub_1.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_1.is_my_company = False
        self.sub_1.save(using=get_db_name_test())

        self.sub_2 = app_transaction_models.Subcontractor(
            name='Подрядчик_2',
            is_deletable=False,
        )
        self.sub_2.save(using=get_db_name_test())
        self.sub_2.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_2.taxe_type = self.tt_2
        self.sub_2.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_2.is_my_company = False
        self.sub_2.save(using=get_db_name_test())

        self.sub_3 = app_transaction_models.Subcontractor(
            name='Подрядчик_3',
            is_deletable=False,
        )
        self.sub_3.save(using=get_db_name_test())
        self.sub_3.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_3.taxe_type = self.tt_3
        self.sub_3.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_3.is_my_company = False
        self.sub_3.save(using=get_db_name_test())

        self.sub_4 = app_transaction_models.Subcontractor(
            name='Подрядчик_4',
            is_deletable=False,
        )
        self.sub_4.save(using=get_db_name_test())
        self.sub_4.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_4.taxe_type = self.tt_4
        self.sub_4.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_4.is_my_company = False
        self.sub_4.save(using=get_db_name_test())

        self.mycompany_1 = app_transaction_models.Subcontractor(name='Тестовая компания 1',
                                                                is_deletable=False)
        self.mycompany_1.save(using=get_db_name_test())
        self.mycompany_1.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[
            0]
        self.mycompany_1.taxe_type = app_transaction_models.TaxeType.objects.using(get_db_name_test()).get(
            name='НДС')
        self.mycompany_1.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.mycompany_1.is_my_company = True
        self.mycompany_1.save(using=get_db_name_test())

        self.ac1 = app_transaction_models.Account(name='Банк', type=1)
        self.ac1.save(using=get_db_name_test())
        self.ac1.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='Rub')
        self.ac1.company_owner = self.mycompany_1
        self.ac1.save(using=get_db_name_test())

        self.ac2 = app_transaction_models.Account(name='Касса', type=1)
        self.ac2.save(using=get_db_name_test())
        self.ac2.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='Rub')
        self.ac2.company_owner = self.mycompany_1
        self.ac2.save(using=get_db_name_test())

        self.ac3 = app_transaction_models.Account(name='Сейф', type=1)
        self.ac3.save(using=get_db_name_test())
        self.ac3.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='USD')
        self.ac3.company_owner = self.mycompany_1
        self.ac3.save(using=get_db_name_test())

        self.mycompany_2 = app_transaction_models.Subcontractor(name='Тестовая компания 2',
                                                                is_deletable=False)
        self.mycompany_2.save(using=get_db_name_test())
        self.mycompany_2.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[
            0]
        self.mycompany_2.taxe_type = app_transaction_models.TaxeType.objects.using(get_db_name_test()).get(
            name='НДФЛ')
        self.mycompany_2.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.mycompany_2.is_my_company = True
        self.mycompany_2.save(using=get_db_name_test())

        self.ac4 = app_transaction_models.Account(name='Заначка', type=1)
        self.ac4.save(using=get_db_name_test())
        self.ac4.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='USD')
        self.ac4.company_owner = self.mycompany_2
        self.ac4.save(using=get_db_name_test())

        self.project_1 = app_project_models.Project(name='P00001')
        self.project_1.save(using=get_db_name_test())
        self.project_1.category = app_project_models.ProjectCategory.objects.using(get_db_name_test()).get(
            name='Проекты')
        self.project_1.save(using=get_db_name_test())

        self.project_tech = app_project_models.Project.objects.using(get_db_name_test()).get(
            name_short='Внутренние переводы')

        self.puprose_income = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(name='Оплата')
        self.puprose_outcome = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(
            name='Расход')

        chromedriver_path = '/Users/bogdan/PycharmProjects/app/Fins_App/fins/app_transaction/tests/chromedriver'
        self.selenium = webdriver.Chrome(chromedriver_path)
        self.create_transaction()
        self.create_transaction()
        self.create_transaction()
        self.loggin()
        link = self.link()
        self.selenium.get(link)
        wait = WebDriverWait(self.selenium, 2)
        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, "js-update-transaction")))

    def tearDown(self):
        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

        self.selenium.quit()

    def test_list(self):
        table = self.selenium.find_element_by_tag_name("tbody")
        table_rows = table.find_elements_by_tag_name("tr")

        row_number = 0
        pay_status = Select(
            table_rows[row_number].find_element_by_id("id_form-" + str(row_number) + "-status"))
        date_fact = table_rows[row_number].find_element_by_id(
            "id_form-" + str(row_number) + "-date_fact")
        name = table_rows[row_number].find_element_by_class_name(
            "name")


        # getting transaction number
        i = 0
        num = ''
        for item in str(name.get_attribute("innerHTML")):
            if item == " " or item == "\n":
                pass
            else:
                if i == 0:
                    num = item
                else:
                    num = num + item
                i += 1
        transaction = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name=num)
        self.assertEqual(transaction.date_fact, None)
        self.assertEqual(transaction.status, 0)

        transaction2 = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000002')
        transaction3 = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000003')


        pay_status.select_by_value("2")
        time.sleep(1)
        transaction = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name=num)
        self.assertEqual(transaction.status, 2)

        self.selenium.execute_script("window.scrollTo(600, 600)")
        date_fact.send_keys("13082018")
        time.sleep(1)
        transaction = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name=num)
        self.assertEqual(transaction.date_fact, date(2018, 8, 13))


        #check that others are untouched
        transaction2_after = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000002')
        transaction3_after = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000003')
        self.assertEqual(transaction2.date_fact, transaction2_after.date_fact)
        self.assertEqual(transaction2.status, transaction2_after.status)
        self.assertEqual(transaction3.date_fact, transaction3_after.date_fact)
        self.assertEqual(transaction3.status, transaction3_after.status)