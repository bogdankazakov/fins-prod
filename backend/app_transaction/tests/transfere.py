from django.test import TestCase, Client, RequestFactory
from app_signup import models as app_signup_models
from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
import psycopg2
from fins.threadlocal import thread_local
from app_transaction import views as app_transaction_views
from app_project import models as app_project_models
from selenium.webdriver.support.ui import Select
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import os
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.conf import settings
from django.contrib.auth import (
    SESSION_KEY, BACKEND_SESSION_KEY, HASH_SESSION_KEY,
    get_user_model
)
from django.contrib.sessions.backends.db import SessionStore
from fins.global_functions import get_db_name_test
from app_transaction.converter import *
from app_setup import models as app_setup_models



class TransferTest_CRUD(TestCase):

        def setUp(self):
            self.client = Client()
            self.factory = RequestFactory()

            site = Site.objects.get(pk=1)
            site.domain = 'testserver'
            site.save()

            c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
            c.save()
            c.create_database()
            c.fill_database()

            user = app_signup_models.User(
                first_name='UserFirst',
                last_name='UserLast',
                email='b@b.ru')
            user.set_password('123')
            user.created_account_pk = c.pk
            user.save()
            user.account.add(c)

            unit1 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[0]
            userdb = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=user.pk)
            userdb.access_app_project = 2
            userdb.access_can_block = 0
            userdb.unit = unit1
            userdb.save(using=get_db_name_test())

            self.my_user = user
            self.account = c

            self.tt_1 = app_transaction_models.TaxeType(name='Налог1', multiplicator=1, is_deletable=False)
            self.tt_1.save(using=get_db_name_test())
            self.tt_2 = app_transaction_models.TaxeType(name='Налог2', multiplicator=2, is_deletable=False)
            self.tt_2.save(using=get_db_name_test())
            self.tt_3 = app_transaction_models.TaxeType(name='Налог3', multiplicator=3, is_deletable=False)
            self.tt_3.save(using=get_db_name_test())
            self.tt_4 = app_transaction_models.TaxeType(name='Налог4', multiplicator=4, is_deletable=False)
            self.tt_4.save(using=get_db_name_test())

            self.sub_1 = app_transaction_models.Subcontractor(
                name='Подрядчик_1',
                is_deletable=False,
            )
            self.sub_1.save(using=get_db_name_test())
            self.sub_1.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
            self.sub_1.taxe_type = self.tt_1
            self.sub_1.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
            self.sub_1.is_my_company = False
            self.sub_1.save(using=get_db_name_test())


            self.sub_2 = app_transaction_models.Subcontractor(
                name='Подрядчик_2',
                is_deletable=False,
            )
            self.sub_2.save(using=get_db_name_test())
            self.sub_2.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
            self.sub_2.taxe_type = self.tt_2
            self.sub_2.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
            self.sub_2.is_my_company = False
            self.sub_2.save(using=get_db_name_test())


            self.sub_3 = app_transaction_models.Subcontractor(
                name='Подрядчик_3',
                is_deletable=False,
            )
            self.sub_3.save(using=get_db_name_test())
            self.sub_3.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
            self.sub_3.taxe_type = self.tt_3
            self.sub_3.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
            self.sub_3.is_my_company = False
            self.sub_3.save(using=get_db_name_test())

            self.sub_4 = app_transaction_models.Subcontractor(
                name='Подрядчик_4',
                is_deletable=False,
            )
            self.sub_4.save(using=get_db_name_test())
            self.sub_4.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
            self.sub_4.taxe_type = self.tt_4
            self.sub_4.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
            self.sub_4.is_my_company = False
            self.sub_4.save(using=get_db_name_test())


            self.mycompany_1 = app_transaction_models.Subcontractor(name='Тестовая компания 1',is_deletable=False)
            self.mycompany_1.save(using=get_db_name_test())
            self.mycompany_1.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
            self.mycompany_1.taxe_type = app_transaction_models.TaxeType.objects.using(get_db_name_test()).get(name='НДС')
            self.mycompany_1.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
            self.mycompany_1.is_my_company = True
            self.mycompany_1.save(using=get_db_name_test())

            self.ac1 = app_transaction_models.Account(name='Банк',type=1)
            self.ac1.save(using=get_db_name_test())
            self.ac1.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='Rub')
            self.ac1.company_owner = self.mycompany_1
            self.ac1.save(using=get_db_name_test())

            self.ac2 = app_transaction_models.Account(name='Касса',type=1)
            self.ac2.save(using=get_db_name_test())
            self.ac2.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='Rub')
            self.ac2.company_owner = self.mycompany_1
            self.ac2.save(using=get_db_name_test())

            self.ac3 = app_transaction_models.Account(name='Сейф',type=1)
            self.ac3.save(using=get_db_name_test())
            self.ac3.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='USD')
            self.ac3.company_owner = self.mycompany_1
            self.ac3.save(using=get_db_name_test())

            self.mycompany_2 = app_transaction_models.Subcontractor(name='Тестовая компания 2',is_deletable=False)
            self.mycompany_2.save(using=get_db_name_test())
            self.mycompany_2.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
            self.mycompany_2.taxe_type = app_transaction_models.TaxeType.objects.using(get_db_name_test()).get(name='НДФЛ')
            self.mycompany_2.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
            self.mycompany_2.is_my_company = True
            self.mycompany_2.save(using=get_db_name_test())

            self.ac4 = app_transaction_models.Account(name='Заначка',type=1)
            self.ac4.save(using=get_db_name_test())
            self.ac4.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='USD')
            self.ac4.company_owner = self.mycompany_2
            self.ac4.save(using=get_db_name_test())

            self.project_1 = app_project_models.Project(name='P00001')
            self.project_1.save(using=get_db_name_test())
            self.project_1.category = app_project_models.ProjectCategory.objects.using(get_db_name_test()).get(name='Проекты')
            self.project_1.save(using=get_db_name_test())

            self.project_tech = app_project_models.Project.objects.using(get_db_name_test()).get(name_short='Внутренние переводы')

            self.puprose_income = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(name='Оплата')
            self.puprose_outcome = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(name='Расход')

        def tearDown(self):
            #   we delete created account db
            connection_params = {
                'database': settings.MAIN_DB['NAME'],
                'host': settings.MAIN_DB['HOST'],
                'user': settings.MAIN_DB['USER'],
                'password': settings.MAIN_DB['PASSWORD']
            }
            connection = psycopg2.connect(**connection_params)
            connection.set_isolation_level(
                psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
            )
            cursor = connection.cursor()

            for obj in app_signup_models.Account.objects.all():
                name = 'account_' + str(obj.pk)

                cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                               " FROM pg_stat_activity"
                               " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
                cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
            connection.close()

        def test_transaction_CRUD(self):
            print('sub_1', self.sub_1, self.sub_1.taxe_type)
            print('sub_2', self.sub_2, self.sub_2.taxe_type)
            print('sub_3', self.sub_3, self.sub_3.taxe_type)
            print('sub_4', self.sub_4, self.sub_4.taxe_type)
            print('mycompany_1', self.mycompany_1.taxe_type)
            print('ac1', self.ac1, self.ac1.currency, self.ac1.company_owner)
            print('ac2', self.ac2, self.ac2.currency, self.ac2.company_owner)
            print('ac3', self.ac3, self.ac3.currency, self.ac3.company_owner)
            print('mycompany_2', self.mycompany_2.taxe_type)
            print('ac4', self.ac4, self.ac4.currency, self.ac4.company_owner)
            print('project_1', self.project_1)
            print('project_tech', self.project_tech)
            print('puprose_income', self.puprose_income)
            print('puprose_outcome', self.puprose_outcome)

            self.client.login(username='b@b.ru', password='123')

            # create transaction
            link = sub_reverse('app_transaction:transfer_create', subdomain='mysubdomain')
            data = {
                'companies': [self.mycompany_1.pk],
                'hidden': ['False'],
                'is_editable': ['True'],
                'form-TOTAL_FORMS': ['2'],
                'form-INITIAL_FORMS': ['0'],
                'form-MIN_NUM_FORMS': ['0'],
                'form-MAX_NUM_FORMS': ['1000'],
                'form-0-account': [self.ac1.pk],
                'form-0-date_fact': ['2019-03-01'],
                'form-0-sum_before_taxes': ['1 000,00'],
                'form-0-sum_after_taxes': ['1 180,00'],
                'form-0-comment': [''],
                'form-0-status': ['0'],
                'form-0-id': [''],
                'form-1-account': [self.ac2.pk],
                'form-1-date_fact': ['2019-03-08'],
                'form-1-sum_before_taxes': ['500,00'],
                'form-1-sum_after_taxes': ['590,00'],
                'form-1-comment': [''],
                'form-1-status': ['0'],
                'form-1-id': [''],
            }
            request = self.factory.post(link, data)
            request.user = self.my_user
            request.account = self.account

            @thread_local(using_db=get_db_name_test())
            def tread(request):
                response = app_transaction_views.transfer_create(request)
                return response

            response = tread(request)

            created_1 = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000001')
            created_2 = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000002')
            self.assertEqual(response.status_code, 200)
            self.assertTrue(created_1)
            self.assertTrue(created_2)
            self.assertEqual(created_1.is_transfer, True)
            self.assertEqual(created_1.transfer, created_2.pk)
            self.assertEqual(created_2.is_transfer, True)
            self.assertEqual(created_2.transfer, created_1.pk)

            # update
            kwargs = {'pk': created_1.pk}
            link = sub_reverse('app_transaction:transfer_update', kwargs=kwargs, subdomain='mysubdomain')
            data = {
                'companies': [self.mycompany_1.pk],
                'hidden': ['False'],
                'is_editable': ['True'],
                'form-TOTAL_FORMS': ['2'],
                'form-INITIAL_FORMS': ['2'],
                'form-MIN_NUM_FORMS': ['0'],
                'form-MAX_NUM_FORMS': ['1000'],
                'form-0-account': [self.ac1.pk],
                'form-0-date_fact': ['2019-04-01'],
                'form-0-sum_before_taxes': ['1 000,00'],
                'form-0-sum_after_taxes': ['1 180,00'],
                'form-0-comment': [''],
                'form-0-status': ['0'],
                'form-0-id': [created_1.pk],
                'form-1-account': [self.ac2.pk],
                'form-1-date_fact': ['2019-04-08'],
                'form-1-sum_before_taxes': ['500,00'],
                'form-1-sum_after_taxes': ['590,00'],
                'form-1-comment': [''],
                'form-1-status': ['0'],
                'form-1-id': [created_2.pk],
            }
            request = self.factory.post(link, data)
            request.user = self.my_user
            request.account = self.account

            @thread_local(using_db=get_db_name_test())
            def tread(request):
                response = app_transaction_views.transfer_update(request, **kwargs)
                return response

            response = tread(request)

            updated_1 = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000001')
            updated_2 = app_transaction_models.Transaction.objects.using(get_db_name_test()).get(name='T000002')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(str(updated_1.date_fact), '2019-04-01')
            self.assertEqual(str(updated_2.date_fact), '2019-04-08')

            # delete
            kwargs = {'pk': updated_1.pk}
            link = sub_reverse('app_transaction:transfer_delete', kwargs=kwargs, subdomain='mysubdomain')
            data = {}
            request = self.factory.post(link, data)
            request.user = self.my_user
            request.account = self.account

            @thread_local(using_db=get_db_name_test())
            def tread(request):
                response = app_transaction_views.transfer_delete(request, **kwargs)
                return response

            response = tread(request)

            deleted_1 = app_transaction_models.Transaction.objects.using(get_db_name_test()).filter(name='T000001')
            deleted_2 = app_transaction_models.Transaction.objects.using(get_db_name_test()).filter(name='T000002')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(len(deleted_1), 0)
            self.assertEqual(len(deleted_2), 0)

class TransferTestJSCreate(StaticLiveServerTestCase):

    def create_session_cookie(self, user):
        session = SessionStore()
        session[SESSION_KEY] = user.pk
        session[BACKEND_SESSION_KEY] = settings.AUTHENTICATION_BACKENDS[0]
        session[HASH_SESSION_KEY] = user.get_session_auth_hash()
        session.save()
        cookie = {
            'name': settings.SESSION_COOKIE_NAME,
            'value': session.session_key,
            'secure': False,
            'path': '/',
        }
        return cookie

    def loggin(self):
        session_cookie = self.create_session_cookie(app_signup_models.User.objects.get(email='b@b.ru'))
        link = sub_reverse('app_dashboard:index', subdomain='mysubdomain')
        self.selenium.get(link)
        self.selenium.add_cookie(session_cookie)
        self.selenium.refresh()
        self.selenium.get(link)

    def graber_transfer_modal(self):
        self.id_companies_select = Select(self.selenium.find_element_by_id("id_companies"))
        self.id_companies = self.selenium.find_element_by_id("id_companies")

        self.id_form_0_account_select = Select(self.selenium.find_element_by_id("id_form-0-account"))
        self.id_form_0_account = self.selenium.find_element_by_id("id_form-0-account")
        self.id_form_0_currency = self.selenium.find_element_by_id("id_form-0-currency")
        self.id_form_0_date_fact = self.selenium.find_element_by_id("id_form-0-date_fact")
        self.id_form_0_sum_before_taxes = self.selenium.find_element_by_id("id_form-0-sum_before_taxes")
        self.id_form_0_sum_after_taxes = self.selenium.find_element_by_id("id_form-0-sum_after_taxes")
        self.id_form_0_sum_taxes = self.selenium.find_element_by_id("id_form-0-sum_taxes")
        self.id_form_0_comment = self.selenium.find_element_by_id("id_form-0-comment")
        self.id_form_0_status = self.selenium.find_element_by_id("id_form-0-status")
        self.id_form_0_subcontractor_select = Select(self.selenium.find_element_by_id("id_form-0-subcontractor"))
        self.id_form_0_subcontractor = self.selenium.find_element_by_id("id_form-0-subcontractor")
        self.id_form_0_sub_taxetype = self.selenium.find_element_by_id("id_form-0-sub_taxetype")
        self.id_form_0_sub_legalform = self.selenium.find_element_by_id("id_form-0-sub_legalform")

        self.id_form_1_account_select = Select(self.selenium.find_element_by_id("id_form-1-account"))
        self.id_form_1_account = self.selenium.find_element_by_id("id_form-1-account")
        self.id_form_1_currency = self.selenium.find_element_by_id("id_form-1-currency")
        self.id_form_1_date_fact = self.selenium.find_element_by_id("id_form-1-date_fact")
        self.id_form_1_sum_before_taxes = self.selenium.find_element_by_id("id_form-1-sum_before_taxes")
        self.id_form_1_sum_after_taxes = self.selenium.find_element_by_id("id_form-1-sum_after_taxes")
        self.id_form_1_sum_taxes = self.selenium.find_element_by_id("id_form-1-sum_taxes")
        self.id_form_1_comment = self.selenium.find_element_by_id("id_form-1-comment")
        self.id_form_1_status = self.selenium.find_element_by_id("id_form-1-status")
        self.id_form_1_subcontractor_select = Select(self.selenium.find_element_by_id("id_form-1-subcontractor"))
        self.id_form_1_subcontractor = self.selenium.find_element_by_id("id_form-1-subcontractor")
        self.id_form_1_sub_taxetype = self.selenium.find_element_by_id("id_form-1-sub_taxetype")
        self.id_form_1_sub_legalform = self.selenium.find_element_by_id("id_form-1-sub_legalform")

    def options_to_list(self, options):
        list = []
        for el in options:
            list.append(el.get_attribute("innerHTML"))
        return list

    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()

        site = Site.objects.get(pk=1)
        # site.domain = 'testserver'
        fdir, fname = os.path.split(self.live_server_url)
        site.domain = fname
        site.save()

        c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
        c.save()
        c.create_database()
        c.fill_database()

        user = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b@b.ru')
        user.set_password('qweqwe111')
        user.created_account_pk = c.pk
        user.save()
        user.account.add(c)

        unit1 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[0]
        userdb = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=user.pk)
        userdb.access_app_project = 2
        userdb.access_can_block = 0
        userdb.unit = unit1
        userdb.save(using=get_db_name_test())

        self.my_user = user
        self.account = c

        self.tt_1 = app_transaction_models.TaxeType(name='Налог1', multiplicator=1, is_deletable=False)
        self.tt_1.save(using=get_db_name_test())
        self.tt_2 = app_transaction_models.TaxeType(name='Налог2', multiplicator=2, is_deletable=False)
        self.tt_2.save(using=get_db_name_test())
        self.tt_3 = app_transaction_models.TaxeType(name='Налог3', multiplicator=3, is_deletable=False)
        self.tt_3.save(using=get_db_name_test())
        self.tt_4 = app_transaction_models.TaxeType(name='Налог4', multiplicator=4, is_deletable=False)
        self.tt_4.save(using=get_db_name_test())

        self.sub_1 = app_transaction_models.Subcontractor(
            name='Подрядчик_1',
            is_deletable=False,
        )
        self.sub_1.save(using=get_db_name_test())
        self.sub_1.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_1.taxe_type = self.tt_1
        self.sub_1.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_1.is_my_company = False
        self.sub_1.save(using=get_db_name_test())

        self.sub_2 = app_transaction_models.Subcontractor(
            name='Подрядчик_2',
            is_deletable=False,
        )
        self.sub_2.save(using=get_db_name_test())
        self.sub_2.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_2.taxe_type = self.tt_2
        self.sub_2.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_2.is_my_company = False
        self.sub_2.save(using=get_db_name_test())

        self.sub_3 = app_transaction_models.Subcontractor(
            name='Подрядчик_3',
            is_deletable=False,
        )
        self.sub_3.save(using=get_db_name_test())
        self.sub_3.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_3.taxe_type = self.tt_3
        self.sub_3.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_3.is_my_company = False
        self.sub_3.save(using=get_db_name_test())

        self.sub_4 = app_transaction_models.Subcontractor(
            name='Подрядчик_4',
            is_deletable=False,
        )
        self.sub_4.save(using=get_db_name_test())
        self.sub_4.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_4.taxe_type = self.tt_4
        self.sub_4.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_4.is_my_company = False
        self.sub_4.save(using=get_db_name_test())

        self.mycompany_1 = app_transaction_models.Subcontractor(name='Тестовая компания 1',
                                                                is_deletable=False)
        self.mycompany_1.save(using=get_db_name_test())
        self.mycompany_1.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[
            0]
        self.mycompany_1.taxe_type = app_transaction_models.TaxeType.objects.using(get_db_name_test()).get(
            name='НДС')
        self.mycompany_1.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.mycompany_1.is_my_company = True
        self.mycompany_1.save(using=get_db_name_test())

        self.ac1 = app_transaction_models.Account(name='Банк', type=1)
        self.ac1.save(using=get_db_name_test())
        self.ac1.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='Rub')
        self.ac1.company_owner = self.mycompany_1
        self.ac1.save(using=get_db_name_test())

        self.ac2 = app_transaction_models.Account(name='Касса', type=1)
        self.ac2.save(using=get_db_name_test())
        self.ac2.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='Rub')
        self.ac2.company_owner = self.mycompany_1
        self.ac2.save(using=get_db_name_test())

        self.ac3 = app_transaction_models.Account(name='Сейф', type=1)
        self.ac3.save(using=get_db_name_test())
        self.ac3.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='USD')
        self.ac3.company_owner = self.mycompany_1
        self.ac3.save(using=get_db_name_test())

        self.mycompany_2 = app_transaction_models.Subcontractor(name='Тестовая компания 2',
                                                                is_deletable=False)
        self.mycompany_2.save(using=get_db_name_test())
        self.mycompany_2.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[
            0]
        self.mycompany_2.taxe_type = app_transaction_models.TaxeType.objects.using(get_db_name_test()).get(
            name='НДФЛ')
        self.mycompany_2.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.mycompany_2.is_my_company = True
        self.mycompany_2.save(using=get_db_name_test())

        self.ac4 = app_transaction_models.Account(name='Заначка', type=1)
        self.ac4.save(using=get_db_name_test())
        self.ac4.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='USD')
        self.ac4.company_owner = self.mycompany_2
        self.ac4.save(using=get_db_name_test())

        self.project_1 = app_project_models.Project(name='P00001')
        self.project_1.save(using=get_db_name_test())
        self.project_1.category = app_project_models.ProjectCategory.objects.using(get_db_name_test()).get(
            name='Проекты')
        self.project_1.save(using=get_db_name_test())

        self.project_tech = app_project_models.Project.objects.using(get_db_name_test()).get(
            name_short='Внутренние переводы')

        self.puprose_income = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(name='Оплата')
        self.puprose_outcome = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(
            name='Расход')


        chromedriver_path = '/Users/bogdan/PycharmProjects/app/Fins_App/fins/app_transaction/tests/chromedriver'
        self.selenium = webdriver.Chrome(chromedriver_path)

        self.loggin()
        link = sub_reverse('app_transaction:index', subdomain='mysubdomain')
        self.selenium.get(link)
        wait = WebDriverWait(self.selenium, 2)
        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, "js-create-transfer")))
        btn_create_transfer = self.selenium.find_element_by_class_name("js-create-transfer")
        btn_create_transfer.click()
        time.sleep(4)
        self.graber_transfer_modal()

    def tearDown(self):
        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

        self.selenium.quit()

    def test_required(self):

        self.assertEqual(self.id_companies.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_account.get_attribute("required"), 'true')
        self.assertEqual(self.id_form_0_currency.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_date_fact.get_attribute("required"), 'true')
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_comment.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_status.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("required"), None)

        self.assertEqual(self.id_form_1_account.get_attribute("required"), 'true')
        self.assertEqual(self.id_form_1_currency.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_date_fact.get_attribute("required"), 'true')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_comment.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_status.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("required"), None)

    def test_companies_conditions(self):

        self.assertEqual(self.id_form_0_account.get_attribute("disabled"), None)
        list = self.options_to_list(self.id_form_0_account_select.options)
        self.assertTrue('Мой банк' in list)
        self.assertTrue('Заначка' not in list)

        self.assertEqual(self.id_form_1_account.get_attribute("disabled"), None)
        list = self.options_to_list(self.id_form_1_account_select.options)
        self.assertTrue('Мой банк' in list)
        self.assertTrue('Заначка' not in list)

        self.id_form_0_account_select.select_by_visible_text('Мой банк')
        self.id_form_1_account_select.select_by_visible_text('Мой банк')
        self.id_form_0_sum_before_taxes.click()
        self.id_form_0_sum_before_taxes.send_keys('100')
        self.id_form_0_comment.send_keys('')
        self.id_form_1_sum_before_taxes.click()
        self.id_form_1_sum_before_taxes.send_keys('100')
        self.id_form_1_comment.send_keys('')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '120,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '120,00')

        self.id_companies_select.select_by_visible_text('Тестовая компания 2')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("value"), '0,00')

    def test_account_conditions(self):
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("required"), None)

        self.id_companies_select.select_by_visible_text('Тестовая компания 1')
        time.sleep(1)
        self.id_form_0_account_select.select_by_visible_text('Банк')
        self.id_form_1_account_select.select_by_visible_text('Банк')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("required"), None)
        self.id_form_0_sum_before_taxes.click()
        self.id_form_0_sum_before_taxes.send_keys('100')
        self.id_form_0_comment.send_keys('')
        self.id_form_1_sum_before_taxes.click()
        self.id_form_1_sum_before_taxes.send_keys('100')
        self.id_form_1_comment.send_keys('')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '120,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '120,00')

        self.id_form_0_account_select.select_by_visible_text('Банк')
        self.id_form_1_account_select.select_by_visible_text('Сейф')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("required"), 'true')
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("required"), 'true')
        self.id_form_0_subcontractor_select.select_by_visible_text('Подрядчик_2')
        self.id_form_1_subcontractor_select.select_by_visible_text('Подрядчик_3')
        self.id_form_0_sum_before_taxes.click()
        self.id_form_0_sum_before_taxes.send_keys('1000')
        self.id_form_0_comment.send_keys('')
        self.id_form_1_sum_before_taxes.click()
        self.id_form_1_sum_before_taxes.send_keys('1000')
        self.id_form_1_comment.send_keys('')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '2 000,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '3 000,00')

        self.id_form_0_account_select.select_by_visible_text('Банк')
        self.id_form_1_account_select.select_by_visible_text('Банк')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '1 200,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '1 200,00')
        self.id_form_0_sum_before_taxes.click()
        self.id_form_0_sum_before_taxes.send_keys('100')
        self.id_form_0_comment.send_keys('')
        self.id_form_1_sum_before_taxes.click()
        self.id_form_1_sum_before_taxes.send_keys('100')
        self.id_form_1_comment.send_keys('')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '120,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '120,00')

        self.id_form_0_account_select.select_by_visible_text('---------')
        self.id_form_1_account_select.select_by_visible_text('---------')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("required"), None)

    def test_sbt_sat_conditions(self):

        self.id_form_0_account_select.select_by_visible_text('Мой банк')
        self.id_form_1_account_select.select_by_visible_text('Мой банк')
        time.sleep(1)

        self.id_form_0_sum_before_taxes.click()
        self.id_form_0_sum_before_taxes.send_keys('100')
        self.id_form_0_comment.send_keys('')
        self.id_form_1_sum_before_taxes.click()
        self.id_form_1_sum_before_taxes.send_keys('100')
        self.id_form_1_comment.send_keys('')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '120,00')
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("value"), '20,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '120,00')
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("value"), '20,00')

        self.id_form_0_sum_after_taxes.click()
        self.id_form_0_sum_after_taxes.send_keys('1200')
        self.id_form_0_comment.send_keys('')
        self.id_form_1_sum_after_taxes.click()
        self.id_form_1_sum_after_taxes.send_keys('1200')
        self.id_form_1_comment.send_keys('')
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("value"), '1 000,00')
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("value"), '200,00')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("value"), '1 000,00')
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("value"), '200,00')

    def test_subcontractor_conditions(self):
        self.id_companies_select.select_by_visible_text('Тестовая компания 1')
        time.sleep(1)
        self.id_form_0_account_select.select_by_visible_text('Банк')
        self.id_form_1_account_select.select_by_visible_text('Сейф')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("disabled"), 'true')

        self.id_form_0_subcontractor_select.select_by_visible_text('Подрядчик_2')
        self.id_form_1_subcontractor_select.select_by_visible_text('Подрядчик_3')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("value"), 'Налог2')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("value"), 'Налог3')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("value"), 'ИП')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("value"), 'ИП')

        self.id_form_0_subcontractor_select.select_by_visible_text('---------')
        self.id_form_1_subcontractor_select.select_by_visible_text('---------')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("value"), '')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("value"), '')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("value"), '')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("value"), '')

class TransferTestJSUpdate(StaticLiveServerTestCase):

    def create_session_cookie(self, user):
        session = SessionStore()
        session[SESSION_KEY] = user.pk
        session[BACKEND_SESSION_KEY] = settings.AUTHENTICATION_BACKENDS[0]
        session[HASH_SESSION_KEY] = user.get_session_auth_hash()
        session.save()
        cookie = {
            'name': settings.SESSION_COOKIE_NAME,
            'value': session.session_key,
            'secure': False,
            'path': '/',
        }
        return cookie

    def loggin(self):
        session_cookie = self.create_session_cookie(app_signup_models.User.objects.get(email='b@b.ru'))
        link = sub_reverse('app_dashboard:index', subdomain='mysubdomain')
        self.selenium.get(link)
        self.selenium.add_cookie(session_cookie)
        self.selenium.refresh()
        self.selenium.get(link)

    def graber_transfer_modal(self):
        self.id_companies_select = Select(self.selenium.find_element_by_id("id_companies"))
        self.id_companies = self.selenium.find_element_by_id("id_companies")

        self.id_form_0_account_select = Select(self.selenium.find_element_by_id("id_form-0-account"))
        self.id_form_0_account = self.selenium.find_element_by_id("id_form-0-account")
        self.id_form_0_currency = self.selenium.find_element_by_id("id_form-0-currency")
        self.id_form_0_date_fact = self.selenium.find_element_by_id("id_form-0-date_fact")
        self.id_form_0_sum_before_taxes = self.selenium.find_element_by_id("id_form-0-sum_before_taxes")
        self.id_form_0_sum_after_taxes = self.selenium.find_element_by_id("id_form-0-sum_after_taxes")
        self.id_form_0_sum_taxes = self.selenium.find_element_by_id("id_form-0-sum_taxes")
        self.id_form_0_comment = self.selenium.find_element_by_id("id_form-0-comment")
        self.id_form_0_status = self.selenium.find_element_by_id("id_form-0-status")
        self.id_form_0_subcontractor_select = Select(self.selenium.find_element_by_id("id_form-0-subcontractor"))
        self.id_form_0_subcontractor = self.selenium.find_element_by_id("id_form-0-subcontractor")
        self.id_form_0_sub_taxetype = self.selenium.find_element_by_id("id_form-0-sub_taxetype")
        self.id_form_0_sub_legalform = self.selenium.find_element_by_id("id_form-0-sub_legalform")

        self.id_form_1_account_select = Select(self.selenium.find_element_by_id("id_form-1-account"))
        self.id_form_1_account = self.selenium.find_element_by_id("id_form-1-account")
        self.id_form_1_currency = self.selenium.find_element_by_id("id_form-1-currency")
        self.id_form_1_date_fact = self.selenium.find_element_by_id("id_form-1-date_fact")
        self.id_form_1_sum_before_taxes = self.selenium.find_element_by_id("id_form-1-sum_before_taxes")
        self.id_form_1_sum_after_taxes = self.selenium.find_element_by_id("id_form-1-sum_after_taxes")
        self.id_form_1_sum_taxes = self.selenium.find_element_by_id("id_form-1-sum_taxes")
        self.id_form_1_comment = self.selenium.find_element_by_id("id_form-1-comment")
        self.id_form_1_status = self.selenium.find_element_by_id("id_form-1-status")
        self.id_form_1_subcontractor_select = Select(self.selenium.find_element_by_id("id_form-1-subcontractor"))
        self.id_form_1_subcontractor = self.selenium.find_element_by_id("id_form-1-subcontractor")
        self.id_form_1_sub_taxetype = self.selenium.find_element_by_id("id_form-1-sub_taxetype")
        self.id_form_1_sub_legalform = self.selenium.find_element_by_id("id_form-1-sub_legalform")

    def options_to_list(self, options):
        list = []
        for el in options:
            list.append(el.get_attribute("innerHTML"))
        return list

    def create_transfer(self):
        self.client.login(username='b@b.ru', password='123')

        # create transaction
        link = sub_reverse('app_transaction:transfer_create', subdomain='mysubdomain')
        data = {
            'companies': [self.mycompany_1.pk],
            'hidden': ['False'],
            'is_editable': ['True'],
            'form-TOTAL_FORMS': ['2'],
            'form-INITIAL_FORMS': ['0'],
            'form-MIN_NUM_FORMS': ['0'],
            'form-MAX_NUM_FORMS': ['1000'],
            'form-0-account': [self.ac1.pk],
            'form-0-date_fact': ['2019-03-01'],
            'form-0-sum_before_taxes': ['1 000,00'],
            'form-0-sum_after_taxes': ['3 000,00'],
            'form-0-comment': [''],
            'form-0-status': ['0'],
            'form-0-subcontractor': [self.sub_3.pk],
            'form-0-id': [''],
            'form-1-account': [self.ac3.pk],
            'form-1-date_fact': ['2019-03-08'],
            'form-1-sum_before_taxes': ['500,00'],
            'form-1-sum_after_taxes': ['1 500,00'],
            'form-1-comment': [''],
            'form-1-status': ['0'],
            'form-1-subcontractor': [self.sub_3.pk],
            'form-1-id': [''],
        }
        request = self.factory.post(link, data)
        request.user = self.my_user
        request.account = self.account

        @thread_local(using_db=get_db_name_test())
        def tread(request):
            response = app_transaction_views.transfer_create(request)
            return response

        tread(request)

    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()

        site = Site.objects.get(pk=1)
        # site.domain = 'testserver'
        fdir, fname = os.path.split(self.live_server_url)
        site.domain = fname
        site.save()

        c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
        c.save()
        c.create_database()
        c.fill_database()

        user = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b@b.ru')
        user.set_password('qweqwe111')
        user.created_account_pk = c.pk
        user.save()
        user.account.add(c)

        unit1 = app_setup_models.Unit.objects.using(get_db_name_test()).all()[0]
        userdb = app_setup_models.DBUser.objects.using(get_db_name_test()).get(user_id=user.pk)
        userdb.access_app_project = 2
        userdb.access_can_block = 0
        userdb.unit = unit1
        userdb.save(using=get_db_name_test())

        self.my_user = user
        self.account = c

        self.tt_1 = app_transaction_models.TaxeType(name='Налог1', multiplicator=1, is_deletable=False)
        self.tt_1.save(using=get_db_name_test())
        self.tt_2 = app_transaction_models.TaxeType(name='Налог2', multiplicator=2, is_deletable=False)
        self.tt_2.save(using=get_db_name_test())
        self.tt_3 = app_transaction_models.TaxeType(name='Налог3', multiplicator=3, is_deletable=False)
        self.tt_3.save(using=get_db_name_test())
        self.tt_4 = app_transaction_models.TaxeType(name='Налог4', multiplicator=4, is_deletable=False)
        self.tt_4.save(using=get_db_name_test())

        self.sub_1 = app_transaction_models.Subcontractor(
            name='Подрядчик_1',
            is_deletable=False,
        )
        self.sub_1.save(using=get_db_name_test())
        self.sub_1.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_1.taxe_type = self.tt_1
        self.sub_1.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_1.is_my_company = False
        self.sub_1.save(using=get_db_name_test())

        self.sub_2 = app_transaction_models.Subcontractor(
            name='Подрядчик_2',
            is_deletable=False,
        )
        self.sub_2.save(using=get_db_name_test())
        self.sub_2.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_2.taxe_type = self.tt_2
        self.sub_2.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_2.is_my_company = False
        self.sub_2.save(using=get_db_name_test())

        self.sub_3 = app_transaction_models.Subcontractor(
            name='Подрядчик_3',
            is_deletable=False,
        )
        self.sub_3.save(using=get_db_name_test())
        self.sub_3.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_3.taxe_type = self.tt_3
        self.sub_3.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_3.is_my_company = False
        self.sub_3.save(using=get_db_name_test())

        self.sub_4 = app_transaction_models.Subcontractor(
            name='Подрядчик_4',
            is_deletable=False,
        )
        self.sub_4.save(using=get_db_name_test())
        self.sub_4.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[0]
        self.sub_4.taxe_type = self.tt_4
        self.sub_4.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.sub_4.is_my_company = False
        self.sub_4.save(using=get_db_name_test())

        self.mycompany_1 = app_transaction_models.Subcontractor(name='Тестовая компания 1',
                                                                is_deletable=False)
        self.mycompany_1.save(using=get_db_name_test())
        self.mycompany_1.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[
            0]
        self.mycompany_1.taxe_type = app_transaction_models.TaxeType.objects.using(get_db_name_test()).get(
            name='НДС')
        self.mycompany_1.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.mycompany_1.is_my_company = True
        self.mycompany_1.save(using=get_db_name_test())

        self.ac1 = app_transaction_models.Account(name='Банк', type=1)
        self.ac1.save(using=get_db_name_test())
        self.ac1.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='Rub')
        self.ac1.company_owner = self.mycompany_1
        self.ac1.save(using=get_db_name_test())

        self.ac2 = app_transaction_models.Account(name='Касса', type=1)
        self.ac2.save(using=get_db_name_test())
        self.ac2.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='Rub')
        self.ac2.company_owner = self.mycompany_1
        self.ac2.save(using=get_db_name_test())

        self.ac3 = app_transaction_models.Account(name='Сейф', type=1)
        self.ac3.save(using=get_db_name_test())
        self.ac3.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='USD')
        self.ac3.company_owner = self.mycompany_1
        self.ac3.save(using=get_db_name_test())

        self.mycompany_2 = app_transaction_models.Subcontractor(name='Тестовая компания 2',
                                                                is_deletable=False)
        self.mycompany_2.save(using=get_db_name_test())
        self.mycompany_2.type = app_transaction_models.SubcontractorType.objects.using(get_db_name_test()).all()[
            0]
        self.mycompany_2.taxe_type = app_transaction_models.TaxeType.objects.using(get_db_name_test()).get(
            name='НДФЛ')
        self.mycompany_2.legal_form = app_transaction_models.Legalform.objects.using(get_db_name_test()).all()[0]
        self.mycompany_2.is_my_company = True
        self.mycompany_2.save(using=get_db_name_test())

        self.ac4 = app_transaction_models.Account(name='Заначка', type=1)
        self.ac4.save(using=get_db_name_test())
        self.ac4.currency = app_transaction_models.Currency.objects.using(get_db_name_test()).get(name='USD')
        self.ac4.company_owner = self.mycompany_2
        self.ac4.save(using=get_db_name_test())

        self.project_1 = app_project_models.Project(name='P00001')
        self.project_1.save(using=get_db_name_test())
        self.project_1.category = app_project_models.ProjectCategory.objects.using(get_db_name_test()).get(
            name='Проекты')
        self.project_1.save(using=get_db_name_test())

        self.project_tech = app_project_models.Project.objects.using(get_db_name_test()).get(
            name_short='Внутренние переводы')

        self.puprose_income = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(name='Оплата')
        self.puprose_outcome = app_transaction_models.Purpose.objects.using(get_db_name_test()).get(
            name='Расход')


        chromedriver_path = '/Users/bogdan/PycharmProjects/app/Fins_App/fins/app_transaction/tests/chromedriver'
        self.selenium = webdriver.Chrome(chromedriver_path)

        self.create_transfer()
        self.loggin()
        link = sub_reverse('app_transaction:index', subdomain='mysubdomain')
        self.selenium.get(link)
        wait = WebDriverWait(self.selenium, 2)
        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, "js-update-transfer")))
        btn_create_transfer = self.selenium.find_element_by_class_name("js-update-transfer")
        btn_create_transfer.click()
        time.sleep(3)
        self.graber_transfer_modal()

    def tearDown(self):
        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

        self.selenium.quit()

    def test_required(self):
        self.assertEqual(self.id_companies.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_account.get_attribute("required"), 'true')
        self.assertEqual(self.id_form_0_currency.get_attribute("required"), None)
        # self.assertEqual(self.id_form_0_date_fact.get_attribute("required"), 'true')
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_comment.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_status.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("required"), 'true')
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("required"), None)
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("required"), None)

        self.assertEqual(self.id_form_1_account.get_attribute("required"), 'true')
        self.assertEqual(self.id_form_1_currency.get_attribute("required"), None)
        # self.assertEqual(self.id_form_1_date_fact.get_attribute("required"), 'true')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_comment.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_status.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("required"), 'true')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("required"), None)

    def test_companies_conditions(self):
        self.assertEqual(self.id_companies_select.first_selected_option.text, 'Тестовая компания 1')
        self.assertEqual(self.id_form_0_account_select.first_selected_option.text, 'Банк')
        self.assertEqual(self.id_form_1_account_select.first_selected_option.text, 'Сейф')

        self.assertEqual(self.id_form_0_account.get_attribute("disabled"), None)
        list = self.options_to_list(self.id_form_0_account_select.options)
        self.assertTrue('Банк' in list)
        self.assertTrue('Заначка' not in list)

        self.assertEqual(self.id_form_1_account.get_attribute("disabled"), None)
        list = self.options_to_list(self.id_form_1_account_select.options)
        self.assertTrue('Сейф' in list)
        self.assertTrue('Заначка' not in list)

        self.id_form_0_account_select.select_by_visible_text('Банк')
        self.id_form_1_account_select.select_by_visible_text('Банк')
        self.id_form_0_sum_before_taxes.click()
        self.id_form_0_sum_before_taxes.send_keys('100')
        self.id_form_0_comment.send_keys('')
        self.id_form_1_sum_before_taxes.click()
        self.id_form_1_sum_before_taxes.send_keys('100')
        self.id_form_1_comment.send_keys('')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '120,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '120,00')

    def test_sbt_sat_conditions(self):
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("value"), '1 000,00')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '3 000,00')
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("value"), '2 000,00')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("value"), '500,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '1 500,00')
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("value"), '1 000,00')

        self.id_form_0_sum_before_taxes.click()
        self.id_form_0_sum_before_taxes.send_keys('100')
        self.id_form_0_comment.send_keys('')
        self.id_form_1_sum_before_taxes.click()
        self.id_form_1_sum_before_taxes.send_keys('100')
        self.id_form_1_comment.send_keys('')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '300,00')
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("value"), '200,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '300,00')
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("value"), '200,00')

        self.id_form_0_sum_after_taxes.click()
        self.id_form_0_sum_after_taxes.send_keys('3000')
        self.id_form_0_comment.send_keys('')
        self.id_form_1_sum_after_taxes.click()
        self.id_form_1_sum_after_taxes.send_keys('3000')
        self.id_form_1_comment.send_keys('')
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("value"), '1 000,00')
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("value"), '2 000,00')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("value"), '1 000,00')
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("value"), '2 000,00')

    def test_account_conditions(self):
        time.sleep(1)
        self.assertEqual(self.id_form_0_account_select.first_selected_option.text, 'Банк')
        self.assertEqual(self.id_form_1_account_select.first_selected_option.text, 'Сейф')
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("required"), 'true')
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("required"), 'true')

        self.id_companies_select.select_by_visible_text('Тестовая компания 1')
        time.sleep(1)
        self.id_form_0_account_select.select_by_visible_text('Банк')
        self.id_form_1_account_select.select_by_visible_text('Банк')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("required"), None)
        self.id_form_0_sum_before_taxes.click()
        self.id_form_0_sum_before_taxes.send_keys('100')
        self.id_form_0_comment.send_keys('')
        self.id_form_1_sum_before_taxes.click()
        self.id_form_1_sum_before_taxes.send_keys('100')
        self.id_form_1_comment.send_keys('')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '120,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '120,00')

        self.id_form_0_account_select.select_by_visible_text('Банк')
        self.id_form_1_account_select.select_by_visible_text('Сейф')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("disabled"), None)
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("hidden"), None)
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("required"), 'true')
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("required"), 'true')
        self.id_form_0_subcontractor_select.select_by_visible_text('Подрядчик_2')
        self.id_form_1_subcontractor_select.select_by_visible_text('Подрядчик_3')
        self.id_form_0_sum_before_taxes.click()
        self.id_form_0_sum_before_taxes.send_keys('1000')
        self.id_form_0_comment.send_keys('')
        self.id_form_1_sum_before_taxes.click()
        self.id_form_1_sum_before_taxes.send_keys('1000')
        self.id_form_1_comment.send_keys('')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '2 000,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '3 000,00')

        self.id_form_0_account_select.select_by_visible_text('Банк')
        self.id_form_1_account_select.select_by_visible_text('Банк')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '1 200,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '1 200,00')
        self.id_form_0_sum_before_taxes.click()
        self.id_form_0_sum_before_taxes.send_keys('100')
        self.id_form_0_comment.send_keys('')
        self.id_form_1_sum_before_taxes.click()
        self.id_form_1_sum_before_taxes.send_keys('100')
        self.id_form_1_comment.send_keys('')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '120,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '120,00')

        self.id_form_0_account_select.select_by_visible_text('---------')
        self.id_form_1_account_select.select_by_visible_text('---------')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sum_before_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_1_sum_before_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_0_sum_after_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_1_sum_after_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_0_sum_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_1_sum_taxes.get_attribute("value"), '0,00')
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("hidden"), 'true')
        self.assertEqual(self.id_form_0_subcontractor.get_attribute("required"), None)
        self.assertEqual(self.id_form_1_subcontractor.get_attribute("required"), None)

    def test_subcontractor_conditions(self):
        self.assertEqual(self.id_form_0_subcontractor_select.first_selected_option.text, 'Подрядчик_3')
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("value"), 'Налог3')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("value"), 'ИП')
        self.assertEqual(self.id_form_1_subcontractor_select.first_selected_option.text, 'Подрядчик_3')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("value"), 'Налог3')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("value"), 'ИП')

        self.id_companies_select.select_by_visible_text('Тестовая компания 2')
        self.id_companies_select.select_by_visible_text('Тестовая компания 1')
        time.sleep(1)
        self.id_form_0_account_select.select_by_visible_text('Банк')
        self.id_form_1_account_select.select_by_visible_text('Сейф')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("disabled"), 'true')

        self.id_form_0_subcontractor_select.select_by_visible_text('Подрядчик_2')
        self.id_form_1_subcontractor_select.select_by_visible_text('Подрядчик_1')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("value"), 'Налог2')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("value"), 'Налог1')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("value"), 'ИП')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("value"), 'ИП')

        self.id_form_0_subcontractor_select.select_by_visible_text('---------')
        self.id_form_1_subcontractor_select.select_by_visible_text('---------')
        time.sleep(1)
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("disabled"), 'true')
        self.assertEqual(self.id_form_0_sub_taxetype.get_attribute("value"), '')
        self.assertEqual(self.id_form_1_sub_taxetype.get_attribute("value"), '')
        self.assertEqual(self.id_form_0_sub_legalform.get_attribute("value"), '')
        self.assertEqual(self.id_form_1_sub_legalform.get_attribute("value"), '')

