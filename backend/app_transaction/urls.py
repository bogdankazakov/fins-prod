from django.contrib import admin
from django.urls import path, include, re_path
from django.contrib.auth import views as auth_views
from . import views

app_name = 'app_transaction'
urlpatterns = [
    # path('', views.transaction_index, name='index'),
    path('api/', views.transaction_list),
    path('api/<int:pk>/', views.transaction_detail),
    path('api/history/', views.api_transaction_history),
    path('api/excel/', views.api_transaction_excel),
    path('api/extra/', views.api_transaction_extra, ),


    path('transactions/create_project/<projectpk>', views.transaction_create_project, name='transaction_create_project'),
    path('transactions/update_project/<pk>', views.transaction_update_project, name='transaction_update_project'),
    path('transactions/delete_project/<pk>', views.transaction_delete_project, name='transaction_delete_project'),

    path('transactions/create_project_period/<projectpk>/<periodpk>', views.transaction_create_project_period,
         name='transaction_create_project_period'),
    path('transactions/update_project_period/<pk>/<projectpk>', views.transaction_update_project_period, name='transaction_update_project_period'),
    path('transactions/delete_project_period/<pk>', views.transaction_delete_project_period, name='transaction_delete_project_period'),



    path('ajax/load-purpose/', views.load_purpose, name='ajax_load_purpose'),
    path('ajax/subcontractor-info/', views.subcontractor_info, name='ajax_subcontractor_info'),
    path('ajax/account-info/', views.account_info, name='ajax_account_info'),
    path('ajax/mycompany-info/', views.mycompany_info, name='ajax_mycompany_info'),
    path('ajax/list_saver/', views.list_saver, name='ajax_list_saver'),

    path('transaction_docs/<pk>/', views.transaction_docs, name='transaction_docs'),

    # path('transactions/create/', views.transaction_create, name='transaction_create'),
    # path('transactions/save/', views.transaction_save, name='transaction_save'),
    # path('transactions/update/<pk>', views.transaction_update, name='transaction_update'),
    # path('transactions/delete/<pk>', views.transaction_delete, name='transaction_delete'),

    # path('transfer/create/', views.transfer_create, name='transfer_create'),
    # path('transfer/save/', views.transfer_save, name='transfer_save'),
    # path('transfer/update/<pk>', views.transfer_update, name='transfer_update'),
    # path('transfer/delete/<pk>', views.transfer_delete, name='transfer_delete'),
]


