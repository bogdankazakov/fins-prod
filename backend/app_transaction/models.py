from django.db import models
from django.utils.translation import ugettext_lazy as _
from fins.global_functions import IsDeletableMixin, BOOL_CHOICES_YESNO
from simple_history.models import HistoricalRecords
from django.conf import settings
from fins.gist import SpanningForeignKey
from app_signup import models as app_signup_models
from app_project import models as app_project_models
from app_setup.models import DBUser, Unit
from decimal import *

class Legalform(models.Model, IsDeletableMixin):
    name = models.CharField(_('Организационно-правовая форма сокращенно'), max_length=200, unique=True)
    name_full = models.CharField(_('Организационно-правовая форма полная форма'), max_length=200, blank=True)

    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='legalforms_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='legalforms_modified_by', on_delete=models.PROTECT, null=True)
    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    # is_global = models.BooleanField(_('Глобальная модель'),default=False, choices = BOOL_CHOICES_YESNO)
    history = HistoricalRecords(user_model=DBUser)


    class Meta:
        ordering = ['name']
        verbose_name = _('subcontractor legal form')
        verbose_name_plural = _('subcontractor legal forms')

    def __str__(self):
        return self.name

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class SubcontractorType(models.Model, IsDeletableMixin):
    name = models.CharField(_('Тип контрагента'), max_length=50, unique=True)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='subcontractors_types_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='subcontractors_types_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)


    class Meta:
        ordering = ['name']
        verbose_name = _('subcontractor type')
        verbose_name_plural = _('subcontractor types')

    def __str__(self):
        return self.name

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class TaxeType(models.Model, IsDeletableMixin):
    name = models.CharField(_('Система налогооблажения'), max_length=200, unique=True)
    multiplicator = models.FloatField(_('Коэффициент'))

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='taxes_types_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='taxes_types_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('taxe type')
        verbose_name_plural = _('taxe types')

    def __str__(self):
        return self.name

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class Subcontractor(models.Model, IsDeletableMixin):
    name = models.CharField(_('Короткое название'), max_length=5000, unique=True)
    type = models.ForeignKey('SubcontractorType', verbose_name = 'Тип контрагента', related_name='subcontractors_with_type',on_delete=models.PROTECT, null=True)
    taxe_type = models.ForeignKey('TaxeType', verbose_name = 'Система налогооблажения', related_name='subcontractors_with_taxetype',on_delete=models.PROTECT, null=True)
    legal_form = models.ForeignKey('Legalform', verbose_name = 'ОПФ', related_name='subcontractors_with_legalform', on_delete=models.PROTECT, null=True)
    site = models.CharField(_('Сайт'), max_length=5000, blank=True)
    comment = models.CharField(_('Комментарии'), max_length=5000, blank=True, default=_('Нет комментариев'))

    pay_delay = models.IntegerField(default='14')
    is_my_company = models.BooleanField(_('Моя компания'), default=False, choices = BOOL_CHOICES_YESNO)

    requisites_full_name = models.CharField(_('Полное название'), max_length=5000, blank=True)
    requisites_official_address = models.CharField(_('Юридический адрес'), max_length=5000, blank=True)
    requisites_real_address = models.CharField(_('Фактический адрес'), max_length=5000, blank=True)
    requisites_inn = models.CharField(_('ИНН'), max_length=5000, blank=True)
    requisites_kpp = models.CharField(_('КРР'), max_length=5000, blank=True)
    requisites_okpo = models.CharField(_('ОКПО'), max_length=5000, blank=True)
    requisites_okdev = models.CharField(_('ОКВЭД'), max_length=5000, blank=True)
    requisites_ogrn = models.CharField(_('ОГРН'), max_length=5000, blank=True)
    requisites_bank_name = models.CharField(_('Название банка'), max_length=5000, blank=True)
    requisites_bank_bik = models.CharField(_('БИК'), max_length=5000, blank=True)
    requisites_bank_address = models.CharField(_('Адрес банка'), max_length=5000, blank=True)
    requisites_bank_pay_acc = models.CharField(_('Расчетный счет'), max_length=5000, blank=True)
    requisites_bank_cor_acc = models.CharField(_('Корреспондентский счет'), max_length=5000, blank=True)
    requisites_bank_card_number = models.CharField(_('Номер карты'), max_length=5000, blank=True)

    director_firstname = models.CharField(_('Имя'), max_length=5000, blank=True)
    director_secondname = models.CharField(_('Фамилия'), max_length=5000, blank=True)
    director_fathername = models.CharField(_('Отчество'), max_length=5000, blank=True)
    director_tel = models.CharField(_('Телефон'), max_length=5000, blank=True)
    director_email = models.CharField(_('Email'), max_length=5000, blank=True)


    contact_name = models.CharField(_('ФИО'), max_length=5000, blank=True)
    contact_tel = models.CharField(_('Телефон'), max_length=5000, blank=True)
    contact_email = models.CharField(_('Электронная почта'), max_length=5000, blank=True)
    contact_messenger_name = models.CharField(_('Название мессенджера'), max_length=5000, blank=True)
    contact_messenger_id = models.CharField(_('ID мессенджера'), max_length=5000, blank=True)
    contact_social_net = models.CharField(_('Соц.сеть'), max_length=5000, blank=True)
    contact_social_link = models.CharField(_('Ссылка на соц.сеть'), max_length=5000, blank=True)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add = True)
    created_by = models.ForeignKey(DBUser, related_name='subcontractors_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='subcontractors_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('Подрядчик')
        verbose_name_plural = _('Подрядчики')

    def __str__(self):
        return self.name


    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value



class Purpose(models.Model, IsDeletableMixin):
    TYPE_CHOICES = ((True, "Доход"), (False, "Расход"))

    name = models.CharField(_('Назначение платежа'), max_length=200)
    type = models.BooleanField(_('Тип транзакции'), choices=TYPE_CHOICES, default=False)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add = True)
    created_by = models.ForeignKey(DBUser, related_name='purpose_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='purpose_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('назначение')
        verbose_name_plural = _('назначения')

    def __str__(self):
        return self.name


    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class Currency(models.Model, IsDeletableMixin):
    BOOLEAN_CHOICES = ((True, "Да"), (False, "Нет"))

    name = models.CharField(_('Валюта'), max_length=5, unique=True)
    is_default = models.BooleanField(_('По умолчанию'), choices=BOOLEAN_CHOICES, default=False)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add = True)
    created_by = models.ForeignKey(DBUser, related_name='currencys_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='currencys_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('валюта')
        verbose_name_plural = _('валюты')

    def __str__(self):
        return self.name


    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class DefaultRate(models.Model, IsDeletableMixin):
    rate = models.DecimalField(_('Курс по умолчанию'), max_digits=15, decimal_places=2, default=Decimal('1.00'))
    target = models.ForeignKey(Currency, related_name='defaultrate_target', on_delete=models.PROTECT, null=True)
    source = models.ForeignKey(Currency, related_name='defaultrate_source', on_delete=models.PROTECT, null=True)

    is_deletable = models.BooleanField(_('Может быть удален'), default=True, choices=BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add=True)
    created_by = models.ForeignKey(DBUser, related_name='rate_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='rate_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['target']
        verbose_name = _('Курс')
        verbose_name_plural = _('Курсы')

    def __str__(self):
        try:
            name = str(str(self.target) + "/" + str(self.source))
        except Exception:
            name = '' + str(self.rate)
        return name

    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value


class Account(models.Model, IsDeletableMixin):
    TYPE_CHOICES = (
        (0, 'Банк'),
        (1, 'Касса'),
    )

    name = models.CharField(_('Счет'), max_length=200, unique=True)
    type = models.SmallIntegerField(_('Тип счета'), choices=TYPE_CHOICES, default=0)
    currency = models.ForeignKey(Currency, verbose_name = 'Валюта', related_name='account_currency', on_delete=models.PROTECT, null=True)
    company_owner = models.ForeignKey(Subcontractor, verbose_name = 'Владелец счета', related_name='subs_accounts', on_delete=models.PROTECT, null=True)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add = True)
    created_by = models.ForeignKey(DBUser, related_name='account_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser, related_name='account_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('банковский счет ')
        verbose_name_plural = _('банковские счета')

    def __str__(self):
        return self.name


    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

class Transaction(models.Model, IsDeletableMixin):
    TYPE_CHOICES = ((True, "Доход"), (False, "Расход"))

    STATUS_CHOICES = (
        (0, 'К оплате'),
        (1, 'Скоро'),
        (2, 'Долг'),
        (3, 'Оплачено'),
    )

    REQUEST_CHOICES = (
        (0, 'Не отправлена'),
        (1, 'Отправлена'),
        (2, 'Получена, в работе'),
        (3, 'Исполненна'),
    )

    PAY_MODEL_CHOICES = (
        (0, 'Предоплата'),
        (1, 'Постоплата'),
    )



    name = models.CharField(_('Номер транзакции'), max_length=50)
    type = models.BooleanField(_('Тип транзакции'), choices=TYPE_CHOICES, default=False)

    status = models.SmallIntegerField(_('Статус транзакции'), choices=STATUS_CHOICES, default=0)
    subcontractor = models.ForeignKey(Subcontractor, verbose_name = 'Контрагент', on_delete=models.PROTECT, blank=True, null=True)
    purpose = models.ForeignKey(Purpose, verbose_name = 'Назначение платежа', on_delete=models.PROTECT, null=True)
    unit = models.ForeignKey(Unit, verbose_name = 'Юнит', on_delete=models.PROTECT, blank=True, null=True)
    account = models.ForeignKey(Account, verbose_name = 'Счет', on_delete=models.PROTECT, blank=True, null=True)

    date_fact = models.DateField(_('Фактическая дата оплаты'), blank=True, null=True)
    date_doc = models.DateField(_('Дата оплаты по договору'),)
    sum_before_taxes = models.DecimalField(_('Сумма транзакции до налогов'),max_digits=15, decimal_places=2)

    agreement_start_date = models.DateField(_('Дата начала работ по договору'))
    agreement_end_date = models.DateField(_('Дата завершения работ по договору'))
    pay_delay = models.IntegerField(_('Задержка для оплаты'), default=0)
    delayed = models.IntegerField(_('Просрочка транзакции'), blank=True, default=0)
    pay_model = models.SmallIntegerField(_('Модель оплаты'), choices=PAY_MODEL_CHOICES, default=0)
    act_date = models.DateField(_('Дата акта'), blank=True, null=True)
    doc_request = models.SmallIntegerField(_('Статус запроса на документы'), choices=REQUEST_CHOICES, default=0)

    description = models.CharField(_('Описание'), max_length=200, blank=True)
    comment = models.CharField(_('Комментарий'), max_length=200, blank=True)
    estimation_link = models.CharField(_('Ссылка на смету'), max_length=200, blank=True)

    hidden = models.BooleanField(_('Скрытая транзакция'), default=False, choices = BOOL_CHOICES_YESNO, blank=True)

    parent = models.IntegerField(_('Родительская транзакция'), blank=True, null=True)
    transfer = models.IntegerField( blank=True, null=True)
    is_transfer = models.BooleanField(_('Перевод'),default=False, choices = BOOL_CHOICES_YESNO)
    project = models.ForeignKey(app_project_models.Project, related_name='project_transaction',verbose_name='Проект', on_delete=models.PROTECT, null=True, blank=True)
    period = models.ForeignKey(app_project_models.ReportPeriod, verbose_name='Отчетный период', on_delete=models.PROTECT, null=True, blank=True)

    is_deletable = models.BooleanField(_('Может быть удален'),default=True, choices = BOOL_CHOICES_YESNO)
    is_editable = models.BooleanField(_('Может быть отредактирован'), default=True, choices = BOOL_CHOICES_YESNO, blank=True)
    created_at = models.DateTimeField(_('Когда создан'), auto_now_add = True)
    created_by = models.ForeignKey(DBUser,  verbose_name = 'Создан пользователем', related_name='transaction_created_by', on_delete=models.PROTECT, null=True)
    modified_at = models.DateTimeField(_('Когда изменен'), null=True, blank=True)
    modified_by = models.ForeignKey(DBUser,  verbose_name = 'Изменен пользователем', related_name='transaction_modified_by', on_delete=models.PROTECT, null=True)
    history = HistoricalRecords(user_model=DBUser)

    class Meta:
        ordering = ['name']
        verbose_name = _('транзакция ')
        verbose_name_plural = _('транзакции')

        indexes = [
            models.Index(fields=['name']),
            models.Index(fields=['type']),
            models.Index(fields=['status']),
            models.Index(fields=['project']),
        ]

    def get_sum_after_taxes(self):
        if self.type:
            multiplicator = self.account.company_owner.taxe_type.multiplicator
            sum_after_taxes = float(self.sum_before_taxes) * multiplicator
        else:
            multiplicator = self.subcontractor.taxe_type.multiplicator
            sum_after_taxes = float(self.sum_before_taxes) * multiplicator

        sum_after_taxes = Decimal(sum_after_taxes).quantize(Decimal('.01'))
        return sum_after_taxes

    def get_sum_taxes(self):
        sum_after_taxes = self.get_sum_after_taxes()
        return sum_after_taxes - self.sum_before_taxes

    def get_date(self):
        if self.date_fact:
            date = self.date_fact
        else:
            date = self.date_doc
        return date


    def __str__(self):
        return self.name


    @property
    def _history_user(self):
        return self.modified_by

    @_history_user.setter
    def _history_user(self, value):
        self.modified_by = value

    def save_without_historical_record(self, *args, **kwargs):
        self.skip_history_when_saving = True
        try:
            ret = self.save(*args, **kwargs)
        finally:
            del self.skip_history_when_saving
        return ret