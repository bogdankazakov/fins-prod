from django.contrib.auth.models import User
import django_filters
from .models import *
from django.http.request import HttpRequest
from django import forms
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from app_setup import models as app_setup_models
from django_filters import Filter
from django_filters.fields import Lookup
import json
from django.db.models import Q, F
from django.db.models import Case, When, DateField, DecimalField, ExpressionWrapper


from django.core.validators import EMPTY_VALUES


BOOL_CHOICES_YESNO = (
    (True,'Да'),
    (False,'Нет'),
)

BOOL_CHOICES_INCOME = (
    (True,'Доход'),
    (False,'Расход'),
)

STATUS_CHOICES = (
    (0, 'К оплате'),
    (1, 'Скоро'),
    (2, 'Долг'),
    (3, 'Оплачено'),
)

REQUEST_CHOICES = (
    (0, 'Не отправлена'),
    (1, 'Отправлена'),
    (2, 'Получена, в работе'),
    (3, 'Исполненна'),
)

PAY_MODEL_CHOICES = (
    (0, 'Предоплата'),
    (1, 'Постоплата'),
)

STAGES_CHOICES = (
    (0, 'Новый'),
    (1, 'Оценка'),
    (2, 'Продажа'),
    (3, 'Реализация'),
    (4, 'Завершен'),
    (5, 'Не продан'),
)

# создан специальный фильтр чтобы определять что строка пустая
class EmptyStringFilter(django_filters.BooleanFilter):
    def filter(self, qs, value):
        if value in EMPTY_VALUES:
            return qs

        exclude = self.exclude ^ (value is True)
        method = qs.exclude if exclude else qs.filter

        return method(**{self.name: ""})



class TransactionsFilter(django_filters.FilterSet):
    # widget = forms.TextInput(attrs={'id': 'text'})
    name = django_filters.CharFilter(lookup_expr='icontains')
    type = django_filters.ChoiceFilter(choices = BOOL_CHOICES_INCOME)
    status = django_filters.MultipleChoiceFilter(choices = STATUS_CHOICES)
    doc_request = django_filters.MultipleChoiceFilter(choices = REQUEST_CHOICES)
    subcontractor = django_filters.CharFilter(lookup_expr='name__icontains')
    subcontractor__type = django_filters.ModelMultipleChoiceFilter(queryset=app_transaction_models.SubcontractorType.objects.all())
    purpose = django_filters.ModelMultipleChoiceFilter(queryset=app_transaction_models.Purpose.objects.all())
    date_fact = django_filters.DateFromToRangeFilter()
    date_doc = django_filters.DateFromToRangeFilter()
    date = django_filters.DateFromToRangeFilter()
    sum_before_taxes = django_filters.RangeFilter()
    sat = django_filters.RangeFilter(method='sat_filter')
    taxes = django_filters.RangeFilter(method='taxes_filter')
    subcontractor__taxe_type = django_filters.ModelMultipleChoiceFilter(queryset=app_transaction_models.TaxeType.objects.all())
    agreement_start_date = django_filters.DateFromToRangeFilter()
    agreement_end_date = django_filters.DateFromToRangeFilter()
    pay_delay = django_filters.RangeFilter()
    delayed = django_filters.RangeFilter()
    pay_model = django_filters.MultipleChoiceFilter(choices = PAY_MODEL_CHOICES)
    act_date = django_filters.DateFromToRangeFilter()
    comment = django_filters.CharFilter(lookup_expr='icontains')
    estimation_link = django_filters.CharFilter(lookup_expr='icontains')
    project = django_filters.ModelMultipleChoiceFilter(queryset=app_project_models.Project.objects.all())
    project__category = django_filters.ModelMultipleChoiceFilter(queryset=app_project_models.ProjectCategory.objects.all())
    project__subcategory = django_filters.ModelMultipleChoiceFilter(queryset=app_project_models.ProjectSubcategory.objects.all())
    project__brand = django_filters.ModelMultipleChoiceFilter(queryset=app_project_models.Brand.objects.all())
    project__name_short = django_filters.CharFilter(lookup_expr='icontains')
    project__stage = django_filters.MultipleChoiceFilter(choices = STAGES_CHOICES)
    unit = django_filters.ModelMultipleChoiceFilter(queryset=app_transaction_models.Unit.objects.all())
    account = django_filters.ModelMultipleChoiceFilter(queryset=app_transaction_models.Account.objects.all())
    created_at = django_filters.DateFromToRangeFilter()
    created_by = django_filters.ModelMultipleChoiceFilter(queryset=app_setup_models.DBUser.objects.all())
    modified_at = django_filters.DateFromToRangeFilter()
    modified_by = django_filters.ModelMultipleChoiceFilter(queryset=app_setup_models.DBUser.objects.all())
    is_transfer = django_filters.MultipleChoiceFilter(choices = BOOL_CHOICES_YESNO)
    hidden = django_filters.MultipleChoiceFilter(choices = BOOL_CHOICES_YESNO)
    is_editable = django_filters.MultipleChoiceFilter(choices = BOOL_CHOICES_YESNO)

    def range(self, value, queryset, param):
        transactions = app_transaction_models.Transaction.objects.all()

        q = Q()
        for t in transactions:
            if param == 'sat':
                sat = t.get_sum_after_taxes()
                arg = sat
            if param == 'taxes':
                taxes = t.get_sum_taxes()
                arg = taxes
            if value.start is not None and value.stop is not None:
                if arg >= Decimal(value.start) and arg <= Decimal(value.stop):
                    q = q | Q(pk=t.pk)
            elif value.start is None and value.stop is not None:
                if arg <= Decimal(value.stop):
                    q = q | Q(pk=t.pk)
            else:
                if arg >= Decimal(value.start):
                    q = q | Q(pk=t.pk)

        if len(q) == 0:
            return queryset.none()
        else:
            return queryset.filter(q)


    def sat_filter(self, queryset, name, value):
        queryset = self.range(value, queryset, 'sat')
        return queryset

    def taxes_filter(self, queryset, name, value):
        queryset = self.range(value, queryset, 'taxes')
        return queryset


    class Meta:
        model = app_transaction_models.Transaction
        fields = []

    def __init__(self, data, *args, **kwargs):
        if  len(data) == 0:
            data = data.copy()
            data.update({'status': '0'})
            data.update({'status': '1'})
            data.update({'status': '2'})


        super().__init__(data, *args, **kwargs)




def filterMain(qs, filter_param, is_json = True):
    qs = qs.annotate(
            date=Case(
                When(date_fact=None, then='date_doc'),
                default='date_fact',
                output_field=DateField(),
            )).annotate(
            sum_after_taxes=Case(
                When(type=False, then=(F('sum_before_taxes') * F('subcontractor__taxe_type__multiplicator'))),
                default=(F('sum_before_taxes') * F('account__company_owner__taxe_type__multiplicator')),
                output_field=DecimalField(),
            )).annotate(
            sum_taxes=Case(
                When(type=False, then=((F('sum_before_taxes') * F('subcontractor__taxe_type__multiplicator'))-F('sum_before_taxes'))),
                default=((F('sum_before_taxes') * F('account__company_owner__taxe_type__multiplicator'))-F('sum_before_taxes')),
                output_field=DecimalField(),
            ))

    if is_json:
        filter_param = json.loads(filter_param)

    query = Q()

    for (key, value) in filter_param.items():

        if key == 'name' and len(value) > 0:
            query &= Q(name__icontains=value)

        elif key == 'type' and len(value)>0:
            query &= Q(type__in=value)

        elif key == 'status' and len(value)>0:
            query &= Q(status__in=value)

        elif key == 'docRequest' and len(value)>0:
            query &= Q(doc_request__in=value)

        elif key == 'subcontractor' and len(str(value)) > 0:
            query &= Q(subcontractor=value)

        elif key == 'sub_type' and len(value) > 0:
            query &= Q(subcontractor__type__in=value)

        elif key == 'purpose' and len(value) > 0:
            query &= Q(purpose__in=value)


        elif key == 'dateFrom' and len(value) > 0:
            query &= Q(date__gte=value)

        elif key == 'dateTo' and len(value) > 0:
            query &= Q(date__lte=value)


        elif key == 'dateFactFrom' and len(value) > 0:
            query &= Q(date_fact__gte=value)

        elif key == 'dateFactTo' and len(value) > 0:
            query &= Q(date_fact__lte=value)


        elif key == 'dateDocFrom' and len(value) > 0:
            query &= Q(date_doc__gte=value)

        elif key == 'dateDocTo' and len(value) > 0:
            query &= Q(date_doc__lte=value)


        elif key == 'sbtFrom' and len(value) > 0:
            query &= Q(sum_before_taxes__gte=value)

        elif key == 'sbtTo' and len(value) > 0:
            query &= Q(sum_before_taxes__lte=value)


        elif key == 'satFrom' and len(value) > 0:
            query &= Q(sum_after_taxes__gte=value)

        elif key == 'satTo' and len(value) > 0:
            query &= Q(sum_after_taxes__lte=value)


        elif key == 'taxeFrom' and len(value) > 0:
            query &= Q(sum_taxes__gte=value)

        elif key == 'taxeTo' and len(value) > 0:
            query &= Q(sum_taxes__lte=value)



        elif key == 'taxeType' and len(value) > 0:
            query &= Q(subcontractor__taxe_type__in=value)


        elif key == 'agreementStartDateFrom' and len(value) > 0:
            query &= Q(agreement_start_date__gte=value)

        elif key == 'agreementStartDateTo' and len(value) > 0:
            query &= Q(agreement_start_date__lte=value)


        elif key == 'agreementEndDateFrom' and len(value) > 0:
            query &= Q(agreement_end_date__gte=value)

        elif key == 'agreementEndDateTo' and len(value) > 0:
            query &= Q(agreement_end_date__lte=value)



        elif key == 'payDelayFrom' and len(value) > 0:
            query &= Q(pay_delay__gte=value)

        elif key == 'payDelayTo' and len(value) > 0:
            query &= Q(pay_delay__lte=value)

        elif key == 'delayedFrom' and len(value) > 0:
            query &= Q(delayed__gte=value)

        elif key == 'delayedTo' and len(value) > 0:
            query &= Q(delayed__lte=value)



        elif key == 'paymodel' and len(value) > 0:
            query &= Q(pay_model__in=value)


        elif key == 'actDateFrom' and len(value) > 0:
            query &= Q(act_date__gte=value)

        elif key == 'actDateTo' and len(value) > 0:
            query &= Q(act_date__lte=value)

        elif key == 'comment' and len(value) > 0:
            query &= Q(comment__icontains=value)

        elif key == 'estimationLink' and len(value) > 0:
            query &= Q(estimation_link__icontains=value)

        elif key == 'projects' and len(str(value)) > 0:
            query &= Q(project=value)

        elif key == 'projectsCategory' and len(value) > 0:
            query &= Q(project__category__in=value)

        elif key == 'projectsSubcategory' and len(value) > 0:
            query &= Q(project__subcategory__in=value)

        elif key == 'brands' and len(value) > 0:
            query &= Q(project__brand__in=value)

        elif key == 'projectShortName' and len(value) > 0:
            query &= Q(project__name_short__icontains=value)

        elif key == 'projectStage' and len(value) > 0:
            query &= Q(project__stage__in=value)

        elif key == 'units' and len(value) > 0:
            query &= Q(unit__in=value)

        elif key == 'accounts' and len(value) > 0:
            query &= Q(account__in=value)

        elif key == 'createdAtFrom' and len(value) > 0:
            query &= Q(created_at__gte=value)

        elif key == 'createdAtTo' and len(value) > 0:
            query &= Q(created_at__lte=value)

        elif key == 'createdby' and len(str(value)) > 0:
            query &= Q(created_by=value)

        elif key == 'modifiedAtFrom' and len(value) > 0:
            query &= Q(modified_at__gte=value)

        elif key == 'modifiedAtTo' and len(value) > 0:
            query &= Q(modified_at__lte=value)

        elif key == 'modifiedby' and len(str(value)) > 0:
            query &= Q(modified_by=value)

        elif key == 'isHidden' and len(value) > 0:
            query &= Q(hidden__in=value)

        elif key == 'isEditable' and len(value) > 0:
            query &= Q(is_editable__in=value)

        elif key == 'period' and len(str(value)) > 0:
            query &= Q(period=value)

    filtered = qs.filter(query)
    return filtered