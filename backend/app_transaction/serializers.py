from rest_framework import serializers
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from app_setup import models as app_setup_models

def field_creation(model, exclude_fields, extra_fields):
    all_fields = [f.name for f in model._meta.get_fields()]
    result = [item for item in all_fields if item not in exclude_fields] + extra_fields
    return result


class TransactionSerializer(serializers.ModelSerializer):
    type_display = serializers.ReadOnlyField(source='get_type_display', read_only=True)
    status_display = serializers.ReadOnlyField(source='get_status_display', read_only=True)
    doc_request_display = serializers.ReadOnlyField(source='get_doc_request_display', read_only=True)
    subcontractor_name = serializers.ReadOnlyField(source='subcontractor.name', read_only=True)
    subcontractor_type = serializers.ReadOnlyField(source='subcontractor.type.name', read_only=True)
    subcontractor_taxe_type = serializers.ReadOnlyField(source='subcontractor.taxe_type.name', read_only=True)
    purpose_display = serializers.ReadOnlyField(source='purpose.name', read_only=True)
    date = serializers.ReadOnlyField(source='get_date', read_only=True)
    sat = serializers.ReadOnlyField(source='get_sum_after_taxes', read_only=True)
    taxe = serializers.ReadOnlyField(source='get_sum_taxes', read_only=True)
    pay_model_display = serializers.ReadOnlyField(source='get_pay_model_display', read_only=True)
    project_display = serializers.ReadOnlyField(source='project.name', read_only=True)
    project_category = serializers.ReadOnlyField(source='project.category.name', read_only=True)
    project_subcategory = serializers.ReadOnlyField(source='project.subcategory.name', read_only=True)
    project_brand = serializers.ReadOnlyField(source='project.brand.name', read_only=True)
    project_name_short = serializers.ReadOnlyField(source='project.name_short', read_only=True)
    project_stage = serializers.ReadOnlyField(source='project.get_stage_display', read_only=True)
    unit_display = serializers.ReadOnlyField(source='unit.name', read_only=True)
    account_display = serializers.ReadOnlyField(source='account.name', read_only=True)
    created_by_display = serializers.ReadOnlyField(source='created_by.get_name', read_only=True)
    modified_by_display = serializers.ReadOnlyField(source='modified_by.get_name', read_only=True)
    is_transfer_display = serializers.ReadOnlyField(source='get_is_transfer_display', read_only=True)
    hidden_display = serializers.ReadOnlyField(source='get_hidden_display', read_only=True)
    is_editable_display = serializers.ReadOnlyField(source='get_is_editable_display', read_only=True)

    name = serializers.ReadOnlyField(read_only=True)




    class Meta:
        model = app_transaction_models.Transaction
        fields = field_creation(
            app_transaction_models.Transaction,
            [
                'transaction_agreements',
                'transaction_accounting_docs',
                'parent',
            ],
            [
                'type_display',
                'status_display',
                'doc_request_display',
                'subcontractor_name',
                'subcontractor_type',
                'subcontractor_taxe_type',
                'purpose_display',
                'date',
                'sat',
                'taxe',
                'pay_model',
                'pay_model_display',
                'project_display',
                'project_category',
                'project_subcategory',
                'project_brand',
                'project_name_short',
                'project_stage',
                'unit_display',
                'account_display',
                'account',
                'created_by_display',
                'modified_by_display',
                'is_transfer_display',
                'hidden_display',
                'is_editable_display',

             ] )
        # fields = '__all__'
        # depth = 1

class AccountSerializer(serializers.ModelSerializer):
    currency_name = serializers.ReadOnlyField(source='currency.name', read_only=True)
    company_owner_name = serializers.ReadOnlyField(source='company_owner.name', read_only=True)
    company_owner_taxe_name = serializers.ReadOnlyField(source='company_owner.taxe_type.name', read_only=True)
    company_owner_taxe_multiplicator = serializers.ReadOnlyField(source='company_owner.taxe_type.multiplicator', read_only=True)

    class Meta:
        model = app_transaction_models.Account
        fields = (
            'name',
            'id',
            'currency_name',
            'currency',
            'company_owner_name',
            'company_owner',
            'company_owner_taxe_name',
            'company_owner_taxe_multiplicator',

        )


class PurposeSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_transaction_models.Purpose
        fields = '__all__'

class SubcontractorTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_transaction_models.SubcontractorType
        fields = '__all__'

class CurrencySerializer(serializers.ModelSerializer):

    class Meta:
        model = app_transaction_models.Currency
        fields = '__all__'



class TaxetypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_transaction_models.TaxeType
        fields = '__all__'

class LegalFormSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_transaction_models.Legalform
        fields = '__all__'

class ProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_project_models.Project
        fields = '__all__'

class ProjectCategotySerializer(serializers.ModelSerializer):

    class Meta:
        model = app_project_models.ProjectCategory
        fields = '__all__'

class ProjectSubcategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = app_project_models.ProjectSubcategory
        fields = '__all__'

class BrandSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_project_models.Brand
        fields = '__all__'

class UnitSerializer(serializers.ModelSerializer):

    class Meta:
        model = app_setup_models.Unit
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    user_display = serializers.ReadOnlyField(source='get_name', read_only=True)

    class Meta:
        model = app_setup_models.DBUser
        fields = (
            'id',
            'user_id',
            'user_display',
        )




class SubcontractorSerializer(serializers.ModelSerializer):
    legal_form_name = serializers.ReadOnlyField(source='legal_form.name', read_only=True)
    taxe_type_name = serializers.ReadOnlyField(source='taxe_type.name', read_only=True)
    taxe_type_multiplicator = serializers.ReadOnlyField(source='taxe_type.multiplicator', read_only=True)

    class Meta:
        model = app_transaction_models.Subcontractor
        fields = (
            'name',
            'id',
            'taxe_type_name',
            'taxe_type',
            'taxe_type_multiplicator',
            'legal_form_name',
            'legal_form',
            'pay_delay',
        )


