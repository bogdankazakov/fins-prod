from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render, redirect
from subdomains.utils import reverse as sub_reverse
from django.urls import reverse
from functools import wraps
from app_signup.models import User
from django.http import HttpResponseRedirect
from django.conf import settings
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from app_setup import models as app_setup_models
from django.db.models import Case, When, DateField


def access_app_transaction_section(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to Project section and let in
        '''

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project
        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))
        else:
            return function(request, *args, **kwargs)

    return wrap


def access_app_transactions_list(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to Project section and returns get all function
        '''


        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project


        if 'pk' in kwargs:
            pk = kwargs['pk']
        else:
            pk = None

        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            def get_access_all():
                all = app_transaction_models.Transaction.objects.select_related(
                    'subcontractor__taxe_type',
                    'subcontractor__legal_form',
                ).filter(project__teamprojects_in_project__user=dbuser).annotate(
                    date=Case(
                        When(date_fact=None, then='date_doc'),
                        default='date_fact',
                        output_field=DateField(),
                    )).order_by('date', 'pk')
                return all

            all = get_access_all().values_list('pk', flat=True)
            all = list(all)
            if pk is not None:
                if int(pk) in all:
                    return function(request, get_access_all, *args, **kwargs)
                else:
                    return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))
            else:
                return function(request, get_access_all, *args, **kwargs)

        elif access_level == 3:
            def get_access_all():
                all = app_transaction_models.Transaction.objects.select_related(
                    'subcontractor__taxe_type',
                    'subcontractor__legal_form',
                ).filter(project__teamprojects_in_project__user__unit=dbuser.unit).annotate(
                    date=Case(
                        When(date_fact=None, then='date_doc'),
                        default='date_fact',
                        output_field=DateField(),
                    )).order_by('date', 'pk')
                return all

            all = get_access_all().values_list('pk', flat=True)
            all = list(all)

            if pk is not None:
                if int(pk) in all:
                    return function(request, get_access_all, *args, **kwargs)
                else:
                    return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))
            else:
                return function(request, get_access_all, *args, **kwargs)

        else:
            def get_access_all():
                all = app_transaction_models.Transaction.objects.select_related(
                    'subcontractor__taxe_type',
                    'subcontractor__legal_form',
                ).all().annotate(
                    date=Case(
                        When(date_fact=None, then='date_doc'),
                        default='date_fact',
                        output_field=DateField(),
                    )).order_by('date', 'pk')
                return all
            return function(request, get_access_all, *args, **kwargs)

    return wrap

def access_app_transaction(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to the project
        '''

        pk = kwargs['pk']

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user_id)
        access_level = dbuser.access_app_project
        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            all = app_transaction_models.Transaction.objects.filter(project__teamprojects_in_project__user=dbuser).order_by('-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))


        elif access_level == 3:
            all = app_transaction_models.Transaction.objects.filter(project__teamprojects_in_project__user__unit=dbuser.unit).order_by(
                '-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        else:
            return function(request, *args, **kwargs)

    return wrap

def access_app_transaction_project_C(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to the project
        '''

        pk = kwargs['projectpk']

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project
        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            all = app_project_models.Project.objects.filter(teamprojects_in_project__user=dbuser).order_by('-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))


        elif access_level == 3:
            all = app_project_models.Project.objects.filter(teamprojects_in_project__user__unit=dbuser.unit).order_by(
                '-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        else:
            return function(request, *args, **kwargs)

    return wrap


def access_app_transaction_project_RUD(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to the project
        '''

        pk = kwargs['pk']

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project
        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            all = app_transaction_models.Transaction.objects.filter(project__teamprojects_in_project__user=dbuser).order_by('-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))


        elif access_level == 3:
            all = app_transaction_models.Transaction.objects.filter(project__teamprojects_in_project__user__unit=dbuser.unit).order_by(
                '-pk').values_list('pk', flat=True)
            all = list(all)
            if int(pk) in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        else:
            return function(request, *args, **kwargs)

    return wrap



def access_app_transaction_lister(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to the project
        '''

        name = request.GET.get('name')
        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_project
        if access_level == 0:
            return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        elif access_level == 1:
            all = app_transaction_models.Transaction.objects.filter(project__teamprojects_in_project__user=dbuser).order_by('-pk').values_list('name', flat=True)
            all = list(all)
            if name in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))


        elif access_level == 3:
            all = app_transaction_models.Transaction.objects.filter(project__teamprojects_in_project__user__unit=dbuser.unit).order_by(
                '-pk').values_list('name', flat=True)
            all = list(all)
            if name in all:
                return function(request, *args, **kwargs)
            else:
                return redirect(sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain=request.account.subdomain))

        else:
            return function(request, *args, **kwargs)

    return wrap
