from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from django.db.models import Sum
from decimal import *
from app_transaction.forms import TransactionListFormSet

def extra_data_pre(instance, type):
    instance.is_transfer = True
    instance.project = app_project_models.Project.objects.get(name_short='Внутренние переводы')
    instance.type = type
    instance.purpose = app_transaction_models.Purpose.objects.get(name='Внутренний перевод', type=type)
    instance.date_doc = instance.date_fact
    instance.agreement_start_date = instance.date_fact
    instance.agreement_end_date = instance.date_fact
    instance.pay_delay = 0
    instance.description = 'Перевод денежных стредств между своими счетами без конвертации валют'

    return instance

def numgen(instance):
    """generate transaction number"""
    if instance.pk > 99999:
        instance.name = 'T' + '%012d' % instance.pk
    else:
        instance.name = 'T' + '%06d' % instance.pk
    return instance

def extra_data_post(instance, source, projectpk, request, periodpk):
    """adds extra info to transction"""
    instance = numgen(instance)
    if source == 1 or source == 2:
        if instance.project is None:
            instance.project = app_project_models.Project.objects.get(pk=projectpk)
        if periodpk is not None:
            instance.period = app_project_models.ReportPeriod.objects.get(pk=periodpk)
    return instance

def transfer_extra_data_post(instance_src, instance_rec, form_company):
    """adds extra info to transfer: number, transfer sender pk"""

    instance_src.hidden = form_company.cleaned_data['hidden']
    instance_src.is_editable = form_company.cleaned_data['is_editable']
    instance_rec.hidden = form_company.cleaned_data['hidden']
    instance_rec.is_editable = form_company.cleaned_data['is_editable']


    instance_src = numgen(instance_src)
    instance_src.transfer = instance_rec.pk
    instance_rec = numgen(instance_rec)
    instance_rec.transfer = instance_src.pk

    if instance_src.account.currency == instance_rec.account.currency:
        instance_src.subcontractor = instance_src.account.company_owner
        instance_rec.subcontractor = instance_rec.account.company_owner
    else:
        pass


    return instance_src, instance_rec


def update_form_extra(item, form):
    form.initial['currency'] = item.account.currency
    form.initial['sub_taxetype'] = item.subcontractor.taxe_type
    form.initial['sub_legalform'] = item.subcontractor.legal_form
    return form

def form_converter(form):
    '''
    Т.к. значения сумм мы получаем в формат строки, то мы сначала конвертируем их в decimal.
    а копируем форму, чтобы можно было редактировать в ней значения
    :param form: форма
    :return: формсет с обновленными значениями сумм
    '''

    form.data = form.data.copy()
    form.data['sum_before_taxes'] = converter_float(form.data['sum_before_taxes'])
    form.data['sum_after_taxes'] = converter_float(form.data['sum_after_taxes'])

    return form

def formset_converter(formset):
    '''
    Т.к. значения сумм мы получаем в формат строки, то мы сначала конвертируем их в decimal.
    а копируем форму, чтобы можно было редактировать в ней значения
    :param form: форма
    :return: формсет с обновленными значениями сумм
    '''
    i=0
    for form in formset:
        form.data = form.data.copy()
        name1 = 'form-' + str(i) +'-sum_before_taxes'
        form.data[name1] = converter_float(form.data[name1])
        name2 = 'form-'+ str(i) +'-sum_after_taxes'
        form.data[name2] = converter_float(form.data[name2])
        i+=1
    return formset


def converter_float(x):
    '''
    Конвертирует строки из AutoNumeric в Deciaml
    '''
    str = ''
    for el in x:
        if el != ' ':
            str = str + el
        else:
            pass

    result = ''
    for el in str:
        if el == ',':
            result = result + '.'
        else:
            result = result + el

    result = float(result)
    return result

def summ(qs):
    """
    :param qs:
    :return: calculate transaction sum
    """
    income = qs.filter(type=True).aggregate(Sum('sum_before_taxes'))['sum_before_taxes__sum']
    outcome = qs.filter(type=False).aggregate(Sum('sum_before_taxes'))['sum_before_taxes__sum']
    try:
        sum = income - outcome
    except Exception:
        sum=0
    return sum

def agency_income_creator(instance, projectpk, source):
    project = app_project_models.Project.objects.get(pk=projectpk)
    sum = instance.sum_before_taxes * Decimal(1 + project.commission/100)
    purpose = app_transaction_models.Purpose.objects.get(name='Оплата')
    if source == 2:
        period = instance.period
    else:
        period = None
    income = app_transaction_models.Transaction(
        type=True,
        subcontractor=instance.subcontractor,
        unit=instance.unit,
        account=instance.account,
        date_fact=instance.date_fact,
        date_doc=instance.date_doc,
        agreement_start_date=instance.agreement_start_date,
        agreement_end_date=instance.agreement_end_date,
        pay_delay=instance.pay_delay,
        act_date=instance.act_date,
        description=instance.description,
        comment=instance.comment,
        estimation_link=instance.estimation_link,
        hidden=instance.hidden,
        project=instance.project,
        sum_before_taxes=sum,
        purpose=purpose,
        period=period,
    )
    income.save()
    numgen(income)
    income.save()
    pass


def filter_extra_data(filter, formset):


    i=0

    for transaction in filter.qs:

        #sum after taxes and taxe
        transaction.sat = transaction.get_sum_after_taxes()
        transaction.taxe = transaction.get_sum_taxes()

        # subcontractior
        transaction.subcontractor_full = str(transaction.subcontractor) + ', ' + str(transaction.subcontractor.legal_form)


        #adding forms
        transaction.date_fact_form = formset[i]['date_fact']
        transaction.status_form = formset[i]['status']

        i+=1

    return filter