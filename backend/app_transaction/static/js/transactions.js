$(function () {
    // vars and options ================================
    window.modal = null
    var modal = $("#modal-global");
    var transactions_list = $("#transactions_list");
    window.autoNumericOptions = {
                    digitGroupSeparator        : ' ',
                    decimalCharacter           : ',',
                    decimalCharacterAlternative: '.',
                    emptyInputBehavior: 'zero',
                    // currencySymbol             : 'RUB',
                    // currencySymbolPlacement    : 'p',
                    // roundingMethod             : AutoNumeric.options.roundingMethod.halfUpSymmetric,
                };






     // transaction ajax edit from the list


    var status_conditions = function (user_can_block, editable, status, date_fact, btn_update, btn_delete) {
        if (editable === true || editable === 'True' || user_can_block === '0' && status.val() !== '3'){
                date_fact.prop('disabled', false);
                $(btn_update).prop('hidden', false);
                $(btn_delete).prop('hidden', false);
            }
        else{
                date_fact.prop('disabled', true);
                $(btn_update).prop('hidden', true);
                $(btn_delete).prop('hidden', true);
            }
    };

    var status_conditions2 = function (status, row) {
        if (status.val() === '3'){
                $(row).prop('class', 'payed');
            }
        if (status.val() === '2'){
                $(row).prop('class', 'debt');
            }
        if (status.val() === '1'){
                $(row).prop('class', 'soon');
            }
        if (status.val() === '0'){
                $(row).prop('class', 'topay');
            }

    };

    var list_data_saver = function (event) {
        var data_set = {
            "name": event.data.name,
            "date_fact": event.data.date_fact.val(),
            "status": event.data.status.val()
        };

        var url = $('#transactions_list').attr("data-list-saver");
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            data: data_set,
            beforeSend: function () {
            },
            success: function (data) {
                status_conditions(data.user_can_block, data.editable, event.data.status, event.data.date_fact, event.data.btn_update, event.data.btn_delete);
                status_conditions2(event.data.status, event.data.row)
            }
        });
    };

    var list_editer = function () {
        var rows = $('#transactions_list_tbody tr');
        var rows_lenth = rows.length;

        for (var i = 0; i < rows_lenth; i++) {
            var row = rows[i];
            var name = $(row.getElementsByClassName('name')).text();
            var editable = $(row.getElementsByClassName('editable')).val();
            var user_can_block = $(row.getElementsByClassName('user_can_block')).val();
            var status = $(row.getElementsByClassName('status_form')).find('select');
            var date_fact = $(row.getElementsByClassName('date_fact_form')).find('input');
            var btn_update = $(row.getElementsByClassName('update')).find('button');
            var btn_delete = $(row.getElementsByClassName('delete')).find('button');
            var data = {
                name: name,
                status: status,
                date_fact: date_fact,
                row: row,
                btn_update: btn_update,
                btn_delete: btn_delete
            };

            status.on("change", data, list_data_saver);
            date_fact.on("change", data, list_data_saver);
            status_conditions(user_can_block, editable, status, date_fact, btn_update, btn_delete)

        }

    };

    list_editer();






    var zeroPad = function(num, places) {
            // нули добавляет
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
            };

    /**
     * mode = 0 - create
     * mode = 1 - update
     * modal = 0 - transaction
     * modal = 1 - transfer
     * source = 0 - transaction list
     * source = 1 project card
     */
    var main = function(mode){
        if (window.modal === 0){

            var autonum_init = function () {
                try {
                    id_sum_before_taxes_1 = document.getElementById("id_sum_before_taxes");
                    id_sum_before_taxes = new AutoNumeric(id_sum_before_taxes_1, autoNumericOptions);

                    id_sum_after_taxes_1 = document.getElementById("id_sum_after_taxes");
                    id_sum_after_taxes = new AutoNumeric(id_sum_after_taxes_1, autoNumericOptions);

                    id_sum_taxes_1 = document.getElementById("id_sum_taxes");
                    id_sum_taxes = new AutoNumeric(id_sum_taxes_1, autoNumericOptions);
                } catch (err) {}
              };

            var type_conditions = function () {
                var source_element = $(".modal-content #id_type");
                var id_create_income = $(".modal-content #id_create_income");

                // change unit display
                var id_unit = $(".modal-content #id_unit");
                if (source_element.val() === 'True'){
                    id_unit.prop('disabled', true);
                    id_unit.val(0);
                    id_create_income.prop('hidden', true);
                }
                else {
                    id_unit.prop('disabled', false);
                    id_create_income.prop('hidden', false);
                    }
                sbt_conditions()
              };

            var type_conditions2 = function () {

                // dependant dropdown type - purpose
                var url = $(".modal-content #transactionForm").attr("data-purposes-url");
                var typeId = $(".modal-content #id_type").val();
                  $.ajax({
                    url: url,
                    data: {
                      'type': typeId
                    },
                    success: function (data) {
                      $(".modal-content #id_purpose").html(data);
                    }
                  });

              };

            var sbt_conditions = function () {
                var source_value = id_sum_before_taxes.getNumericString();

                 // check if tranfer modal is open - so take taxe from ajax and not from global var
                if (window.modal === 0){
                    var taxe;
                    var type = $(".modal-content #id_type").val();
                    if (type === "True"){
                        taxe = window.mycompanytaxe;
                    } else{
                        taxe = window.taxe;
                    }

                    // recalculate sum AFTER taxe if sum BEFORE taxe has changed
                    var result_1 = (source_value * taxe).toFixed(2);
                    id_sum_after_taxes.set(result_1);
                    var result_2 = (source_value * taxe - source_value).toFixed(2);
                    id_sum_taxes.set(result_2);
                }
                else{
                    var url = $(".modal-content #transferForm").attr("data-mycompanytaxe-url");
                    var pk = $(".modal-content #id_account").val();
                    $.ajax({
                        url: url,
                        data: {
                          'pk': pk
                        },
                        success: function (data) {
                            var taxe = data.taxevalue;

                            // recalculate sum AFTER taxe if sum BEFORE taxe has changed
                            var result_1 = (source_value * taxe).toFixed(2);
                            id_sum_after_taxes.set(result_1);
                            var result_2 = (source_value * taxe - source_value).toFixed(2);
                            id_sum_taxes.set(result_2);
                        }
                    });
                }
              };

            var sat_conditions = function () {
                var source_value = id_sum_after_taxes.getNumericString();

                // check if tranfer modal is open - so take taxe from ajax and not from global var
                if (window.modal === 0){
                    var type = $(".modal-content #id_type").val();
                    if (type === "True"){
                        var taxe = window.mycompanytaxe;
                    } else{
                        var taxe = window.taxe;
                    }

                    // recalculate sum BEFORE taxe if sum AFTER taxe has changed
                    var result_1 = (source_value / taxe).toFixed(2);
                    id_sum_before_taxes.set(result_1);
                    var result_2 = (source_value - source_value / taxe).toFixed(2);
                    id_sum_taxes.set(result_2);
                }
                else{
                    var url = $(".modal-content #transferForm").attr("data-mycompanytaxe-url");
                    var pk = $(".modal-content #id_account").val();
                    $.ajax({
                        url: url,
                        data: {
                          'pk': pk
                        },
                        success: function (data) {
                            var taxe = data.taxevalue;

                            // recalculate sum BEFORE taxe if sum AFTER taxe has changed
                            var result_1 = (source_value / taxe).toFixed(2);
                            id_sum_before_taxes.set(result_1);
                            var result_2 = (source_value - source_value / taxe).toFixed(2);
                            id_sum_taxes.set(result_2);
                        }
                    });
                }
              };

            /**
             * load taxetype and legal form of subcontractor selected and
             * recalculate sum due to new subcontractor taxes
             */
            var subcontractor_conditions = function () {
                var source_element = $(".modal-content #id_subcontractor");
                var target_element0 = $(".modal-content #id_sub_taxetype");
                var target_element1 = $(".modal-content #id_sub_legalform");
                var target_element2 = $(".modal-content #id_sum_before_taxes");
                var target_element3 = $(".modal-content #id_sum_after_taxes");
                var target_element4 = $(".modal-content #id_sum_taxes");
                var target_element5 = $(".modal-content #id_pay_delay");
                var pk = source_element.val();

                var url = $(".modal-content #transactionForm").attr("data-subcontractor-url");
                  $.ajax({
                    url: url,
                    data: {
                      'pk': pk
                    },
                    success: function (data) {

                        var legalform = data.legalform;
                        var taxetype = data.taxetype;
                        var paydelay = data.paydelay;

                        if (legalform !== null) {
                            target_element0.val(taxetype);
                            target_element1.val(legalform);
                            target_element2.prop('disabled', false);
                            target_element3.prop('disabled', false);
                            target_element5.val(paydelay);
                            taxe = data.taxevalue;
                            }
                        else{
                            target_element0.val(taxetype);
                            target_element1.val(legalform);
                            target_element2.prop('disabled', true);
                            id_sum_before_taxes.set(0);
                            target_element3.prop('disabled', true);
                            target_element5.val(0);
                            id_sum_after_taxes.set(0);
                            id_sum_taxes.set(0);

                            }

                          sbt_conditions();


                    }
                  });


              };

            /**
             * changes date_doc in whichever pay madel, pay delay and argeements dates are selected
             */
            var date_doc = function () {
                var source_element = $(".modal-content #id_agreement_start_date");
                var source_element2 = $(".modal-content #id_agreement_end_date");
                var source_element3 = $(".modal-content #id_pay_delay");
                var source_element4 = $(".modal-content #id_pay_model");
                var target_element = $(".modal-content #id_date_doc");


                if (source_element.val() !== '' && source_element2.val() !== ''){
                    if (parseInt(source_element4.val()) === 0) {
                        var date_start = new Date(source_element.val());
                        var newdate = new Date(date_start);
                        newdate.setDate(parseInt(newdate.getDate() - parseInt(source_element3.val())));
                        }
                    if (parseInt(source_element4.val()) === 1) {
                        var date_end = new Date(source_element2.val());
                        var newdate = new Date(date_end);
                        newdate.setDate(parseInt(newdate.getDate() + parseInt(source_element3.val())));
                        }

                    var dd = zeroPad(newdate.getDate(), 2);
                    var mm = zeroPad((newdate.getMonth() + 1), 2);
                    var y = newdate.getFullYear();

                    var new_doc_transaction_date = y + '-' + mm + '-' + dd ;
                    target_element.val(new_doc_transaction_date);

                    }
                };

            /**
             * load currency of account selected
             */
            var account_conditions = function () {
                var source_element = $(".modal-content #id_account");
                var target_element1 = $(".modal-content #id_currency");
                var target_element2 = $(".modal-content #id_mycompanytaxe");
                var pk = source_element.val();
                var url = $(".modal-content #transactionForm").attr("data-account-url");

                $.ajax({
                    url: url,
                    data: {
                        'modal' : window.modal,
                        'pk': pk
                        },
                    success: function (data) {
                        var currency = data.currency;
                        var taxe = data.taxe;
                        target_element1.val(currency);
                        target_element2.val(taxe);
                        mycompanytaxe = data.taxevalue;

                        }
                    });
                };

            /**
             * init auto function for CREATE(mode=0) and UPDATE(mode=1)
             */
            if (mode === 0){
                autonum_init();
                }
            else{
                autonum_init();
                type_conditions();
                subcontractor_conditions();
                account_conditions();
                }

            modal.on("change", "#id_account", account_conditions);
            modal.on("change", "#id_sum_before_taxes", sbt_conditions);
            modal.on("change", "#id_sum_after_taxes", sat_conditions);
            modal.on("change", "#id_type", type_conditions);
            modal.on("change", "#id_type", type_conditions2);
            modal.on("change", "#id_subcontractor", subcontractor_conditions);
            modal.on("change", "#id_agreement_start_date", date_doc);
            modal.on("change", "#id_agreement_end_date", date_doc);
            modal.on("change", "#id_pay_delay", date_doc);
            modal.on("change", "#id_pay_model", date_doc);

    }
        else{
            /**
             * form-0 - Src
             * form-1 - Rec
             */

            /**
             * Initiate AutoNumbers plugin
             */
            var autonum_init = function () {
                try {
                    id_sum_before_taxes_1 = document.getElementById("id_form-0-sum_before_taxes");
                    id_sum_before_taxes_src = new AutoNumeric(id_sum_before_taxes_1, autoNumericOptions);

                    id_sum_after_taxes_1 = document.getElementById("id_form-0-sum_after_taxes");
                    id_sum_after_taxes_src = new AutoNumeric(id_sum_after_taxes_1, autoNumericOptions);

                    id_sum_taxes_1 = document.getElementById("id_form-0-sum_taxes");
                    id_sum_taxes_src = new AutoNumeric(id_sum_taxes_1, autoNumericOptions);

                    id_sum_before_taxes_2 = document.getElementById("id_form-1-sum_before_taxes");
                    id_sum_before_taxes_rec = new AutoNumeric(id_sum_before_taxes_2, autoNumericOptions);

                    id_sum_after_taxes_2 = document.getElementById("id_form-1-sum_after_taxes");
                    id_sum_after_taxes_rec = new AutoNumeric(id_sum_after_taxes_2, autoNumericOptions);

                    id_sum_taxes_2 = document.getElementById("id_form-1-sum_taxes");
                    id_sum_taxes_rec = new AutoNumeric(id_sum_taxes_2, autoNumericOptions);
                } catch (err) {}

              };

            /**
             * Default input disable.
             * Most elements are disabled in django forms,
             * but those who should change - we disable here. If not on validation error they will be disabled
             * with filled data
             */
            var disable_init = function () {
                $(".modal-content #id_form-0-account").prop('disabled', true);
                $(".modal-content #id_form-1-account").prop('disabled', true);
                $(".modal-content #id_form-0-sum_before_taxes").prop('disabled', true);
                $(".modal-content #id_form-1-sum_before_taxes").prop('disabled', true);
                $(".modal-content #id_form-0-sum_after_taxes").prop('disabled', true);
                $(".modal-content #id_form-1-sum_after_taxes").prop('disabled', true);
              };

             /**
             * load accounts of selected company and sets taxe
             */
            var companies_conditions = function () {
                var source_element = $(".modal-content #id_companies");
                var pk = source_element.val();
                var target_element1 = $(".modal-content #id_form-0-account");
                var target_element2 = $(".modal-content #id_form-1-account");

                // dependant dropdown companies - accounts + setting taxe
                var url = $(".modal-content #transferForm").attr("data-mycompanyinfo-url");
                $.ajax({
                    url: url,
                    data: {
                      'pk': pk
                    },
                    success: function (data) {
                        var taxe = data.taxevalue;
                        if (taxe !== null) {
                            if (target_element1.val() === ''){
                                target_element1.html(data.dropdown);
                                target_element1.prop('disabled', false);
                            }
                            else{
                                var tval1 = target_element1.val();
                                target_element1.html(data.dropdown);
                                target_element1.prop('disabled', false);
                                target_element1.val(tval1);
                            }
                             if (target_element2.val() === ''){
                                target_element2.html(data.dropdown);
                                target_element2.prop('disabled', false);
                            }
                            else{
                                var tval2 = target_element2.val();
                                target_element2.html(data.dropdown);
                                target_element2.prop('disabled', false);
                                target_element2.val(tval2);
                            }
                        }
                        else{
                            target_element1.html(data.dropdown);
                            target_element1.prop('disabled', true);
                            target_element2.html(data.dropdown);
                            target_element2.prop('disabled', true);

                            id_sum_before_taxes_src.set(0);
                            $(".modal-content #id_form-0-sum_before_taxes").prop('disabled', true);
                            id_sum_after_taxes_src.set(0);
                            $(".modal-content #id_form-0-sum_after_taxes").prop('disabled', true);
                            id_sum_taxes_src.set(0);
                            $(".modal-content #id_form-0-sum_taxes").prop('disabled', true);
                            id_sum_before_taxes_rec.set(0);
                            $(".modal-content #id_form-1-sum_before_taxes").prop('disabled', true);
                            id_sum_after_taxes_rec.set(0);
                            $(".modal-content #id_form-1-sum_after_taxes").prop('disabled', true);
                            id_sum_taxes_rec.set(0);
                            $(".modal-content #id_form-1-sum_taxes").prop('disabled', true);
                            }

                        if (mode === 0){
                            window.taxe_src = taxe;
                            window.taxe_rec = taxe;
                            try {
                                id_sum_before_taxes_src.set(0);
                                id_sum_after_taxes_src.set(0);
                                id_sum_taxes_src.set(0);
                                id_sum_before_taxes_rec.set(0);
                                id_sum_after_taxes_rec.set(0);
                                id_sum_taxes_rec.set(0);
                            } catch (err) {}

                        }
                    }
                  });
             };

            /**
             * recalculate sum AFTER taxe if sum BEFORE taxe has changed
             */
            var sbt_conditions_wraper = function (event) {
                var type = event.data.type;
                sbt_conditions(type)
            };
            var sbt_conditions = function (type) {
                if (type === 'src'){
                    var source_element = id_sum_before_taxes_src;
                    var target_element1 = id_sum_after_taxes_src;
                    var target_element2 = id_sum_taxes_src;
                    var pk = $(".modal-content #id_form-0-account").val();
                    var taxe = window.taxe_src
                }
                else{
                    var source_element = id_sum_before_taxes_rec;
                    var target_element1 = id_sum_after_taxes_rec;
                    var target_element2 = id_sum_taxes_rec;
                    var pk = $(".modal-content #id_form-1-account").val();
                    var taxe = window.taxe_rec

                }
                var source_value = source_element.getNumericString();
                var result_1 = (source_value * taxe).toFixed(2);
                target_element1.set(result_1);
                var result_2 = (source_value * taxe - source_value).toFixed(2);
                target_element2.set(result_2);
            };

            /**
             * recalculate sum BEFORE taxe if sum AFTER taxe has changed
             */
            var sat_conditions_wraper = function (event) {
                var type = event.data.type;
                sat_conditions(type)
            };
            var sat_conditions = function (type) {
                if (type === 'src'){
                    var source_element = id_sum_after_taxes_src;
                    var target_element1 = id_sum_before_taxes_src;
                    var target_element2 = id_sum_taxes_src;
                    var pk = $(".modal-content #id_form-0-account").val();
                    var taxe = window.taxe_src
                }
                else{
                    var source_element = id_sum_after_taxes_rec;
                    var target_element1 = id_sum_before_taxes_rec;
                    var target_element2 = id_sum_taxes_rec;
                    var pk = $(".modal-content #id_form-1-account").val();
                    var taxe = window.taxe_rec
                }

                var source_value = source_element.getNumericString();
                var result_1 = (source_value / taxe).toFixed(2);
                target_element1.set(result_1);
                var result_2 = (source_value - source_value / taxe).toFixed(2);
                target_element2.set(result_2);
              };

            /**
             * load currency of account selected
             */
            var account_conditions_wraper = function (event) {
                var type = event.data.type;
                var init = false;
                account_conditions(type,init)
            };
            var account_conditions = function (type, init) {
                if (type === 'src'){
                    var source_element = $(".modal-content #id_form-0-account");
                    var target_element1 = $(".modal-content #id_form-0-currency");
                    var target_element2 = $(".modal-content #id_form-0-sum_before_taxes");
                    var target_element2an = id_sum_before_taxes_src;
                    var target_element3 = $(".modal-content #id_form-0-sum_after_taxes");
                    var target_element3an = id_sum_after_taxes_src;
                    var target_element4an = id_sum_taxes_src;
                    var pk = source_element.val();
                }
                else{
                    var source_element = $(".modal-content #id_form-1-account");
                    var target_element1 = $(".modal-content #id_form-1-currency");
                    var target_element2 = $(".modal-content #id_form-1-sum_before_taxes");
                    var target_element2an = id_sum_before_taxes_rec;
                    var target_element3 = $(".modal-content #id_form-1-sum_after_taxes");
                    var target_element3an = id_sum_after_taxes_rec;
                    var target_element4an = id_sum_taxes_rec;
                    var pk = source_element.val();
                }

                var url = $(".modal-content #transferForm").attr("data-account-url");
                $.ajax({
                    url: url,
                    data: {
                        'pk': pk
                    },
                    success: function (data) {
                        var currency = data.currency
                        if (currency !== null) {
                            target_element1.val(currency);
                            target_element2.prop('disabled', false);
                            target_element3.prop('disabled', false);

                            var cur_1 = $(".modal-content #id_form-0-currency");
                            var cur_2 = $(".modal-content #id_form-1-currency");
                            if (cur_1.val() !== '' && cur_2.val() !== '' && cur_1.val() !== cur_2.val()){
                                $(".modal-content #id_form-0-subcontractor").prop('hidden', false);
                                $(".modal-content #id_form-0-subcontractor").prop('required', true);
                                $(".modal-content #id_form-0-sub_taxetype").prop('hidden', false);
                                $(".modal-content #id_form-0-sub_legalform").prop('hidden', false);

                                $(".modal-content #id_form-1-subcontractor").prop('hidden', false);
                                $(".modal-content #id_form-1-subcontractor").prop('required', true);
                                $(".modal-content #id_form-1-sub_taxetype").prop('hidden', false);
                                $(".modal-content #id_form-1-sub_legalform").prop('hidden', false);

                            }
                            else{
                                $(".modal-content #id_form-0-subcontractor").prop('hidden', true);
                                $(".modal-content #id_form-0-subcontractor").prop('required', false);
                                $(".modal-content #id_form-0-sub_taxetype").prop('hidden', true);
                                $(".modal-content #id_form-0-sub_legalform").prop('hidden', true);

                                $(".modal-content #id_form-1-subcontractor").prop('hidden', true);
                                $(".modal-content #id_form-1-subcontractor").prop('required', false);
                                $(".modal-content #id_form-1-sub_taxetype").prop('hidden', true);
                                $(".modal-content #id_form-1-sub_legalform").prop('hidden', true);

                                if(init === false ) {
                                    $(".modal-content #id_form-0-subcontractor :contains('---------')").attr("selected", "selected");
                                    $(".modal-content #id_form-1-subcontractor :contains('---------')").attr("selected", "selected");
                                    window.taxe_src = data.taxevalue;
                                    window.taxe_rec = data.taxevalue;
                                    sbt_conditions('src');
                                    sbt_conditions('rec');
                                }
                            }
                        }
                        else{
                            target_element1.val(currency);
                            target_element2.prop('disabled', true);
                            target_element2an.set(0);
                            target_element3.prop('disabled', true);
                            target_element3an.set(0);
                            target_element4an.set(0);
                            $(".modal-content #id_form-0-subcontractor").prop('hidden', true);
                            $(".modal-content #id_form-0-sub_taxetype").prop('hidden', true);
                            $(".modal-content #id_form-0-sub_legalform").prop('hidden', true);
                            $(".modal-content #id_form-0-subcontractor").prop('required', false);
                            $(".modal-content #id_form-1-subcontractor").prop('hidden', true);
                            $(".modal-content #id_form-1-sub_taxetype").prop('hidden', true);
                            $(".modal-content #id_form-1-sub_legalform").prop('hidden', true);
                            $(".modal-content #id_form-1-subcontractor").prop('required', false);
                            }
                        }
                    });
                };

            /**
             * load taxetype and legal form of subcontractor selected and
             * recalculate sum due to new subcontractor taxes
             */
            var subcontractor_conditions_wraper = function (event) {
                var type = event.data.type;
                subcontractor_conditions(type)
            };
            var subcontractor_conditions = function (type) {
                if (type === 'src'){
                    var source_element = $(".modal-content #id_form-0-subcontractor");
                    var target_element0 = $(".modal-content #id_form-0-sub_taxetype");
                    var target_element1 = $(".modal-content #id_form-0-sub_legalform");
                }
                else{
                    var source_element = $(".modal-content #id_form-1-subcontractor");
                    var target_element0 = $(".modal-content #id_form-1-sub_taxetype");
                    var target_element1 = $(".modal-content #id_form-1-sub_legalform");
                }
                var pk = source_element.val();
                var url = $(".modal-content #transferForm").attr("data-subcontractor-url");
                  $.ajax({
                    url: url,
                    data: {
                      'pk': pk
                    },
                    success: function (data) {
                        var legalform = data.legalform;
                        var taxetype = data.taxetype;
                        if (legalform !== null) {
                            target_element0.val(taxetype);
                            target_element1.val(legalform);
                            if (type === 'src'){
                                window.taxe_src = data.taxevalue;
                            }
                            else{
                                window.taxe_rec = data.taxevalue;
                            }
                            sbt_conditions(type);
                        }
                        else{
                            target_element0.val(taxetype);
                            target_element1.val(legalform);
                        }
                    }
                  });

              };


            /**
             * init auto function for CREATE(mode=0) and UPDATE(mode=1)
             */
            if (mode === 0){
                try {autonum_init();} catch (err) {}
                try {disable_init();} catch (err) {}
                try {companies_conditions();} catch (err) {}
                }
            else{
                try {autonum_init();} catch (err) {}
                try {companies_conditions();} catch (err) {}
                try {account_conditions('src', true);} catch (err) {}
                try {account_conditions('rec', true);} catch (err) {}
                try {subcontractor_conditions('src');} catch (err) {}
                try {subcontractor_conditions('rec');} catch (err) {}

                // autonum_init();
                // companies_conditions();
                // account_conditions('src', true);
                // account_conditions('rec', true);
                // subcontractor_conditions('src');
                // subcontractor_conditions('rec');
                }

            modal.on("change", "#id_companies", companies_conditions);

            modal.on("change", "#id_form-0-account",{type: 'src' }, account_conditions_wraper);
            modal.on("change", "#id_form-0-sum_before_taxes", {type: 'src' }, sbt_conditions_wraper);
            modal.on("change", "#id_form-0-sum_after_taxes", {type: 'src' }, sat_conditions_wraper);
            modal.on("change", "#id_form-0-subcontractor", {type: 'src' }, subcontractor_conditions_wraper);

            modal.on("change", "#id_form-1-account", {type: 'rec' },account_conditions_wraper);
            modal.on("change", "#id_form-1-sum_before_taxes",{type: 'rec' }, sbt_conditions_wraper);
            modal.on("change", "#id_form-1-sum_after_taxes",{type: 'rec' }, sat_conditions_wraper);
            modal.on("change", "#id_form-1-subcontractor",{type: 'rec' }, subcontractor_conditions_wraper);

    }
};


    // modal load functions ================================

    var loadForm = function () {

        var btn = $(this);
        $.ajax({
          url: btn.attr("data-url"),
          type: 'get',
          dataType: 'json',
          beforeSend: function () {
            $("#modal-global").modal("show");
          },
          success: function (data) {
            $("#modal-global .modal-content").html(data.html_form);
            window.modal = data.modal_info;
            main(0);
          }
        });
    };

    var saveForm = function (event) {
        var is_from_period = event.data.is_from_period;

        var form = $(this);
        var link = new URL(window.location.href);
        var page = link.searchParams.get("page");
        $.ajax({
          url: form.attr("action"),
          data: form.serialize() + "&page=" + page,
          type: form.attr("method"),
          dataType: 'json',
          success: function (data) {
            window.modal = data.modal_info;
            if (data.form_is_valid) {
                if (is_from_period){
                    $("#report_periods").html(data.html_list);
                    $("#messages").html(data.html_partial_project_messages);



                }
                else{
                    $("#transactions_list tbody").html(data.html_list);
                    $("#pagination").html(data.html_pagination);
                    $("#statistics tbody").html(data.html_list_statistics);
                    $("#messages").html(data.html_partial_project_messages);
                    console.log('!!!' + data.html_partial_project_messages)


                }

                $(".js-create-period").click(loadForm_period);
                $(".js-update-period").click(loadForm_period);
                $(".js-delete-period").click(loadForm_period);

                $(".js-create-transaction").click(loadForm);
                transactions_list.on("click", ".js-update-transaction", loadForm_update);
                transactions_list.on("click", ".js-delete-transaction", loadForm);

                $(".js-create-report").click(loadForm_report);
                $(".js-update-report").click(loadForm_report);
                $(".js-delete-report").click(loadForm_report);

                list_editer();

              $("#modal-global").modal("hide");

            }
            else {
              $("#modal-global .modal-content").html(data.html_form);
            }
          },
          error: function () {
              console.log("error")
            }
        });
        return false;
    };

    var loadForm_update = function () {
        var btn = $(this);
        $.ajax({
          url: btn.attr("data-url"),
          type: 'get',
          dataType: 'json',
          beforeSend: function () {
            $("#modal-global").modal("show");
          },
          success: function (data) {
            $("#modal-global .modal-content").html(data.html_form);
            window.modal = data.modal_info;
            main(1);
          }
        });
    };

    // Create transaction from transactions list or from project card
    $(".js-create-transaction").click(loadForm);
    modal.on("submit", ".js-transaction-create-form", {is_from_period:false}, saveForm);
    modal.on("submit", ".js-transaction-create-fromperiod-form", {is_from_period:true},  saveForm);

    // Update transaction from transactions list or from project card
    transactions_list.on("click", ".js-update-transaction", loadForm_update);
    $("#report_periods").on("click", ".js-update-transaction", loadForm_update);
    modal.on("submit", ".js-transaction-update-form", {is_from_period:false} , saveForm);
    modal.on("submit", ".js-transaction-update-fromperiod-form", {is_from_period:true},  saveForm);


    // Delete transaction from transactions list or from project card
    transactions_list.on("click", ".js-delete-transaction", loadForm);
    $("#report_periods").on("click", ".js-delete-transaction", loadForm);
    modal.on("submit", ".js-transaction-delete-form", {is_from_period:false}, saveForm);
    modal.on("submit", ".js-transaction-delete-fromperiod-form", {is_from_period:true},  saveForm);





    var saveForm_transfere = function () {

        var form = $(this);
        var link = new URL(window.location.href);
        var page = link.searchParams.get("page");
        $.ajax({
          url: form.attr("action"),
          data: form.serialize() + "&page=" + page,
          type: form.attr("method"),
          dataType: 'json',
          success: function (data) {
            window.modal = data.modal_info;
            if (data.form_is_valid) {

                $("#transactions_list tbody").html(data.html_list);
                $("#pagination").html(data.html_pagination);
                $("#messages").html(data.html_partial_project_messages);


                list_editer();
              $("#modal-global").modal("hide");

            }
            else {
              $("#modal-global .modal-content").html(data.html_form);
            }
          },
          error: function () {
              console.log("error")
            }
        });
        return false;
    };


    // Create transfer from transactions list
    $(".js-create-transfer").click(loadForm);
    modal.on("submit", ".js-transfer-create-form", saveForm_transfere);

    // Update transfer from transactions list
    transactions_list.on("click", ".js-update-transfer", loadForm_update);
    modal.on("submit", ".js-transfer-update-form",  saveForm_transfere);

    // Delete transfer from transactions list
    transactions_list.on("click", ".js-delete-transfer", loadForm);
    modal.on("submit", ".js-transfer-delete-form", saveForm_transfere);

    // Show history
    var loadList = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-global").modal("show");
      },
      success: function (data) {
        $("#modal-global .modal-content").html(data.html_form);
      }
    });
    };

    $(".js-history-transaction").click(loadList);




    // CRUD periods in project card ================================


    var loadForm_period = function (event) {
        var btn = $(this);
        var url = btn.attr("data-url");
        $.ajax({
          url: url,
          type: 'get',
          dataType: 'json',
          beforeSend: function () {
            $("#modal-global").modal("show");
          },
          success: function (data) {
            $("#modal-global .modal-content").html(data.html_form);

          }
        });
    };

    var saveForm_period = function (event) {
        var form = $(this);
        var url = form.attr("action");

        $.ajax({
          url: url,
          data: form.serialize(),
          type: form.attr("method"),
          dataType: 'json',
          success: function (data) {

                if (data.form_is_valid) {
                    $("#report_periods").html(data.html_list);
                    $("#messages").html(data.html_partial_project_messages);

                    $(".js-create-period").click(loadForm_period);
                    $(".js-update-period").click(loadForm_period);
                    $(".js-delete-period").click(loadForm_period);

                    $(".js-create-transaction").click(loadForm);
                    transactions_list.on("click", ".js-update-transaction", loadForm_update);
                    transactions_list.on("click", ".js-delete-transaction", loadForm);

                    $(".js-create-report").click(loadForm_report);
                    $(".js-update-report").click(loadForm_report);
                    $(".js-delete-report").click(loadForm_report);

                    $("#modal-global").modal("hide");
                }
              },
          error: function () {
              console.log("error")
            }
        });
        return false;
    };
    // Create period
    $(".js-create-period").click(loadForm_period);
    modal.on("submit", ".js-period-create-form", saveForm_period);

    // Update period
    $(".js-update-period").click(loadForm_period);
    modal.on("submit", ".js-period-update-form", saveForm_period);


    // Delete period
    $(".js-delete-period").click(loadForm_period);
    modal.on("submit", ".js-period-delete-form", saveForm_period);




   // CRUD reports in project card ================================


    var loadForm_report = function (event) {
        var btn = $(this);
        var url = btn.attr("data-url");
        $.ajax({
          url: url,
          type: 'get',
          dataType: 'json',
          beforeSend: function () {
            $("#modal-global").modal("show");
          },
          success: function (data) {
            $("#modal-global .modal-content").html(data.html_form);

          }
        });
    };

    var saveForm_report = function (event) {
        var form = $(this);
        var url = form.attr("action");

        $.ajax({
          url: url,
          data: form.serialize(),
          type: form.attr("method"),
          dataType: 'json',
          success: function (data) {

                if (data.form_is_valid) {
                    $("#report_periods").html(data.html_list);

                    $(".js-create-period").click(loadForm_period);
                    $(".js-update-period").click(loadForm_period);
                    $(".js-delete-period").click(loadForm_period);

                    $(".js-create-transaction").click(loadForm);
                    transactions_list.on("click", ".js-update-transaction", loadForm_update);
                    transactions_list.on("click", ".js-delete-transaction", loadForm);

                    $(".js-create-report").click(loadForm_report);
                    $(".js-update-report").click(loadForm_report);
                    $(".js-delete-report").click(loadForm_report);

                    $("#modal-global").modal("hide");
                }
              },
          error: function () {
              console.log("error")
            }
        });
        return false;
    };
    // Create report
    $(".js-create-report").click(loadForm_report);
    modal.on("submit", ".js-report-create-form", saveForm_report);

    // Update report
    $(".js-update-report").click(loadForm_report);
    modal.on("submit", ".js-report-update-form", saveForm_report);


    // Delete report
    $(".js-delete-report").click(loadForm_report);
    modal.on("submit", ".js-report-delete-form", saveForm_report);







    // Show transaction docs


    var load_docs = function (event) {
        var btn = $(this);
        var url = btn.attr("data-url");
        $.ajax({
          url: url,
          type: 'get',
          dataType: 'json',
          beforeSend: function () {
            $("#modal-global").modal("show");
          },
          success: function (data) {
            $("#modal-global .modal-content").html(data.html);

          }
        });
    };


     $(".js-docs-transaction").click(load_docs);










});
