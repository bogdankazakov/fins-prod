from django.core.management.base import BaseCommand, CommandError
from app_signup import models as app_signup_models
from fins.db_setup_test import db_setup, db_setup_converter
class Command(BaseCommand):
    help = 'Populate all accounts databases'

    def add_arguments(self, parser):
        parser.add_argument('account_id', nargs='+', type=int)

    def handle(self, *args, **options):
        self.stdout.write('Starting populating...')

        for id in options['account_id']:
            try:
                account = app_signup_models.Account.objects.get(pk=id)
                name = 'account_' + str(id)
                db_setup(name)
                # db_setup_converter(name)
                self.stdout.write(self.style.SUCCESS('Account "%s" successfully populated' % id))

            except app_signup_models.Account.DoesNotExist:
                self.stdout.write(self.style.ERROR('Account "%s" does not exist' % id))
                # raise CommandError('Account "%s" does not exist' % id)



