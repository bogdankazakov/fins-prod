from django.core.management.base import BaseCommand, CommandError
from app_signup import models as app_signup_models

class Command(BaseCommand):
    help = 'Run migrations for accounts databases'

    def handle(self, *args, **options):
        self.stdout.write('Starting migrations...')

        all_acc = app_signup_models.Account.objects.all()


        for account in all_acc:
            self.stdout.write('Migrating %s...' % account.get_database_name())
            # with use_account(account.id):
            try:
                account.migrate_database()
            except Exception:
                self.stdout.write('Problem %s' % account.get_database_name())
                self.stdout.write('%s' % Exception)
                pass

        self.stdout.write('Migrations finished.')