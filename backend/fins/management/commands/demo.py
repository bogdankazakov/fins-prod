from django.core.management.base import BaseCommand, CommandError
from app_signup import models as app_signup_models
from app_setup import models as app_setup_models
from app_signup.tools import *
from app_signup.db_setup import demo_setup
from fins.db_setup_test import *
from fins.db_setup_test import db_setup, db_setup_converter
import pytz
from datetime import datetime


class Command(BaseCommand):
    help = 'Populate all accounts databases'

    def add_arguments(self, parser):
        parser.add_argument(
            '--create',
            action='store_true',
            help='Create Demo user and db',
        )

        parser.add_argument(
            '--delete',
            action='store_true',
            help='Delete Demo user,db and files',
        )

    def handle(self, *args, **options):

        if options['create']:
            self.stdout.write('Starting creation...')

            demo_account = app_signup_models.Account(
                name='Demo Ltd',
                subdomain='demo',
                is_free=True,
                is_mine=True,
            )
            demo_account.save()
            demo_account.create_database()
            demo_account.fill_database()

            user = app_signup_models.User(
                email='demo@finsapp.ru',
                first_name='Иван',
                last_name='Сидоров',
            )
            user.created_account_pk = demo_account.pk
            user.set_password("testtest1")
            user.save()
            user.account.add(demo_account)


            dbuser = app_setup_models.DBUser.objects.using('account_' + str(demo_account.pk)).get(
                user_id=user.id)
            dbuser.is_owner = True
            dbuser.is_joined = True
            dbuser.joined_at = pytz.utc.localize(datetime.utcnow())
            access_default(dbuser)
            dbuser.save(using=('account_' + str(demo_account.pk)))

            owner_info = app_signup_models.Owner(user= user,account=demo_account)
            owner_info.save()

            demo_setup(demo_account.get_database_name())




            self.stdout.write(self.style.SUCCESS('Created'))
        elif options['delete']:
            self.stdout.write('Starting deleting...')

            demo_account = app_signup_models.Account.objects.get(subdomain='demo')
            user = app_signup_models.User.objects.get(email='demo@finsapp.ru')
            owner_info = app_signup_models.Owner.objects.get(user=user, account=demo_account)

            owner_info.delete()
            demo_account.delete()
            user.delete()


            self.stdout.write(self.style.SUCCESS('Deleted'))
        else:
            self.stdout.write(self.style.SUCCESS('no  tag'))
