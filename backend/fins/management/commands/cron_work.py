from django.db.models import Case, When, DateField
from datetime import datetime, timedelta
from django.core.management.base import BaseCommand, CommandError
from app_signup import models as app_signup_models
from app_project import models as app_project_models
from app_transaction import models as app_transaction_models
import pytz
import logging
from django.template.loader import render_to_string
from django.contrib.sites.shortcuts import get_current_site



logger = logging.getLogger(__name__)

class Command(BaseCommand):

        def handle(self, *args, **options):
            self.stdout.write('Cron started...')
            all_acc = app_signup_models.Account.objects.all()
            delete_list = []

            for account in all_acc:
                account_name = 'account_' + str(account.pk)

                try:
                    transactions = app_transaction_models.Transaction.objects.using(account.get_database_name()).all().annotate(
                        date=Case(
                            When(date_fact=None, then='date_doc'),
                            default='date_fact',
                            output_field=DateField(),
                        )).order_by('date', 'pk')

                    if len(transactions) != 0:
                        for t in transactions:

                            if t.status != 3:
                                # changing Receivables
                                delay = (pytz.utc.localize(datetime.utcnow()).date() - t.date_doc).days
                                if delay > 0:
                                    t.delayed = delay

                                # changing Pay Status
                                if t.date < pytz.utc.localize(datetime.utcnow()).date():
                                    t.status = 2
                                if t.date >= pytz.utc.localize(datetime.utcnow()).date() and t.date <= (pytz.utc.localize(datetime.utcnow()).date() + timedelta(days=14)):
                                    t.status = 1

                            t.save_without_historical_record(using=account_name)

                except Exception:
                    logger.error('Something went wrong in transactions editing in account #{}'.format(account.pk), Exception)
                    # raise CommandError('Something went wrong in transactions editing in account #{}'.format(account.pk), Exception)
                    pass

                try:
                    delay_from_creation = (pytz.utc.localize(datetime.utcnow()).date() - account.created_at.date()).days
                    owner = account.owners.all()[0].user

                    if delay_from_creation == 10 and account.emails.trial_ends_10_is_sent is False:
                        subject = 'До окончания тестового периода осатлось 10 дней'
                        message = render_to_string('app_signup/emails/trial_ends_10.html', {
                            'user': owner,
                        })
                        owner.email_user(subject, message)

                        email_info = account.emails
                        email_info.trial_ends_10_is_sent=True
                        email_info.trial_ends_10_date=pytz.utc.localize(datetime.utcnow())
                        email_info.save()

                    if delay_from_creation == 19 and account.emails.trial_ends_1_is_sent is False:
                        subject = 'До окончания тестового периода осатлся 1 день'
                        message = render_to_string('app_signup/emails/trial_ends_1.html', {
                            'user': owner,
                        })
                        owner.email_user(subject, message)

                        email_info = account.emails
                        email_info.trial_ends_1_is_sent=True
                        email_info.trial_ends_1_date=pytz.utc.localize(datetime.utcnow())
                        email_info.save()

                    if delay_from_creation > 20 and account.is_free is False and account.emails.account_blocked_is_sent is False:
                        subject = 'Ваш аккаунт заблокирован из-за отсутствия оплаты'
                        message = render_to_string('app_signup/emails/account_blocked.html', {
                            'user': owner,
                            'account': account,
                        })
                        owner.email_user(subject, message)

                        email_info = account.emails
                        email_info.account_blocked_is_sent=True
                        email_info.account_blocked_date=pytz.utc.localize(datetime.utcnow())
                        email_info.save()

                        account.is_active = False
                        account.active_state_date = pytz.utc.localize(datetime.utcnow())
                        account.save()


                    # Create list of account to delete manualy
                    if account.is_active is False:
                        if (pytz.utc.localize(datetime.utcnow()).date() - account.active_state_date.date()).days >= 30 :
                            delete_list.append(account)





                except Exception:
                    logger.error('Something went wrong with emails in account #{}'.format(account.pk), Exception)
                    # raise CommandError('Something went wrong with emails in account #{}'.format(account.pk))
                    pass


            #Message to admin: Delete list
            if len(delete_list) != 0:
                admins = app_signup_models.User.objects.filter(is_superuser=True)
                for admin in admins:
                    subject = 'Список аккаунтов для удаления'
                    message = render_to_string('app_signup/emails/accounts_to delete.html', {
                        'delete_list': delete_list,

                    })
                    admin.email_user(subject, message)



            self.stdout.write(self.style.SUCCESS('Cron succefully finished its work' ))



