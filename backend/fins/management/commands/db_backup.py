from datetime import datetime, timedelta
from django.core.management.base import BaseCommand, CommandError
from app_signup import models as app_signup_models

import pytz
import logging
import os
import gzip
from subprocess import Popen

logger = logging.getLogger(__name__)

class Command(BaseCommand):

        def handle(self, *args, **options):
            self.stdout.write('Backup list started...')

            #making account name list
            all_acc_qs = app_signup_models.Account.objects.all()
            all_acc_list = []
            for acc in all_acc_qs:
                all_acc_list.append(acc.get_database_name())


            now = pytz.utc.localize(datetime.utcnow())

            #making file of accout name list
            file_name = 'backup/accountdb/backup_list_'+ now.strftime("%d-%m-%Y-%H-%M") + '.txt'
            with open(file_name, 'w') as the_file:
                for acc in all_acc_list:
                    the_file.write(acc + '\n')
            os.chmod(file_name, 0o777)

            # os.chown(file_name, 'prod_user', 'prod_user')
            # #backup main db
            #
            # file_name = 'backup/backup_maindb_'+ now.strftime("%d-%m-%Y-%H-%M-%S") + '.gz'
            # args = [
            #     'pg_dump',
            #     '-h',
            #     'localhost',
            #     '-U',
            #     'db_main_user',
            #     'fins_prod',
            #     '>',
            #     file_name,
            # ]
            # Popen(args)
            #
            #
            # #backup account db


            self.stdout.write(self.style.SUCCESS('Backup list succefully finished its work' ))



