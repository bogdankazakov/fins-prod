from .threadlocal import *


class TenantRouter(object):
    def db_for_read(self, model, **hints):
        # print('read', model.__module__)

        if model.__module__.startswith('django') or  model.__module__.startswith('auth') or  model.__module__.startswith('app_signup'):
            return 'default'
        return get_thread_local('using_db', 'default')

    def db_for_write(self, model, **hints):
        # print('write', model.__module__)
        if model.__module__.startswith('django') or  model.__module__.startswith('auth') or  model.__module__.startswith('app_signup'):
            return 'default'
        return get_thread_local('using_db', 'default')

    def allow_relation(self, obj1, obj2, **hints):
        return obj1._state.db == obj2._state.db

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        tenant_apps = [
            'app_setup',
            'app_dashboard',
            'app_docs',
            'app_transaction',
            'app_project',
        ]
        if db == 'default':
            if app_label in tenant_apps:
                return False
        else:
            if app_label not in tenant_apps:
                return False
        return True