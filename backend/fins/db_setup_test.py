from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from app_setup import models as app_setup_models
import random
from datetime import datetime, date, time, timedelta
from decimal import *
from app_project.tools import numgen as numgen_p
from app_transaction.tools import numgen as numgen_t


def db_setup(name):

    lf = app_transaction_models.Legalform.objects.using(name).get(name='ООО')
    lf2 = app_transaction_models.Legalform.objects.using(name).get(name='Физ')
    st = app_transaction_models.SubcontractorType.objects.using(name).get(name='Подрядчик')
    st2 = app_transaction_models.SubcontractorType.objects.using(name).get(name='Клиент')
    st3 = app_transaction_models.SubcontractorType.objects.using(name).get(name='Сотрудник')
    st4 = app_transaction_models.SubcontractorType.objects.using(name).get(name='Моя компания')
    tt = app_transaction_models.TaxeType.objects.using(name).get(name='НДС')
    tt2 = app_transaction_models.TaxeType.objects.using(name).get(name='Без налога')

    q = 3
    i=0
    while i < q:
        sub = app_transaction_models.Subcontractor(name='Подрядчик ' + str(i))
        sub.save(using=name)
        sub.type = st
        sub.taxe_type = tt
        sub.legal_form = lf
        sub.save(using=name)
        i+=1


    q = 3
    i=0
    while i < q:
        sub = app_transaction_models.Subcontractor(name='Клиент ' + str(i))
        sub.save(using=name)
        sub.type = st2
        sub.taxe_type = tt
        sub.legal_form = lf
        sub.save(using=name)
        i+=1

    q = 3
    i = 0
    while i < q:
        sub = app_transaction_models.Subcontractor(name='Сотрудник ' + str(i))
        sub.save(using=name)
        sub.type = st3
        sub.taxe_type = tt2
        sub.legal_form = lf2
        sub.save(using=name)
        i += 1

    my = app_transaction_models.Subcontractor(name='Моя компания 2')
    my.save(using=name)
    my.type = st4
    my.taxe_type = tt
    my.legal_form = lf
    my.is_my_company = True
    my.save(using=name)

    cur1 = app_transaction_models.Currency.objects.using(name).get(name='Rub')
    cur2 = app_transaction_models.Currency.objects.using(name).get(name='USD')


    a = app_transaction_models.Account(name='Альфабанк Rub', type=0)
    a.save(using=name)
    a.currency = cur1
    a.company_owner = my
    a.save(using=name)

    a = app_transaction_models.Account(name='Альфабанк USD', type=0)
    a.save(using=name)
    a.currency = cur2
    a.company_owner = my
    a.save(using=name)

    a = app_transaction_models.Account(name='Касса Rub', type=1)
    a.save(using=name)
    a.currency = cur1
    a.company_owner = my
    a.save(using=name)


    def random_date(start, end):
        return start + timedelta(seconds=random.randint(0, int((end - start).total_seconds())))

    q=5
    i=0
    while i<q:
        sum = random.randint(1000, 3000)
        pay_model = random.randint(0, 1)
        pay_delay = random.randint(0, 40)
        agreement_start_date = random_date(date(2012, 12, 14), date(2019, 12, 14))
        agreement_end_date = random_date(agreement_start_date, date(2019, 12, 14))
        act_date = random_date(date(2012, 12, 14), date(2019, 12, 14))
        date_fact = random_date(date(2012, 12, 14), date(2019, 12, 14))
        status = random.randint(0, 3)
        doc_request = random.randint(0, 3)
        if pay_model == 0:
            date_doc = agreement_start_date - timedelta(pay_delay)
        else:
            date_doc = agreement_end_date + timedelta(pay_delay)
        delayed = random.randint(0, 40)

        t = app_transaction_models.Transaction(
            name='T00000'+str(i),
            type=False,
            date_fact=date_fact,
            date_doc=date_doc,
            sum_before_taxes=sum,
            agreement_start_date=agreement_start_date,
            agreement_end_date=agreement_end_date,
            pay_model=pay_model,
            pay_delay=pay_delay,
            delayed=delayed,
            description='test description',
            comment='test comment',
            estimation_link='test estimation lionk',
            act_date=act_date,
            status=status,
            doc_request=doc_request,
        )
        t.save(using=name)
        t.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
        t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Клиент 1')
        t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
        t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
        t.account = app_transaction_models.Account.objects.using(name).all()[0]
        t.save(using=name)
        i+=1

    pass

def db_setup_converter(name):

    lf = app_transaction_models.Legalform.objects.using(name).get(name='ООО')
    lf2 = app_transaction_models.Legalform.objects.using(name).get(name='Физ')
    st = app_transaction_models.SubcontractorType.objects.using(name).get(name='Подрядчик')
    st2 = app_transaction_models.SubcontractorType.objects.using(name).get(name='Клиент')
    st3 = app_transaction_models.SubcontractorType.objects.using(name).get(name='Сотрудник')
    st4 = app_transaction_models.SubcontractorType.objects.using(name).get(name='Моя компания')
    tt = app_transaction_models.TaxeType.objects.using(name).get(name='НДС')
    tt2 = app_transaction_models.TaxeType.objects.using(name).get(name='Без налога')


    sub = app_transaction_models.Subcontractor(name='Подрядчик')
    sub.save(using=name)
    sub.type = st
    sub.taxe_type = tt
    sub.legal_form = lf
    sub.save(using=name)

    my = app_transaction_models.Subcontractor(name='Моя компания 2')
    my.save(using=name)
    my.type = st4
    my.taxe_type = tt
    my.legal_form = lf
    my.is_my_company = True
    my.save(using=name)

    cur1 = app_transaction_models.Currency.objects.using(name).get(name='Rub', is_default=True)
    cur2 = app_transaction_models.Currency.objects.using(name).get(name='USD')
    cur3 = app_transaction_models.Currency(name='EUR')
    cur3.save(using=name)

    rub = app_transaction_models.Account(name='Альфабанк Rub', type=0)
    rub.save(using=name)
    rub.currency = cur1
    rub.company_owner = my
    rub.save(using=name)

    usd = app_transaction_models.Account(name='Альфабанк USD', type=0)
    usd.save(using=name)
    usd.currency = cur2
    usd.company_owner = my
    usd.save(using=name)

    eur = app_transaction_models.Account(name='Альфабанк EUR', type=0)
    eur.save(using=name)
    eur.currency = cur3
    eur.company_owner = my
    eur.save(using=name)

    cat = app_project_models.ProjectCategory.objects.using(name).get(name='Проекты')


    testproj = app_project_models.Project(name_short='Для теста конвертации', is_deletable=False)
    testproj.save(using=name)
    numgen_p(testproj)
    testproj.category = cat
    testproj.save(using=name)


    proj2 = app_project_models.Project(name_short='Другой проект', is_deletable=False)
    proj2.save(using=name)
    numgen_p(proj2)
    proj2.category = cat
    proj2.save(using=name)





    mydate=date(2018, 12, 31)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('5.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)




    mydate=date(2018, 12, 31)
    t_src = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('500.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t_src.save(using=name)
    numgen_t(t_src)
    t_src.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_src.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_src.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_src.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_src.account = rub
    t_src.save(using=name)

    mydate=date(2018, 12, 31)
    t_rec = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('100.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t_rec.save(using=name)
    numgen_t(t_rec)
    t_rec.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_rec.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_rec.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_rec.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_rec.account = usd
    t_rec.save(using=name)

    t_src.is_transfer = True
    t_src.transfer = t_rec.pk
    t_src.save(using=name)
    t_rec.is_transfer = True
    t_rec.transfer = t_src.pk
    t_rec.save(using=name)





    mydate=date(2018, 12, 30)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('30.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate=date(2018, 12, 30)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('10.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)







    mydate=date(2018, 12, 31)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('15.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)







    mydate=date(2019, 1, 1)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('10.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)





    mydate=date(2019, 1, 2)
    t_src = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('700.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t_src.save(using=name)
    numgen_t(t_src)
    t_src.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_src.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_src.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_src.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_src.account = rub
    t_src.save(using=name)

    mydate=date(2019, 1, 2)
    t_rec = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('200.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t_rec.save(using=name)
    numgen_t(t_rec)
    t_rec.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_rec.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_rec.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_rec.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_rec.account = usd
    t_rec.save(using=name)

    t_src.is_transfer = True
    t_src.transfer = t_rec.pk
    t_src.save(using=name)
    t_rec.is_transfer = True
    t_rec.transfer = t_src.pk
    t_rec.save(using=name)





    mydate=date(2019, 1, 3)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('600.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = rub
    t.save(using=name)






    mydate=date(2019, 1, 4)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('15.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate=date(2019, 1, 5)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('500.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = rub
    t.save(using=name)






    mydate=date(2019, 1, 6)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('90.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate=date(2019, 1, 6)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('960.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate=date(2019, 1, 6)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('20.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate=date(2019, 1, 6)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('35.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)







    mydate = date(2019, 1, 7)
    t_src = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('400.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t_src.save(using=name)
    numgen_t(t_src)
    t_src.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_src.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_src.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_src.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_src.account = eur
    t_src.save(using=name)

    mydate = date(2019, 1, 7)
    t_rec = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('130.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t_rec.save(using=name)
    numgen_t(t_rec)
    t_rec.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_rec.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_rec.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_rec.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_rec.account = usd
    t_rec.save(using=name)

    t_src.is_transfer = True
    t_src.transfer = t_rec.pk
    t_src.save(using=name)
    t_rec.is_transfer = True
    t_rec.transfer = t_src.pk
    t_rec.save(using=name)







    mydate=date(2019, 1, 9)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('500.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = rub
    t.save(using=name)







    mydate=date(2019, 1, 10)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('1500.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = rub
    t.save(using=name)







    mydate=date(2019, 1, 11)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('200.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = rub
    t.save(using=name)







    mydate=date(2019, 1, 12)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('30.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = proj2
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate=date(2019, 1, 13)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('200.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate=date(2019, 1, 14)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('100.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=0,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate=date(2019, 1, 15)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('60.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate=date(2019, 1, 16)
    t_src = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('140.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t_src.save(using=name)
    numgen_t(t_src)
    t_src.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_src.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_src.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_src.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_src.account = usd
    t_src.save(using=name)

    mydate=date(2019, 1, 16)
    t_rec = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('420.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t_rec.save(using=name)
    numgen_t(t_rec)
    t_rec.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_rec.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_rec.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_rec.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_rec.account = rub
    t_rec.save(using=name)

    t_src.is_transfer = True
    t_src.transfer = t_rec.pk
    t_src.save(using=name)
    t_rec.is_transfer = True
    t_rec.transfer = t_src.pk
    t_rec.save(using=name)







    mydate=date(2019, 1, 17)
    t_src = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('40.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t_src.save(using=name)
    numgen_t(t_src)
    t_src.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_src.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_src.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_src.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_src.account = usd
    t_src.save(using=name)

    mydate=date(2019, 1, 17)
    t_rec = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('25.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t_rec.save(using=name)
    numgen_t(t_rec)
    t_rec.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_rec.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_rec.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_rec.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_rec.account = eur
    t_rec.save(using=name)

    t_src.is_transfer = True
    t_src.transfer = t_rec.pk
    t_src.save(using=name)
    t_rec.is_transfer = True
    t_rec.transfer = t_src.pk
    t_rec.save(using=name)





    mydate=date(2019, 1, 18)
    t_src = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('5.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t_src.save(using=name)
    numgen_t(t_src)
    t_src.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_src.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_src.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_src.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_src.account = eur
    t_src.save(using=name)

    mydate=date(2019, 1, 18)
    t_rec = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('25.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t_rec.save(using=name)
    numgen_t(t_rec)
    t_rec.project = app_project_models.Project.objects.using(name).get(name_short='Внутренние переводы')
    t_rec.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t_rec.purpose = app_transaction_models.Purpose.objects.using(name).filter(name='Внутренний перевод')[0]
    t_rec.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t_rec.account = usd
    t_rec.save(using=name)

    t_src.is_transfer = True
    t_src.transfer = t_rec.pk
    t_src.save(using=name)
    t_rec.is_transfer = True
    t_rec.transfer = t_src.pk
    t_rec.save(using=name)





    mydate=date(2019, 1, 18)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('75.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)









    mydate=date(2019, 1, 19)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('50.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)






    mydate=date(2019, 1, 20)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('670.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)







    mydate=date(2019, 1, 21)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('5.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)





    mydate=date(2019, 1, 22)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('50.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)









    mydate=date(2019, 1, 23)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('50.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)













    mydate=date(2019, 1, 24)
    t = app_transaction_models.Transaction(
        type=False,
        date_doc=mydate,
        sum_before_taxes=Decimal('20.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Расход')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)













    mydate=date(2019, 1, 25)
    t = app_transaction_models.Transaction(
        type=True,
        date_doc=mydate,
        sum_before_taxes=Decimal('5.00'),
        agreement_start_date=mydate,
        agreement_end_date=mydate,
        status=3,
    )
    t.save(using=name)
    numgen_t(t)
    t.project = testproj
    t.subcontractor = app_transaction_models.Subcontractor.objects.using(name).get(name='Подрядчик')
    t.purpose = app_transaction_models.Purpose.objects.using(name).get(name='Оплата')
    t.unit = app_transaction_models.Unit.objects.using(name).get(name='Client Service')
    t.account = usd
    t.save(using=name)

    pass