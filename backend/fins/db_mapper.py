class DynamicDatabaseMap(object):
    def __init__(self, default, account):
        self.data = {
            'default': default,
            'account': account,
        }

    def __getitem__(self, item):
        if item not in self.data:
            self.load_db(item)
        return self.data[item]

    def __contains__(self, item):
        return item in self.data

    def __iter__(self):
        return iter(self.data)

    def keys(self):
        return self.data.keys()

    def load_db(self, name):
        self.data[name] = {
            'NAME': name,
            'ENGINE': self.data['default']['ENGINE'],
            'USER': self.data['default']['USER'],
            'PASSWORD': self.data['default']['PASSWORD'],
            'HOST': self.data['default']['HOST'],
            'PORT': self.data['default']['PORT']
        }

class DynamicDatabaseMapDBServer(DynamicDatabaseMap):


    def load_db(self, name):
        self.data[name] = {
            'NAME': name,
            'ENGINE': self.data['account']['ENGINE'],
            'USER': self.data['account']['USER'],
            'PASSWORD': self.data['account']['PASSWORD'],
            'HOST': self.data['account']['HOST'],
            'PORT': self.data['account']['PORT']
        }