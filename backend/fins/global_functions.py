class IsDeletableMixin(object):
    """
    allows to check whether model object can be deleated or not
    """

    def can_delete(self):
        # get all the related object
        for rel in self._meta.get_fields():
            if not self.is_deletable:
                return False, None
            try:
                # check if there is a relationship with at least one related object
                related = rel.related_model.objects.filter(**{rel.field.name: self})
                print(related)
                if related.exists():
                    # if there is return a Tuple of flag = False the related_model object
                    return False, related
            except AttributeError:  # an attribute error for field occurs when checking for AutoField
                pass  # just pass as we dont need to check for AutoField
        return True, None

# =============


BOOL_CHOICES_YESNO=((True, "Да"),(False, "Нет"))

# =============


def history_action_verbose(event):
    if str(event) is '+':
        return 'создан'
    elif str(event) is '-':
        return 'удален'
    elif str(event) is '~':
        return 'изменен'
    else:
        pass

def history_wording_sex(sex):
    male_dict ={
        'was':'был'
    }
    female_dict ={
        'was':'была'
    }

    if sex:
        return male_dict
    else:
        return female_dict

def history_get_changes(event):
    curent_event = event
    prev_event = event.prev_record

    model_fields = event._meta.get_fields()
    changes = []
    exeptions = [
        'modified_at',
        'modified_by',
        'created_at',
        'created_by',
        'history_id',
        'history_date',
        'history_change_reason',
        'history_type',
        'history_user_id',

    ]
    for field in model_fields:

        if getattr(curent_event, str(field.name)) != getattr(prev_event, str(field.name)) and str(field.name) not in exeptions:
            difference = 'Значение поля ' \
                         + str(field.verbose_name ) \
                         + ' изменено с ' \
                         + str(getattr(prev_event, str(field.name))) \
                         + ' на ' \
                         + str(getattr(curent_event, str(field.name)))
            changes.append(difference)
    return changes

def history_record(qs, who, sex):
    records = []
    for event in qs:
        try:
            record = '— ' + who + ' <b class="text-primary">' \
                     + str(event.name) \
                     + '</b> ' \
                     + history_wording_sex(sex)['was'] \
                     + ' ' \
                     + history_action_verbose(event.history_type) + ' <span class="font-italic text-dark">' \
                     + str(event.history_date.strftime("%A, %d/%m/%Y %H:%M")) \
                     + '</span> пользователем <span class="font-italic text-dark">' + str(event.history_user) + "</span>"
            if event.history_type is '~':

                changes = ' ' + 'Были следующие изменения: '
                for event in history_get_changes(event):
                    changes = changes + ' ' + str(event) + '. '
                record = record + changes
        except Exception:
            record = ''
        records.append(record)
    return records

from app_signup import models as app_signup_models

def get_db_name(request):
    name = 'account_' + str(request.account.pk)
    return name

def get_db_name_test():
    q = app_signup_models.Account.objects.all().order_by('-pk')[0]
    name = 'account_' + str(q.pk)
    return name