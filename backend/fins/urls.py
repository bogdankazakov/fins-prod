"""fins URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView


urlpatterns = [
    path('', include('app_about.urls')),
    # path('app/', include('app_frontend.urls')),
    path('app/', RedirectView.as_view(url='https://finsapp.ru'), name='app'),
    path('auth/', include('app_auth.urls')),
    path('signup/', include('app_signup.urls')),
    path('setup/', include('app_setup.urls')),
    path('dashboard/', include('app_dashboard.urls')),
    path('projects/', include('app_project.urls')),
    path('transactions/', include('app_transaction.urls')),
    path('docs/', include('app_docs.urls')),
    path('reports/', include('app_reports.urls')),
    path('admin/', admin.site.urls, name='admin'),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    # import debug_toolbar
    # urlpatterns = [
    #     path('__debug__/', include(debug_toolbar.urls)),
    #
    #     # For django versions before 2.0:
    #     # url(r'^__debug__/', include(debug_toolbar.urls)),
    #
    # ] + urlpatterns