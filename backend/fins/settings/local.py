from fins.settings.base import *
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
from  fins.db_mapper import DynamicDatabaseMapDBServer


SECRET_KEY = os.environ.get("SECRET_KEY")
ALLOWED_HOSTS = os.environ.get("DJANGO_ALLOWED_HOSTS").split(" ")


MAIN_DB = {
    'ENGINE': os.environ.get("DB_ENGINE"),
    'NAME': os.environ.get("DB_NAME"),
    'USER': os.environ.get("DB_USER"),
    'PASSWORD': os.environ.get("DB_PASSWORD"),
    'HOST': os.environ.get("DB_HOST"),
    'PORT': int(os.environ.get("DB_PORT")),
}

ACCOUNTS_DB = {
    'ENGINE': os.environ.get("DB_ENGINE"),
    'NAME': os.environ.get("DB_NAME"),
    'USER': os.environ.get("DB_USER"),
    'PASSWORD': os.environ.get("DB_PASSWORD"),
    'HOST': os.environ.get("DB_HOST"),
    'PORT': int(os.environ.get("DB_PORT")),
}

INTERNAL_IPS = [
    '127.0.0.1',
]

DATABASES = DynamicDatabaseMapDBServer(MAIN_DB, ACCOUNTS_DB)

DATABASE_ROUTERS = ['fins.routers.TenantRouter']

SESSION_COOKIE_DOMAIN='.fins.my'
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'



MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'