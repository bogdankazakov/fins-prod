from fins.settings.base import *
import os
from  fins.db_mapper import DynamicDatabaseMapDBServer

SECRET_KEY = os.environ.get("SECRET_KEY")
DEBUG = False
ALLOWED_HOSTS = os.environ.get("DJANGO_ALLOWED_HOSTS").split(" ")


MAIN_DB = {
    'ENGINE': os.environ.get("DB_ENGINE"),
    'NAME': os.environ.get("DB_NAME"),
    'USER': os.environ.get("DB_USER"),
    'PASSWORD': os.environ.get("DB_PASSWORD"),
    'HOST': os.environ.get("DB_HOST"),
    'PORT': int(os.environ.get("DB_PORT")),
}

ACCOUNTS_DB = {
    'ENGINE': os.environ.get("DB_ENGINE"),
    'NAME': os.environ.get("DB_NAME"),
    'USER': os.environ.get("DB_USER"),
    'PASSWORD': os.environ.get("DB_PASSWORD"),
    'HOST': os.environ.get("DB_HOST"),
    'PORT': int(os.environ.get("DB_PORT")),
}

DATABASES = DynamicDatabaseMapDBServer(MAIN_DB, ACCOUNTS_DB)

DATABASE_ROUTERS = ['fins.routers.TenantRouter']


# MEDIA_ROOT = os.path.join('/mnt/fins_app_media', 'media')
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'



SESSION_COOKIE_DOMAIN='.' + os.environ.get("DOMAIN")

SENDGRID_API_KEY="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"
EMAIL_BACKEND = "sendgrid_backend.SendgridBackend"

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

sentry_sdk.init(
    dsn="https://c658facb56df4c5b8f757452b933b782@sentry.io/1387026",
    integrations=[DjangoIntegration()]
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
            'filters': ['require_debug_true'],

        },
        'file': {
            'level': 'WARNING',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'verbose',
            'filename': os.path.join(BASE_DIR, 'logs/django.log'),
            'maxBytes': 1024*1024*15,
            'backupCount': 10,
        },

    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
        '': {
            'level': 'WARNING',
            'handlers': ['file',],
            'propagate': True,
        },
        'django.security.DisallowedHost': {
            'handlers': ['null'],
            'propagate': False,
        },
    }
}
