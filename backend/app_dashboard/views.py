from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from app_auth.decorators import access_company_level
from app_dashboard.decorators import *

from app_setup import models as app_setup_models
from app_signup import models as app_signup_models
import logging

logger = logging.getLogger(__name__)

@login_required
@access_company_level
def dashboard(request):
    company = request.account.name
    print('мой субдомен', request.subdomain)
    #
    # all = app_transaction_models.Transaction.objects.select_related('subcontractor__taxe_type').get(
    #     name='T000019')

    #
    #
    #
    # print(all.subcontractor)
    #
    # print(all.subcontractor.taxe_type)
    #
    # # print(all.subcontractor.taxe_type.multiplicator)


    context = {
        'company':company,
        'subdomain':request.subdomain,

    }

    return render(request, 'app_dashboard/dashboard.html', context)