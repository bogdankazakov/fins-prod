from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render, redirect
from subdomains.utils import reverse as sub_reverse
from django.urls import reverse
from functools import wraps
from app_signup.models import User
from django.http import HttpResponseRedirect
from django.conf import settings
from app_transaction import models as app_transaction_models
from app_setup import models as app_setup_models


def access_app_dashboard(function):
    @wraps(function)
    def wrap(request, *args, **kwargs):
        '''
        Checks if user has access to Dashboard section

        
        '''

        dbuser = app_setup_models.DBUser.objects.get(user_id=request.user.id)
        access_level = dbuser.access_app_dashboard
        if access_level == 1:
            return function(request, *args, **kwargs)
        else:
            return function(request, *args, **kwargs)

    return wrap
