from django.test import TestCase, Client, RequestFactory
from app_signup import models as app_signup_models
from django.contrib.sites.models import Site
from subdomains.utils import reverse as sub_reverse
from django.conf import settings
from app_auth.tests import load_url_pattern_names
from fins import urls  as fins_url
import psycopg2
from fins.threadlocal import thread_local
from app_setup import views as app_setup_views
from app_transaction import models as app_transaction_models
from django.contrib.messages.storage.fallback import FallbackStorage
from app_setup import models as app_setup_models
from fins.global_functions import get_db_name_test as get_db_name


class AccessPermissionsTest(TestCase):


    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()

        site = Site.objects.get(pk=1)
        site.domain = 'testserver'
        site.save()

        c = app_signup_models.Account(name='MyCompany', subdomain='mysubdomain')
        c.save()
        c.create_database()
        c.fill_database()

        user = app_signup_models.User(
            first_name='UserFirst',
            last_name='UserLast',
            email='b@b.ru')
        user.set_password('123')
        user.created_account_pk = c.pk
        user.save()
        user.account.add(c)


        self.my_user = user
        self.account = c
        self.dbuser = app_setup_models.DBUser.objects.using(get_db_name()).get(
            user_id=user.pk)

        #fill db ... stupidly)

        self.client.login(username='b@b.ru', password='123')
        link = sub_reverse('app_dashboard:index',subdomain='mysubdomain')
        request = self.factory.get(link)
        request.user = self.my_user
        request.account = self.account
        db_name_1 = get_db_name()

        @thread_local(using_db=db_name_1)
        def tread(request):
            response = app_setup_views.subcontractor_create(request)
            return response

    def tearDown(self):

        #   we delete created account db
        connection_params = {
            'database': settings.MAIN_DB['NAME'],
            'host': settings.MAIN_DB['HOST'],
            'user': settings.MAIN_DB['USER'],
            'password': settings.MAIN_DB['PASSWORD']
        }
        connection = psycopg2.connect(**connection_params)
        connection.set_isolation_level(
            psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT
        )
        cursor = connection.cursor()

        for obj in app_signup_models.Account.objects.all():
            name = 'account_' + str(obj.pk)

            cursor.execute("SELECT pg_terminate_backend(pg_stat_activity.pid)"
                           " FROM pg_stat_activity"
                           " WHERE pg_stat_activity.datname = '%s' AND pid <> pg_backend_pid();" % name)
            cursor.execute('DROP DATABASE IF EXISTS "%s"' % name)
        connection.close()

    def test_access_permission(self):
        """
        test app_setup access and redirect if denied
        Exceptions are given in exception list

        """

        Exeption_app_list_default = ['admin', ]
        Exeption_app_list_no_login = ['app_about', 'app_auth', 'app_signup',]
        Exeption_name_list_default = []
        Exeption_name_list_no_login = []
        App_name = ['app_reports']


        for item in load_url_pattern_names(fins_url.urlpatterns,
                                           Exeption_app_list_default, Exeption_app_list_no_login,
                                           Exeption_name_list_default, Exeption_name_list_no_login, App_name):
            self.client.login(username='b@b.ru', password='123')

            # tests access permited
            self.dbuser.access_app_reports = True
            self.dbuser.save(using=get_db_name())
            response = self.client.get(
                str(item), HTTP_HOST='mysubdomain.testserver')
            print(str(item) + ' - start access level 2')
            self.assertEqual(response.status_code, 200)
            print(str(item) + ' - checked access level 2')

            # tests access level denied
            self.dbuser.access_app_reports = False
            self.dbuser.save(using=get_db_name())
            response = self.client.get(
                str(item), HTTP_HOST='mysubdomain.testserver', follow=True)
            print(str(item) + ' - start access level 0')
            self.assertRedirects(response,
                                 sub_reverse(settings.ACCESS_DENIED_TO_APP_REDIRECT_URL, subdomain='mysubdomain'),
                                 status_code=302, target_status_code=200,
                                 msg_prefix='', fetch_redirect_response=True)
            print(str(item) + ' - checked access level 0')