from django.forms import ModelForm, Form
from app_signup import models as app_signup_models
from app_setup import models as app_setup_models
from app_transaction import models as app_transaction_models
from app_project import models as app_project_models
from app_docs import models as app_docs_models
from django import forms
from django.utils.translation import gettext as _

GRID_CHOICES = (
    (1, _("День")),
    (2, _("Неделя")),
    (3, _("Месяц")),
    (4, _("Квартал")),
    (5, _("Год"))
)

WEEKS_CHOICES = (
    (2, ("2 недели")),
    (4, ("4 недели")),
    (6, ("6 недель")),
    (8, ("8 недель")),
    (12, ("12 недель")),


)
MONTH_CHOICES = (
    (1, ("Январь")),
    (2, ("Февраль")),
    (3, ("Март")),
    (4, ("Апрель")),
    (5, ("Май")),
    (6, ("Июнь")),
    (7, ("Июль")),
    (8, ("Август")),
    (9, ("Сентябюрь")),
    (10, ("Октябрь")),
    (11, ("Ноябрь")),
    (12, ("Декабрь")),

)

Q_CHOICES = (
    (1, ("I")),
    (2, ("II")),
    (3, ("III")),
    (4, ("IV")),

)

YEAR_CHOICES = (
    (2018, ("2018")),
    (2019, ("2019")),
    (2020, ("2020")),
    (2021, ("2021")),
    (2021, ("2022")),
    (2021, ("2023")),
    (2021, ("2024")),
    (2021, ("2025")),

)


GROUP_CHOICES_REVENU = (
    (6, ("По Категориям / Доход-Расход" )),
    (7, ("По Категориям / Субкатегориям / Доход-Расход")),
    (8, ("По Категориям / Субкатегориям / Проектам / Доход-Расход")),

)
PAY_CHOICES = (
    (1, ("Все")),
    (2, ("Оплаченные")),
    (3, ("Запланированные")),
)
TAXE_CHOICES = (
    (0, ("Без налогов")),
    (1, ("С налогами")),
)

STAGES_CHOICES = (
    (0, 'Новый'),
    (1, 'Оценка'),
    (2, 'Продажа'),
    (3, 'Реализация'),
    (4, 'Завершен'),
    (5, 'Не продан'),
)

class BaseForm(Form):

    grid = forms.ChoiceField(
        label='Шаг',
        required=True,
        choices=GRID_CHOICES,
        widget=forms.Select(
            attrs={
                'v-model': 'grid',
            }),
        initial={'grid': '1'}

    )

    dayFrom = forms.DateField(
        label='c даты',
        required=False,
        widget=forms.TextInput(
            attrs={
                'type': 'date',
                ':required': 'day',
                'v-model': 'dayFrom',
            })
    )

    weekFrom = forms.DateField(
        label='недель c дня',
        required=False,
        widget=forms.TextInput(
            attrs={
                'type': 'date',
                ':required': 'week',
                'v-model': 'weekFrom',
            })
    )


    weekIntervalFrom = forms.ChoiceField(
        label='количество недель',
        required=False,
        choices=WEEKS_CHOICES,
        widget=forms.Select(
            attrs={
                ':required': 'week',
                'v-model': 'weekIntervalFrom',
            }),

    )


    monthFrom = forms.ChoiceField(
        label='c месяца',
        required=False,
        choices=MONTH_CHOICES,
        widget=forms.Select(
            attrs={
                ':required': 'month',
                'v-model': 'monthFrom',
            }),

    )

    monthYearFrom = forms.ChoiceField(
        label='c года месяца',
        required=False,
        choices=YEAR_CHOICES,
        widget=forms.Select(
            attrs={
                ':required': 'month',
                'v-model': 'monthYearFrom',
            }),

    )

    quartalFrom = forms.ChoiceField(
        label='c квартала',
        required=False,
        choices=Q_CHOICES,
        widget=forms.Select(
            attrs={
                ':required': 'quartal',
                'v-model': 'quartalFrom',
            }),

    )

    quartalYearFrom = forms.ChoiceField(
        label='c года квартала',
        required=False,
        choices=YEAR_CHOICES,
        widget=forms.Select(
            attrs={
                ':required': 'quartal',
                'v-model': 'quartalYearFrom',
            }),

    )

    yearFrom = forms.ChoiceField(
        label='c года',
        required=False,
        choices=YEAR_CHOICES,
        widget=forms.Select(
            attrs={
                ':required': 'year',
                'v-model': 'yearFrom',
            }),

    )

    dayTo = forms.DateField(
        label='по дату',
        required=False,
        widget=forms.TextInput(
            attrs={
                'type': 'date',
                ':required':'day',
                'v-model': 'dayTo',
            })
    )

    monthTo = forms.ChoiceField(
        label='по месяц',
        required=False,
        choices=MONTH_CHOICES,
        widget=forms.Select(
            attrs={
                ':required': 'month',
                'v-model': 'monthTo',
            }),

    )

    monthYearTo = forms.ChoiceField(
        label='по год месяца',
        required=False,
        choices=YEAR_CHOICES,
        widget=forms.Select(
            attrs={
                ':required': 'month',
                'v-model': 'monthYearTo',
            }),

    )

    quartalTo = forms.ChoiceField(
        label='по квартал',
        required=False,
        choices=Q_CHOICES,
        widget=forms.Select(
            attrs={
                ':required': 'quartal',
                'v-model': 'quartalTo',
            }),

    )

    quartalYearTo = forms.ChoiceField(
        label='по год квартала',
        required=False,
        choices=YEAR_CHOICES,
        widget=forms.Select(
            attrs={
                ':required': 'quartal',
                'v-model': 'quartalYearTo',
            }),

    )

    yearTo = forms.ChoiceField(
        label='по год',
        required=False,
        choices=YEAR_CHOICES,
        widget=forms.Select(
            attrs={
                ':required': 'quartal',
                'v-model': 'yearTo',
            }),

    )

class CashflowForm(BaseForm):


    pay_status = forms.ChoiceField(
        label='статус платежа',
        required=True,
        choices=PAY_CHOICES,
        widget=forms.Select(
            attrs={
                'v-model': 'pay_status',
            }),

    )

    taxes = forms.ChoiceField(
        label='налоги',
        required=True,
        choices=TAXE_CHOICES,
        widget=forms.Select(
            attrs={
                'v-model': 'taxes',
            }),

    )

class PLForm(BaseForm):


    stage = forms.MultipleChoiceField(
        label='этап проекта',
        required=True,
        choices=STAGES_CHOICES,
        widget=forms.SelectMultiple(
            attrs={
                'v-model': 'stage',
            }),

    )

    taxes = forms.ChoiceField(
        label='налоги',
        required=True,
        choices=TAXE_CHOICES,
        widget=forms.Select(
            attrs={
                'v-model': 'taxes',
            }),

    )


class SubcontractorsForm(BaseForm):


    pay_status = forms.ChoiceField(
        label='статус платежа',
        required=True,
        choices=PAY_CHOICES,
        widget=forms.Select(
            attrs={
                'v-model': 'pay_status',
            }),

    )

    taxes = forms.ChoiceField(
        label='налоги',
        required=True,
        choices=TAXE_CHOICES,
        widget=forms.Select(
            attrs={
                'v-model': 'taxes',
            }),

    )

class BrandsForm(BaseForm):


    pay_status = forms.ChoiceField(
        label='статус платежа',
        required=True,
        choices=PAY_CHOICES,
        widget=forms.Select(
            attrs={
                'v-model': 'pay_status',
            }),

    )

    taxes = forms.ChoiceField(
        label='налоги',
        required=True,
        choices=TAXE_CHOICES,
        widget=forms.Select(
            attrs={
                'v-model': 'taxes',
            }),

    )