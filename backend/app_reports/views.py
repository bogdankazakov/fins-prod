from django.contrib.auth.decorators import login_required
from app_auth.decorators import access_company_level
import logging
from app_reports.decorators import *
from app_reports.builders.cashflow import RequestFilter as CashflowRequestFilter, ReportBuilder as CashflowReportBuilder,ReportFormater as CashflowReportFormater
from app_reports.builders.pl import RequestFilter as PLRequestFilter, ReportBuilder as PLReportBuilder, ReportFormater as PLReportFormater
from app_reports.builders.subcontractors import RequestFilter as SubcontractorsRequestFilter, ReportBuilder as SubcontractorsReportBuilder, ReportFormater as SubcontractorsReportFormater
from app_reports.builders.brands import RequestFilter as BrandsRequestFilter, ReportBuilder as BrandsReportBuilder, ReportFormater as BrandsReportFormater
import json
from app_reports.forms import *
from django.db.models import Q, Sum, F
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

logger = logging.getLogger(__name__)



# @login_required
# @access_company_level
# @access_app_reports
# def index(request):
#     my_companies = app_transaction_models.Subcontractor.objects.filter(is_my_company=True)
#     list=[]
#     i=0
#     for company in my_companies:
#         acc_list=[]
#         accounts = company.subs_accounts.all()
#         list.append(dict(name=company.name))
#         for account in accounts:
#             sum = app_transaction_models.Transaction.objects.filter(account=account).aggregate(Sum('sum_before_taxes'))
#             sum = sum['sum_before_taxes__sum']
#             acc_list.append(dict(name=account.name, sum = sum))
#
#         list[i].update(accounts=acc_list)
#         i+=1
#     context = {
#         'list':list,
#     }
#
#     return render(request, 'app_reports/index.html', context)

#
# @login_required
# @access_company_level
# @access_app_reports
# def cashflow(request):
#     if request.method == 'POST':
#         form = CashflowForm(request.POST)
#         if form.is_valid():
#             report_request = CashflowRequestFilter(form)
#             report_raw = CashflowReportBuilder(report_request).raw()
#             report_formated = CashflowReportFormater(report_raw, report_request).structure()
#         context = {
#             'form': form,
#             'report': json.dumps(report_formated, ensure_ascii=False, default=str),
#             'initial': json.dumps(dict(request.POST.lists())),
#         }
#     else:
#         form = CashflowForm()
#         context = {
#             'form':form
#         }
#
#     return render(request, 'app_reports/cashflow.html', context)


@api_view(['GET', 'POST'])
@login_required
@access_company_level
@access_app_reports
def cashflow(request):
    if request.method == 'POST':
        report_request = CashflowRequestFilter(request.data)
        report_raw = CashflowReportBuilder(report_request).raw()
        report_formated = CashflowReportFormater(report_raw, report_request).structure()
        data = {'report': json.dumps(report_formated, ensure_ascii=False, default=str)}
        return Response(data, status=status.HTTP_200_OK)


#
# @login_required
# @access_company_level
# @access_app_reports
# def pl(request):
#     if request.method == 'POST':
#         form = PLForm(request.POST)
#         if form.is_valid():
#             report_request = PLRequestFilter(form)
#             report_raw = PLReportBuilder(report_request).raw()
#             report_formated = PLReportFormater(report_raw, report_request).structure()
#         context = {
#             'form': form,
#             'report': json.dumps(report_formated, ensure_ascii=False, default=str),
#             'initial': json.dumps(dict(request.POST.lists())) ,
#         }
#         print(context['initial'])
#
#     else:
#         form = PLForm()
#         context = {
#             'form':form
#         }
#
#     return render(request, 'app_reports/pl.html', context)

@api_view(['GET', 'POST'])
@login_required
@access_company_level
@access_app_reports
def pl(request):
    if request.method == 'POST':
        report_request = PLRequestFilter(request.data)
        report_raw = PLReportBuilder(report_request).raw()
        report_formated = PLReportFormater(report_raw, report_request).structure()
        data = {'report': json.dumps(report_formated, ensure_ascii=False, default=str)}
        return Response(data, status=status.HTTP_200_OK)



#
# @login_required
# @access_company_level
# @access_app_reports
# def subcontractors(request):
#     if request.method == 'POST':
#         form = SubcontractorsForm(request.POST)
#         if form.is_valid():
#             report_request = SubcontractorsRequestFilter(form)
#             report_raw = SubcontractorsReportBuilder(report_request).raw()
#             report_formated = SubcontractorsReportFormater(report_raw, report_request).structure()
#         context = {
#             'form': form,
#             'report': json.dumps(report_formated, ensure_ascii=False, default=str),
#             'initial': json.dumps(dict(request.POST.lists())),
#         }
#     else:
#         form = SubcontractorsForm()
#         context = {
#             'form':form
#         }
#
#     return render(request, 'app_reports/subcontractors.html', context)
#

@api_view(['GET', 'POST'])
@login_required
@access_company_level
@access_app_reports
def subcontractors(request):
    if request.method == 'POST':
        report_request = SubcontractorsRequestFilter(request.data)
        report_raw = SubcontractorsReportBuilder(report_request).raw()
        report_formated = SubcontractorsReportFormater(report_raw, report_request).structure()
        data = {'report': json.dumps(report_formated, ensure_ascii=False, default=str)}
        return Response(data, status=status.HTTP_200_OK)

#
#
# @login_required
# @access_company_level
# @access_app_reports
# def brands(request):
#     if request.method == 'POST':
#         form = BrandsForm(request.POST)
#         if form.is_valid():
#             report_request = BrandsRequestFilter(form)
#             report_raw = BrandsReportBuilder(report_request).raw()
#             report_formated = BrandsReportFormater(report_raw, report_request).structure()
#         context = {
#             'form': form,
#             'report': json.dumps(report_formated, ensure_ascii=False, default=str),
#             'initial': json.dumps(dict(request.POST.lists())),
#         }
#     else:
#         form = BrandsForm()
#         context = {
#             'form':form
#         }
#
#     return render(request, 'app_reports/brands.html', context)



@api_view(['GET', 'POST'])
@login_required
@access_company_level
@access_app_reports
def brands(request):
    if request.method == 'POST':
        report_request = BrandsRequestFilter(request.data)
        report_raw = BrandsReportBuilder(report_request).raw()
        report_formated = BrandsReportFormater(report_raw, report_request).structure()
        data = {'report': json.dumps(report_formated, ensure_ascii=False, default=str)}
        return Response(data, status=status.HTTP_200_OK)