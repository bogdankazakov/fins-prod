from datetime import datetime, date
from dateutil.relativedelta import *
from django.db.models import Q, Sum, F
from app_transaction.models import Transaction
from app_project.models import ProjectCategory, ProjectSubcategory, Project
from django.db.models import Case, When, DateField, DecimalField
from operator import itemgetter as i
from functools import cmp_to_key
from decimal import *


class RequestFilter():
    def __init__(self, rawdata):
        # rawdata = form.cleaned_data
        print(rawdata)

        if int(rawdata['taxes']) == 0:
            self.taxeIncl = False
        if int(rawdata['taxes']) == 1:
            self.taxeIncl = True


        rawdata['stage'] = self.tointloop(rawdata['stage'])
        if len(rawdata['stage']) == 0:
            self.stage = Q()
        else:
            self.stage = Q(project__stage__in=rawdata['stage'])


        if rawdata['grid'] == '1':
            self.dateFrom = datetime.strptime(rawdata['dayFrom'], '%Y-%m-%d')
            self.dateDelta = relativedelta(days = 1)
            self.dateTo = datetime.strptime(rawdata['dayTo'], '%Y-%m-%d')
        if rawdata['grid'] == '2':
            self.dateFrom = datetime.strptime(rawdata['weekFrom'], '%Y-%m-%d')
            self.dateDelta = relativedelta(weeks=1)
            self.dateTo = self.dateFrom + self.dateDelta * int(rawdata['weekIntervalFrom']) - relativedelta(days=1)
        if rawdata['grid'] == '3':
            self.dateFrom = date(int(rawdata['monthYearFrom']), int(rawdata['monthFrom']), 1)
            self.dateDelta = relativedelta(months=1)
            self.dateTo = date(int(rawdata['monthYearTo']), int(rawdata['monthTo']), 1) + relativedelta(months=1, days=-1)
        if rawdata['grid'] == '4':
            self.dateFrom = date(int(rawdata['quartalYearFrom']), int(rawdata['quartalFrom']) * 3 - 2, 1)
            self.dateDelta = relativedelta(months=3)
            self.dateTo = date(int(rawdata['quartalYearTo']), int(rawdata['quartalTo']) * 3 , 1) + relativedelta(months=1, days=-1)
        if rawdata['grid'] == '5':
            self.dateFrom = date(int(rawdata['yearFrom']), 1, 1)
            self.dateDelta = relativedelta(years=1)
            self.dateTo = date(int(rawdata['yearTo']), 12 , 1) + relativedelta(months=1, days=-1)


    def tointloop(self, list):
        newlist = []
        for item in list:
            newlist.append(int(item))
        return newlist

    def rawdata(self):
        return self.dateFrom, self.dateTo, self.dateDelta, self.stage, self.taxeIncl


class ReportBuilder():

    def __init__(self, report_request):
        self.dateFrom, self.dateTo, self.dateDelta, self.stage, self.taxeIncl = report_request.rawdata()
        self.transactions = Transaction.objects.select_related(
            'subcontractor__taxe_type',
            'subcontractor__legal_form',
            'project__category',
            'project__subcategory',
        ).annotate(
            date=Case(
                When(date_fact=None, then='date_doc'),
                default='date_fact',
                output_field=DateField(),
            ))
        self.raw_data = []
        self.timegrid = []
        self.report = {}


    def sum_calculator(self, transaction):

        agreement_start_date = transaction.agreement_start_date
        agreement_end_date = transaction.agreement_end_date
        duration = (agreement_end_date - agreement_start_date).days + 1

        timegrid = []
        date = self.dateFrom
        while date <= self.dateTo:
            date_from = date
            date_to = date + self.dateDelta - relativedelta(days=1)

            if agreement_start_date <= date_to and agreement_end_date >= date_from  :
                if agreement_start_date <= date_from and agreement_end_date <= date_to:
                    project_report_delta = abs((agreement_end_date - date_from).days) + 1
                if agreement_start_date <= date_from and agreement_end_date > date_to:
                    project_report_delta = abs((date_to - date_from).days) + 1
                if agreement_start_date > date_from and agreement_end_date <= date_to:
                    project_report_delta = abs((agreement_end_date - agreement_start_date).days) + 1
                if agreement_start_date > date_from and agreement_end_date > date_to:
                    project_report_delta = abs((date_to - agreement_start_date).days) + 1
                q = project_report_delta / duration

                if self.taxeIncl:
                    timegrid.append(float(transaction.get_sum_after_taxes()) * q)
                else:
                    timegrid.append(float(transaction.sum_before_taxes) * q)

            else:
                timegrid.append(0.0)

            date += self.dateDelta
        transaction_sum = sum(timegrid)
        return transaction_sum, timegrid

    def start_balance(self):

        if self.taxeIncl:
            sum_NOT_in_period = self.transactions.filter(
            agreement_end_date__lt=self.dateFrom).filter(self.stage).order_by('date').annotate(multiplicator=Sum(
                Case(
                    When(type=True, then='account__company_owner__taxe_type__multiplicator'),
                    default=F('subcontractor__taxe_type__multiplicator')*-1,
                    output_field=DecimalField()
                ))).aggregate(
                total=Sum(F('sum_before_taxes') * F('multiplicator')))['total']

        else:
            sum_NOT_in_period = self.transactions.filter(
            agreement_end_date__lt=self.dateFrom).filter(self.stage).order_by('date').aggregate(
                total=Sum('sum_before_taxes'))['total']

            sum_NOT_in_period = self.transactions.filter(
            agreement_end_date__lt=self.dateFrom).filter(self.stage).order_by('date').annotate(multiplicator=Sum(
                Case(
                    When(type=True, then=1),
                    default=-1,
                    output_field=DecimalField()
                ))).aggregate(
                total=Sum(F('sum_before_taxes') * F('multiplicator')))['total']

        if sum_NOT_in_period is None:
            sum_NOT_in_period = 0

        t_IN_period = self.transactions.filter(agreement_end_date__gte=self.dateFrom,
            agreement_start_date__lt=self.dateFrom).filter(self.stage).order_by('date')

        t_IN_period_list = []
        for transaction in t_IN_period:
            agreement_start_date = transaction.agreement_start_date
            agreement_end_date = transaction.agreement_end_date
            transaction_duration = (agreement_end_date - agreement_start_date).days + 1
            period_duration = abs((self.dateFrom - agreement_start_date).days)

            q = period_duration / transaction_duration
            if transaction.type:
                m = 1
            else:
                m = -1

            if self.taxeIncl:
                t = float(transaction.get_sum_after_taxes()) * q * m
            else:
                t = float(transaction.sum_before_taxes) * q * m

            t_IN_period_list.append(float(t))

        sum_IN_period = sum(t_IN_period_list)
        start_balance = sum_IN_period + float(sum_NOT_in_period)
        start_balance = round(start_balance, 2)

        # all = self.transactions.filter(
        #     agreement_end_date__lt=self.dateFrom).filter(self.stage).order_by('date')



        return start_balance

    def raw(self):

        t_filtred = self.transactions.filter(
            agreement_end_date__gte=self.dateFrom, agreement_start_date__lte=self.dateTo).filter(self.stage).order_by('date')

        date = self.dateFrom
        while date <= self.dateTo:
            date_from = date
            date_to = date + self.dateDelta - relativedelta(days=1)

            self.timegrid.append({
                'date_from': date_from,
                'date_to': date_to
            })
            date += self.dateDelta


        if len(t_filtred) != 0:
            for t in t_filtred:
                transaction_sum,timegrid =  self.sum_calculator(t)
                obj = {
                    'from': t.agreement_start_date,
                    'to': t.agreement_end_date,
                    'category': t.project.category,
                    'subcategory': t.project.subcategory,
                    'project': t.project,
                    'transaction': t,
                    'transaction_type': t.type,
                    'transaction_sum': transaction_sum,
                    'timegrid': timegrid,
                }
                self.raw_data.append(obj)

        self.report = {
            'raw_data' : self.raw_data,
            'timegrid' : self.timegrid,
            'start_balance' : self.start_balance()
        }
        return self.report

class ReportFormater():
    def __init__(self, report_raw, report_request):
        self.dateFrom, self.dateTo, self.dateDelta, self.payStatus, self.taxeIncl = report_request.rawdata()
        self.report_raw = report_raw
        self.categories = ProjectCategory.objects.order_by('name').all()
        self.subcategories = ProjectSubcategory.objects.order_by('name').all()
        self.projects = Project.objects.order_by('name').all()

    def timegrid_formater(self):
        timegrid = []
        for item in self.report_raw['timegrid']:
            if item['date_from'] == item['date_to']:
                timegrid.append(item['date_from'].strftime('%d %B %Y'))
            else:
                timegrid.append(item['date_from'].strftime('%d %B %Y') + ' - ' + item['date_to'].strftime('%d %B %Y'))
        return timegrid

    def data_formater(self):
        data = self.report_raw['raw_data']
        for item in data:
            if item['transaction_type']:
                item['transaction_sum'] = float(round(item['transaction_sum'], 2))
                list = []
                for el in item['timegrid']:
                    list.append(float(round(el, 2)))
                item['timegrid'] = list
            else:
                item['transaction_sum'] = float(round(item['transaction_sum'], 2)) * -1
                list = []
                for el in item['timegrid']:
                    list.append(float(round(el, 2)) * -1)
                    item['timegrid'] = list
            item['from_timestamp'] = datetime.timestamp(datetime.combine(item['from'], datetime.min.time()))
            item['to_timestamp'] = datetime.timestamp(datetime.combine(item['to'], datetime.min.time()))
            item['transaction_type'] = int(item['transaction_type'])
            item['transaction'] = item['transaction'].name
            item['category'] = item['category'].name
            item['project'] = '#' + item['project'].name + ' '+ item['project'].name_short

            try:
                item['subcategory'] = item['subcategory'].name
            except Exception:
                item['subcategory'] = 'Без субкатегории'
        return data


    def structure(self):


        sort_by_1 = [
            {
                'prop': 'transaction_type',
                'direction': -1
            }, {
                'prop': 'category',
                'direction': 1
            }, {
                'prop': 'subcategory',
                'direction': -1
            }, {
                'prop': 'project',
                'direction': 1
            }, {
                'prop': 'from_timestamp',
                'direction': 1
            }, {
                'prop': 'transaction',
                'direction': 1
            }

        ]

        levels_1 = [
            'transaction_type',
            'category',
            'subcategory',
            'project'
        ]

        levels_verbose_1 =[
            'Доход/Расход',
            'Категория',
            'Субкатегория',
            'Проект',
            'Транзакция'
        ]

        report_structured = {
            'header' : self.timegrid_formater(),
            'data' : self.data_formater(),
            'sort_by' : [sort_by_1, ],
            'start_balance' : float(self.report_raw['start_balance']),
            'levels' : [levels_1, ],
            'levels_verbose' : [levels_verbose_1, ],
        }

        return report_structured
