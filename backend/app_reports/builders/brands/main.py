from datetime import datetime, date
from dateutil.relativedelta import *
from django.db.models import Q, Sum, F
from app_transaction.models import Transaction
from app_project.models import ProjectCategory, ProjectSubcategory, Project
from django.db.models import Case, When, DateField
from operator import itemgetter as i
from functools import cmp_to_key
from decimal import *


class RequestFilter():
    def __init__(self, rawdata):
        # rawdata = form.cleaned_data


        if int(rawdata['taxes']) == 0:
            self.taxeIncl = False
        if int(rawdata['taxes']) == 1:
            self.taxeIncl = True

        if int(rawdata['pay_status']) == 1:
            self.payStatus = Q() #[0, 1, 2, 3]
        if int(rawdata['pay_status']) == 2:
            self.payStatus = Q(status=3) #[3,]
        if int(rawdata['pay_status']) == 3:
            self.payStatus = Q(status__in=[0, 1, 2]) #[0, 1, 2]

        if rawdata['grid'] == '1':
            self.dateFrom = datetime.strptime(rawdata['dayFrom'], '%Y-%m-%d')
            self.dateDelta = relativedelta(days = 1)
            self.dateTo = datetime.strptime(rawdata['dayTo'], '%Y-%m-%d')
        if rawdata['grid'] == '2':
            self.dateFrom = datetime.strptime(rawdata['weekFrom'], '%Y-%m-%d')
            self.dateDelta = relativedelta(weeks=1)
            self.dateTo = self.dateFrom + self.dateDelta * int(rawdata['weekIntervalFrom']) - relativedelta(days=1)
        if rawdata['grid'] == '3':
            self.dateFrom = date(int(rawdata['monthYearFrom']), int(rawdata['monthFrom']), 1)
            self.dateDelta = relativedelta(months=1)
            self.dateTo = date(int(rawdata['monthYearTo']), int(rawdata['monthTo']), 1) + relativedelta(months=1, days=-1)
        if rawdata['grid'] == '4':
            self.dateFrom = date(int(rawdata['quartalYearFrom']), int(rawdata['quartalFrom']) * 3 - 2, 1)
            self.dateDelta = relativedelta(months=3)
            self.dateTo = date(int(rawdata['quartalYearTo']), int(rawdata['quartalTo']) * 3 , 1) + relativedelta(months=1, days=-1)
        if rawdata['grid'] == '5':
            self.dateFrom = date(int(rawdata['yearFrom']), 1, 1)
            self.dateDelta = relativedelta(years=1)
            self.dateTo = date(int(rawdata['yearTo']), 12 , 1) + relativedelta(months=1, days=-1)

    def rawdata(self):
        return self.dateFrom, self.dateTo, self.dateDelta, self.payStatus, self.taxeIncl


class ReportBuilder():

    def __init__(self, report_request):
        self.dateFrom, self.dateTo, self.dateDelta, self.payStatus, self.taxeIncl = report_request.rawdata()
        self.transactions = Transaction.objects.select_related(
            'subcontractor__taxe_type',
            'project__brand',
        ).annotate(
            date=Case(
                When(date_fact=None, then='date_doc'),
                default='date_fact',
                output_field=DateField(),
            ))
        self.raw_data = []
        self.timegrid = []
        self.report = {}

    def raw(self):


        date = self.dateFrom
        while date <= self.dateTo:
            date_from = date
            date_to = date + self.dateDelta - relativedelta(days=1)

            self.timegrid.append({
                'date_from' : date_from,
                'date_to' :date_to
            })

            t_filtred = self.transactions.filter(
                date__gte=date_from, date__lte=date_to).filter(self.payStatus).order_by('date')

            if len(t_filtred) != 0:

                for t in t_filtred:
                    obj = {
                        'from': date_from,
                        'to': date_to,
                        'transaction': t,
                        'transaction_type': t.type,
                        'transaction_sum': t.sum_before_taxes,
                        'transaction_taxe_type': t.subcontractor.taxe_type,
                        'brand': t.project.brand,

                    }
                    if self.taxeIncl:
                        obj['transaction_sum'] = t.get_sum_after_taxes()
                    self.raw_data.append(obj)

            date += self.dateDelta


        self.report = {
            'raw_data' : self.raw_data,
            'timegrid' : self.timegrid,
        }
        return self.report

class ReportFormater():
    def __init__(self, report_raw, report_request):
        self.dateFrom, self.dateTo, self.dateDelta, self.payStatus, self.taxeIncl = report_request.rawdata()
        self.report_raw = report_raw
        self.categories = ProjectCategory.objects.order_by('name').all()
        self.subcategories = ProjectSubcategory.objects.order_by('name').all()
        self.projects = Project.objects.order_by('name').all()

    def timegrid_formater(self):
        timegrid = []
        for item in self.report_raw['timegrid']:
            if item['date_from'] == item['date_to']:
                timegrid.append(item['date_from'].strftime('%d %B %Y'))
            else:
                timegrid.append(item['date_from'].strftime('%d %B %Y') + ' - ' + item['date_to'].strftime('%d %B %Y'))
        return timegrid



    def sum_in_grid(self, item):
        timegrid_lenth = len(self.report_raw['timegrid'])
        grid = [0] * timegrid_lenth

        i=0
        for el in self.report_raw['timegrid']:
            if el['date_from'] == item['from']:
                break
            i+=1
        grid[i] = item['transaction_sum']

        return grid

    def data_formater(self):
        data = self.report_raw['raw_data']
        for item in data:
            if item['transaction_type']:
                item['transaction_sum'] = float(round(item['transaction_sum'], 2))
            else:
                item['transaction_sum'] = float(round(item['transaction_sum'], 2)) * -1
            item['from_timestamp'] = datetime.timestamp(datetime.combine(item['from'], datetime.min.time()))
            item['to_timestamp'] = datetime.timestamp(datetime.combine(item['to'], datetime.min.time()))
            item['transaction_type'] = int(item['transaction_type'])
            item['transaction'] = item['transaction'].name
            item.update({
                'timegrid': self.sum_in_grid(item),
            })

            item['transaction_taxe_type'] = item['transaction_taxe_type'].name
            try:
                item['brand'] = item['brand'].name
            except Exception:
                item['brand'] = 'Без бренда'
        return data





    def structure(self):


        sort_by_1 = [
            {
                'prop': 'brand',
                'direction': -1
            }, {
                'prop': 'from_timestamp',
                'direction': 1
            }, {
                'prop': 'transaction',
                'direction': 1
            }

        ]

        levels_1 = [
            'brand',
        ]
        levels_verbose_1 =[
            'Бренд',
            'Транзакция'
        ]

        report_structured = {
            'header' : self.timegrid_formater(),
            'data' : self.data_formater(),
            'sort_by' : [sort_by_1, ],
            'start_balance' : '',
            'levels' : [levels_1, ],
            'levels_verbose' : [levels_verbose_1, ],
        }

        return report_structured
