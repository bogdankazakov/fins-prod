from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from app_auth.decorators import access_company_level
from app_dashboard.decorators import *
import logging
from django.views.decorators.csrf import ensure_csrf_cookie

logger = logging.getLogger(__name__)

@ensure_csrf_cookie
@login_required
@access_company_level
def index(request):
    context = {}
    return render(request, 'app_frontend/index.html', context)