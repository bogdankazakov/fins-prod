#!/bin/bash
docker_postgres_container=$(docker ps -aqf "volume=fins_pgdata")

docker exec -i $docker_postgres_container \
pg_dump -h localhost -U  db_main_user -F c -f /backup/maindb/fins_prod_$(date +"%d-%m-%Y-%H-%M").tar.gz fins_prod

find /mnt/backup/maindb -name "fins_prod*.gz" -mtime +7 -type f -delete





#alternative ways
#pg_dump -h localhost -U  db_main_user  fins_prod | gzip > /mnt/fins_app_backup/maindb/fins_prod_$(date +"%d-%m-%Y-%H-%M").pgsql.gz
#pg_dump -h localhost -U  db_main_user  -f /mnt/fins_app_backup/maindb/fins_prod_$(date +"%d-%m-%Y-%H-%M").sql fins_prod
#pg_dump -h localhost -U  db_main_user  fins_prod | gzip > /mnt/fins_app_backup/maindb/fins_prod_$(date +"%d-%m-%Y-%H-%M").pgsql.gz


