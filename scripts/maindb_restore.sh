#!/bin/bash
docker_postgres_container=$(docker ps -aqf "volume=fins_pgdata")

#GET LATEST BACKEUP
backup_url="../backup/maindb/"
fullfile=$( find $backup_url -name "*.gz" -print0 | xargs -r -0 ls -1 -t | head -1 )

#GET FILE PASS IN CONTAINER
file_name="${fullfile##*/}"
backup_url_container="/backup/maindb/"
file=${backup_url_container}${file_name}
echo $file

#RESTORE (use -c to ovewrite)
docker exec -i $docker_postgres_container \
pg_restore -c -h localhost -U db_main_user -F c -d fins_prod $file






#alternative ways

#gunzip -c /mnt/fins_app_backup/maindb/fins_prod_24-01-2020-17-57.pgsql.gz | \
#docker exec -i $docker_postgres_container psql -h localhost -U db_main_user -d fins_prod

#gunzip -c /mnt/fins_app_backup/maindb/fins_prod_25-01-2020-09-54.pgsql.gz | psql -h localhost -U db_main_user -d test
#cat /mnt/fins_app_backup/maindb/fins_prod_25-01-2020-09-42.sql | psql -h localhost -U db_main_user fins_prod


