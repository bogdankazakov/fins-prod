#!/bin/bash
#export PASSPHRASE=SomeLongGeneratedHardToCrackKey

duplicity full --no-encryption \
 --verbosity=8 \
 --include=/mnt/fins_app_media/media \
 --exclude=/** \
 / file:///mnt/backup/media

find /mnt/backup/media -name "duplicity*.gz" -mtime +7 -type f -delete
find /mnt/backup/media -name "duplicity*.manifest" -mtime +7 -type f -delete

#unset PASSPHRASE