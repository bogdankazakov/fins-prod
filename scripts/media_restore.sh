#!/bin/bash
#export PASSPHRASE=SomeLongGeneratedHardToCrackKey

duplicity restore --no-encryption \
file:///mnt/backup/media /home/prod_user/restored_media
