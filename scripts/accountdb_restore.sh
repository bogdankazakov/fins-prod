#!/bin/bash

docker_postgres_container=$(docker ps -aqf "volume=fins_pgdata")
#docker_postgres_container=$(docker ps -aqf "volume=finsapp_pgdata")

backup_url="../backup/accountdb/"


list_file=$( find $backup_url -name "*.txt" -print0 | xargs -r -0 ls -1 -t | head -1 )
list_file_date=$(date -d "@$( stat -c '%Y' $list_file )" +'%F')
#list_file=/mnt/fins_app_backup/accountdb/backup_list_25-01-2020-01-00-04.txt
echo $list_file
echo $list_file_date



#use -c to overwrite
for db in $(cat $list_file)
do
fullfile=$( find $backup_url -name "*$db*.gz" -newermt $list_file_date)
file_name="${fullfile##*/}"
backup_url_container="/backup/accountdb/"
file=${backup_url_container}${file_name}
echo $file

docker exec -i $docker_postgres_container \
pg_restore -c -h localhost -U db_main_user -F c -d $db $file
done


