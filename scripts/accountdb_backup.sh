#!/bin/bash
docker_python_container=$(docker ps -aqf "name=fins_python")
docker_postgres_container=$(docker ps -aqf "volume=fins_pgdata")

docker exec $docker_python_container \
./manage.py db_backup

folder="/mnt/backup/accountdb/"
file=$(ls $folder -Art | tail -n 1)
fullpath="${folder}${file}"

echo $fullpath

for var in $(cat $fullpath)
do
docker exec -i $docker_postgres_container \
pg_dump -h localhost -U db_main_user -F c -f /backup/accountdb/accountdb_$(date +"%d-%m-%Y-%H-%M")_"$var".tar.gz $var
done

find /mnt/backup/accountdb -name "backup*.txt" -mtime +7 -type f -delete
find /mnt/backup/accountdb -name "account*.gz" -mtime +7 -type f -delete
